//
//  CircleMenuViewController.h
//  KYCircleMenuDemo
//
//  Created by Kjuly on 7/18/12.
//  Copyright (c) 2012 Kjuly. All rights reserved.
//

#import "KYCircleMenu.h"
#import "ReaderViewController.h"

@interface CircleMenuViewController : UIViewController <ReaderViewControllerDelegate>{
    NSString *mode;
    BOOL alreadyRotatedTheScreen;
    IBOutlet UILabel *welcomeLabel;
    IBOutlet UILabel *subLabel;
    IBOutlet UIButton *PrepareBtn;
    IBOutlet UIButton *PracticeBtn;
    IBOutlet UIButton *PresentBtn;
    IBOutlet UIButton *objectionHandling;

    NSTimer *timer1;
    NSTimer *timer2;
    NSTimer *timer3;
    CGRect prepareBtn_frame;

}

@property (nonatomic, strong) NSString *mode;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) BOOL alreadyRotatedTheScreen;

- (void)handleOrientation;
- (IBAction)runButtonActions:(id)sender;
@end
