//
//  CardView.h
//  ZLSwipeableViewDemo
//
//  Created by Zhixuan Lai on 11/1/14.
//  Copyright (c) 2014 Zhixuan Lai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardView : UIView {


    
}

@property (nonatomic, strong) UILabel *question;
@property (nonatomic, strong) UILabel *answer;
@property (nonatomic) BOOL flipped;

@end
