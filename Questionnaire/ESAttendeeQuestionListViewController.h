//
//  ESAttendeeQuestionListViewController.h
//  iProjector
//
//  Created by Manoj Katragadda on 13/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESAttendeeQuestionListViewController : UITableViewController

@property(nonatomic,strong)NSMutableArray *attendeeAskedQuestionArray;

@end
