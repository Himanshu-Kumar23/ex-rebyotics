//
//  ESAttendeeQuestionListViewController.m
//  iProjector
//
//  Created by Manoj Katragadda on 13/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAttendeeQuestionListViewController.h"

#import "ESAttendeeQuestion.h"

#import "ESSettingListViewController.h"

#import "ESPDFDocumentViewController.h"

@interface ESAttendeeQuestionListViewController () {
//    UITableViewCell *switchTableViewCell;
    NSUserDefaults *userDefaults;
}

@end

@implementation ESAttendeeQuestionListViewController
@synthesize attendeeAskedQuestionArray;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Attendee Queries";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissViewController:)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableViewData:) name:Attendee_Query_Posted object:nil];

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
//    switchTableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
//    switchTableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
//    switchTableViewCell.textLabel.text = @"Enable queries";
//    UISwitch *uiSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
//    //    uiSwitch.on = YES; //FIXME: J.P. default value should come from NSUserDefaults.
//    switchTableViewCell.accessoryView = uiSwitch;
//    [uiSwitch addTarget:self action:@selector(switchTapped:) forControlEvents:UIControlEventValueChanged];
//    //User defaults
//    userDefaults = [NSUserDefaults standardUserDefaults];
//    uiSwitch.on = [userDefaults boolForKey:KEY_ATTENDEE_CAN_QUERY];
    
}

//- (void) switchTapped:(UISwitch *) uiSwitch {
//    [userDefaults setBool:uiSwitch.on forKey:KEY_ATTENDEE_CAN_QUERY];
//    [userDefaults synchronize];
//    if (self.delegate!= nil) {
//        [self.delegate attendeesQuestionsTurnedOn:uiSwitch.on];
//    }
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
//    return 2;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
//    if (section == 0) {
//        return 1;
//    }
    // Return the number of rows in the section.
    return [self.attendeeAskedQuestionArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if (indexPath.section == 0) {
//        return switchTableViewCell;
//    }
//    
    static NSString *CellIdentifier = @"CellIdentifer";
    UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (tableViewCell == nil) {
        tableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        tableViewCell.textLabel.font = [UIFont systemFontOfSize:[UIFont labelFontSize]];
        tableViewCell.textLabel.numberOfLines = 0;
        tableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    ESAttendeeQuestion *qus = [self.attendeeAskedQuestionArray objectAtIndex:indexPath.row];
    tableViewCell.textLabel.text =[[qus aQuestDescription] stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    tableViewCell.detailTextLabel.text = [@"- " stringByAppendingString:[qus attendeeName]];
    return tableViewCell;
}

#pragma mark - Table view delegate

-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ESAttendeeQuestion *qus = [self.attendeeAskedQuestionArray objectAtIndex:indexPath.row];
    NSString *text=[[qus aQuestDescription] stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    
    //CGSize requiredSize = [text sizeWithFont:[UIFont systemFontOfSize:[UIFont labelFontSize]] constrainedToSize:CGSizeMake(self.tableView.bounds.size.width - 10, 2000) lineBreakMode:ES_LINE_BREAK_WORD_WRAP];
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text
                                                                         attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:[UIFont labelFontSize]]}];
    
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){self.tableView.bounds.size.width - 10, 2000}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize requiredSize = rect.size;
    
    
    return requiredSize.height + 20 + 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return @"Setting";
//    }
    return @"Queries posted";
}
-(void)dismissViewController:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma Orientations Methods

-(BOOL)shouldAutorotate{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}
-(void)reloadTableViewData:(NSNotification *)aNotification{
    
    self.attendeeAskedQuestionArray = [aNotification object];
    [self.tableView reloadData];

}
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:Attendee_Query_Posted object:nil];
}
@end
