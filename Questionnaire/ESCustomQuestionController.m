//
//  ESCustomQuestionController.m
//  X Detailer
//
//  Created by Himanshukumar on 11/11/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//

//
//
//

#import "ESQuestionsListViewController.h"
#import "ESQuestionnaireParser.h"
#import "ESQuestionDetailViewController.h"
#import "ESQuestionsTypeViewController.h"
#import "ESQuestionAddViewController.h"
#import "ESXMLWriter.h"
#import "ESCustomQuestionController.h"
#import "ESQuestionDetailViewController.h"
#import "ESAttendeeCustomAnswer.h"
#import "ESPDFDocumentViewController.h"

@interface ESCustomQuestionController () <UITextViewDelegate> {
//    UITextField *textField;
    TPKeyboardAvoidingTableView *tableView;
    UITextView *textView;
    UITableViewCell *tableViewCell, *tableViewCell1, *tableViewCll, *tableViewCell2;
    UILabel *textLabel;
     UILabel *answerLabel;
    ESQuestion *questions;
    
    
//    ESAttendeeCustomAnswer *answer;
 
    int rows;
}

@end


@implementation ESCustomQuestionController
@synthesize attendeeName;
@synthesize attendeeAswerArray;
@synthesize attendeeAnswer;


- (id)initWithCustomQuestionControllerWithText:(NSString *) text
{
    self = [super init];
//    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        tableView = [[TPKeyboardAvoidingTableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:tableView];
        tableView.delegate = self;
        tableView.dataSource = self;
        
        tableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        tableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
      
//        textField.backgroundColor = [UIColor lightGrayColor];
        textView = [[UITextView alloc] initWithFrame:CGRectInset(tableViewCell.contentView.bounds, 5, 5) ];
        textView.font = [UIFont systemFontOfSize:14.f];
        textView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [tableViewCell.contentView addSubview:textView];
        textView.backgroundColor = [UIColor clearColor];
        textView.text = text;
//        textView.delegate = self;
        
        tableViewCell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        tableViewCell1.selectionStyle = UITableViewCellSelectionStyleNone;
//        tableViewCell1.textLabel.textColor = [UIColor blueColor] ;
        tableViewCell1.textLabel.textColor =[UIColor colorWithRed:64/255.0 green:154/255.0 blue:254/255.0 alpha:1.0];
        tableViewCell1.textLabel.text = @"Send to all attendees";
        tableViewCell2 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        tableViewCell2.selectionStyle = UITableViewCellSelectionStyleNone;
        tableViewCell2.textLabel.text = attendeeAnswer;

    }
    return self;
}

- (void)viewDidLoad
{
    rows = 2;

//    [super viewDidLoad];
//    self.tableView.scrollsToTop = false;
    questions =  [[ESQuestion alloc]init];
    questions.delegate = self;
    
    UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonItemTapped:)];
//    doneButtonItem.title = @"Send";
    self.navigationItem.rightBarButtonItem = doneButtonItem;

    textLabel.text = @"Send to all Attendees";
    UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissViewController:)];
    self.navigationItem.leftBarButtonItem = closeButtonItem;
    [tableView reloadData];
//    textView.scrollsToTop = NO;
    
}
- (void) questionStateChanged:(ESQuestion *) question{
    if (question.state == ESQuestionStateAsked){
        tableViewCell2.textLabel.text = @" ";
        [tableView reloadData];
    }
    else if (question.state == ESQuestionStateUpdated){
//        answer = [question.answersArray[0] objectAtIndex: 0];
        rows = rows + question.answersArray.count;
       ESAttendeeCustomAnswer *answer = question.answersArray[0];
        tableViewCell2.textLabel.text = answer.aAnswerDescription;
        tableViewCell2.textLabel.numberOfLines = 0;
         [tableView reloadData];
    }
    else{
        
    }

}

-(void)dismissViewController:(id) sender{
    if (_customControllerDelegate && [_customControllerDelegate respondsToSelector:@selector(cancelledEditingCustomQuestionController:)]) {
        [_customControllerDelegate cancelledEditingCustomQuestionController:self];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.delegate viewController:self didCloseQuestion:questions];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
//- (void)textViewDidBeginEditing:(UITextView *)textView{
//    tableView.scrollEnabled = NO;
// }
- (void) viewDidAppear:(BOOL)animated {
    //    [textView becomeFirstResponder];
  
//    [super viewDidAppear:animated];
//    if ([textView canBecomeFirstResponder]) {
//        [textView becomeFirstResponder];
//    }
}
  


- (void) doneButtonItemTapped:(id) sender {
   
    
    [self.delegate viewController:self didCloseQuestion:questions];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    if (indexPath.row == 0){

        return tableViewCell;
    }
    else if (indexPath.row == 1){

        return tableViewCell1;
    }
    else if(indexPath.row >= 2){

        return tableViewCell2;
    }else{
        
    }
   
}

-(void)reloadTableViewData:(NSNotification *)aNotification{
//    self. = [aNotification object];
//    [self.tableView reloadData];
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0){
        return 120.f;
    }
    else if (indexPath.row == 1){
        return 30.f;
    }
    else{
        return 60.f;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1){
        if (textView.text.length == 0) {
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:nil
                                         message:@"Please add text"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            
            [alert addAction:okButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
        else{
//            ESQuestion *question = [[ESQuestion alloc]init];
            
            questions.q_description = textView.text;
//            questions.options = [@[@"a", @"b"]mutableCopy];
            questions.options = nil;
            questions.state = ESQuestionStateAsked;
            
//            questions = question;
            [self.delegate viewController:self didPickQuestion:questions];

        }
    }
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        return NO;
    }
    return YES;
}

#pragma Orientations Methods

-(BOOL)shouldAutorotate{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}

@end

