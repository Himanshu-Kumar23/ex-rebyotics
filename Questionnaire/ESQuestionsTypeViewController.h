//
//  ESQuestionsTypeViewController.h
//  X Detailer
//
//  Created by Himanshukumar on 08/11/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESQuestionPickerDelegate.h"

@interface ESQuestionsTypeViewController : UITableViewController <ESQuestionPickerDelegate> {
    
}

-(void)methodForAttendeeWithQuestionArray:(NSMutableArray *)questionsType;

@property (nonatomic, strong) NSMutableArray *questionTypeArray;
@property (nonatomic, strong) NSMutableArray *questionList;
@property(nonatomic, unsafe_unretained) id<ESQuestionPickerDelegate> delegate;
@end
