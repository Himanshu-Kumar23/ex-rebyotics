//
//  ESQuestionAddViewController.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/22/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESQuestionEditViewController.h"

@class ESQuestionAddViewController;

@protocol ESQuestionAddViewControllerDelegate <NSObject>

@required
- (void) questionAddViewController:(ESQuestionAddViewController *) addViewController finishWithQuestion:(ESQuestion *) question;

@end

@interface ESQuestionAddViewController : ESQuestionEditViewController

@property(nonatomic, unsafe_unretained) id<ESQuestionAddViewControllerDelegate> delegate;
-(void)initQuestionPackage;

@end
