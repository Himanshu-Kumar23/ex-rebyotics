//
//  ESQuestionaryParser.m
//  iProjector
//
//  Created by Manoj Katragadda on 08/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESQuestionnaireParser.h"
#import "ESQuestion.h"

@implementation ESQuestionnaireParser

@synthesize optionArray = optionArray;

-(id) init {
    
    self = [super init];
    if (self) {
        optionArray = [[NSMutableArray alloc] init];
        self.questionArray =[[NSMutableArray alloc]init];
    }
    return self;
}

-(void)parseWithData:(NSData *)data{
    NSXMLParser *xmlParser;
    xmlParser =[[NSXMLParser alloc] initWithData:data];
    [xmlParser setDelegate:self];
    BOOL success = [xmlParser parse];
    if (!success) {
//        NSLog(@"Cannot parse questionnaire. %@",xmlParser.parserError);
    }
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{

    if( [elementName isEqualToString:@"question"] ){
        question = [[ESQuestion alloc]init];
    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    if ([elementName isEqualToString:@"question"]){
        [self.questionArray addObject:question];
    } else if ([elementName isEqualToString:@"description"]) {
        question.q_description = qString;
    } else if ([elementName isEqualToString:@"type"]){
        question.type = [qString integerValue];
    }else if ([elementName isEqualToString:@"option"]) {
        [question.options addObject:qString];
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
    string =[string stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    string =[string stringByReplacingOccurrencesOfString:@"\n" withString:@""];

    qString = string;
}


@end
