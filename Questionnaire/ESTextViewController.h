//
//  ESTextViewController.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/10/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>

//typedef enum{
//    ESTextViewControllerTypeTextField,
//    ESTextViewControllerTypeTextView
//} ESTextViewControllerType;

@class ESTextViewController;

@protocol ESTextViewControllerDelegate <NSObject>
- (void) textViewController:(ESTextViewController *) viewController doneEditingWithText:(NSString *) text forAttendee:(NSString *)attendeeName;

@optional
- (void) cancelledEditingTextViewController:(ESTextViewController *) viewController;

@end

@interface ESTextViewController : UITableViewController

@property(nonatomic, strong) NSIndexPath *userInfo;
@property(nonatomic, strong) NSString *attendeeName;

//@property(nonatomic) ESTextViewControllerType textViewControllerType;
@property(nonatomic, unsafe_unretained) id<ESTextViewControllerDelegate> delegate;


- (id)initWithTextViewControllerWithText:(NSString *) text;

@end
