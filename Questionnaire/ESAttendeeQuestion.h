//
//  ESAttendeeQuestion.h
//  iProjector
//
//  Created by Manoj Katragadda on 12/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESAttendeeQuestion : NSObject<NSCoding>{
    NSString *attendeeName;
}

@property(nonatomic,strong) NSString *aQuestDescription;
@property(nonatomic,strong) NSString *attendeeName;

@end
