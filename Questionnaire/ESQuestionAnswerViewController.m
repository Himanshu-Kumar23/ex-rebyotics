//
//  ESQuestionAnswerViewController.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/11/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESQuestionAnswerViewController.h"
#import "ESBarViewTableCell.h"
#import "ESCustomQuestionController.h"
#import "ESMultipeerManager.h"

@interface ESQuestionAnswerViewController () <UITextFieldDelegate,UITextViewDelegate> {
    UITextField *textField;
    UITextView *textView;
    UITableViewCell *tableViewCell;
     UITableViewCell *tableViewCell1;
    UILabel *textLabel;
    UILabel *lbl;
    UIView *view;
    
//    NSMutableIndexSet *indexSet;
    NSTimer *timer;
    int timeLeft;
    UITableViewCell *waitingCell;
    ESBarViewTableCell *barViewCell;
}

@property (nonatomic, strong) NSMutableIndexSet *indexSet;

@end

@implementation ESQuestionAnswerViewController
@synthesize attendeeName;

//@synthesize indexSet = indexSet;
- (id)initWithTextViewControllerWithText:(NSString *) text
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {

//        tableViewCell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
//        tableViewCell1.selectionStyle = UITableViewCellSelectionStyleNone;
//        textLabel = [[UILabel alloc] initWithFrame:CGRectInset(tableViewCell.contentView.bounds, 5, 5) ];
//
//        textLabel.font = [UIFont systemFontOfSize:14.f];
//        textLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
//        textLabel.text = @"Send to all attendees";
//        [tableViewCell1.contentView addSubview:textLabel];
//        tableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self initilizeTable:text];
    }
    return self;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        timeLeft = 60;
        [self initilizeTable:@""];
    }
    return self;
}
-(void) initilizeTable : (NSString *) text {
    
    tableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    tableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    textField.backgroundColor = [UIColor lightGrayColor];
    textView = [[UITextView alloc] initWithFrame:CGRectInset(tableViewCell.contentView.bounds, 5, 5) ];
    textView.font = [UIFont systemFontOfSize:14.f];
    textView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [tableViewCell.contentView addSubview:textView];
    textView.backgroundColor = [UIColor clearColor];
    textView.text = text;
//    textView.delegate = self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    if (self.question.state == ESQuestionStateAsked) {
        UIBarButtonItem *sendButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Send" style:UIBarButtonItemStyleDone target:self action:@selector(sendAnswer:)];
        self.navigationItem.rightBarButtonItem = sendButtonItem;        
    }
//    self.question.q_description;
    _indexSet = [NSMutableIndexSet indexSet];
    
    self.title = @"Presenter asks";
    if(self.question.options == nil){
        self.indexSet = nil;
      
    }
    
}
-(void)viewWillAppear:(BOOL)animated {
    if ([textView canBecomeFirstResponder]) {
        [textView becomeFirstResponder];
    }
}
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if(self.question.options == nil){
//        return 2;
//    }
//    
    if (section == SECTION_WAIT) {
        return (self.question.state == ESQuestionStateAnswered || self.question.state == ESQuestionStateStopped) ? 1:0;
    }
    
    if (section == SECTION_ANSWER_GRAPH) {
        return self.question.state == ESQuestionStateShared?2:0;
    }
    if (section == SECTION_OPTIONS && self.question.options == nil){
        
        return 1;
        
    }

    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == SECTION_WAIT) {
        if (waitingCell == nil) {
            waitingCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [activityIndicatorView startAnimating];
            waitingCell.textLabel.text = @"Waiting for presenter";
            waitingCell.accessoryView = activityIndicatorView;
            waitingCell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return waitingCell;
    }
    
    if (indexPath.section == SECTION_ANSWER_GRAPH) {
        if (indexPath.row == 0) {
            
            if (barViewCell == nil) {
               
                barViewCell = [[ESBarViewTableCell alloc] initWithIndexSetArray:self.question.answersArray numberOfBars:self.question.options.count];
                
            } else {
                [barViewCell.datasource setIndexSetsArray:self.question.answersArray];
                [barViewCell.barView reloadData];
            }
            return barViewCell;
        }
    }
    
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if (indexPath.section == SECTION_OPTIONS) {
        
        if (self.question.options == nil && indexPath.row == 0) {
          
            return tableViewCell;
        
        }
       
    else if ([self.indexSet containsIndex:indexPath.row]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
                 } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
                    }
        
        }
        else if (indexPath.section == SECTION_ANSWER_GRAPH) {
        cell.textLabel.text = [NSString stringWithFormat:@"Total answers : #%lu", (unsigned long)self.question.answersArray.count];
          }
    
    

    return cell;

}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == SECTION_OPTIONS) {
        if (self.question.state == ESQuestionStateAsked) {
            if(self.question.options == nil){
                return @"Type Answer";
            }
            else{
            return self.question.type == ESQuestionTypeSingleChoice ? @"Choose an option": @"Choose one or more options";
            }
        } else if (self.question.state == ESQuestionStateAnswered){
            return @"Answers locked";
        }
    } else if (section == SECTION_ANSWER_GRAPH) {
        if([self tableView:tableView numberOfRowsInSection:SECTION_ANSWER_GRAPH] > 0)
            return @"Cumulative answers";
        else
            return nil;
    }
    return [super tableView:tableView titleForHeaderInSection:section];
}

//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    if (section == SECTION_OPTIONS){
//   view = [[UIView alloc]init];
//   lbl = [[UILabel alloc]init];
//    [view addSubview:lbl];
//    [lbl sizeToFit];
//         return view;
//     }
//    return nil;
//}
- (NSString *) tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (section == SECTION_OPTIONS && self.question.options != nil) {
//        lbl = [[UILabel alloc]init];
//        return [self returnTxt];

        if (timeLeft == 60) {
            return @"Answer in 01:00";
        } else if (timeLeft == 0) {
            return nil;
        } else if (timeLeft < 10) {
            return [NSString stringWithFormat:@"Answer in 00:0%d",timeLeft];
        } else {
            return [NSString stringWithFormat:@"Answer in 00:%d",timeLeft];
        }

    }
    return nil;
}

- (void) sendAnswer:(id) sender {
//    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
//    if (self.question.type == ESQuestionTypeSingleChoice) {
//        [indexSet addIndex:self.tableView.indexPathForSelectedRow.row];
//    } else {
//        for (NSIndexPath *path in self.tableView.indexPathsForSelectedRows) {
//            [indexSet addIndex:path.row];
//        }
//    }
    if(self.question.options == nil){
    if (textView.text.length == 0) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please add text"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        //[[[UIAlertView alloc]initWithTitle:nil message:@"Please add text" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
    }else{
        [self.delegate questionCustomAnswerController:self doneEditingWithText:textView.text forAttendee:attendeeName];
        self.navigationItem.rightBarButtonItem = nil;
        
        ESCustomQuestionController *textViewController = [[ESCustomQuestionController alloc] initWithCustomQuestionControllerWithText:nil];
        textViewController.customControllerDelegate = self;
        textViewController.title = @"Describe your query";
        textViewController.attendeeName = [self.multipeerManager displayName];
        textViewController.attendeeAnswer = textView.text;
     
    }
    
    }
    else{
    
        if([_indexSet count] > 0 ){
        [self.delegate questionAnswerViewController:self finishedWithOptions:_indexSet];
        self.navigationItem.rightBarButtonItem = nil;
        
        timeLeft = 0.f;
        [self updateFooter:nil];
        }
    
       else{
         NSString *str;
        if (self.question.type == ESQuestionTypeSingleChoice ) {
            str = @" Please choose an option";
        }else if (self.question.type == ESQuestionTypeMultipleChoice){
            str = @"Please choose one or more options";
        }
        
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:str
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        //[[[UIAlertView alloc]initWithTitle:nil message:str delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
    }
    }
}
#pragma mark - Table view delegate

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == SECTION_ANSWER_GRAPH && indexPath.row == 0) {
        return 100.f;
    }
    if(self.question.options == nil){
    if (indexPath.section == SECTION_OPTIONS && indexPath.row == 0){
        return 120.f;
    }
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];

}

- (BOOL) tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.question.state == ESQuestionStateAsked && indexPath.section == SECTION_OPTIONS;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.question.state == ESQuestionStateAsked) {
        if (self.question.type == ESQuestionTypeSingleChoice) {
            _indexSet = [NSMutableIndexSet indexSetWithIndex:indexPath.row];
        } else {
            if ([_indexSet containsIndex:indexPath.row]) {
                [_indexSet removeIndex:indexPath.row];
            } else {
                [_indexSet addIndex:indexPath.row];
            }
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView reloadData];
}

- (void) setQuestion:(ESQuestion *)question {
    if (question.state == ESQuestionStateAsked) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateFooter:) userInfo:nil repeats:YES];
    } else {
        timeLeft = 0;
        [timer invalidate];
        timer = nil;
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    //Do we need to enable the close button once graph is ared !. No
    if (question.state == ESQuestionStateShared) {
        
        
//        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self.presentingViewController action:@selector(dismissModalViewControllerAnimated:)];
    }
    
    super.question = question;
    [self.tableView reloadData];
}
//-(NSString *) returnTxt {
////    if (section == SECTION_OPTIONS) {
//    if (timeLeft == 60) {
//        return @"Answer in 01:00";
//    } else if (timeLeft == 0) {
//        return nil;
//    } else if (timeLeft < 10) {
//        return [NSString stringWithFormat:@"Answer in 00:0%d",timeLeft];
//    } else {
//        return [NSString stringWithFormat:@"Answer in 00:%d",timeLeft];
//    }
////            }
//}
- (void) updateFooter:(id) _timer {
    timeLeft = MAX(0, timeLeft-1);
    if (timeLeft == 0) {
        [timer invalidate];
    }
    if(self.question.options != nil){
    [self.tableView reloadData];
    }
//    lbl.text = [self returnTxt];
    
//    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:SECTION_OPTIONS] withRowAnimation:UITableViewRowAnimationNone];
//    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:SECTION_OPTIONS]] withRowAnimation:UITableViewRowAnimationNone];
//    [self.tableView reloadData];
}
#pragma Orientations Methods

-(BOOL)shouldAutorotate{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}


@end
