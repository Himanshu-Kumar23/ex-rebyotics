//
//  ESCustomAnswer.h
//  X Detailer
//
//  Created by Himanshukumar on 14/11/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface ESAttendeeCustomAnswer : NSObject<NSCoding>{
    NSString *attendeeName;
}

@property(nonatomic,strong) NSString *aAnswerDescription;
@property(nonatomic,strong) NSString *attendeeName;

@end
