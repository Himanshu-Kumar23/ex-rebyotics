//
//  ESDescriptionTableViewController.h
//  iProjector
//
//  Created by Manoj Katragadda on 08/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESQuestionPickerDelegate.h"

@interface ESQuestionsListViewController : UITableViewController <ESQuestionPickerDelegate> {

}

-(void)methodForAttendeeWithQuestionArray:(NSMutableArray *)questions;

@property (nonatomic, strong) NSMutableArray *questionArray;
@property (nonatomic, strong) NSArray *questionTypeArray;

@property(nonatomic, unsafe_unretained) id<ESQuestionPickerDelegate> delegate;

@end
