//
//  ESCustomQuestionController.h
//  X Detailer
//
//  Created by Himanshukumar on 11/11/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//
//

#import <UIKit/UIKit.h>
#import "ESQuestionPickerDelegate.h"
#import <TPKeyboardAvoiding/TPKeyboardAvoidingTableView.h>
//typedef enum{
//    ESTextViewControllerTypeTextField,
//    ESTextViewControllerTypeTextView
//} ESTextViewControllerType;

@class ESCustomQuestionController;

@protocol ESCustomQuestionControllerDelegate <NSObject>
//- (void) customQuestionController:(ESCustomQuestionController *) viewController doneEditingWithText:(NSString *) text forAttendee:(NSString *)attendeeName;

@optional
- (void) cancelledEditingCustomQuestionController:(ESCustomQuestionController *) viewController;

@end
//@interface ESCustomQuestionController : ESQuestionEditViewController<ESQuestionStateChangeDelegate>
//
//@end

@interface ESCustomQuestionController : UIViewController<ESQuestionStateChangeDelegate , UITableViewDelegate , UITableViewDataSource>;

@property(nonatomic, strong) NSIndexPath *userInfo;
@property(nonatomic, strong) NSString *attendeeName;
@property(nonatomic, strong) NSString *attendeeAnswer;
@property(nonatomic,strong)NSMutableArray *attendeeAswerArray;

//@property(nonatomic) ESTextViewControllerType textViewControllerType;
@property(nonatomic, unsafe_unretained) id<ESCustomQuestionControllerDelegate> customControllerDelegate;


@property(nonatomic, unsafe_unretained) id<ESQuestionPickerDelegate> delegate;

//@property(nonatomic, strong) ESQuestion *question;

- (id)initWithCustomQuestionControllerWithText:(NSString *) text;

@end
