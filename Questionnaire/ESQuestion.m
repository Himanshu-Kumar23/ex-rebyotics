//
//  ESDescription.m
//  iProjector
//
//  Created by Manoj Katragadda on 08/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESQuestion.h"
#import "ESQuestionnaireParser.h"
#import "ESAttendeeCustomAnswer.h"
@interface ESQuestion() {
    NSInteger _answerCount;
    NSMutableArray *answers;
}
@end

@implementation ESQuestion

@synthesize answersArray = answers;
@synthesize totalAnswers = _answerCount;

-(id) init {
    self = [super init];
    if (self) {
        self.options = [[NSMutableArray alloc] init];
        self.state = ESQuestionStateNonTransactional;
//        answers = [NSMutableArray arrayWithObject:[NSIndexSet indexSetWithIndex:0]];
    }
    return self;
}

- (void) setState:(ESQuestionState) newState {
    _state = newState;
    if (_delegate) {
        [_delegate questionStateChanged:self];
    }
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
//    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.q_description forKey:@"kESQuestionDescription"];
    [aCoder encodeObject:self.options forKey:@"kESQuestionOptions"];
    [aCoder encodeInteger:self.type forKey:@"kESQuestionType"];
    [aCoder encodeInteger:self.state forKey:@"kESQuestionState"];
    [aCoder encodeInteger:self.totalAnswers forKey:@"kEStotalAnswers"];
    [aCoder encodeObject:self.answersArray forKey:@"kESAnswersArray"];
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [self init];
    if (self) {
        self.q_description = [aDecoder decodeObjectForKey:@"kESQuestionDescription"];
        self.options = [aDecoder decodeObjectForKey:@"kESQuestionOptions"];
        self.type = [aDecoder decodeIntegerForKey:@"kESQuestionType"];
        self.state = [aDecoder decodeIntegerForKey:@"kESQuestionState"];
        self.answersArray = [aDecoder decodeObjectForKey:@"kESAnswersArray"];
        self.totalAnswers = [aDecoder decodeIntegerForKey:@"kEStotalAnswers"];

    }
    return self;
}

-(void) addCustomAnswer:(ESAttendeeCustomAnswer *) answer {
    if (answers == nil){
        answers = [NSMutableArray arrayWithCapacity:self.totalAnswers];
    }
      [answers addObject:answer];
}

- (void) addAnswer:(NSIndexSet *) indexSet {
    if (answers == nil) {
        answers = [NSMutableArray arrayWithCapacity:self.options.count];
   }
    
    [answers addObject:indexSet];
    //        for (int i=0; i<self.options.count; i++) {
    //            [answers addObject:[NSNumber numberWithInt:0]];
    //        }
    
//    //FIXME: Add checks for single vs multiple choice answers.
//    
//    _answerCount++;
//    [indexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
//        answers[idx] = [NSNumber numberWithInt:1 +[answers[idx] integerValue]];
//    }];
//    NSLog(@"%@",answers);
}

- (void) cleanup {
    [answers removeAllObjects];
}

- (NSString *) textForDisplayInPDFWithQuestionNumber:(int) i {
    NSMutableString *text = [NSMutableString string];

    [text appendString:[NSString stringWithFormat:@"Question %d.%@ \nType - %@ \n \n",i+1,[self q_description],self.type==ESQuestionTypeSingleChoice?@"Single choice":@"Multiple choice"]];
    [text appendString:@"Options\n\n"];
    for (int j = 0; j < self.options.count; j++) {
        [text appendString:[NSString stringWithFormat:@"%d. %@.\n",j+1,[self.options objectAtIndex:j]]];
    }
    [text appendString:[NSString stringWithFormat:@"\n\nTotal answers #%lu\n",(unsigned long)self.answersArray.count]];
    return text;
}

@end
