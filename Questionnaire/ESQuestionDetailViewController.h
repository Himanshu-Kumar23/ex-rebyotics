//
//  ESQuestionDetailViewController.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/11/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESQuestionEditViewController.h"
#import "ESQuestionPickerDelegate.h"


@interface ESQuestionDetailViewController : ESQuestionEditViewController<ESQuestionStateChangeDelegate>

@property(nonatomic, unsafe_unretained) id<ESQuestionPickerDelegate> delegate;

@end
