//
//  ESQuestionaryParser.h
//  iProjector
//
//  Created by Manoj Katragadda on 08/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESQuestion.h"

@interface ESQuestionnaireParser : NSObject<NSXMLParserDelegate> {
    
    NSString *qString;
    NSMutableArray *optionArray;
    ESQuestion *question;
}

@property(nonatomic,strong)NSMutableArray *questionArray;
@property(nonatomic,strong)NSMutableArray *optionArray;

-(void)parseWithData:(NSData *)data;

@end
