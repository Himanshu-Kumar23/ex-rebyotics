//
//  ESAttendeeQuestion.m
//  iProjector
//
//  Created by Manoj Katragadda on 12/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAttendeeQuestion.h"

@implementation ESAttendeeQuestion
@synthesize aQuestDescription;
@synthesize attendeeName;

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.aQuestDescription = [aDecoder decodeObjectForKey:@"kESAttendeeQuestionDescription"];
        self.attendeeName = [aDecoder decodeObjectForKey:@"kESAttendeeName"];
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.aQuestDescription forKey:@"kESAttendeeQuestionDescription"];
    [aCoder encodeObject:self.attendeeName forKey:@"kESAttendeeName"];
}

@end
