//
//  ESTextViewController.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/10/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESTextViewController.h"

@interface ESTextViewController () <UITextFieldDelegate,UITextViewDelegate> {
    UITextField *textField;
    UITextView *textView;
    UITableViewCell *tableViewCell;
}

@end

@implementation ESTextViewController
@synthesize attendeeName;
- (id)initWithTextViewControllerWithText:(NSString *) text
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        
        tableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        tableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //        if (viewControllerType == ESTextViewControllerTypeTextField) {
        //            textField = [[UITextField alloc] initWithFrame:CGRectInset(tableViewCell.contentView.bounds, 5, 5) ];
        //            textField.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        //            textField.font = [UIFont systemFontOfSize:16.f];
        //            [tableViewCell.contentView addSubview:textField];
        textField.backgroundColor = [UIColor lightGrayColor];
        //            textField.text = text;
        //        } else {
        textView = [[UITextView alloc] initWithFrame:CGRectInset(tableViewCell.contentView.bounds, 5, 5) ];
        textView.font = [UIFont systemFontOfSize:14.f];
        textView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [tableViewCell.contentView addSubview:textView];
        textView.backgroundColor = [UIColor clearColor];
        textView.text = text;
        textView.delegate = self;
        //        }
        
        tableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonItemTapped:)];
    self.navigationItem.rightBarButtonItem = doneButtonItem;
    
    UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissViewController:)];
    self.navigationItem.leftBarButtonItem = closeButtonItem;

}

-(void)dismissViewController:(id) sender{
    if (_delegate && [_delegate respondsToSelector:@selector(cancelledEditingTextViewController:)]) {
        [_delegate cancelledEditingTextViewController:self];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];        
    }
}

- (void) viewDidAppear:(BOOL)animated {
//    [textView becomeFirstResponder];
    
    [super viewDidAppear:animated];
    if ([textView canBecomeFirstResponder]) {
        [textView becomeFirstResponder];
    }

}

- (void) doneButtonItemTapped:(id) sender {
    if (textView.text.length == 0) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please add text"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        //[[[UIAlertView alloc]initWithTitle:nil message:@"Please add text" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
    }else{
    [_delegate textViewController:self doneEditingWithText:textView.text forAttendee:attendeeName];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return tableViewCell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 160.f;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        return NO;
    }
    return YES;
}

#pragma Orientations Methods

-(BOOL)shouldAutorotate{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}

@end
