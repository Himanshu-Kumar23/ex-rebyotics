//
//  Header.h
//  X Detailer
//
//  Created by Himanshukumar on 13/11/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESQuestionBaseViewController.h"

#define SECTION_ANSWER_GRAPH 2
#define SECTION_WAIT 3

@class ESQuestionAnswerViewController;

@protocol ESQuestionAnswerViewControllerDelegate <NSObject>

- (void) questionAnswerViewController:(ESQuestionAnswerViewController *) viewController finishedWithOptions:(NSIndexSet *) options;

@end

@interface ESQuestionAnswerViewController : ESQuestionBaseViewController

@property (nonatomic, unsafe_unretained) id<ESQuestionAnswerViewControllerDelegate> delegate;

@end
