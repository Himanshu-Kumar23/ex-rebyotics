//
//  ESQuestionPickerDelegate.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/12/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ESQuestion.h"

@protocol ESQuestionPickerDelegate <NSObject>

@required

- (void) viewController:(UIViewController *) questionRelatedViewController didPickQuestion:(ESQuestion *) question;
- (void) viewController:(UIViewController *)questionRelatedViewController didStopQuestion:(ESQuestion *)question;
- (void) viewController:(UIViewController *)questionRelatedViewController didShareQuestion:(ESQuestion *)question;
- (void) viewController:(UIViewController *)questionRelatedViewController didCloseQuestion:(ESQuestion *)question;

@end
