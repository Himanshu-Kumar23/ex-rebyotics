//
//  ESQuestionDetailViewController.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/9/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESQuestion.h"
#import "ESQuestionBaseViewController.h"

@interface ESQuestionEditViewController : ESQuestionBaseViewController

@end
