//
//  ESQuestionsTypeViewController.m
//  X Detailer
//
//  Created by Himanshukumar on 08/11/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//

#import "ESQuestionsTypeViewController.h"
#import "ESQuestionnaireParser.h"
#import "ESQuestionDetailViewController.h"
#import "ESQuestionsListViewController.h"
#import "ESAppDelegate.h"
#import "ESQuestionAddViewController.h"
#import "ESQuestion.h"
#import "ESQuestionPacket.h"
#import "ESAnswerPacket.h"
#import "ESAttendeeQuestion.h"
#import "ESAttendeeQuestionSwitchPacket.h"
#import "ESQuestionAnswerViewController.h"
#import "ESAttendeeQuestionListViewController.h"
#import "ESQuestionsTypeViewController.h"
#import "ESTextViewController.h"
#import "ESAttendeeQuestionPacket.h"
#import "ESEndPresentationPacket.h"
#import "ESCustomQuestionController.h"

@interface ESQuestionsTypeViewController () <ESQuestionAddViewControllerDelegate> {
    ESAppDelegate *appDelegate;
}
@property (nonatomic, strong) NSMutableArray *questions;

@end

@implementation ESQuestionsTypeViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

-(void)methodForAttendeeWithQuestionArray:(NSMutableArray *)questions{
 
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Questions";
    _questionTypeArray = @[@"Custom", @"Standard"];
    [self.tableView reloadData];
    
    //    UIBarButtonItem *addQuestionButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(launchAddQuestionVC:)];
    //    self.navigationItem.rightBarButtonItem = addQuestionButtonItem;
    
    UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissViewController:)];
    self.navigationItem.leftBarButtonItem = closeButtonItem;
    
}

- (void)dismissViewController:(id)sender{
  //  [self writeQuestionsToDisk];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return _questionTypeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifer";
    UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (tableViewCell == nil) {
        tableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (_questionTypeArray != nil){
        tableViewCell.textLabel.text = [_questionTypeArray objectAtIndex:indexPath.row];
    }
    return tableViewCell;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0){
        
        ESCustomQuestionController *textViewController = [[ESCustomQuestionController alloc] initWithCustomQuestionControllerWithText:@"Add the custom question"];
        textViewController.delegate = self;
//        textViewController.customControllerDelegate = self;
//        textViewController.questionArray = self.questionList;
//        textViewController.question = [self.questionList objectAtIndex:indexPath.row];
        textViewController.title = @"Add the custom question";
 
        [self.navigationController pushViewController:textViewController animated:YES];
        
//        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:textViewController];
//        //        navController.modalPresentationStyle = UIModalPresentationFormSheet;
//        navController.modalPresentationStyle = UIModalPresentationPageSheet;
//        [self presentViewController:navController animated:YES completion:nil];
    }
    
    
    else{
        ESQuestionsListViewController *questionTableVC = [[ESQuestionsListViewController alloc]initWithStyle:UITableViewStylePlain];
            
           questionTableVC.questionArray = self.questionList;
            questionTableVC.delegate = [appDelegate.sessionState isEqualToString:@"1"]?self:nil;
        questionTableVC.delegate = self;
          [self.navigationController pushViewController:questionTableVC animated:YES];
//        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:questionTableVC];
//         navController.modalPresentationStyle = UIModalPresentationPageSheet;
//        [self presentViewController:navController animated:YES completion:nil];
        [self.tableView reloadData];
       
    }
}



- (void) viewController:(UIViewController *)questionRelatedViewController didPickQuestion:(ESQuestion *)question {
    [self.delegate viewController:self didPickQuestion:question];
}

- (void) viewController:(UIViewController *)questionRelatedViewController didShareQuestion:(ESQuestion *)question {
    [self.delegate viewController:self didShareQuestion:question];
}

- (void) viewController:(UIViewController *)questionRelatedViewController didStopQuestion:(ESQuestion *)question {
    [self.delegate viewController:self didStopQuestion:question];
}

- (void) viewController:(UIViewController *)questionRelatedViewController didCloseQuestion:(ESQuestion *)question {
    [self.delegate viewController:self didCloseQuestion:question];
    if (question.state == ESQuestionStateAsked) {
        question.state = ESQuestionStateBegin;
    } else if (question.state == ESQuestionStateShared) {
        question.state = ESQuestionStateStopped;
    } else if (question.state == ESQuestionStateClosed) {
        question.state = ESQuestionStateStopped;
    } else if (question.state == ESQuestionStateUpdated) {
        question.state = ESQuestionStateBegin;
        [question cleanup];
    }
}




@end
