//
//  ESQuestionDetailViewController.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/11/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#define SECTION_ANSWER_GRAPH 2
#define SECTION_ASK_QUESTION 3

#import "ESQuestionDetailViewController.h"
#import "ESBarViewTableCell.h"

@interface ESQuestionDetailViewController () {
    ESBarViewTableCell *barViewCell;
    UITableViewCell *stopTableViewCell;
    NSTimer *timer;
    int timeLeft;
}

@end

@implementation ESQuestionDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        timeLeft = 60;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.question.delegate = self;
    
    if (self.question.state != ESQuestionStateBegin) {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (void) setEditing:(BOOL)editing animated:(BOOL)animated {
    if (self.question.state == ESQuestionStateBegin) {
        [super setEditing:editing animated:animated];
    }
}

- (void) questionStateChanged:(ESQuestion *)question {
    
    if (question.state != ESQuestionStateBegin) {
        
        self.navigationItem.rightBarButtonItem = nil;
        
        UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(cancelAndCleanup:)];
        self.navigationItem.leftBarButtonItem = leftButtonItem;
    }
    if (question.state == ESQuestionStateAsked) {
        if (timer == nil) {
            timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
        }
    } else if (question.state == ESQuestionStateUpdated) {
        [self.tableView reloadData];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:SECTION_ANSWER_GRAPH] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    [self.tableView reloadData];
}

- (void) cancelAndCleanup:(id) sender {
    
    if (timer) {
        [timer invalidate];
    }
    
    [self.delegate viewController:self didCloseQuestion:self.question];
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    //    if (self.question.state == ESQuestionStateNonTransactional) {
    //
    //        return [super numberOfSectionsInTableView:tableView];
    //    }
    return 4;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == SECTION_ASK_QUESTION) {
        if (self.question.state == ESQuestionStateBegin) {
            return 1;
        }else if (self.question.state == ESQuestionStateNonTransactional){
            return 1;
            
        }
    } else if (section == SECTION_ANSWER_GRAPH) {
        if (self.question.state == ESQuestionStateShared) {
            return 2;
        } else if (self.question.state == ESQuestionStateNonTransactional){
            return 0;
        }else if (self.question.state != ESQuestionStateBegin) {
            return 3;
        }
    }
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == SECTION_ANSWER_GRAPH) {
        if (indexPath.row == 0) {
            if (barViewCell == nil) {
                barViewCell = [[ESBarViewTableCell alloc] initWithIndexSetArray:self.question.answersArray numberOfBars:self.question.options.count];
            } else {
                [barViewCell.datasource setIndexSetsArray:self.question.answersArray];
                [barViewCell.barView reloadData];
            }
            return barViewCell;
        } else if (indexPath.row == 2) {
            if (self.question.state == ESQuestionStateStopped) {
                UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
                cell.textLabel.text = @"Share answers to attendees";
                return cell;
            } else {
                if (stopTableViewCell == nil) {
                    stopTableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
                    UIActivityIndicatorView * activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    [activityIndicatorView startAnimating];
                    stopTableViewCell.accessoryView = activityIndicatorView;
                }
                stopTableViewCell.textLabel.text = @"Stop";
                if (timeLeft == 60) {
                    stopTableViewCell.detailTextLabel.text = @"01:00";
                } else if (timeLeft < 10) {
                    stopTableViewCell.detailTextLabel.text = [NSString stringWithFormat:@"00:0%d", timeLeft];
                } else {
                    stopTableViewCell.detailTextLabel.text = [NSString stringWithFormat:@"00:%d", timeLeft];
                }
                
                return stopTableViewCell;
            }
        }
    }
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.section == SECTION_ASK_QUESTION) {
        cell.textLabel.text = @"Send Question";
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    } else if (indexPath.section == SECTION_ANSWER_GRAPH) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.text = [NSString stringWithFormat:@"Total answers : #%lu", (unsigned long)self.question.answersArray.count];
    }
    return cell;
}

- (BOOL) tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    if( indexPath.section == SECTION_ANSWER_GRAPH || indexPath.section == SECTION_ASK_QUESTION) {
        return YES;
    }
    return [super tableView:tableView shouldHighlightRowAtIndexPath:indexPath];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == SECTION_ANSWER_GRAPH) {
        //        if (self.question.type == ESQuestionTypeMultipleChoice) {
        //            [self.question addAnswer:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 2)]];
        //        } else {
        //            [self.question addAnswer:[NSIndexSet indexSetWithIndex:2]];
        //        }
        //        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:SECTION_ANSWER_GRAPH] withRowAnimation:UITableViewRowAnimationNone];
        if (indexPath.row == 2) {
            if (self.question.state == ESQuestionStateStopped) {
                //ask delegate to share question.
                [self.delegate viewController:self didShareQuestion:self.question];
                
            } else {
                //ask delegate to stop the question.
                [self.delegate viewController:self didStopQuestion:self.question];
            }
        }
        return;
    } else if (indexPath.section == SECTION_ASK_QUESTION) {
        self.navigationItem.rightBarButtonItem.enabled = NO;
        [self.delegate viewController:self didPickQuestion:self.question];
    } else {
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == SECTION_ASK_QUESTION) {
        if(self.question.state == ESQuestionStateBegin)
            return @"Send to all attendees";
        else if (self.question.state == ESQuestionStateNonTransactional){
            return @"Send to all attendees";
        }
    } else if (section == SECTION_ANSWER_GRAPH) {
        if ([self tableView:tableView numberOfRowsInSection:section] > 0) {
            return @"Answers";
        } else
            return nil;
    }
    return [super tableView:tableView titleForHeaderInSection:section];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == SECTION_ANSWER_GRAPH && indexPath.row == 0) {
        return 100.f;
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (void) updateTime:(id) _timer {
    timeLeft = MAX(0, timeLeft-1);
    if (timeLeft == 0) {
        [timer invalidate];
        self.question.state = ESQuestionStateStopped;
    }
    [self.tableView reloadData];
}
#pragma Orientations Methods

-(BOOL)shouldAutorotate{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}

@end
