//
//  ESCustomBadgeButton.h
//  CustomBadgeButton
//
//  Created by Manoj Katragadda on 13/08/13.
//
//

#import <UIKit/UIKit.h>

@interface ESCustomBadgeButton : UIButton{
    UILabel *badgeLabel;
}
-(void)setbadgeValue:(NSString *)badgeValue;
@end
