//
//  ESQuestionAddViewController.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/22/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESQuestionAddViewController.h"

@interface ESQuestionAddViewController ()

@end

@implementation ESQuestionAddViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
     }
    return self;
}
-(void)initQuestionPackage{
    self.question = [ESQuestion new];
    self.question.type = ESQuestionTypeSingleChoice;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setEditing:YES];
    self.title = @"New question";
    
    UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(validateAndContinue:)];
    self.navigationItem.rightBarButtonItem = doneButtonItem;
}

- (void) validateAndContinue:(id) sender {
    //make sure a question has a description and an option.
    if (self.question.q_description.length == 0) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please describe the question"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        //[[[UIAlertView alloc] initWithTitle:nil message:@"Please describe the question" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    if (self.question.options.count < 2) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please add atleast 2 options"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        //[[[UIAlertView alloc] initWithTitle:nil message:@"Please add atleast 2 options" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    [self.delegate questionAddViewController:self finishWithQuestion:self.question];
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == SECTION_DESCRIPTION) {
        return @"Describe";
    }
    return [super tableView:tableView titleForHeaderInSection:section];
}
#pragma Orientations Methods

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}


@end
