//
//  ESQuestionBaseViewController.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/11/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESQuestionBaseViewController.h"

#define ES_DEFAULT_LABEL_FONT_SIZE 16

@interface ESQuestionBaseViewController () 

@end

@implementation ESQuestionBaseViewController

    
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == SECTION_DESCRIPTION) {
        return 1;
    } else if (section == SECTION_OPTIONS){
        
        return _question.options.count;
        
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.font = [UIFont systemFontOfSize:ES_DEFAULT_LABEL_FONT_SIZE];
    }
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    if (indexPath.section == SECTION_DESCRIPTION) {
        if (indexPath.row == ROW_DESCRIPTION) {
            cell.textLabel.text = _question.q_description;
        }
    } else if (indexPath.section == SECTION_OPTIONS) {
       
        if (!tableView.editing){
            cell.textLabel.text = [NSString stringWithFormat:@"%zd. %@",indexPath.row+1,[_question.options objectAtIndex:indexPath.row]];
        }
    }
    
    return cell;
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    //    if (section == SECTION_DESCRIPTION)
    //        return @"Question";
    //    else
    if (section == SECTION_OPTIONS)
      
        return @"Options";
        
    return nil;
}

- (BOOL) tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *text = nil;
    if (indexPath.section == SECTION_DESCRIPTION) {
        text=[_question.q_description stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    } else if (indexPath.section == SECTION_OPTIONS) {
        text=[[_question.options objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    }
    //    if (text != nil) {//without writting anything ,click on done button text != nil
    if (text.length != 0) {
        
        //CGSize actualSize = [text sizeWithFont:[UIFont systemFontOfSize:ES_DEFAULT_LABEL_FONT_SIZE] constrainedToSize:CGSizeMake(self.tableView.frame.size.width, self.tableView.frame.size.height) lineBreakMode:ES_LINE_BREAK_WORD_WRAP];//18 is the default size for cell.textlabel
        
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text
                                                                             attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:ES_DEFAULT_LABEL_FONT_SIZE]}];
        
        CGRect rect = [attributedText boundingRectWithSize:(CGSize){self.tableView.frame.size.width, self.tableView.frame.size.height}
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                   context:nil];
        CGSize actualSize = rect.size;
        
        return actualSize.height + 20;
    }
    
    return tableView.rowHeight;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    //    if ([self tableView:tableView titleForHeaderInSection:section] == nil) {
    //        return 1.f;
    //    }
    return UITableViewAutomaticDimension;
    //    return 0.f;
}

- (void) dealloc {
    self.question.delegate = nil;
}

@end
