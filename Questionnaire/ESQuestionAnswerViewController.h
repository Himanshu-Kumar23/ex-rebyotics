//
//  ESQuestionAnswerViewController.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/11/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESQuestionBaseViewController.h"
#import "ESMultipeerManager.h"

#define SECTION_ANSWER_GRAPH 2
#define SECTION_WAIT 3

@class ESQuestionAnswerViewController;

@protocol ESQuestionAnswerViewControllerDelegate <NSObject>
- (void) questionCustomAnswerController:(ESQuestionAnswerViewController *) viewController doneEditingWithText:(NSString *) text forAttendee:(NSString *)attendeeName;

- (void) questionAnswerViewController:(ESQuestionAnswerViewController *) viewController finishedWithOptions:(NSIndexSet *) options;

@end

@interface ESQuestionAnswerViewController : ESQuestionBaseViewController
@property(nonatomic, strong) ESMultipeerManager *multipeerManager;
@property(nonatomic,strong) MCPeerID *presenterPeerId;
@property(nonatomic, strong) NSString *attendeeName;
@property (nonatomic, unsafe_unretained) id<ESQuestionAnswerViewControllerDelegate> delegate;
- (id)initWithTextViewControllerWithText:(NSString *) text;
@end
