//
//  ESQuestionDetailViewController.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/9/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESQuestionEditViewController.h"
#import "ESTextViewController.h"

#define ROW_QUESTION_TYPE 1

@interface ESQuestionEditViewController () <ESTextViewControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
    UIView *overlayView;
    UIPickerView *pickerView;
}

@end

@implementation ESQuestionEditViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.tableView.allowsSelectionDuringEditing = YES;
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = @"Question";
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numRows = [super tableView:tableView numberOfRowsInSection:section];

    if (section == SECTION_DESCRIPTION) {
        return numRows+1;
    }
    if (section == SECTION_OPTIONS && tableView.editing) {
        return numRows + 1;
    }
    return numRows;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == SECTION_DESCRIPTION && indexPath.row == ROW_QUESTION_TYPE) {
        NSString *TypeCellIdentifier = @"TypeCellIdentifier";
        UITableViewCell *questionTypeCell = [tableView dequeueReusableCellWithIdentifier:TypeCellIdentifier];
        
        if (questionTypeCell == nil) {
            questionTypeCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:TypeCellIdentifier];
            questionTypeCell.textLabel.text = @"Type";
        }
        questionTypeCell.detailTextLabel.text = self.question.type == ESQuestionTypeSingleChoice ? @"Single Choice":@"Multiple Choice";
        
        if (tableView.editing) {
            questionTypeCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        } else {
            questionTypeCell.accessoryType = UITableViewCellAccessoryNone;
        }
        return questionTypeCell;
    }
    
    if (tableView.editing && indexPath.section == SECTION_OPTIONS && indexPath.row == self.question.options.count) {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        cell.textLabel.text = @"Add an option";
        return cell;
    }

    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if (tableView.editing) {
        if(indexPath.section == SECTION_DESCRIPTION) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        } else if (indexPath.section == SECTION_OPTIONS) {
            cell.textLabel.text = [self.question.options objectAtIndex:indexPath.row];
        }
    }

    return cell;
}

- (BOOL) tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return tableView.editing;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == SECTION_DESCRIPTION && indexPath.row == 1) {
        return tableView.rowHeight;
    }
    if (indexPath.section == SECTION_OPTIONS && indexPath.row == self.question.options.count) {
        return tableView.rowHeight;
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

#pragma mark -
#pragma mark Editing additions

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    if (indexPath.section == SECTION_OPTIONS) {
        return YES;
    }
    return NO;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self.question.options removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

- (void) setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    [self.tableView beginUpdates];
    if (editing) {
        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:self.question.options.count inSection:SECTION_OPTIONS]] withRowAnimation:UITableViewRowAnimationLeft];
    } else {
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:self.question.options.count inSection:SECTION_OPTIONS]] withRowAnimation:UITableViewRowAnimationLeft];
    }
    NSMutableArray *indexPaths = [NSMutableArray arrayWithObjects:INDEX_PATH(SECTION_DESCRIPTION,ROW_DESCRIPTION), INDEX_PATH(SECTION_DESCRIPTION,ROW_QUESTION_TYPE), nil];
    for (int i =0 ; i<self.question.options.count; i++) {
        [indexPaths addObject:INDEX_PATH(SECTION_OPTIONS, i)];
    }
    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    [self.question.options exchangeObjectAtIndex:toIndexPath.row withObjectAtIndex:fromIndexPath.row];
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    
    if (proposedDestinationIndexPath.section < SECTION_OPTIONS) {
        return [NSIndexPath indexPathForRow:0 inSection:SECTION_OPTIONS];
    } else if (proposedDestinationIndexPath.section == SECTION_OPTIONS && proposedDestinationIndexPath.row == self.question.options.count) {
        return [NSIndexPath indexPathForRow:self.question.options.count - 1 inSection:SECTION_OPTIONS];
    }else if (proposedDestinationIndexPath.section > SECTION_OPTIONS){
        return [NSIndexPath indexPathForRow:self.question.options.count - 1 inSection:SECTION_OPTIONS];
    }
    return proposedDestinationIndexPath;
}

- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.question.options.count) {
        return UITableViewCellEditingStyleInsert;
    }
    if (self.editing) {
        return UITableViewCellEditingStyleDelete;
    }else{
        return UITableViewCellEditingStyleNone;
    }
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.section == SECTION_OPTIONS && indexPath.row < self.question.options.count;
}

#pragma mark -
#pragma mark ESTextViewControllerDelegate methods

- (void) textViewController:(ESTextViewController *)viewController doneEditingWithText:(NSString *)text forAttendee:(NSString *)attendeeName{
    [self.tableView beginUpdates];
    
    NSIndexPath *indexPath = viewController.userInfo;
    if (indexPath.section == SECTION_DESCRIPTION) {
        if (indexPath.row == ROW_DESCRIPTION) {
            self.question.q_description = text;
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    } else if (indexPath.section == SECTION_OPTIONS) {
        if (indexPath.row == self.question.options.count) {
            [self.question.options addObject:text];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        } else {
            [self.question.options replaceObjectAtIndex:indexPath.row withObject:text];
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    [self.tableView endUpdates];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) cancelledEditingTextViewController:(ESTextViewController *)viewController {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView.editing) {
        if (indexPath.section == SECTION_DESCRIPTION) {
            if (indexPath.row == ROW_DESCRIPTION) {
                ESTextViewController *textViewController = [[ESTextViewController alloc] initWithTextViewControllerWithText:self.question.q_description];
                textViewController.userInfo = indexPath;
                textViewController.delegate = self;
                [self.navigationController pushViewController:textViewController animated:YES];
            } else {
                [self initializePickerView];
                [self showOverlay];
            }
        } else if (indexPath.section == SECTION_OPTIONS) {
            NSString *text = indexPath.row == self.question.options.count ? @"":[self.question.options objectAtIndex:indexPath.row];
            ESTextViewController *textViewController = [[ESTextViewController alloc] initWithTextViewControllerWithText:text];
            textViewController.userInfo = indexPath;
            textViewController.delegate = self;
            [self.navigationController pushViewController:textViewController animated:YES];            
        }
    }
}

#pragma mark -
#pragma mark Picker implementation

- (void) initializePickerView {
    overlayView = [[UIView alloc] initWithFrame:self.view.bounds];
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, overlayView.bounds.size.height, overlayView.bounds.size.width, 44.f)];
    UIBarButtonItem *cancelButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissPickerOverlay:)];
    UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneWithPickerOverlay:)];
    UIBarButtonItem *flexibleSpaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    toolbar.items = [NSArray arrayWithObjects:cancelButtonItem, flexibleSpaceItem, doneButtonItem, nil];
    [overlayView addSubview:toolbar];
    
    pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, overlayView.bounds.size.height + 44.f, overlayView.bounds.size.width, 216.f)];
    pickerView.showsSelectionIndicator = YES;
    pickerView.delegate = self;
    pickerView.dataSource = self;
    [overlayView addSubview:pickerView];
    [pickerView selectRow:self.question.type-1 inComponent:0 animated:YES];
    overlayView.autoresizingMask =  UIViewAutoresizingFlexibleHeight;
    pickerView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    toolbar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
}

- (void) dismissPickerOverlay:(id) sender {
    [self hideOverlay];
}

- (void) doneWithPickerOverlay:(id) sender {
    self.question.type = [pickerView selectedRowInComponent:0] + 1;
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:INDEX_PATH(SECTION_DESCRIPTION, ROW_QUESTION_TYPE)] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    [self hideOverlay];
}

- (void) showOverlay{
    overlayView.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.0f];
//    overlayView.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, overlayView.frame.size.height);
    [self.view addSubview:overlayView];
    [UIView animateWithDuration:0.3 animations:^{
        overlayView.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.2f];
        for (UIView *subview in overlayView.subviews) {
            subview.frame = CGRectOffset(subview.frame, 0, -44.f-216.f);
        }
    } completion:^(BOOL finished) {
        
    }];
}

- (void) hideOverlay {
    [UIView animateWithDuration:0.3 animations:^{
        overlayView.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.0f];
        for (UIView *subview in overlayView.subviews) {
            subview.frame = CGRectOffset(subview.frame, 0, 44.f+216.f);
        }
    } completion:^(BOOL finished) {
        [overlayView removeFromSuperview];
        overlayView = nil;
        pickerView = nil;
    }];
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 2;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (row == 0) {
        return @"Single Choice";
    } else {
        return @"Multiple Choice";
    }
}
#pragma Orientations Methods

-(BOOL)shouldAutorotate{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}

@end
