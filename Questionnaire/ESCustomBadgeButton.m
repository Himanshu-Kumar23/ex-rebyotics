//
//  ESCustomBadgeButton.m
//  CustomBadgeButton
//
//  Created by Manoj Katragadda on 13/08/13.
//
//

#import "ESCustomBadgeButton.h"
#import <QuartzCore/QuartzCore.h>
@implementation ESCustomBadgeButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        badgeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.origin.x+self.bounds.size.width - 20, 0, 25, 16)];
        [self addSubview:badgeLabel];
        badgeLabel.font = [UIFont systemFontOfSize:10.f];

        badgeLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:14.f];
//        [self setImageEdgeInsets:UIEdgeInsetsMake(10, 11, 10, 0)];
        [self setBackgroundImage:[UIImage imageNamed:@"show-Button"] forState:UIControlStateNormal];
        badgeLabel.textColor = [UIColor redColor];
        badgeLabel.layer.cornerRadius = 6.0f;
        badgeLabel.layer.borderColor = [UIColor redColor].CGColor;
        badgeLabel.layer.borderWidth = 2.0f;
    }
    return self;
}
-(void)setbadgeValue:(NSString *)badgeValue{
    badgeLabel.text = badgeValue;
}

@end
