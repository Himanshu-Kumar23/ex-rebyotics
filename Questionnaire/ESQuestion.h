//
//  ESDescription.h
//  iProjector
//
//  Created by Manoj Katragadda on 08/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESAttendeeCustomAnswer.h"

typedef NS_ENUM (NSInteger,ESQuestionType) {
    ESQuestionTypeSingleChoice = 1,
    ESQuestionTypeMultipleChoice = 2,
};

typedef NS_ENUM (NSInteger,ESQuestionState)  {
    
    ESQuestionStateNonTransactional = -1,
    
    ESQuestionStateBegin = 0,
    ESQuestionStateAsked,
    ESQuestionStateUpdated,
    ESQuestionStateStopped,
    ESQuestionStateShared,
    
    //for attendee
    ESQuestionStateClosed = ESQuestionStateNonTransactional,
    ESQuestionStateAnswered,
    
};

@class ESQuestion;

@protocol ESQuestionStateChangeDelegate <NSObject>

- (void) questionStateChanged:(ESQuestion *) question;

@end

@interface ESQuestion : NSObject <NSCoding> {
}

@property(nonatomic,strong)NSString *q_description;
@property(nonatomic,strong)NSMutableArray *options;

@property(nonatomic)NSInteger type;

@property (nonatomic, unsafe_unretained) id<ESQuestionStateChangeDelegate> delegate;

@property(nonatomic) ESQuestionState state;

- (void) addAnswer:(NSIndexSet *) indexSet;
-(void) addCustomAnswer:(ESAttendeeCustomAnswer *) answer;
@property(nonatomic) NSInteger totalAnswers;
@property(nonatomic, strong) NSArray *answersArray;

- (void) cleanup;

- (NSString *) textForDisplayInPDFWithQuestionNumber:(int) i;

@end
