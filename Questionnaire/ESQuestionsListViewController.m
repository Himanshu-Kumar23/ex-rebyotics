//
//  ESDescriptionTableViewController.m
//  iProjector
//
//  Created by Manoj Katragadda on 08/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESQuestionsListViewController.h"
#import "ESQuestionnaireParser.h"
#import "ESQuestionDetailViewController.h"
#import "ESQuestionsTypeViewController.h"
#import "ESQuestionAddViewController.h"
#import "ESXMLWriter.h"
@interface ESQuestionsListViewController () <ESQuestionAddViewControllerDelegate> {
    
}

@end

@implementation ESQuestionsListViewController
@synthesize questionArray;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

-(void)methodForAttendeeWithQuestionArray:(NSMutableArray *)questions{
    questionArray = questions;
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Questions";
    [self.tableView reloadData];
    
//    UIBarButtonItem *addQuestionButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(launchAddQuestionVC:)];
//    self.navigationItem.rightBarButtonItem = addQuestionButtonItem;
    
    UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissViewController:)];
    self.navigationItem.leftBarButtonItem = closeButtonItem;
    
}

- (void)dismissViewController:(id)sender{
    [self writeQuestionsToDisk];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) launchAddQuestionVC:(id) sender {
    ESQuestionAddViewController *addViewcontroller = [ESQuestionAddViewController new];
    [addViewcontroller initQuestionPackage];
    [self.navigationController pushViewController:addViewcontroller animated:YES];
    addViewcontroller.delegate = self;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return questionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifer";
    UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (tableViewCell == nil) {
        tableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    ESQuestion *qus = [questionArray objectAtIndex:indexPath.row];
    tableViewCell.textLabel.text = [[qus q_description] stringByReplacingOccurrencesOfString:@"  " withString:@""];
    return tableViewCell;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    //    if (self.isPresenter) {
    ESQuestionDetailViewController *esQdetailVC = [[ESQuestionDetailViewController alloc] init];
    esQdetailVC.question = [questionArray objectAtIndex:indexPath.row];
    esQdetailVC.delegate = self;
    [self.navigationController pushViewController:esQdetailVC animated:YES];
    //    }
    
}

- (void) viewController:(UIViewController *)questionRelatedViewController didPickQuestion:(ESQuestion *)question {
    [self.delegate viewController:self didPickQuestion:question];
}

- (void) viewController:(UIViewController *)questionRelatedViewController didShareQuestion:(ESQuestion *)question {
    [self.delegate viewController:self didShareQuestion:question];
}

- (void) viewController:(UIViewController *)questionRelatedViewController didStopQuestion:(ESQuestion *)question {
    [self.delegate viewController:self didStopQuestion:question];
}

- (void) viewController:(UIViewController *)questionRelatedViewController didCloseQuestion:(ESQuestion *)question {
    [self.delegate viewController:self didCloseQuestion:question];
    if (question.state == ESQuestionStateAsked) {
        question.state = ESQuestionStateBegin;
    } else if (question.state == ESQuestionStateShared) {
        question.state = ESQuestionStateStopped;
    } else if (question.state == ESQuestionStateClosed) {
        question.state = ESQuestionStateStopped;
    } else if (question.state == ESQuestionStateUpdated) {
        question.state = ESQuestionStateBegin;
        [question cleanup];
    }
}

- (void) setDelegate:(id<ESQuestionPickerDelegate>)delegate {
    _delegate = delegate;
    if (delegate) {
        for (ESQuestion *question in questionArray) {
            question.state = ESQuestionStateBegin;
        }
    }
}

- (void) questionAddViewController:(ESQuestionAddViewController *)addViewController finishWithQuestion:(ESQuestion *)question {
    [self.questionArray addObject:question];
    [self.tableView reloadData];
    [self.navigationController popViewControllerAnimated:YES];
}
/*
 XMLWriter* xmlWriter = [[XMLWriter alloc]init];
 
 // start writing XML elements
 [xmlWriter writeStartElement:"questions"];
 for () {
 [xmlWriter writeStartElement:"question"];
 
 //description
 //type
 // write options
 //
 for (options) {
 //write tha
 }
 [xmlWriter writeEndElement:@"question"];
 }
 [xmlWriter writeEndElement];
 
 NSString* xml = [xmlWriter toString];
 //write the string "xml" to file.
 
 <question>
 <description>What is your age ?</description>
 <type>1</type>
 <options>
 <option>less than 20</option>
 <option>between 20 and 40</option>
 <option>greater than 40</option>
 </options>
 </question>
 
 */

- (void) writeQuestionsToDisk {
    
    ESXMLWriter *xmlWriter = [[ESXMLWriter alloc]init];
    [xmlWriter writeStartElement:@"questions"];
    
    for (ESQuestion *question in questionArray) {
        
        [xmlWriter writeStartElement:@"question"];
        
        [xmlWriter writeStartElement:@"description"];
        
        [xmlWriter writeCharacters:question.q_description];
        
        [xmlWriter writeEndElement:@"description"];
        
        [xmlWriter writeStartElement:@"type"];
        
        [xmlWriter writeCharacters:[NSString stringWithFormat:@"%ld",(long)question.type]];
        
        [xmlWriter writeEndElement:@"type"];
        
        [xmlWriter writeStartElement:@"options"];
        
        for (int i = 0; i < question.options.count; i++) {
            [xmlWriter writeStartElement:@"option"];
            [xmlWriter writeCharacters:[question.options objectAtIndex:i]];
            [xmlWriter writeEndElement:@"option"];
        }
        [xmlWriter writeEndElement:@"options"];
        
        [xmlWriter writeEndElement:@"question"];
    }
    
    [xmlWriter writeEndElement:@"questions"];
    NSString *xml = [xmlWriter toString];
    NSError *error;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    NSString *pathLocal = [documentsDir stringByAppendingPathComponent:@"questions.xml"];
    
    
    BOOL ok = [xml writeToFile:pathLocal atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (ok) {
        //        NSLog(@"Saved questions to disk successfully");
    }else{
        //        NSLog(@"Error saving questions to disk");
    }
}
#pragma Orientations Methods

-(BOOL)shouldAutorotate{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}

@end
