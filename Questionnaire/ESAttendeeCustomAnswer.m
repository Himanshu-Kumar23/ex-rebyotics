//
//  ESCustomAnswer.m
//  X Detailer
//
//  Created by Himanshukumar on 14/11/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//
#import "ESAttendeeCustomAnswer.h"
@implementation ESAttendeeCustomAnswer
@synthesize aAnswerDescription;
@synthesize attendeeName;

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.aAnswerDescription = [aDecoder decodeObjectForKey:@"kESAttendeeCustomAnswerDescription"];
        self.attendeeName = [aDecoder decodeObjectForKey:@"kESAttendeeName"];
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.aAnswerDescription forKey:@"kESAttendeeCustomAnswerDescription"];
    [aCoder encodeObject:self.attendeeName forKey:@"kESAttendeeName"];
}

@end
