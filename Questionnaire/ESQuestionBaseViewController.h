//
//  ESQuestionBaseViewController.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/11/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESQuestion.h"

#define SECTION_DESCRIPTION 0
#define SECTION_OPTIONS 1

#define ROW_DESCRIPTION 0

#define INDEX_PATH(section,row) [NSIndexPath indexPathForRow:row inSection:section]

@interface ESQuestionBaseViewController : UITableViewController

@property(nonatomic, strong) ESQuestion *question;

@end
