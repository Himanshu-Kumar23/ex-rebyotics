//
//  ESMultipeerManager.h
//  X-Sarcoidosis
//
//  Created by Vinodh Raju on 19/08/16.
//  Copyright © 2016 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>

@protocol ESMultipeerManagerDelegate;

@interface ESMultipeerManager : NSObject

@property (nonatomic, weak) id<ESMultipeerManagerDelegate> delegate;

@property (nonatomic, readonly) BOOL connected;
@property (nonatomic, readonly) BOOL isInSession;
@property (nonatomic, readonly, strong) MCPeerID *peerID;
@property (nonatomic, readonly) NSArray *connectedPeers;
@property (nonatomic, readonly, strong) NSString *serviceType;
@property (nonatomic, readonly, strong) NSString *displayName;

- (instancetype)initWithServiceType:(NSString *)serviceType;

- (instancetype)initWithServiceType:(NSString *)serviceType
                        displayName:(NSString *)displayName;

- (void)joinSession;

- (void)joinSessionAsPresenter;

- (void)joinSessionAsAttendee;

- (void)stopAcceptingGuests;

- (void)leaveSession;

- (BOOL)sendData:(NSData *)data
        withMode:(MCSessionSendDataMode)mode
           error:(NSError **)error;

- (BOOL)sendData:(NSData *)data
         toPeers:(NSArray *)peerIDs
        withMode:(MCSessionSendDataMode)mode
           error:(NSError **)error;

- (NSOutputStream *)startStreamWithName:(NSString *)streamName
                                 toPeer:(MCPeerID *)peerID
                                  error:(NSError **)error;

- (NSProgress *)sendResourceAtURL:(NSURL *)resourceURL
                         withName:(NSString *)resourceName
                           toPeer:(MCPeerID *)peerID
            withCompletionHandler:(void (^)(NSError *error))completionHandler;



@end

@protocol ESMultipeerManagerDelegate <NSObject>

@required
- (void)manager:(ESMultipeerManager *)manager
           peer:(MCPeerID *)peer
   changedState:(MCSessionState)state
   currentPeers:(NSArray *)currentPeers;

- (void)manager:(ESMultipeerManager *)manager
failedToJoinSession:(NSError *)error;

@optional
- (void)manager:(ESMultipeerManager *)manager
 didReceiveData:(NSData *)data
       fromPeer:(MCPeerID *)peerID;

- (void)manager:(ESMultipeerManager *)manager
didReceiveStream:(NSInputStream *)stream
       withName:(NSString *)streamName
       fromPeer:(MCPeerID *)peerID;

- (void)manager:(ESMultipeerManager *)manager
didStartReceivingResourceWithName:(NSString *)resourceName
       fromPeer:(MCPeerID *)peerID
   withProgress:(NSProgress *)progress;

- (void)manager:(ESMultipeerManager *)manager
didFinishReceivingResourceWithName:(NSString *)resourceName
       fromPeer:(MCPeerID *)peerID
          atURL:(NSURL *)localURL
      withError:(NSError *)error;

@end
