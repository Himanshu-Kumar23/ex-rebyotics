//
//  ESCurve.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/1/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESCurve.h"

@implementation ESCurve

- (void) addPoint:(CGPoint) point {
    if (self.points == nil) {
        self.points = [NSMutableArray arrayWithCapacity:10];
    }
    [self.points addObject:[NSValue valueWithCGPoint:point]];
}

- (CGPoint) pointAtIndex:(int) index {
    NSValue *pointValue = [self.points objectAtIndex:index];
    return pointValue.CGPointValue;
}

- (CGPoint) pointFromLast:(int) index {
    NSValue *pointValue = [self.points objectAtIndex:self.points.count - 1 - index];
    return pointValue.CGPointValue;
}

@end