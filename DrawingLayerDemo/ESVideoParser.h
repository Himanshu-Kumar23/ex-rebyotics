//
//  ESVideoParser.h
//  iProjector
//
//  Created by Jayaprada Behera on 25/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import  "ESVideo.h"

@interface ESVideoParser : NSObject<NSXMLParserDelegate>{
    
}

@property(nonatomic, readonly) NSMutableArray *videoArray;
@property(nonatomic,readonly) NSString *presentationName;

-(void)parseData:(NSData *)data forPresentation:(NSString *) fileName;

@end
