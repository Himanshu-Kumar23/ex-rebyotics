//
//  ESVideoParser.m
//  iProjector
//
//  Created by Jayaprada Behera on 25/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESVideoParser.h"

@interface ESVideoParser () {
    NSMutableArray *videos;
    ESVideo *video;
    NSMutableString *vString;
    BOOL shouldParseElements;
}

@end

@implementation ESVideoParser

@synthesize videoArray = videos;

-(id) init {
    
    self = [super init];
    if (self) {
        shouldParseElements = NO;
    }
    return self;
}

- (void) parseData:(NSData *)data forPresentation:(NSString *)fileName {
    
    _presentationName = fileName;
    
    NSXMLParser *xmlParser;
    xmlParser =[[NSXMLParser alloc] initWithData:data];
    [xmlParser setDelegate:self];
    BOOL success = [xmlParser parse];
    if (!success) {
//        NSLog(@"Cannot parse video.xml %@",xmlParser.parserError);
    }
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    vString = [NSMutableString stringWithString:@""];
    
    if ([elementName isEqualToString:@"presentation"]) {
        NSString *name = [attributeDict objectForKey:@"name"];
        if (name && [name isEqualToString:_presentationName]) {
            shouldParseElements = YES;
            videos = [[NSMutableArray alloc] initWithCapacity:1];
        }
    }
    if ([elementName isEqualToString:@"video"]){
        video = [[ESVideo alloc]init];
    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    if (shouldParseElements) {
        if ([elementName isEqualToString:@"presentation"]) {
            shouldParseElements = NO;
        }
        if ([elementName isEqualToString:@"video"]){
            [self.videoArray addObject:video];
        }else if ([elementName isEqualToString:@"rect"]){
            video.rect = vString;
        }else if ([elementName isEqualToString:@"file"]){
            video.fileName = vString;
        }else if ([elementName isEqualToString:@"page"]){
            video.pageNumber = [vString integerValue];
        }
    }
    
    vString = nil;
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
    string =[string stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    string =[string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    [vString appendString:string];
}


@end
