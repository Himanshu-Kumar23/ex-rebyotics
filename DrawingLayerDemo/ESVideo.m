//
//  ESVideo.m
//  iProjector
//
//  Created by Jayaprada Behera on 25/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESVideo.h"

@implementation ESVideo
@synthesize pageNumber;
@synthesize rect;
@synthesize fileName;

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.pageNumber = [aDecoder decodeIntegerForKey:@"kESPageNumber"];
        self.fileName = [aDecoder decodeObjectForKey:@"kESFileName"];
        self.rect = [aDecoder decodeObjectForKey:@"kESRect"];
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInteger:self.pageNumber forKey:@"kESPageNumber"];
    [aCoder encodeObject:self.fileName forKey:@"kESFileName"];
    [aCoder encodeObject:self.rect forKey:@"kESRect"];
}

@end
