//
//  ESZoomingViewController.m
//  ZoomingViewExample
//
//  Created by Rajiv Narayana Singaseni on 8/1/13.
//  Copyright (c) 2013 WebileApps. All rights reserved.
//

#import "ESZoomingViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ESAnnotationsLayer.h"
#import <UIKit/UIGestureRecognizerSubclass.h>

@interface ESSingleFingerTouchRecognizer : UIGestureRecognizer

@end

@interface ESZoomingViewController () <UIScrollViewDelegate, UIGestureRecognizerDelegate> {
    UIView *contentView;
    ESAnnotationsLayer *layer;
    ESSingleFingerTouchRecognizer *drawGestureRecognizer;
}

@end


@implementation ESZoomingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    [super loadView];
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectInset(self.view.bounds, 0, 0)];
//    scrollView.backgroundColor = [UIColor lightGrayColor];
    scrollView.delegate = self;
    scrollView.bouncesZoom = NO;
    scrollView.minimumZoomScale=0.5;
    scrollView.maximumZoomScale=6.0;

    [self.view addSubview: scrollView];
    
    contentView =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, scrollView.bounds.size.width, scrollView.bounds.size.width)];
    contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"startup"]];
//    contentView.backgroundColor = [UIColor lightGrayColor];
    [scrollView addSubview:contentView];
    contentView.center = scrollView.center;
    layer = [ESAnnotationsLayer layer];
//    layer.backgroundColor = [UIColor blueColor].CGColor;
    layer.contentsScale = [UIScreen mainScreen].scale;
    layer.frame = contentView.layer.bounds;
    [contentView.layer addSublayer:layer];
    
    drawGestureRecognizer = [[ESSingleFingerTouchRecognizer alloc] initWithTarget:self action:@selector(didRecognizePanGesture:)];
//    drawGestureRecognizer.maximumNumberOfTouches = 1;
//    drawGestureRecognizer.delegate = self;
//    drawGestureRecognizer.enabled = NO;
    [scrollView addGestureRecognizer:drawGestureRecognizer];
    [scrollView.panGestureRecognizer requireGestureRecognizerToFail:drawGestureRecognizer];
//    [scrollView.pinchGestureRecognizer requireGestureRecognizerToFail:drawGestureRecognizer];
}

- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if ([otherGestureRecognizer isMemberOfClass:[UIPinchGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}

- (void) toggleDrawing:(id) sender {
    drawGestureRecognizer.enabled= !drawGestureRecognizer.enabled;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *drawItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(toggleDrawing:)];
    self.navigationItem.rightBarButtonItem = drawItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return contentView;
}

- (void) didRecognizePanGesture:(UIGestureRecognizer *) panGestureRecognizer {
    
    if (panGestureRecognizer.state == UIGestureRecognizerStateBegan) {
        CGPoint point = [panGestureRecognizer locationOfTouch:0 inView:contentView];
        [layer startCurveAtPoint:point withColor:[UIColor blueColor] andLineWidth:3.f];
    } else if (panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        CGPoint point = [panGestureRecognizer locationOfTouch:0 inView:contentView];
        [layer moveCurveToPoint:point];
    } else if (panGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint point = [panGestureRecognizer locationOfTouch:0 inView:contentView];
        [layer endCurveAtPoint:point];
    } else if (panGestureRecognizer.state == UIGestureRecognizerStateFailed) {
//        NSLog(@"gesture failed");
    }
}

@end

@implementation ESSingleFingerTouchRecognizer

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([event touchesForGestureRecognizer:self].count > 1) {
        self.state = UIGestureRecognizerStateFailed;
//        [self ignoreTouch:[touches anyObject] forEvent:event];
    } else {
        self.state = UIGestureRecognizerStateBegan;
    }
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
//    if (self.state == UIGestureRecognizerStateFailed) {
//        [self ignoreTouch:[touches anyObject] forEvent:event];
//        return;
//    }
    if ([[touches anyObject] tapCount] != 1) {
        self.state = UIGestureRecognizerStateFailed;
    } else {
        self.state = UIGestureRecognizerStateChanged;
    }
    
//    if (self.state == UIGestureRecognizerStateFailed || self.state == UIGestureRecognizerStatePossible) {
//        self.state = UIGestureRecognizerStateEnded;
//        return;
//    }
//    if ([event.allTouches count] == 1) {
//        self.state = UIGestureRecognizerStateChanged;
//    } else {
//        self.state = UIGestureRecognizerStateEnded;
//    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    self.state = UIGestureRecognizerStateEnded;
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    self.state = UIGestureRecognizerStateCancelled;
}

- (void) setState:(UIGestureRecognizerState)state {
    switch (state) {
        case UIGestureRecognizerStatePossible:
//            NSLog(@"state: possible");
            break;
        case UIGestureRecognizerStateFailed:
//            NSLog(@"state: failed");
            break;
        case UIGestureRecognizerStateBegan:
//            NSLog(@"state: began");
            break;
        case UIGestureRecognizerStateChanged:
//            NSLog(@"state: changed");
            break;
        case UIGestureRecognizerStateEnded:
//            NSLog(@"state: ended");
            break;
        case UIGestureRecognizerStateCancelled:
//            NSLog(@"state: cancelled");
            break;
            
        default:
            break;
    }
    [super setState:state];
}

@end