//
//  ESAnnotationsLayer.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/1/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ESText1.h"
@interface ESAnnotationsLayer : CALayer {

}

- (void) startCurveAtPoint:(CGPoint) point withColor:(UIColor *) color andLineWidth:(CGFloat) lineWidth;
- (void) moveCurveToPoint:(CGPoint) point;
- (void) endCurveAtPoint:(CGPoint) point;
- (void) removeLastCurve;
- (void) clear;
- (void) addCurves:(NSArray *) curvesArray;
- (void) addCurve:(NSArray *) points withColor:(UIColor *) color andLineWidth:(CGFloat) lineWidth;
-(void) moveHighlightCurveToPoint:(CGPoint) scaledPoint;
-(void) endHighlightCurveAtPoint:(CGPoint) scaledPoint;

@end

@interface ESCustomLayer : CALayer
- (void) addTexts:(NSArray *) curvesArray;
    @end