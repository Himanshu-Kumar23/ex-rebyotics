//
//  ESVideo.h
//  iProjector
//
//  Created by Jayaprada Behera on 25/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESVideo : NSObject<NSCoding>

@property(nonatomic)NSInteger pageNumber;
@property(nonatomic,strong)NSString *fileName;
@property(nonatomic,strong)NSString *rect;

@end
