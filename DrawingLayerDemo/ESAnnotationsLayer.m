//
//  ESAnnotationsLayer.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/1/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

//Cubic smoothing curve from http://mobile.tutsplus.com/tutorials/iphone/ios-sdk_freehand-drawing/
#define USE_CUBIC_SMOOTHING FALSE

#import "ESAnnotationsLayer.h"
#import "ESCurve.h"

//A custom class to hold properties like blend mode and 
@interface ESExtendedBezierPath : UIBezierPath

@property (nonatomic) CGPoint firstPoint;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic) BOOL erase;

@end

@implementation ESExtendedBezierPath

- (id) init {
    self = [super init];
    if (self) {
        _firstPoint = CGPointMake(-1, -1);
    }
    return self;
}

- (void) stroke {
    [self.color setStroke];
    [self strokeWithBlendMode:_erase?kCGBlendModeClear:kCGBlendModeNormal alpha:1.0f];
    
}

- (void) moveToPoint:(CGPoint)point {
    if (_firstPoint.x < 0) {
        self.firstPoint = point;
    }
    [super moveToPoint:point];
}

@end

@interface ESAnnotationsLayer () {
#if (USE_CUBIC_SMOOTHING == TRUE)
    CGPoint pts[5]; // we now need to keep track of the four points of a Bezier segment and the first control point of the next segment
    uint ctr;

#else
    CGPoint currentPoint;
    CGPoint previousPoint1;
    CGPoint previousPoint2;
#endif
    ESExtendedBezierPath *myPath;
    NSMutableArray *pathArray;
    
}

@end

@implementation ESAnnotationsLayer

- (id) init {
    self = [super init];
    if (self) {
        pathArray = [NSMutableArray arrayWithCapacity:5];
        self.contentsScale = [UIScreen mainScreen].scale;
    }
    return self;
}

- (void) drawInContext:(CGContextRef)context {
    
    UIGraphicsPushContext(context);
    CGRect rect = CGContextGetClipBoundingBox(context);
    
//    NSLog(@"drawing rect: %@",NSStringFromCGRect(rect));
    //Draw all the paths
    for (int i=0; i<pathArray.count; i++) {
        ESExtendedBezierPath *path = [pathArray objectAtIndex:i];
        if (CGRectIntersectsRect(path.bounds, rect)) {
            [path stroke];
        }
    }

    UIGraphicsPopContext();
}

#pragma mark Private Helper function

CGPoint midPoint(CGPoint p1, CGPoint p2)
{
    return CGPointMake((p1.x + p2.x) /2, (p1.y + p2.y) /2);
}

- (void) startCurveAtPoint:(CGPoint) scaledPoint withColor:(UIColor *) color andLineWidth:(CGFloat) lineWidth {
    CGPoint point = CGPointMake(scaledPoint.x * self.bounds.size.width, scaledPoint.y * self.bounds.size.height);
    
    myPath=[[ESExtendedBezierPath alloc]init];
    myPath.lineWidth = ABS(lineWidth);
    myPath.lineCapStyle = kCGLineCapRound;
    myPath.lineJoinStyle = kCGLineJoinRound;
    myPath.color = color;
    myPath.erase = lineWidth<0;
    [pathArray addObject:myPath];
    
#if (USE_CUBIC_SMOOTHING == TRUE)
    ctr = 0;
    pts[0] = point;
#else
    previousPoint2 = point;
    previousPoint1 = point;
    currentPoint = point;
    
    [myPath moveToPoint:point];
//    [myPath addLineToPoint:point];
    [self invalidateRectForSmallestBoundingBox];
#endif
    
}

-(void) moveHighlightCurveToPoint:(CGPoint) scaledPoint{
    CGPoint point = CGPointMake(scaledPoint.x * self.bounds.size.width, scaledPoint.y * self.bounds.size.height);
    //    [myPath strokeWithBlendMode:kCGBlendModeNormal alpha:1.0f];
    
    point.y = myPath.firstPoint.y;
    
    previousPoint2  = previousPoint1;
    previousPoint1  = currentPoint;
    currentPoint    = point;

    [myPath addLineToPoint:point];
    [self invalidateRectForSmallestBoundingBox];
}

-(void) endHighlightCurveAtPoint:(CGPoint) scaledPoint{
    [self moveHighlightCurveToPoint:scaledPoint];
}


- (void) moveCurveToPoint:(CGPoint) scaledPoint {
    CGPoint point = CGPointMake(scaledPoint.x * self.bounds.size.width, scaledPoint.y * self.bounds.size.height);
//    [myPath strokeWithBlendMode:kCGBlendModeNormal alpha:1.0f];

#if (USE_CUBIC_SMOOTHING == TRUE)
    ctr++;
    pts[ctr] = point;
    if (ctr == 4)
    {
        pts[3] = CGPointMake((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0); // move the endpoint to the middle of the line joining the second control point of the first Bezier segment and the first control point of the second Bezier segment
        [myPath moveToPoint:pts[0]];
        [myPath addCurveToPoint:pts[3] controlPoint1:pts[1] controlPoint2:pts[2]]; // add a cubic Bezier from pt[0] to pt[3], with control points pt[1] and pt[2]
        [self invalidateRectForSmallestBoundingBox];
        
        // replace points and get ready to handle the next segment
        pts[0] = pts[3];
        pts[1] = pts[4];
        ctr = 1;
    }
#else
    previousPoint2  = previousPoint1;
    previousPoint1  = currentPoint;
    currentPoint    = point;
    
    CGPoint mid1    = midPoint(previousPoint1, previousPoint2);
    CGPoint mid2    = midPoint(currentPoint, previousPoint1);
    [myPath moveToPoint:mid1];
    [myPath addQuadCurveToPoint:mid2 controlPoint:previousPoint1];
    [self invalidateRectForSmallestBoundingBox];
#endif
    
}

- (void) endCurveAtPoint:(CGPoint) scaledPoint {
    CGPoint point = CGPointMake(scaledPoint.x * self.bounds.size.width, scaledPoint.y * self.bounds.size.height);
    
#if (USE_CUBIC_SMOOTHING == TRUE)
    ctr ++;
    pts[ctr] = point;
    switch (ctr) {
        case 1:
            if (CGPointEqualToPoint(CGPointZero, myPath.currentPoint)) {
                [myPath moveToPoint:[currentCurve pointFromLast:1]];
            }
            [myPath addLineToPoint:[currentCurve pointFromLast:0]];
            break;
        case 2:
            if (CGPointEqualToPoint(CGPointZero, myPath.currentPoint)) {
                [myPath moveToPoint:[currentCurve pointFromLast:2]];
            }
            [myPath addQuadCurveToPoint:[currentCurve pointFromLast:0] controlPoint:[currentCurve pointFromLast:1]];
            break;
        case 3:
            if (CGPointEqualToPoint(CGPointZero, myPath.currentPoint)) {
                [myPath moveToPoint:[currentCurve pointFromLast:3]];
            }

            [myPath addQuadCurveToPoint:pts[3] controlPoint:pts[2]];
            break;
        case 4:
            if (CGPointEqualToPoint(CGPointZero, myPath.currentPoint)) {
                [myPath moveToPoint:[currentCurve pointFromLast:4]];
            }
            [myPath addCurveToPoint:pts[4] controlPoint1:pts[3] controlPoint2:pts[2]];
            break;
        default:
            [myPath moveToPoint:point];
            [myPath addLineToPoint:point];
    }
    [self invalidateRectForSmallestBoundingBox];
#else
    previousPoint2  = previousPoint1;
    previousPoint1  = currentPoint;
    currentPoint    = point;
    
    CGPoint mid1    = midPoint(previousPoint1, previousPoint2);
    CGPoint mid2    = midPoint(currentPoint, previousPoint1);
    [myPath moveToPoint:mid1];
    [myPath addQuadCurveToPoint:mid2 controlPoint:previousPoint1];
    [self invalidateRectForSmallestBoundingBox];
    
#endif
  
}

- (void) addCurve:(NSArray *) points withColor:(UIColor *) color andLineWidth:(CGFloat) lineWidth {
    [self startCurveAtPoint:[points[0] CGPointValue] withColor:color andLineWidth:lineWidth];
   
    UIColor *highlightColor = [UIColor colorWithRed:83/255.f green:230/258.f blue:83/255.f alpha:0.4];

    if (lineWidth == 10.f && [highlightColor isEqual:color]) {//highlight
        for (int i=1; i<points.count-1; i++) {
            [self moveHighlightCurveToPoint:[points[i]CGPointValue]];
        }
        [self endHighlightCurveAtPoint:[points[points.count -1] CGPointValue]];
        
    }else{
        for (int i=1; i<points.count-1; i++) {
            [self moveCurveToPoint:[points[i] CGPointValue]];
        }
        [self endCurveAtPoint:[points[points.count -1] CGPointValue]];
    }
    [self setNeedsDisplay];
}

- (void) addCurves:(NSArray *) curvesArray {
    for (ESCurve *curve in curvesArray) {
        [self addCurve:curve.points withColor:curve.color andLineWidth:curve.lineWidth];
    }
}

#if (USE_CUBIC_SMOOTHING == FALSE)

- (void) invalidateRectForSmallestBoundingBox {
    CGPoint mid1    = midPoint(previousPoint1, previousPoint2);
    CGPoint mid2    = midPoint(currentPoint, previousPoint1);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, mid1.x, mid1.y);
    CGPathAddQuadCurveToPoint(path, NULL, mid2.x, mid2.y, previousPoint1.x, previousPoint1.y);
    CGRect bounds = CGPathGetBoundingBox(path);
    
    bounds.origin.x -= 1 + ceilf(myPath.lineWidth/2.f);
    bounds.origin.y -= 1 + ceilf(myPath.lineWidth/2.f);
    bounds.size.width += myPath.lineWidth + 2;
    bounds.size.height += myPath.lineWidth + 2;
    
//    NSLog(@"invalidate in bounds: %@",NSStringFromCGRect(bounds));
    
    CGPathRelease(path);
    [self setNeedsDisplayInRect:bounds];
}

#else
- (void) invalidateRectForSmallestBoundingBox {
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, pts[0].x, pts[0].y);
    
    for (int i=1; i<=ctr; i++) {
        CGPathAddLineToPoint(path, NULL, pts[i].x, pts[i].y);
    }
    
    CGRect bounds = CGPathGetBoundingBox(path);
    
    bounds.origin.x -= 1 + ceilf(myPath.lineWidth/2.f);
    bounds.origin.y -= 1 + ceilf(myPath.lineWidth/2.f);
    bounds.size.width += myPath.lineWidth + 2;
    bounds.size.height += myPath.lineWidth + 2;

    CGPathRelease(path);

    [self setNeedsDisplayInRect:bounds];
    
}
#endif

- (void) removeLastCurve {
    if (pathArray.count >0) {
        [pathArray removeLastObject];
    }
    [self setNeedsDisplay];
}

- (void) clear {
    [pathArray removeAllObjects];
    [self setNeedsDisplay];
}

@end

@interface ESCustomLayer () {
    
    NSMutableArray *textArray;
    
}
@end
@implementation ESCustomLayer

- (id) init {
    self = [super init];
    if (self) {
        textArray =[[NSMutableArray alloc] init];
    }
    return self;
}

- (void) drawInContext:(CGContextRef)context {
    //    [super drawInContext:ctx];
    
    UIGraphicsPushContext(context);
    for (ESText1 *text in textArray) {
        
        [self addTextToPDF:text.textValue atPoint:text.textFrame anfFontSize:text.fontSize];
    }
    UIGraphicsPopContext();
//    CGRect rect = UIGraphicsGetPDFContextBounds();
//    
//    rect = CGContextGetClipBoundingBox(context);
//    
//    UIColor * redColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0];
//    
//
//    CGContextSetFillColorWithColor(context, redColor.CGColor);
//    CGContextFillRect(context, CGRectInset(self.bounds, 50, 50));
    
}
- (void) drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
    
    CGContextSetFillColorWithColor(ctx, [[UIColor darkTextColor] CGColor]);
    
    UIGraphicsPushContext(ctx);
    CGContextScaleCTM(ctx, 1, -1);
    for (ESText1 *text in textArray) {
        
        [self addTextToPDF:text.textValue atPoint:text.textFrame anfFontSize:text.fontSize];
    }


    
    UIGraphicsPopContext();
}
- (void) addTexts:(NSArray *) curvesArray {
    for (ESText1 *text in curvesArray) {
        [textArray addObject:text];
    }

}

-(void)addTextToPDF:(NSString *)text atPoint:(CGRect)frame anfFontSize:(CGFloat )fontSize{
//    ESText *text = [ESText new];
    NSDictionary *textAttributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:fontSize]};
    
    // Create string drawing context
    NSStringDrawingContext *drawingContext = [[NSStringDrawingContext alloc] init];
    drawingContext.minimumScaleFactor = 0.5; // Half the font size
    
    CGRect drawRect = CGRectMake(0.0, 0.0, -frame.origin.x, -frame.origin.y);
    [text drawWithRect:drawRect
               options:NSStringDrawingUsesLineFragmentOrigin
            attributes:textAttributes
               context:drawingContext];

}

@end
