//
//  ESAnnotationsViewController.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/1/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAnnotationsViewController.h"
#import "ESAnnotationsLayer.h"

@interface MyView : UIView {
    ESAnnotationsLayer *layer;
}

@end

@implementation MyView

- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor grayColor];
        self.userInteractionEnabled = YES;
        
        layer = [ESAnnotationsLayer layer];
        layer.frame = self.bounds;
        [self.layer addSublayer:layer];
        //    layer.delegate = layer;
        layer.contentsScale = [UIScreen mainScreen].scale;
        [layer setNeedsDisplay];
//        layer.shouldRasterize = self.layer.shouldRasterize;
    }
    return self;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint point = [[touches anyObject] locationInView:self];
    [layer startCurveAtPoint:point withColor:[UIColor blueColor] andLineWidth:5];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint point = [[touches anyObject] locationInView:self];
    [layer moveCurveToPoint:point];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint point = [[touches anyObject] locationInView:self];
    [layer endCurveAtPoint:point];
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint point = [[touches anyObject] locationInView:self];
    [layer endCurveAtPoint:point];
}


@end

@interface ESAnnotationsViewController () {
    ESAnnotationsLayer *layer;
}

@end

@implementation ESAnnotationsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    [super loadView];
    
//    MyView *myView = [[MyView alloc] initWithFrame:self.view.bounds];
//    [self.view addSubview:myView];
    layer = [ESAnnotationsLayer layer];
    layer.frame = self.view.layer.bounds;
    layer.contentsScale = [UIScreen mainScreen].scale;
    [self.view.layer addSublayer:layer];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint point = [[touches anyObject] locationInView:self.view];
    [layer startCurveAtPoint:point withColor:[UIColor blueColor] andLineWidth:5];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint point = [[touches anyObject] locationInView:self.view];
    [layer moveCurveToPoint:point];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint point = [[touches anyObject] locationInView:self.view];
    [layer endCurveAtPoint:point];
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint point = [[touches anyObject] locationInView:self.view];
    [layer endCurveAtPoint:point];
}

@end
