//
//  ESCurve.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/1/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESCurve : NSObject

@property(nonatomic, strong) NSMutableArray *points;
@property(nonatomic) CGFloat lineWidth;
@property(nonatomic, strong) UIColor *color;

- (void) addPoint:(CGPoint) point;
- (CGPoint) pointAtIndex:(int) index;
- (CGPoint) pointFromLast:(int) index;

@end