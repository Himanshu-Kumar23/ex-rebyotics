//
//  ESPDFsListViewController.m
//  X Detailer
//
//  Created by Vikramjeet Singh on 02/04/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//

#import "ESPDFsListViewController.h"
#import "ESSettingListViewController.h"

#import "ESPresentationInfoRequestPacket.h"
#import "ESPDFPacket.h"

#import <QuartzCore/QuartzCore.h>

#import "ESWebViewController.h"

#import "ESAppDelegate.h"

#import "ZLSwipeableViewController.h"

#define UIDeviceOrientationIsPortrait(orientation)  ((orientation) == UIDeviceOrientationPortrait || (orientation) == UIDeviceOrientationPortraitUpsideDown)
#define UIDeviceOrientationIsLandscape(orientation) ((orientation) == UIDeviceOrientationLandscapeLeft || (orientation) == UIDeviceOrientationLandscapeRight)

#define IMPORT_ALERT_TAG 0x12390
@interface ESPDFsListViewController ()<UITableViewDataSource, UITableViewDelegate,UIGestureRecognizerDelegate> {
    //    NSMutableArray *pdfFiles;
    NSUserDefaults *userDefaults;
    NSDateFormatter *formatter;
    UILongPressGestureRecognizer *longPressCell;
    UITapGestureRecognizer *docSingleTap;
    NSInteger docMenuIndex;
    ESAppDelegate *appDelegate;
    
}

@end

@implementation ESPDFsListViewController


- (void) loadView {
    [super loadView];
    
    //This will be inside a UI navigation controller. not necessary to add a navBar.
    
    //Add a tableView.
    if (UIDeviceOrientationPortrait) {
        
    }else{
        
    }
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - 44.f) style:UITableViewStylePlain];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.tableView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (ESAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeStyle:NSDateFormatterLongStyle];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    
    self.navigationItem.leftBarButtonItem = backItem;
    
    //app is supporting all orientations
    //    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    //    {
    //        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    //    }
    //    else
    //    {
    //        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    //    }
    
    toolbar.hidden = YES;
    
    self.title = @"Documents";
    
    self.pdfFiles = [self findFiles:@"pdf"];
    
//    self.navigationItem.rightBarButtonItem = [self editButtonItem];
    
#if ENABLE_IMPORT
//    self.navigationItem.leftBarButtonItem = [self importBarButtonItem];
#endif
    longPressCell = [[UILongPressGestureRecognizer alloc]
                     initWithTarget:self action:@selector(handleLongPress:)];
    longPressCell.minimumPressDuration = 1; //seconds
    longPressCell.delegate = self;
    //[self.tableView addGestureRecognizer:longPressCell];
    
    docSingleTap = [[UITapGestureRecognizer alloc]
                    initWithTarget:self action:@selector(handleSingleTapForSelecting:)];
    longPressCell.delegate = self;
    [self.tableView addGestureRecognizer:docSingleTap];
    
    [docSingleTap requireGestureRecognizerToFail:longPressCell]; // Single tap requires double tap to fail
    
}

-(void) backButtonPressed{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)handleSingleTapForSelecting:(UITapGestureRecognizer *)gesture
{
    CGPoint p = [gesture locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    
    if (indexPath) {
        
        NSString *fileName = [_pdfFiles objectAtIndex:indexPath.row];
        NSLog(@"%@",fileName);
//        [[NSUserDefaults standardUserDefaults] setValue:fileName forKey:@"SelectedPDFFile"];

//        [self dismissViewControllerAnimated:YES completion:nil];
        
        
        //        NSString *file = [[NSBundle mainBundle] pathForResource:@"Document_Left&Right" ofType:@"pdf"];
        NSString *mode = [[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedMode"];
        
        if (![mode isEqualToString:@"Objection Handling"]){
            [self copyFilesFromBundleToDocumentsWithFileName:fileName];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
            NSString *documentsDirectoryPath = [paths objectAtIndex:0];
            
            NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:fileName];
            
            ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:nil];
            if (document != nil)
            {
                DetailerReaderViewController *readerViewController = [[DetailerReaderViewController alloc] initWithReaderDocument:document];
                readerViewController.delegate = self;
                //            readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                //            readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
                readerViewController.readerMode = mode;
                readerViewController.presentationName=fileName;
                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:readerViewController];
                navController.modalPresentationStyle = UIModalPresentationFullScreen;
                [self presentViewController:navController animated:YES completion:nil];
                
                //            [self presentViewController:readerViewController animated:YES completion:nil];
            }
        }else{
            NSLog(@"objection Handling");
            
            NSArray *sysPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory ,NSUserDomainMask, YES);
            NSString *documentsDirectory = [sysPaths objectAtIndex:0];
            NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:@"objectionHandling.plist"];
            
            NSMutableDictionary *plistDict; // needs to be mutable
            if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
            } else {
                // Doesn't exist, start with an empty dictionary
                plistDict = [[NSMutableDictionary alloc] init];
            }
           
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            ZLSwipeableViewController *vc = [self demoViewControllerForTitle:@"Objection Handling"];
            vc.title = @"Objection Handling";
            vc.objectionHandlingQuestions = [plistDict objectForKey:fileName];
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
}

- (ZLSwipeableViewController *)demoViewControllerForTitle:(NSString *)title {
    if ([title isEqual:@"Objection Handling"]) {
        return [[ZLSwipeableViewController alloc] init];
    }
    return [[ZLSwipeableViewController alloc] init];
}


-(void)copyFilesFromBundleToDocumentsWithFileName:(NSString *)fileName{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    NSString *pathLocal = [documentsDir stringByAppendingPathComponent:fileName];
    
    NSString *pathBundle = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
    
    //    NSArray *fileList = [manager contentsOfDirectoryAtPath:documentsDirectory
    //                                                     error:nil];
    //
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:pathLocal];
    if (!fileExists) {
        NSError *error;
        BOOL success = [fileManager copyItemAtPath:pathBundle toPath:pathLocal error:&error];
        if (!success) {
            NSLog(@"Could not copy file");
        }
    }else{
        NSLog(@"FIle already exsits at path");
    }
    
}
-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan){
        docMenuIndex = NSNotFound;
        
        CGPoint p = [gestureRecognizer locationInView:self.tableView];
        
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
        docMenuIndex = indexPath.row;
        
        if (indexPath == nil)
            return;
        UIMenuItem *edit = [[UIMenuItem alloc] initWithTitle:@"Rename" action:@selector(editDoc:)];
        UIMenuItem *delete = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(deleteDoc:)];
        
        UIMenuController *menu =nil;
        menu=[UIMenuController sharedMenuController];
        [menu setMenuItems:[NSArray arrayWithObjects:edit, delete, nil]];
        [menu setTargetRect:CGRectMake(p.x,p.y, 0.0f, 0.0f) inView:self.tableView];
        [menu setMenuVisible:YES animated:YES];
        
        NSAssert([self becomeFirstResponder], @"Sorry, UIMenuController will not work with %@ since it cannot become first responder", self);
    }
    
}
-(BOOL)canBecomeFirstResponder{
    return YES;
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    
    if (action == @selector(editDoc:)) {
        return YES;
    }else if (action == @selector(deleteDoc:)){
        return YES;
    }
    return NO;
}
-(void)editDoc:(id)sender{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:@"Do you want to rename the existing document"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                               }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"name";
        textField.text = [ self.pdfFiles objectAtIndex:docMenuIndex];
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    
    UIAlertAction* renameButton = [UIAlertAction
                                   actionWithTitle:@"Rename"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                       if (docMenuIndex != NSNotFound) {
                                           
                                           //UITextField *textField = [alertView textFieldAtIndex:0];
                                           UITextField *textField = [[alert textFields] objectAtIndex:0];
                                           
                                           NSError * err = NULL;
                                           NSString *newFilePath = @"";
                                           NSFileManager * fm = [[NSFileManager alloc] init];
                                           NSString *filePath = [[self getDocumentDirectory] stringByAppendingString:[NSString stringWithFormat:@"/%@",[_pdfFiles objectAtIndex:docMenuIndex]]];
                                           
                                           NSArray *strArray = [textField.text componentsSeparatedByString:@"."];
                                           
                                           if (strArray.count > 1) {
                                               if( [[strArray lastObject] caseInsensitiveCompare:@"pdf"] == NSOrderedSame ) {
                                                   // strings are equal except for possibly case
                                                   newFilePath =[[self getDocumentDirectory] stringByAppendingString:[NSString stringWithFormat:@"/%@",textField.text]];
                                                   
                                               }else{
                                                   newFilePath =[[self getDocumentDirectory] stringByAppendingString:[NSString stringWithFormat:@"/%@.pdf",textField.text]];
                                               }
                                           }else{
                                               newFilePath =[[self getDocumentDirectory] stringByAppendingString:[NSString stringWithFormat:@"/%@.pdf",textField.text]];
                                           }
                                           
                                           BOOL result = [fm moveItemAtPath:filePath toPath:newFilePath error:&err];
                                           if(!result)
                                               NSLog(@"Error: %@", err);
                                           else{
                                               [self findPDFFileAndReloadDataAfterDismissingView];
                                           }
                                       }
                                   }];
    
    
    [alert addAction:noButton];
    [alert addAction:renameButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    //    UIAlertView *renameAlert = [[UIAlertView alloc]initWithTitle:@"" message:@"Do you want to rename the existing document" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"Rename", nil];
    //
    //    renameAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    //
    //    UITextField *textField = [renameAlert textFieldAtIndex:0];
    //
    //    textField.text = [ self.pdfFiles objectAtIndex:docMenuIndex];
    //
    //    renameAlert.tag = RENAME_ALERT_TAG;
    //
    //    [renameAlert show];
    
    
}
-(void)deleteDoc:(id)sender{
    [self removeFile:[_pdfFiles objectAtIndex:docMenuIndex]];
    [_pdfFiles removeObjectAtIndex:docMenuIndex];
    [self findPDFFileAndReloadDataAfterDismissingView];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.pdfFiles = [self findFiles:@"pdf"];
    
    //Statusbar is getting UIStatusBarStyleBlackTranslucent after coming from pdfDocument viewcontroller
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
}


- (UIBarButtonItem *) editButtonItem {
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(startEditingTableView:)];
}

- (UIBarButtonItem *) doneEditButtonItem {
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditingTableView:)];
}

- (void) startEditingTableView:(id) sender {
    docSingleTap.enabled = NO;
    longPressCell.enabled = NO;
    [self.tableView setEditing:YES animated:YES];
    self.navigationItem.rightBarButtonItem = [self doneEditButtonItem];
    
}

- (void) doneEditingTableView:(id) sender {
    docSingleTap.enabled = YES;
    longPressCell.enabled = YES;
    [self.tableView setEditing:NO animated:YES];
    self.navigationItem.rightBarButtonItem = [self editButtonItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDatasource implementation

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _pdfFiles.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellIdentifer";
    UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (tableViewCell == nil) {
        tableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    tableViewCell.textLabel.text = [_pdfFiles objectAtIndex:indexPath.row];
    
    tableViewCell.detailTextLabel.text = [self detailTextLabelAtIndexPath:indexPath.row].length>0?[self detailTextLabelAtIndexPath:indexPath.row]:@"";
    
    return tableViewCell;
}

-(NSString *)detailTextLabelAtIndexPath:(NSInteger )row{
    
    NSString *lastModifiedString ;
    
    NSString *filePath = [[self getDocumentDirectory] stringByAppendingString:[NSString stringWithFormat:@"/%@",[_pdfFiles objectAtIndex:row]]];
    
    NSFileManager* fm = [NSFileManager defaultManager];
    
    NSDictionary* attrs = [fm attributesOfItemAtPath:filePath error:nil];
    
    if (attrs != nil) {
        NSDate *date = (NSDate*)[attrs objectForKey: NSFileModificationDate];
        lastModifiedString = [NSString stringWithFormat:@"Last modified on %@",[formatter stringFromDate:date]];
    }
    else {
        //        NSLog(@"Not found");
    }
    
    return lastModifiedString;
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    CGFloat navigationToolbarFrameHeight = self.navigationController.navigationBar.frame.size.height;
    //    [UIView animateWithDuration:duration animations:^{
    toolbar.frame = CGRectMake(0, self.view.bounds.size.height - navigationToolbarFrameHeight, self.view.bounds.size.width, navigationToolbarFrameHeight);
    self.tableView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - toolbar.frame.size.height);
    
}

#pragma mark FileSharing

/**
 * What should happen when a pdf document is tapped ?
 * 1. If the current device is connected as presenter, then we should try to send the <file name,created timestamp> information to all the peers.
 * 1.1. Peers should respond to this by mentioning whether they need the file to be transferred or not.
 * 1.2. Presenter device should transfer the file to all the peers attendees which require the file.
 * 1.2.1. Presenter device should figure out how to send the <file name, created timestamp> along with the file contents.
 1.2.2. Presenter device should write the file with the given name and created time stamp by using the following method on NSFileManager class
 - (BOOL)setAttributes:(NSDictionary *)attributes ofItemAtPath:(NSString *)path error:(NSError **)error
 with the attribute NSFileCreationDate.
 * 2. When the current device is connected as a attendee. Then we should try to send the file only to the presenter and not to everybody.
 *
 *
 */
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *fileName = [_pdfFiles objectAtIndex:indexPath.row];
    NSLog(@"%@",fileName);
    //    if ([appDelegate.sessionState isEqualToString:@"1"])
    //    {
    //        if ([[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter){
    //            [self sendPresentationInfoRequestToAllAttendeesForFile:fileName];
    //        } else {
    //            return;
    //        }
    //    }
    //[self openFileName:fileName];
    //if ([self.mode isEqualToString:@"Present"]) {
//    [self showConfirmationAlertForIndexPath:indexPath];
    
    //}else{
    
    //    [self openFileAtRow:indexPath.row];
    //}
}

-(void) showConfirmationAlertForIndexPath:(NSIndexPath *) indexPath{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:PRESENTER_ALERT_MSG
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                               }];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [self openFileAtRow:indexPath.row];
                                }];
    
    
    
    [alert addAction:noButton];
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    //    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"" message:PRESENTER_ALERT_MSG delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes, please proceed", nil];
    //    alertview.tag= indexPath.row;
    //    [alertview show];
}
-(void) openFileAtRow:(NSInteger) row{
    
    NSString *fileName = [_pdfFiles objectAtIndex:row];
    if ([appDelegate.sessionState isEqualToString:@"1"])
    {
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter){
            [self sendPresentationInfoRequestToAllAttendeesForFile:fileName];
        } else {
            return;
        }
    }
    [self openFileName:fileName];
}
/* (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self removeFile:[_pdfFiles objectAtIndex:indexPath.row]];
        [_pdfFiles removeObjectAtIndex:indexPath.row];
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
        [tableView endUpdates];
    }
}*/

- (void) removeFile:(NSString *) fileName {
    
    NSError *error = nil;
    
    //    NSString *docDir = [self getDocumentDirectory];
    
    NSString *path = [self pathForFileName:fileName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //    NSLog(@"Documents directory before: %@", [fileManager contentsOfDirectoryAtPath:docDir error:&error]);
    
    if([fileManager fileExistsAtPath:path] == YES)
        
    {
        BOOL success = [fileManager removeItemAtPath:path error:&error];
        
        if (!success) {
            
            NSLog(@"error:%@", error);
            
        }
        
    }
    
    //    NSLog(@"Documents directory after: %@", [fileManager contentsOfDirectoryAtPath:docDir error:&error]);
}

-(NSMutableArray *)findFiles:(NSString *)extension {
    
    NSMutableArray *matches = [[NSMutableArray alloc]init];
    NSFileManager *fManager = [NSFileManager defaultManager];
    NSString *item;
    
    NSString *documentsDir = [self getDocumentDirectory];
    [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:documentsDir]];
    
    NSArray *contents = [fManager contentsOfDirectoryAtPath:documentsDir error:nil];
    
    // >>> this section here adds all files with the chosen extension to an array
    for (item in contents){
        if ([[item pathExtension] isEqualToString:extension]) {
            [matches addObject:item];
        }
    }
    
    return matches;
}

-(NSString *) getDocumentDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}


-(NSString *) pathForFileName:(NSString *) fileName {
    NSFileManager *fManager = [NSFileManager defaultManager];
    
    NSString *documentsDir = [self getDocumentDirectory];
    
    NSString *path = [documentsDir stringByAppendingPathComponent:fileName];
    if([fManager fileExistsAtPath:path] == YES)
    {
        return path;
    }
    return nil;
}
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL{
    return  [super addSkipBackupAttributeToItemAtURL:URL];
}

-(NSURL *) getURLForFileName:(NSString *) name {
    NSRange rangeOfSubstring = [name rangeOfString:@".pdf"];
    NSString *fileName = [name stringByReplacingCharactersInRange:rangeOfSubstring withString:@""];
    return  [[NSBundle mainBundle] URLForResource:fileName withExtension:@"pdf"];
}

- (BOOL)shouldAutorotate{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    //    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
    //        return UIInterfaceOrientationMaskLandscape;
    //    }else{
    return UIInterfaceOrientationMaskAll;
    //    }
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    //    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
    //        return (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft);
    //    }else{
    return YES;
    //    }
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    //    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
    return UIInterfaceOrientationLandscapeRight;
    //    }else{
    //        return UIInterfaceOrientationPortrait;
    //    }
}

- (void) resetData {
    _pdfFiles = [self findFiles:@"pdf"];
    [self.tableView reloadData];
}

-(UIBarButtonItem *)importBarButtonItem{
    return [[UIBarButtonItem alloc]initWithTitle:@"Add Files" style:UIBarButtonItemStylePlain target:self action:@selector(importFilesFormWeb:)];
}

-(IBAction)importFilesFormWeb:(id)sender{
    NSString *msg= @"1. Click on any PDF in your email and download it automatically to open in Presinteract\n2. Use your browser to find the PDF document on the web, open it and then click on 'Open in PresInteract'\n3. Use iTunes to add files to PresInteract\n4. Send files automatically to devices connected to PresInteract";
    
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Four simple ways to import documents"
                                 message:msg
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                               }];
    
    UIAlertAction* openButton = [UIAlertAction
                                 actionWithTitle:@"Open Browser"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action) {
                                     //Handle your yes please button action here
                                     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.google.com"]];
                                 }];
    
    [alert addAction:okButton];
    [alert addAction:openButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Four simple ways to import documents" message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Open Browser", nil];
    alertView.tag = IMPORT_ALERT_TAG;
    [alertView show];
    
        ESWebViewController *webVC = [[ESWebViewController alloc]init];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:webVC];
        navController.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:navController animated:YES completion:nil];
}

-(void)downloadCompletedFromWeb:(NSNotification *)aNotification{
    
    [self findPDFFileAndReloadDataAfterDismissingView];
}
-(void)fileDeletedFromDownLoadedList:(NSNotification *)aNotification{
    
    [self findPDFFileAndReloadDataAfterDismissingView];
}
-(void)findPDFFileAndReloadDataAfterDismissingView{
    
    self.pdfFiles = [self findFiles:@"pdf"];
    [self.tableView reloadData];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}
@end
