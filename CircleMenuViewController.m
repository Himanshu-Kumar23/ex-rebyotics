//
//  CircleMenuViewController.m
//  KYCircleMenuDemo
//
//  Created by Kjuly on 7/18/12.
//  Copyright (c) 2012 Kjuly. All rights reserved.
//

#import "CircleMenuViewController.h"
#import "DetailerReaderViewController.h"
#import "ESDocumentsListViewController.h"

#import "ESDocumentsCollectionViewController.h"
#import "ESWebViewController.h"
#import "CircleMenuViewController.h"
#import "Constants.h"
#import <sys/utsname.h>
#import "ESPDFsListViewController.h"
#import "Flurry.h"
#import "ZLSwipeableViewController.h"

@implementation CircleMenuViewController
@synthesize mode, alreadyRotatedTheScreen;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nil])) // Designated initializer
    {
    }
    return self;
}
- (void)dealloc {
    //[super dealloc];
}

int mynumber;
- (id)init {
    if (self = [super init])
        [self setTitle:@"KYCircleMenu"];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Modify buttons' style in circle menu
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];

}

- (void) orientationChanged:(NSNotification *)note
{
    UIDevice * device = note.object;
    switch(device.orientation)
    {
        case UIDeviceOrientationPortrait:
        {
            NSString *nibName;
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
                nibName=@"CircleMenuViewController-ipad";
            }
            else
            {
                nibName=@"CircleMenuViewController";
            }
            NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
            UIView *aView = [nibObjs objectAtIndex:0];
            self.view = aView;
        }
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
        {
            NSString *nibName;
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
                nibName=@"CircleMenuViewController-ipad";
            }
            else
            {
                nibName=@"CircleMenuViewController";
            }
            NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
            UIView *aView = [nibObjs objectAtIndex:0];
            self.view = aView;
        }
            break;
            
        case UIDeviceOrientationLandscapeRight:
        {
            NSString *nibName;
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
                nibName=@"CircleMenuViewController-ipad-landscape";
            }
            else
            {
                nibName=@"CircleMenuViewController-landscape";
            }
            NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
            UIView *aView = [nibObjs objectAtIndex:0];
            self.view = aView;
        }
            break;

        case UIDeviceOrientationLandscapeLeft:
        {
            NSString *nibName;
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
                nibName=@"CircleMenuViewController-ipad-landscape";
            }
            else
            {
                nibName=@"CircleMenuViewController-landscape";
            }
            NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
            UIView *aView = [nibObjs objectAtIndex:0];
            self.view = aView;
        }
            break;

        default:
            break;
    };
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
//    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedPDFFile"] != nil && ![[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedPDFFile"] isEqualToString:@""])
//    {
//        if ([mode isEqualToString:@"Prepare"])
//        {
//            NSString *fileName = [[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedPDFFile"];
//
//            //        NSString *file = [[NSBundle mainBundle] pathForResource:@"Document_Left&Right" ofType:@"pdf"];
//            [self copyFilesFromBundleToDocumentsWithFileName:fileName];
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
//            NSString *documentsDirectoryPath = [paths objectAtIndex:0];
//
//            NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:fileName];
//
//            ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:nil];
//            if (document != nil)
//            {
//                DetailerReaderViewController *readerViewController = [[DetailerReaderViewController alloc] initWithReaderDocument:document];
//                readerViewController.delegate = self;
//                //            readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//                //            readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
//                readerViewController.readerMode = mode;
//                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:readerViewController];
//                navController.modalPresentationStyle = UIModalPresentationFullScreen;
//                [self presentViewController:navController animated:YES completion:nil];
//
//                //            [self presentViewController:readerViewController animated:YES completion:nil];
//            }
//        }
//        else{
//            NSString *fileName = [[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedPDFFile"];
//            [self copyFilesFromBundleToDocumentsWithFileName:fileName];
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
//            NSString *documentsDirectoryPath = [paths objectAtIndex:0];
//
//            NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:fileName];
//
//            ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:nil];
//            if (document != nil)
//            {
//                DetailerReaderViewController *readerViewController = [[DetailerReaderViewController alloc] initWithReaderDocument:document];
//                readerViewController.delegate = self;
//                //            readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//                //            readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
//                readerViewController.readerMode = mode;
//                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:readerViewController];
//                navController.modalPresentationStyle = UIModalPresentationFullScreen;
//                [self presentViewController:navController animated:YES completion:nil];
//
//
//                //            [self presentViewController:readerViewController animated:YES completion:nil];
//            }
//        }
//
//        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"SelectedPDFFile"];
//
//    }

//    CGRect rect = PrepareBtn.frame;
//    rect.origin.y=self.view.frame.size.height + PrepareBtn.frame.size.height;
//    PrepareBtn.frame = rect;
    
  //  [self PrepareBtnAnimate];
    
    
//    timer1 = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(PrepareBtnAnimate) userInfo:nil repeats:NO];
//
//    timer2 = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(PracticeBtnAnimate) userInfo:nil repeats:NO];
//
//    timer3 = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(PresentBtnAnimate) userInfo:nil repeats:NO];


}

-(void)PrepareBtnAnimate
{
    
    [UIView animateWithDuration:3.0 animations:^{
        self->PrepareBtn.frame = self->prepareBtn_frame;
    }];
//    [UIView animateWithDuration:5.0f
//                          delay:0.0f
//                        options:UIViewAnimationOptionAllowUserInteraction
//                     animations:^(void) {
//                         [self->PrepareBtn setAlpha:0.3f];
//                     } completion:^(BOOL finished) {
//                         [self->PrepareBtn setAlpha:1.0f];
//                     }];

}

-(void)PracticeBtnAnimate
{
    [PracticeBtn setAlpha:0.3f];
    
    [UIView animateWithDuration:2.0f
                          delay:0.0f
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^(void) {
                         [self->PracticeBtn setAlpha:1.0f];
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:2.0f
                                               delay:0.0f
                                             options:UIViewAnimationOptionAllowUserInteraction
                                          animations:^(void) {
                                              [self->PracticeBtn setAlpha:1.0f];
                                          } completion:^(BOOL finished) {
                                              //Maybe set the new alpha here when the first animation is done?
                                          }];
                     }];

}


-(void)PresentBtnAnimate
{
    [PresentBtn setAlpha:0.3f];
    
    [UIView animateWithDuration:2.0f
                          delay:0.0f
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^(void) {
                         [self->PresentBtn setAlpha:1.0f];
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:2.0f
                                               delay:0.0f
                                             options:UIViewAnimationOptionAllowUserInteraction
                                          animations:^(void) {
                                              [self->PresentBtn setAlpha:1.0f];
                                          } completion:^(BOOL finished) {
                                              //Maybe set the new alpha here when the first animation is done?
                                          }];
                     }];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - KYCircleMenu Button Action

// Run button action depend on their tags:
//
// TAG:        1       1   2      1   2     1   2     1 2 3     1 2 3
//            \|/       \|/        \|/       \|/       \|/       \|/
// COUNT: 1) --|--  2) --|--   3) --|--  4) --|--  5) --|--  6) --|--
//            /|\       /|\        /|\       /|\       /|\       /|\
// TAG:                             3       3   4     4   5     4 5 6
//
- (IBAction)runButtonActions:(id)sender {
   // [super runButtonActions:sender];
    
    // Configure new view & push it with custom |pushViewController:| method
    /*UIViewController * viewController = [[UIViewController alloc] init];
     [viewController.view setBackgroundColor:[UIColor blackColor]];
     [viewController setTitle:[NSString stringWithFormat:@"View %d", [sender tag]]];
     // Use KYCircleMenu's |-pushViewController:| to push vc
     [self pushViewController:viewController];
     [viewController release];*/
    
    [timer1 invalidate];
    [timer2 invalidate];
    [timer3 invalidate];

    if ([sender tag] == 1) {
        
        mode = @"Prepare";
        [[NSUserDefaults standardUserDefaults] setValue:mode forKey:@"SelectedMode"];
        
        NSArray *ArrayPdfs=[self findFiles:@"pdf"];
        if (ArrayPdfs.count>1) {
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            ESPDFsListViewController *pdfsList = [[ESPDFsListViewController alloc] init];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:pdfsList];
            [self presentViewController:navController animated:YES completion:nil];
            [Flurry logAllPageViewsForTarget:navController];
            
            return;
        }

        NSString *fileName = @"Rebyota.pdf";

//        NSString *file = [[NSBundle mainBundle] pathForResource:@"Document_Left&Right" ofType:@"pdf"];
        [self copyFilesFromBundleToDocumentsWithFileName:fileName];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
        NSString *documentsDirectoryPath = [paths objectAtIndex:0];
        
        NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:fileName];

        ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:nil];
        if (document != nil)
        {
            DetailerReaderViewController *readerViewController = [[DetailerReaderViewController alloc] initWithReaderDocument:document];
            readerViewController.delegate = self;
//            readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//            readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
            readerViewController.readerMode = mode;
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:readerViewController];
            navController.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:navController animated:YES completion:nil];

//            [self presentViewController:readerViewController animated:YES completion:nil];
        }
        
    }else if ([sender tag] == 2){
        
        mode = @"Practice";
        [[NSUserDefaults standardUserDefaults] setValue:mode forKey:@"SelectedMode"];
        NSArray *ArrayPdfs=[self findFiles:@"pdf"];
        if (ArrayPdfs.count>1) {
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            ESPDFsListViewController *pdfsList = [[ESPDFsListViewController alloc] init];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:pdfsList];
            [self presentViewController:navController animated:YES completion:nil];
            [Flurry logAllPageViewsForTarget:navController];
            return;
        }
        //     NSString *file = [[NSBundle mainBundle] pathForResource:@"Document_LeftAlone" ofType:@"pdf"];
        
        NSString *fileName = @"Working_With_PowerPoint_Combined.pdf";
        [self copyFilesFromBundleToDocumentsWithFileName:fileName];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
        NSString *documentsDirectoryPath = [paths objectAtIndex:0];
        
        NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:fileName];
        
        ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:nil];
        if (document != nil)
        {
            DetailerReaderViewController *readerViewController = [[DetailerReaderViewController alloc] initWithReaderDocument:document];
            readerViewController.delegate = self;
            readerViewController.presentationName=fileName;
//            readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//            readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
            readerViewController.readerMode = mode;
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:readerViewController];
                        navController.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:navController animated:YES completion:nil];
            

//            [self presentViewController:readerViewController animated:YES completion:nil];
        }
        
        
    }else if ([sender tag] == 3){
        
        mode = @"Present";
        [[NSUserDefaults standardUserDefaults] setValue:mode forKey:@"SelectedMode"];

        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        ESDocumentsListViewController *_documentListVC = [[ESDocumentsListViewController alloc] init];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:_documentListVC];
        [self presentViewController:navController animated:YES completion:nil];

//        self.window.rootViewController = navController;
//        //    }else{
//        //    self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:[ESWebViewController new]];
//        //    }
//        //
//        
//        self.window.backgroundColor = [UIColor whiteColor];
//        [self.window makeKeyAndVisible];
        
//        [Flurry logAllPageViews:navController];
        [Flurry logAllPageViewsForTarget:navController];
        
    }else if ([sender tag] == 4){
        
        mode = @"Objection Handling";
        [[NSUserDefaults standardUserDefaults] setValue:mode forKey:@"SelectedMode"];
        NSArray *ArrayPdfs=[self findFiles:@"pdf"];
        NSLog(@"%@",ArrayPdfs);
        if (ArrayPdfs.count>1) {
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            ESPDFsListViewController *pdfsList = [[ESPDFsListViewController alloc] init];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:pdfsList];
            [self presentViewController:navController animated:YES completion:nil];
            [Flurry logAllPageViewsForTarget:navController];
            return;
        }else if (ArrayPdfs.count == 1){
            NSArray *sysPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory ,NSUserDomainMask, YES);
            NSString *documentsDirectory = [sysPaths objectAtIndex:0];
            NSString *fileName = [ArrayPdfs objectAtIndex:0];
            NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:@"objectionHandling.plist"];
            
            NSMutableDictionary *plistDict; // needs to be mutable
            if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
            } else {
                // Doesn't exist, start with an empty dictionary
                plistDict = [[NSMutableDictionary alloc] init];
            }
            
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            ZLSwipeableViewController *vc = [self demoViewControllerForTitle:@"Objection Handling"];
            vc.title = @"Objection Handling";
            vc.objectionHandlingQuestions = [plistDict objectForKey:fileName];
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
}

- (ZLSwipeableViewController *)demoViewControllerForTitle:(NSString *)title {
    if ([title isEqual:@"Objection Handling"]) {
        return [[ZLSwipeableViewController alloc] init];
    }
    return [[ZLSwipeableViewController alloc] init];
}

-(NSMutableArray *)findFiles:(NSString *)extension {
    
    NSMutableArray *matches = [[NSMutableArray alloc]init];
    NSFileManager *fManager = [NSFileManager defaultManager];
    NSString *item;
    
    NSString *documentsDir = [self getDocumentDirectory];
    [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:documentsDir]];
    
    NSArray *contents = [fManager contentsOfDirectoryAtPath:documentsDir error:nil];
    
    // >>> this section here adds all files with the chosen extension to an array
    for (item in contents){
        if ([[item pathExtension] isEqualToString:extension]) {
            [matches addObject:item];
        }
    }
    
    return matches;
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL{
    
    if ([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]) {
        NSError *error = nil;
        
        BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                        
                                      forKey: NSURLIsExcludedFromBackupKey error: &error];
        if(!success){
            
            NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
            
        }
        return success;
    }else{
        return NO;
    }
    
}
-(NSString *) getDocumentDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}


- (BOOL)shouldAutorotate{
    return YES;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}


-(void)copyFilesFromBundleToDocumentsWithFileName:(NSString *)fileName{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    NSString *pathLocal = [documentsDir stringByAppendingPathComponent:fileName];
    
    NSString *pathBundle = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
    
//    NSArray *fileList = [manager contentsOfDirectoryAtPath:documentsDirectory
//                                                     error:nil];
//
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:pathLocal];
    if (!fileExists) {
        NSError *error;
        BOOL success = [fileManager copyItemAtPath:pathBundle toPath:pathLocal error:&error];
        if (!success) {
            NSLog(@"Could not copy file");
        }
    }else{
        NSLog(@"FIle already exsits at path");
    }
    
}
- (void)handleOrientation{}
@end
