//
//  ESDownLoadFileViewController.h
//  iProjector
//
//  Created by Jayaprada Behera on 19/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESDownLoadFileViewController : UITableViewController
@property(nonatomic,strong) NSMutableArray *fileNameArray;
@end
