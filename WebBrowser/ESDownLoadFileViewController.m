//
//  ESDownLoadFileViewController.m
//  iProjector
//
//  Created by Jayaprada Behera on 19/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESDownLoadFileViewController.h"
#import "ESPDFDownLoaderOperation.h"
#import "ESWebViewController.h"
@interface ESDownLoadFileViewController ()
{
    NSString *afileName;
}

@end

@implementation ESDownLoadFileViewController
@synthesize fileNameArray;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
-(void)downloadCompleted:(NSNotification *)aNotification{
    
    NSLog(@"downloadCompletedwith notofication");
    NSDictionary *dict = [aNotification object];
    NSString *fileName1 = [dict objectForKey:@"filename"];
//    for (NSString *fName in self.fileName){
//        
//        if ([fileName isEqualToString:fName]){
//            afileName = fName;
//        }
//    }
    
    [self.tableView reloadData];

}
-(void)isExecutingFile:(NSNotification *)aNotification{
    
    NSLog(@"isExecutingFile notofication");

    [self.tableView reloadData];

}
- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(isExecutingFile:) name:OPERATION_IS_EXECUTING_NOTIFICATION object:nil ];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(downloadCompleted:) name:OPERATION_DID_FINISH_NOTIFICATION object:nil ];
    
    if (self.fileNameArray == nil) {
        self.fileNameArray = [[NSMutableArray alloc] init];
    }else if (self.fileNameArray.count >0) {
       self.navigationItem.leftBarButtonItem = [self editButtonItem];
    }
    self.navigationItem.rightBarButtonItem = [self donebarButton];
}
-(UIBarButtonItem *)donebarButton{
    
    return [[UIBarButtonItem alloc]initWithTitle:@"Dismiss" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissPageTapped:)];
}
- (UIBarButtonItem *) editButtonItem {
    
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(startEditingTableView:)];
}

- (UIBarButtonItem *) doneEditButtonItem {
    
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditingTableView:)];
}

- (void) startEditingTableView:(id) sender {
    
    [self.tableView setEditing:YES animated:YES];
    self.navigationItem.leftBarButtonItem = [self doneEditButtonItem];
}

- (void) doneEditingTableView:(id) sender {
    
    [self.tableView setEditing:NO animated:YES];
    self.navigationItem.leftBarButtonItem = [self editButtonItem];
}

-(IBAction)dismissPageTapped:(id)sender{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    // Return the number of rows in the section.
    return self.fileNameArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"CellIdentifer";
    UIActivityIndicatorView *spinner;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        cell.accessoryView = spinner;
    }
    
    
    cell.textLabel.text = [self.fileNameArray objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.f];
    
    
//
//    if ([afileName isEqualToString:[self.fileName objectAtIndex:indexPath.row]]){
//        
//        //downloaded file ...
//
//        [spinner stopAnimating];
//        spinner.hidden = YES;
//    }else{
//        //executing file ...
//
//        [spinner startAnimating];
//        spinner.hidden = NO;
//    }
//    
    return cell;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source ...
        [self removeFile:[self.fileNameArray objectAtIndex:indexPath.row]];
        [self.fileNameArray removeObjectAtIndex:indexPath.row];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"PDFFileDeletedFromDownLoad" object:[NSString stringWithFormat:@"%d",self.fileNameArray.count]];

        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
        
    }
}
-(void)removeFile:(NSString *)fileName{
    
    NSError *error = nil;
    
    NSString *docDir = [self getDocumentDirectory];
    NSString *path = [self pathForFileName:fileName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSLog(@"Documents directory before: %@", [fileManager contentsOfDirectoryAtPath:docDir error:&error]);
    
    
    if([fileManager fileExistsAtPath:path] == YES)
    {
        BOOL success = [fileManager removeItemAtPath:path error:&error];
        if (!success) {
            NSLog(@"error:%@", error);
        }
    }

    NSLog(@"Documents directory after: %@", [fileManager contentsOfDirectoryAtPath:docDir error:&error]);

}
-(NSString *) getDocumentDirectory {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

-(NSString *) pathForFileName:(NSString *) fileName {
    
    NSFileManager *fManager = [NSFileManager defaultManager];
    
    NSString *documentsDir = [self getDocumentDirectory];
    NSString *path = [documentsDir stringByAppendingPathComponent:fileName];
    if([fManager fileExistsAtPath:path] == YES)
    {
        return path;
    }
    return nil;
}
#pragma Orientations Methods
//Availabale in 6.0
- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}

@end
