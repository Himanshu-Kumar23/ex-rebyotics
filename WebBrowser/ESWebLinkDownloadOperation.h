//
//  ESWebLinkDownloadOperation.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 10/3/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ES_FILE_DOWNLOADED_NOTIFICATION @"kESFileDownloadCompleteNotification"

@class ESWebLinkDownloadOperation;

@protocol ESWebLinkDownloadOperationDelegate <NSObject>


@required
- (void) didFinishDownloadingContentForDownloadOperation:(ESWebLinkDownloadOperation *) downloadOperation;
- (void) didFailDownloadingContentForDownloadOperation:(ESWebLinkDownloadOperation *) downloadOperation withError:(NSError *) error;

@end

@interface ESWebLinkDownloadOperation : NSObject {
    UIBackgroundTaskIdentifier bgTaskId;
}

@property(nonatomic, readonly) BOOL finished;
@property(nonatomic, strong) NSURL *urlToDownload;
@property(nonatomic, unsafe_unretained) id<ESWebLinkDownloadOperationDelegate> delegate;

+ (ESWebLinkDownloadOperation *) downloadWithURL:(NSURL *) url andDelegate:(id<ESWebLinkDownloadOperationDelegate>) delegate;

@end