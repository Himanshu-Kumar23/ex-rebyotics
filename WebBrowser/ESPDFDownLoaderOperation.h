//
//  ESPDFDownLoaderOperation.h
//  iProjector
//
//  Created by Jayaprada Behera on 19/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    ESOperationCancelledState      = -1,
    ESOperationReadyState,
    ESOperationExecutingState,
    ESOperationFinishedState,
} OperationState;


@interface ESPDFDownLoaderOperation : NSOperation<NSURLConnectionDelegate>{
    BOOL _isExecuting;
    BOOL _isFinished;
    BOOL _isCancelled;
    BOOL downloadDone;
    NSMutableData *downloadData;
}
@property(nonatomic,strong) NSURLRequest *urlRequest;
- (id)initWithRequest:(NSURLRequest *)urlRequest;

@end
