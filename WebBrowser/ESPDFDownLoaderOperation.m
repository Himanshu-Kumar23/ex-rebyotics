//
//  ESPDFDownLoaderOperation.m
//  iProjector
//
//  Created by Jayaprada Behera on 19/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESPDFDownLoaderOperation.h"

@implementation ESPDFDownLoaderOperation
- (id)initWithRequest:(NSURLRequest *)url {
    self = [super init];
    if (self == nil) {
		return nil;
    }
    self.urlRequest = url;
    _isExecuting = NO;
    _isFinished = NO;
    downloadDone = NO;
    downloadData =[[NSMutableData alloc]init];
    return self;
}

-(void)main {
    
    if (self.isCancelled) {
        return;
    }
    if (![self isCancelled]) {
        
        [self willChangeValueForKey:@"isExecuting"];
        _isExecuting = YES;
        
        NSLog(@"%s: downloadRequest: %@",__FUNCTION__,[[self.urlRequest URL] absoluteString]);
        NSURLConnection *downloadConnection = [[NSURLConnection alloc] initWithRequest:self.urlRequest delegate:self startImmediately:NO];
        
        // This block SHOULD keep the NSOperation from releasing before the download has been finished
        if (downloadConnection) {
            NSLog(@"connection established!");
            do {
                [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
            } while (!downloadDone);
            
        } else {
            NSLog(@"couldn't establish connection for: %@", [[self.urlRequest URL] absoluteString]);
            
            // Cleanup Operation so next one (if any) can run
            [self terminateOperation];
        }
    }
    else { // Operation has been cancelled, clean up
        [self terminateOperation];
    }
    
}

#pragma mark -
#pragma mark NSURLConnection Delegate methods
// NSURLConnectionDelegate method: handle the initial connection
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSHTTPURLResponse*)response {
    NSLog(@"%s: Received response!", __FUNCTION__);
}

// NSURLConnectionDelegate method: handle data being received during connection
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [downloadData appendData:data];
    NSLog(@"downloaded %d bytes", [data length]);
}

// NSURLConnectionDelegate method: What to do once request is completed
-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSLog(@"%s: Download finished! File: %@", __FUNCTION__, [[self.urlRequest URL] absoluteString]);
  
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString *targetPath;// = [docDir stringByAppendingPathComponent:downloadPath];
    BOOL isDir;
    
    // If target folder path doesn't exist, create it
    if (![fileManager fileExistsAtPath:[targetPath stringByDeletingLastPathComponent] isDirectory:&isDir]) {
        NSError *makeDirError = nil;
        [fileManager createDirectoryAtPath:[targetPath stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:&makeDirError];
        if (makeDirError != nil) {
            NSLog(@"MAKE DIR ERROR: %@", [makeDirError description]);
            [self terminateOperation];
        }
    }
    
    NSError *saveError = nil;
    //NSLog(@"downloadData: %@",downloadData);
    [downloadData writeToFile:targetPath options:NSDataWritingAtomic error:&saveError];
    if (saveError != nil) {
        NSLog(@"Download save failed! Error: %@", [saveError description]);
        [self terminateOperation];
    }
    else {
        NSLog(@"file has been saved!: %@", targetPath);
    }
    downloadDone = YES;
}

// NSURLConnectionDelegate method: Handle the connection failing
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"%s: File download failed! Error: %@", __FUNCTION__, [error description]);
    [self terminateOperation];
}

// Function to clean up the variables and mark Operation as finished
-(void) terminateOperation {
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    _isFinished = YES;
    _isExecuting = NO;
    downloadDone = YES;
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}


#pragma mark -
#pragma mark NSOperation state Delegate methods
// NSOperation state methods
- (BOOL)isConcurrent {
    return YES;
}
- (BOOL)isExecuting {
    return _isExecuting;
}
- (BOOL)isFinished {
    return _isFinished;
}
@end
