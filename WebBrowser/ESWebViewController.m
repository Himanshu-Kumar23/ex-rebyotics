//
//  ESWebViewController.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 9/19/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESWebViewController.h"

#import "ESWebLinkDownloadOperation.h"

#define ES_ALERTVIEW_REPLACE_DOWNLOAD 0x1

@interface NSURL (uriEquivalence)

- (BOOL)isEquivalent:(NSURL *)aURL;

@end

@interface ESWebViewController () <UIWebViewDelegate,UITextFieldDelegate, ESWebLinkDownloadOperationDelegate, UIAlertViewDelegate> {
    UIWebView *webView;
    UITextField *urlTextfield;
    UIToolbar *toolbar;
    
    NSMutableArray *downloadOperations;
    
    UIBarButtonItem *backButtonItem;
    UIBarButtonItem *forwardButtonItem;
    UIBarButtonItem *cancelButtonItem;
    
    UIBarButtonItem *doneButtonItem;
    UIBarButtonItem *flexibleButtonItem;
    UIBarButtonItem *downloadIndicatorItem;
    
    UIButton *reloadButton;
    UIButton *stopButton;
    
    NSURL *_urlPendingDownload;
    
    NSInteger _webViewLoadingCount;
    //UIAlertView *_alertView;

    UIBarButtonItem *textFieldBarButtonItem;
}


@end

@implementation ESWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView {
    [super loadView];
    
    webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    //    NSURL *path = [NSURL URLWithString:@"http://developer.apple.com/iphone/library/documentation/UIKit/Reference/UIWebView_Class/UIWebView_Class.pdf"];
    
    
    //    NSURL *targetURL = [NSURL fileURLWithPath:[path objectAtIndex:0]];
    NSURL *targetURL = [NSURL URLWithString:@"http://google.com"];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [webView loadRequest:request];
    webView.delegate = self;
    [self.view addSubview:webView];

    [self barButtons];
//    backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"\u2190" style:UIBarButtonItemStyleBordered target:self action:@selector(backAction)];
//    forwardButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"\u2192" style:UIBarButtonItemStyleBordered target:self action:@selector(forwardAction)];
    doneButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done " style:UIBarButtonItemStyleDone target:self action:@selector(goBackToPDFList:)];
    
    stopButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 28, 28)];
    [stopButton setTitle:@"\u2715" forState:UIControlStateNormal];
    [stopButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //    [stopButton setTitle:@"X" forState:UIControlStateNormal];
    [stopButton addTarget:self action:@selector(stopAction) forControlEvents:UIControlEventTouchUpInside];
    
    reloadButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 22, 28)];
    [reloadButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [reloadButton setTitle:@"\u21BB" forState:UIControlStateNormal];
    
    [reloadButton addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat navBarHeight = self.navigationController.navigationBar.frame.size.height;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        urlTextfield = [[UITextField alloc]initWithFrame:CGRectMake(0,7,self.view.bounds.size.width-200, navBarHeight - 14)];
    } else {
        urlTextfield = [[UITextField alloc]initWithFrame:CGRectMake(0, 0,self.navigationController.navigationBar.frame.size.width-100, navBarHeight - 14)];
        toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - navBarHeight-5, self.view.bounds.size.width, navBarHeight + 5)];
        toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight;
        cancelButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:urlTextfield action:@selector(resignFirstResponder)];
        flexibleButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

    }
    
    urlTextfield.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    urlTextfield.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    urlTextfield.backgroundColor = [UIColor whiteColor];
    urlTextfield.autocorrectionType = UITextAutocorrectionTypeNo;
    urlTextfield.borderStyle = UITextBorderStyleRoundedRect;
    urlTextfield.placeholder = @"Search or Enter address";
    urlTextfield.delegate = self;
    urlTextfield.font = [UIFont systemFontOfSize:14.f];
	urlTextfield.keyboardType = UIKeyboardTypeURL;
	urlTextfield.returnKeyType = UIReturnKeyGo;
    //    urlTextfield.autoresizingMask =  UIViewAutoresizingFlexibleWidth   |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
	urlTextfield.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    urlTextfield.rightView = reloadButton;
    
    textFieldBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:urlTextfield];

    [self setUpButtonItemsWithDownLoadNumber:0];
    
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    urlTextfield.leftView = activityIndicatorView;
    [activityIndicatorView startAnimating];
    urlTextfield.leftViewMode = UITextFieldViewModeNever;
}

-(void)setUpButtonItemsWithDownLoadNumber:(NSInteger)downLoadArrayCount{
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        self.navigationItem.titleView = urlTextfield;

        NSArray *items = [NSArray arrayWithObjects:doneButtonItem, backButtonItem, forwardButtonItem, nil];
        [self.navigationItem setLeftBarButtonItems:items];
        
    } else {
        [self.view addSubview:toolbar];
        NSArray *items = [NSArray arrayWithObjects:backButtonItem, forwardButtonItem, nil];
        [toolbar setItems:items];
        
        NSArray *a = [NSArray arrayWithObjects:doneButtonItem, textFieldBarButtonItem , nil];
        [self.navigationItem setLeftBarButtonItems:a];
        
        self.navigationItem.rightBarButtonItem = nil;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    downloadOperations = [NSMutableArray arrayWithCapacity:3];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //    [self hideDataProgressIndicatorForAttendee ];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBarButtonItems:) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void) updateBarButtonItems:(id) sender {
    doneButtonItem.title = [NSString stringWithFormat:@"Done(%lu)",(unsigned long)downloadOperations.count];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:doneButtonItem, backButtonItem, forwardButtonItem, nil];
    } else {
        self.navigationItem.leftBarButtonItem = doneButtonItem;
    }
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) webView:(UIWebView *)webView1 shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSString *lastComponent = [request.URL lastPathComponent];
    if ([lastComponent rangeOfString:@".pdf"].location == lastComponent.length - 4) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[request.URL lastPathComponent]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            _urlPendingDownload = request.URL;
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:nil
                                         message:@"A document with the same name exists already. Do you wish to replace it?"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* cancelButton = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction * action) {
                                           //Handle your yes please button action here
                                       }];
            
            UIAlertAction* replaceButton = [UIAlertAction
                                       actionWithTitle:@"Replace"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle your yes please button action here
                                           [self continueDownloadForURL:_urlPendingDownload];
                                           _urlPendingDownload = nil;
                                       }];

            
            [alert addAction:cancelButton];
            [alert addAction:replaceButton];
            
            [self presentViewController:alert animated:YES completion:nil];

            
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"A document with the same name exists already. Do you wish to replace it?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Replace", nil];
//            alertView.tag = ES_ALERTVIEW_REPLACE_DOWNLOAD;
//            [alertView show];
            return NO;
        }
        
        [self continueDownloadForURL:request.URL];
        
        return NO;
    }
    
    urlTextfield.text = [request.URL absoluteString];
    urlTextfield.leftViewMode = UITextFieldViewModeAlways;
    
    return YES;
}

- (void) continueDownloadForURL:(NSURL *)url {
    BOOL downloadRequired = YES;
    for (ESWebLinkDownloadOperation *operation in downloadOperations) {
        if ([operation.urlToDownload isEquivalent:url]) {
            downloadRequired = NO;
            if (operation.finished) {
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:nil
                                             message:@"The document you specified is downloaded already"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* okButton = [UIAlertAction
                                           actionWithTitle:@"OK"
                                           style:UIAlertActionStyleCancel
                                           handler:^(UIAlertAction * action) {
                                               //Handle your yes please button action here
                                           }];
                
                
                [alert addAction:okButton];
                
                [self presentViewController:alert animated:YES completion:nil];

                
                //[[[UIAlertView alloc] initWithTitle:nil message:@"The document you specified is downloaded already" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            } else {
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:nil
                                             message:@"This document is already being downloaded."
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* okButton = [UIAlertAction
                                           actionWithTitle:@"OK"
                                           style:UIAlertActionStyleCancel
                                           handler:^(UIAlertAction * action) {
                                               //Handle your yes please button action here
                                           }];
                
                
                [alert addAction:okButton];
                
                [self presentViewController:alert animated:YES completion:nil];
                
                //[[[UIAlertView alloc] initWithTitle:nil message:@"This document is already being downloaded." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            }
            break;
        }
    }
    if (downloadRequired) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"The document has started downloading. Go back to Documents screen to browse it."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
//        _alertView =
//        [[UIAlertView alloc] initWithTitle:nil message:@"The document has started downloading. Go back to Documents screen to browse it." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [_alertView show];
        urlTextfield.leftViewMode = UITextFieldViewModeAlways;
        [downloadOperations addObject:[ESWebLinkDownloadOperation downloadWithURL:url andDelegate:self]];
    }
}

//- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
//    if (_alertView == alertView) {
//        _alertView = nil;
//    }
//    if (alertView.cancelButtonIndex == buttonIndex) {
//        return;
//    }
//    if (alertView.tag == ES_ALERTVIEW_REPLACE_DOWNLOAD) {
//        [self continueDownloadForURL:_urlPendingDownload];
//        _urlPendingDownload = nil;
//    }
//}

- (void) didFinishDownloadingContentForDownloadOperation:(ESWebLinkDownloadOperation *)downloadOperation {
    //    [downloadOperations removeObject:downloadOperation];
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        [self updateBarButtonItems:nil];
        [self resetActivityIndicator];
    }
}

- (void) didFailDownloadingContentForDownloadOperation:(ESWebLinkDownloadOperation *)downloadOperation withError:(NSError *)error {

    [downloadOperations removeObject:downloadOperation];

    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        [self resetActivityIndicator];
        //[_alertView dismissWithClickedButtonIndex:_alertView.cancelButtonIndex animated:NO];
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[NSString stringWithFormat:@"Could not download document. Error: %@", error.localizedDescription]
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];

        
        //[[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"Could not download document. Error: %@", error.localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        if (self.navigationItem.rightBarButtonItem == nil) {
            self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:flexibleButtonItem,textFieldBarButtonItem,nil];
            self.navigationItem.rightBarButtonItem = cancelButtonItem;
        }
        
    }
    return YES;
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        [self setUpButtonItemsWithDownLoadNumber:0];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
	NSString *urlString = urlTextfield.text;
    
    if ([urlString rangeOfString:@"://"].location == NSNotFound) {
        urlString = [@"http://" stringByAppendingString:urlString];
    }
    
    if ([urlString rangeOfString:@"."].location == NSNotFound) {
        urlString = [@"https://www.google.com/search?q=" stringByAppendingString:urlTextfield.text];
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [webView loadRequest:request];
    
	[textField resignFirstResponder];
	return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
	[textField becomeFirstResponder];
	return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    textField.rightViewMode = UITextFieldViewModeNever;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
}

- (void) resetActivityIndicator {
    
    __block BOOL allOperationsFinished = YES;
    [downloadOperations enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (!((ESWebLinkDownloadOperation *)obj).finished) {
            allOperationsFinished = NO;
            *stop = YES;
        }
    }];
    if (!allOperationsFinished) {
        urlTextfield.leftViewMode = UITextFieldViewModeAlways;
    } else {
        _webViewLoadingCount = MAX(0, _webViewLoadingCount);
        if (_webViewLoadingCount > 0) {
            urlTextfield.leftViewMode = UITextFieldViewModeAlways;
            urlTextfield.rightView = stopButton;
        } else {
            urlTextfield.leftViewMode = UITextFieldViewModeNever;
            urlTextfield.rightView = reloadButton;
        }
        
    }
}

-(IBAction)goBackToPDFList:(id)sender{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)backAction {
	[webView goBack];
}
- (void)forwardAction {
	[webView goForward];
}
- (void)refreshAction {
	[webView reload];
}
- (void)stopAction {
	[webView stopLoading];
    [self resetActivityIndicator];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView_{
    
    _webViewLoadingCount--;
    
//    NSLog(@"Webview finished load %u",webView.loading);
    
    urlTextfield.text = [webView.request.URL description];
    urlTextfield.rightViewMode = UITextFieldViewModeAlways;
    
    [self resetActivityIndicator];
    
}

- (void) webView:(UIWebView *)webView_ didFailLoadWithError:(NSError *)error {
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Error"
                                 message:error.localizedDescription
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                               }];
    
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];

    
    //[[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    _webViewLoadingCount--;
//    NSLog(@"Webview failed load %u",webView.loading);
    [self resetActivityIndicator];
    urlTextfield.text = [webView.request.URL description];
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView_ {
    
    _webViewLoadingCount ++;
    [self resetActivityIndicator];
    
    urlTextfield.clearButtonMode = UITextFieldViewModeNever;
    urlTextfield.rightViewMode = UITextFieldViewModeAlways;
    urlTextfield.rightView = stopButton;
    
}

#pragma Orientations Methods

- (BOOL)shouldAutorotate{
    
    return YES;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}

- (void) dealloc {
    
    //FIXME: Show find out what happens to these downloads when view controller deallocates.
    for (ESWebLinkDownloadOperation *operation in downloadOperations) {
        operation.delegate = nil;
    }
    
    downloadOperations = nil;
}
-(void)barButtons{
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [backButton setBackgroundImage:[UIImage imageNamed:@"backward.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImage: [UIImage imageNamed:@"backward.png"] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 0, 30, 30);
    
    UIButton *forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [forwardButton setBackgroundImage:[UIImage imageNamed:@"forward.png"] forState:UIControlStateNormal];
    [forwardButton addTarget:self action:@selector(forwardAction) forControlEvents:UIControlEventTouchUpInside];
    
    forwardButton.frame = CGRectMake(0, 0, 30, 30);
    
    backButtonItem = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    
    forwardButtonItem = [[UIBarButtonItem alloc]initWithCustomView:forwardButton];

}
@end

@implementation NSURL (uriEquivalence)

- (BOOL)isEquivalent:(NSURL *)aURL {
    
    if ([self isEqual:aURL]) return YES;
    if ([[self scheme] caseInsensitiveCompare:[aURL scheme]] != NSOrderedSame) return NO;
    if ([[self host] caseInsensitiveCompare:[aURL host]] != NSOrderedSame) return NO;
    
    // NSURL path is smart about trimming trailing slashes
    // note case-sensitivty here
    if ([[self path] compare:[aURL path]] != NSOrderedSame) return NO;
    
    // at this point, we've established that the urls are equivalent according to the rfc
    // insofar as scheme, host, and paths match
    
    // according to rfc2616, port's can weakly match if one is missing and the
    // other is default for the scheme, but for now, let's insist on an explicit match
    if ([[self port] compare:[aURL port]] != NSOrderedSame) return NO;
    
    if ([[self query] compare:[aURL query]] != NSOrderedSame) return NO;
    
    // for things like user/pw, fragment, etc., seems sensible to be
    // permissive about these.  (plus, I'm tired :-))
    return YES;
}


@end
