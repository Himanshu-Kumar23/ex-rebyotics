//
//  ESWebLinkDownloadOperation.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 10/3/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESWebLinkDownloadOperation.h"

@implementation ESWebLinkDownloadOperation

//@synthesize urlToDownload = urlToDownload;
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL

{
//    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    if ([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]) {
        NSError *error = nil;
        
        BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                        
                                      forKey: NSURLIsExcludedFromBackupKey error: &error];
        
        if(!success){
            
            NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
            
        }
        return success;
    }else{
        return NO;
    }
    
}

- (void) download {
    bgTaskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        if (bgTaskId != UIBackgroundTaskInvalid) {
            [self endTask:NO];
        }
    }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSHTTPURLResponse *response;
        NSError *error = nil;
        NSData *fileData = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:_urlToDownload] returningResponse:&response error:&error];
        
        
        if (error || response.statusCode != 200) {
            
            bgTaskId = UIBackgroundTaskInvalid;
            [self endTask:YES];
            
            dispatch_async( dispatch_get_main_queue(), ^{
                [_delegate didFailDownloadingContentForDownloadOperation:self withError:error];
            });
            return;
        }
        
       
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
         NSURL * finalURL = [NSURL fileURLWithPath:documentsDirectory];
      
        BOOL  excludeBackupSuccessFul= [self addSkipBackupAttributeToItemAtURL:finalURL];
        
        if (!excludeBackupSuccessFul) {
            NSString *message = @"";
            if ([_urlToDownload lastPathComponent]) {
                
                message = [NSString stringWithFormat:@"We are unable to open %@ file at this moment.\n Please retry after sometime",[_urlToDownload lastPathComponent]];
            }else{
                message = @"We are unable to open the file  at this moment.\n Please retry after sometime";

            }
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:nil
                                         message:message
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"Dismiss"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle your yes please button action here
                                       }];
            
            
            [alert addAction:okButton];
            
            [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
            
            //[[[UIAlertView alloc]initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil]show];
            return ;
        }
        
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[_urlToDownload lastPathComponent]];
        
        //replace existing file if we have to.
        BOOL success = [fileData writeToFile:filePath atomically:YES];
        if (success) {
//            NSLog(@"%@ saved to disk successfully.", _urlToDownload.lastPathComponent);
        }
        
        if (![ESWebLinkDownloadOperation isPDF:filePath]) {
            bgTaskId = UIBackgroundTaskInvalid;
            [self endTask:YES];
            
            NSError *removeError = nil;
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:&removeError];
            
            dispatch_async( dispatch_get_main_queue(), ^{
                [_delegate didFailDownloadingContentForDownloadOperation:self withError:[NSError errorWithDomain:@"com.eiqservices" code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Not a valid PDF document",NSLocalizedDescriptionKey, nil]]];
            });

            return;
        }
        
        bgTaskId = UIBackgroundTaskInvalid;
        [self endTask:YES];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
                [[NSNotificationCenter defaultCenter] postNotificationName:ES_FILE_DOWNLOADED_NOTIFICATION object:nil];
            }
            [_delegate didFinishDownloadingContentForDownloadOperation:self];
        });
        
    });
}

- (void) endTask:(BOOL) success {
    [[UIApplication sharedApplication] endBackgroundTask:bgTaskId];
}

+ (ESWebLinkDownloadOperation *) downloadWithURL:(NSURL *) url andDelegate:(id<ESWebLinkDownloadOperationDelegate>) delegate {
    ESWebLinkDownloadOperation *operation = [[ESWebLinkDownloadOperation alloc] init];
    operation.urlToDownload = url;
    operation.delegate = delegate;
    [operation download];
    return operation;
}

- (BOOL) finished {
    return bgTaskId == UIBackgroundTaskInvalid;
}

+ (BOOL)isPDF:(NSString *)filePath
{
	BOOL state = NO;
    
	if (filePath != nil) // Must have a file path
	{
		const char *path = [filePath fileSystemRepresentation];
        
		int fd = open(path, O_RDONLY); // Open the file
        
		if (fd > 0) // We have a valid file descriptor
		{
			const char sig[1024]; // File signature buffer
            
			ssize_t len = read(fd, (void *)&sig, sizeof(sig));
            
			state = (strnstr(sig, "%PDF", len) != NULL) || (strnstr(sig, "%pdf", len) != NULL);
            
			close(fd); // Close the file
		}
	}
    
	return state;
}

@end
