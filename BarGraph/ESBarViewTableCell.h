//
//  ESBarViewTableCell.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/15/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESBarView.h"

@interface ESBarViewTableCell : UITableViewCell {
    ESBarView *barView;
    ESBarViewDataSourceImpl *datasource;
}

@property(nonatomic, readonly) ESBarViewDataSourceImpl *datasource;
@property(nonatomic, readonly) ESBarView *barView;

- (id) initWithIndexSetArray:(NSArray *) array numberOfBars:(NSUInteger) barsCount;

@end
