//
//  ESBarViewTableCell.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/15/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESBarViewTableCell.h"

@implementation ESBarViewTableCell

@synthesize datasource = datasource;
@synthesize barView = barView;

- (id) initWithIndexSetArray:(NSArray *) array numberOfBars:(NSUInteger) barsCount {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    if (self) {
        barView = [[ESBarView alloc] initWithFrame:self.contentView.bounds];
        barView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:barView];
        
        datasource = [[ESBarViewDataSourceImpl alloc] initWithIndexSets:array andNumberOfBars:barsCount];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        barView.dataSource = datasource;
    }
    return self;
}

@end
