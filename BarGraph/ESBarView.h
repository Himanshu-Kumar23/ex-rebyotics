//
//  ESBarView.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/13/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ESBarView;

@protocol ESBarViewDataSource <NSObject>

- (NSUInteger) numberOfBarsInBarView:(ESBarView *) barView;
- (CGFloat) barView:(ESBarView *) barView fractionOfBarVisibleAtIndex:(NSUInteger) index;
- (NSString *) barView:(ESBarView *) barView titleForLabelBelowBarAtIndex:(NSUInteger) index;
- (NSString *) barView:(ESBarView *) barView titleForLabelAboveBarAtIndex:(NSUInteger) index;

@end

@interface ESBarViewDataSourceImpl : NSObject<ESBarViewDataSource>

//- (void) addIndexSets:(NSArray *) indexSets;
- (void) setIndexSetsArray:(NSArray *) indexSets;
- (id) initWithIndexSets:(NSArray *)indexSets andNumberOfBars:(NSUInteger) noOfBars;

@end

@interface ESBarView : UIView

@property(nonatomic, unsafe_unretained) id<ESBarViewDataSource> dataSource;

- (void) reloadData;

@end