//
//  ESBarView.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/13/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESBarView.h"

@interface ESBarViewBar : UIView {
    UILabel *topLabel;
    UILabel *bottomLabel;
    UIView *bar;
}

@property (nonatomic, strong) NSString *bottomString;
@property (nonatomic, strong) NSString *topString;
@property (nonatomic) CGFloat fraction;

@end

#define ES_HEIGHT_OF_BARVIEWBAR_LABEL 18.f
@implementation ESBarViewBar

- (id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
//        self.backgroundColor = [UIColor cyanColor];
        topLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        topLabel.textAlignment = ESTextAlignmentCenter;
//        topLabel.textColor = [UIColor whiteColor];
        topLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:topLabel];
        
        bottomLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        bottomLabel.textAlignment = ESTextAlignmentCenter;
        bottomLabel.textColor = [UIColor whiteColor];
        bottomLabel.backgroundColor = [UIColor blueColor];
        [self addSubview:bottomLabel];

        bar = [[UIView alloc] initWithFrame:CGRectZero];
        bar.backgroundColor = [UIColor darkGrayColor];
        [self addSubview:bar];
    }
    return self;
}

- (void) layoutSubviews {
//    [UIView animateWithDuration:0.3f animations:^{
        bottomLabel.frame = CGRectMake(0, self.bounds.size.height - ES_HEIGHT_OF_BARVIEWBAR_LABEL, self.bounds.size.width, ES_HEIGHT_OF_BARVIEWBAR_LABEL);
        
        bar.frame = CGRectMake(0, ES_HEIGHT_OF_BARVIEWBAR_LABEL + (self.bounds.size.height - 2 * ES_HEIGHT_OF_BARVIEWBAR_LABEL) * (1 -_fraction), self.bounds.size.width , (self.bounds.size.height - 2 * ES_HEIGHT_OF_BARVIEWBAR_LABEL) * _fraction);
        
        topLabel.frame = CGRectMake(0, bar.frame.origin.y - ES_HEIGHT_OF_BARVIEWBAR_LABEL, self.bounds.size.width, ES_HEIGHT_OF_BARVIEWBAR_LABEL);        
//    }];
}

- (void) setBottomString:(NSString *)bottomString {
    [bottomLabel setText:bottomString];
    _bottomString = bottomString;
}

- (void) setTopString:(NSString *)topString {
    _topString = topString;
    [topLabel setText:topString];
}

- (void) setFraction:(CGFloat)fraction {
    _fraction = fraction;
    [self setNeedsLayout];
}

- (CGSize) sizeThatFits:(CGSize)size {
    return CGSizeMake(
                      MAX([bottomLabel sizeThatFits:size].width, [topLabel sizeThatFits:size].width),
                      size.height);
}

@end

@implementation ESBarView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
//        self.backgroundColor = [UIColor grayColor];
    }
    return self;
}

- (void) setDataSource:(id<ESBarViewDataSource>)dataSource {
    _dataSource = dataSource;
    [self reloadData];
}

- (void) reloadData {
    while (self.subviews.count > 0) {
        [[self.subviews objectAtIndex:0] removeFromSuperview];
    }
    
    NSInteger noOfBars = [_dataSource numberOfBarsInBarView:self];
    for(int i=0; i<noOfBars; i++) {
        ESBarViewBar *barViewBar = [[ESBarViewBar alloc] initWithFrame:self.frame];
        barViewBar.topString = [_dataSource barView:self titleForLabelAboveBarAtIndex:i];
        barViewBar.bottomString = [_dataSource barView:self titleForLabelBelowBarAtIndex:i];
        barViewBar.fraction = [_dataSource barView:self fractionOfBarVisibleAtIndex:i];
        [self addSubview:barViewBar];
    }
}

- (void) layoutSubviews {
    CGFloat width = 100.f;
    if (self.subviews.count == 0) {
        return;
    }
    if (width * self.subviews.count > (self.bounds.size.width - 20.f) ) {
        width = (self.bounds.size.width - (self.subviews.count + 1) * 20) / self.subviews.count;
    }

    for (int i=0; i<self.subviews.count; i++) {
        ESBarViewBar *barViewBar = (ESBarViewBar *)[self.subviews objectAtIndex:i];
        
        if (width == 100.f) {
            barViewBar.frame = CGRectMake(i*100+20, 0, 80, self.bounds.size.height);
        } else {
            barViewBar.frame = CGRectMake((i+1)*(width+20) - width, 0, width, self.bounds.size.height);
        }
    }
}

@end

typedef NS_ENUM (NSInteger,ESBarViewOptionType)  {
    ESBarViewOptionTypeSingleChoice = 0,
    ESBarViewOptionTypeMultipleChoice = 1,
};

@interface ESBarViewDataSourceImpl ()

@property(nonatomic, strong) NSMutableArray *indexSets;
@property(nonatomic) NSUInteger numberOfBars;

@end

@implementation ESBarViewDataSourceImpl

- (id) initWithIndexSets:(NSArray *)indexSets andNumberOfBars:(NSUInteger) noOfBars{
    
    self = [super init];
    if (self) {
        self.numberOfBars = noOfBars;
        self.indexSets = [NSMutableArray arrayWithArray:indexSets];
    }
    return self;
}

- (NSUInteger) numberOfBarsInBarView:(ESBarView *) barView {
    return _numberOfBars;
}

- (CGFloat) barView:(ESBarView *) barView fractionOfBarVisibleAtIndex:(NSUInteger) index {
    if (_indexSets.count == 0) {
        return 0.f;
    }
    __block int count = 0;
    for (NSIndexSet *indexSet in _indexSets) {
        [indexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            if (idx == index) {
                count++;
                *stop = YES;
            }
        }];
    }
    return 1.f * count/_indexSets.count;
}

- (NSString *) barView:(ESBarView *) barView titleForLabelBelowBarAtIndex:(NSUInteger) index {
    return [NSString stringWithFormat:@"Option %tu",1 + index];
}

- (NSString *) barView:(ESBarView *) barView titleForLabelAboveBarAtIndex:(NSUInteger) index {
    return [NSString stringWithFormat:@"%.f%%",100.f * [self barView:barView fractionOfBarVisibleAtIndex:index]];
}

//- (void) addIndexSets:(NSArray *)indexSets {
//    [self.indexSets addObjectsFromArray:indexSets];
//}

- (void) setIndexSetsArray:(NSArray *)indexSets {
    _indexSets = [NSMutableArray arrayWithArray:indexSets];
}

@end
