//
//  WAAttendeesViewController.m
//  AutoConnect
//
//  Created by Rajiv Narayana Singaseni on 8/26/13.
//  Copyright (c) 2013 WebileApps. All rights reserved.
//

#import "ESAttendeesViewController.h"

@interface ESAttendeesViewController ()



@end

@implementation ESAttendeesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *disconnectButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"End Session" style:UIBarButtonItemStylePlain target:self action:@selector(promptDisconnect:)];
    self.navigationItem.rightBarButtonItem = disconnectButtonItem;
    
    UIBarButtonItem *cancelButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(cancelAttendeesVC:)];
    self.navigationItem.leftBarButtonItem = cancelButtonItem;
    
    self.title = @"Attendees";
}

- (void) promptDisconnect:(id) sender {
    [self.delegate attendeesViewControllerPromptDisconnect:self];
}

- (void) cancelAttendeesVC:(id) sender {
    [self.delegate attendeesViewControllerCancelled:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.multipeerManager.connectedPeers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    //NSArray *peers = [_session peersWithConnectionState:GKPeerStateConnected];
    cell.textLabel.text = ((MCPeerID *)[self.multipeerManager.connectedPeers objectAtIndex:indexPath.row]).displayName;

    return cell;
}
#pragma Orientations Methods

- (BOOL)shouldAutorotate
{
    return YES;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}

@end
