//
//  WAViewController.h
//  AutoConnect
//
//  Created by Rajiv Narayana Singaseni on 8/26/13.
//  Copyright (c) 2013 WebileApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESAttendeesViewController.h"
#import <GameKit/GameKit.h>

#import "ESPDFDocumentViewController.h"
#import "DetailerReaderViewController.h"

#define IPROJECTOR_SESSION_ID @"iProjector"
//#define ACTION_PROMPT_CONNECT 10
#define ACTION_PROMPT_DISCONNECT 11
#define X_PRESENTER_APP_ID     915200116

#define UNAVAILABLE_FEATURE_ALERT_TAG    1030

#define RENAME_ALERT_TAG                 1050

@interface ESSessionBaseViewController : UIViewController <UIActionSheetDelegate, ESAttendeesViewControllerDelegate, UINavigationControllerDelegate, ESPDFViewControllerSessionDataDelegate,UIAlertViewDelegate,ReaderViewControllerDelegate, ESMultipeerManagerDelegate> {
    UIBarButtonItem *connectButtonItem;
    UIBarButtonItem *userRoleButton;
    //GKSession *gkSession;
    ESAttendeesViewController *attendeesViewController;
    UIToolbar *toolbar;
    MCPeerID *presenterPeerId;
    UIActivityIndicatorView *activityIndicatorView;
    UILabel *activityLabel;
    UIProgressView *progressView;
    
    NSString *openedFileName;
    NSDate *openedFileLastModifiedAt;
    ESPDFDocumentViewController *readerViewController;
    BOOL hideUserRoleButton;
    NSString *connectButtonItemTitle;
}
@property (strong, nonatomic) UIWindow *window;
;

- (NSArray *) toolbarButtonItems;
- (void) showLoadingIndictorWithMessage:(NSString *) message;
- (void) hideLoadingIndicator;

- (void) resetData;
-(void) openFileName:(NSString *) fileName;
-(void) openFileName:(NSString *) fileName withMode:(NSString *)mode;
//- (void) transferFileWithFileName:(NSString *)fileName;
- (void) transferPresentationWithFileName:(NSString *)fileName toPeerWithId:(MCPeerID *) peerId;
- (void) transferPresentationWithName:(NSString *) presentationName toPeers:(NSArray *) peers;


- (void) sendPresentationInfoRequestToAllAttendeesForFile:(NSString *) fileName;

- (void) createSession;
//- (void) receiveData:(NSData *)data fromPeer:(NSString *) peer inSession:(GKSession *) session context:(NSString *) context ;
//- (void)manager:(ESMultipeerManager *)manager didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID;

-(void)sendPacketDataToAllPeers:(NSData *)packetData;
-(void)sendPacketData:(NSData *)packetData toPeerWithId:(MCPeerID *) peerId;
-(void)sendPacketData:(NSData *)packetData toPeers:(NSArray *) peers;

-(UIBarButtonItem *)importBarButtonItem;
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
@end
