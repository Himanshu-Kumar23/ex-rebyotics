//
//  WAAttendeesViewController.h
//  AutoConnect
//
//  Created by Rajiv Narayana Singaseni on 8/26/13.
//  Copyright (c) 2013 WebileApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>
#import "ESMultipeerManager.h"

@class ESAttendeesViewController;

@protocol ESAttendeesViewControllerDelegate <NSObject>

- (void) attendeesViewControllerPromptDisconnect:(ESAttendeesViewController *) attendeesViewController;
- (void) attendeesViewControllerCancelled:(ESAttendeesViewController *) attendeesViewController;

@end

//A view controller which displays the connected attendees in a presentation
@interface ESAttendeesViewController : UITableViewController

@property (nonatomic, strong) ESMultipeerManager *multipeerManager;
//@property (nonatomic, strong) GKSession *session;
@property (nonatomic, unsafe_unretained) id<ESAttendeesViewControllerDelegate> delegate;

@end
