//
//  WAViewController.m
//  AutoConnect
//
//  Created by Rajiv Narayana Singaseni on 8/26/13.
//  Copyright (c) 2013 WebileApps. All rights reserved.
//

#import "ESSessionBaseViewController.h"
#import "ESSettingListViewController.h"
#import "ESAbstractPacket.h"
#import "ESPDFPacket.h"

#import "ReaderDocument.h"
#import "ESPresentationInfoRequestPacket.h"
#import "ESPresentationInfoResponsePacket.h"
#import "ESEndPresentationPacket.h"
#import "CircleMenuViewController.h"
#import "Constants.h"

#import "ESVideoParser.h"

#import "ESWebLinkDownloadOperation.h"
#import "DetailerReaderViewController.h"

#import "ESMultipeerManager.h"
#import "ESAppDelegate.h"

#define KEY_FILES_DOWNLOADED        @"kESFDownloadedFiles"
#define PRESENTER @"Presenter"
#define ATTENDEE @"Attendee"

#if DEBUG
#define ES_CACHE_DURATION           3600 / 4 //15 minutes in debug.
#else
#define ES_CACHE_DURATION           3600 * 24 * 2 //2 days in production.
#endif


static NSUInteger DefaultChunkLength = 12800;

@protocol ESTimerTargetDelegate <NSObject>

- (void) timerFired:(NSTimer *) timer;

@end

@interface ESTimerTarget : NSObject

@property(nonatomic, weak) id<ESTimerTargetDelegate> delegate;

- (void) timerFired:(id) sender;

@end

@implementation ESTimerTarget

- (void) timerFired:(id)sender {
    [_delegate timerFired:sender];
}

@end

@interface ESSessionBaseViewController () <ESTimerTargetDelegate> {
    
    NSMutableData *receivedData;
    NSInteger totalData;
    UILabel *presentationLabel;
    UIAlertController *disconnectActionSheet;
    
    NSMutableArray *peersWhoNeedPresentationData;
    NSArray *peersWeJustSentPresentationData;
    
    NSTimer *lockTimer; //a one second timer before the transfer starts.
    NSTimer *resetTimer; // a 30 second timer before we resend if no ack.
    
    ESPresentationInfoRequestPacket *presentationInfoPacket; //packet containing the presentation info.
    NSInteger noOfFilesReceivedFromPresenter;
    
    ESAppDelegate *appDelegate;
    
    Boolean shouldDisconnect;
}

@property (nonatomic, strong) ESMultipeerManager *multipeerManager;

@end

@implementation ESSessionBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (ESAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.multipeerManager = [[ESMultipeerManager alloc] initWithServiceType:@"iProjector"];
    self.multipeerManager.delegate = self;
    
    self.title = @"Documents";
    hideUserRoleButton = NO;
    connectButtonItemTitle = @"Connect";
    
    shouldDisconnect = NO;
    
    toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 44.f, self.view.bounds.size.width, 44.f)];
    toolbar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    
    toolbar.items = [self toolbarButtonItems];
    
    [self.view addSubview:toolbar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetData) name:ES_FILE_DOWNLOADED_NOTIFICATION object:nil];
}


- (void) removeStalePDFs:(id) sender {
    [self resetData];
#if X_PRESENTER
    return;
#endif
    
    //    NSLog(@"Removing stale PDFs");
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictionary = [userDefaults dictionaryForKey:KEY_FILES_DOWNLOADED];
    NSMutableDictionary *downloadCacheDictionary = [NSMutableDictionary dictionaryWithCapacity:2];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *keys = [dictionary allKeys];
    for (NSString *fileName in keys) {
        
        NSDate *dateOfDownload = (NSDate *)[dictionary objectForKey:fileName];
        
        if ([[NSDate date] timeIntervalSinceDate:dateOfDownload] > ES_CACHE_DURATION) {
            //try to delete the file from filesystem.
            NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:fileName];
            if ([fileManager fileExistsAtPath:filePath]) {
                NSError *error = nil;
                BOOL deleted = [fileManager removeItemAtPath:filePath error:&error];
                if (deleted) {
                    //                    NSLog(@"Deleted file \"%@\" successfully from cache.", fileName);
                } else {
                    //                    NSLog(@"Could not delete file: \"%@\", %@",fileName, [error localizedDescription]);
                }
            }
            
        } else {
            [downloadCacheDictionary setObject:dateOfDownload forKey:fileName];
        }
    }
    [userDefaults setValue:downloadCacheDictionary forKey:KEY_FILES_DOWNLOADED];
    [self resetData];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    readerViewController = nil;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (toolbar) {
        toolbar.items = [self toolbarButtonItems];
    }
    
    //    [self hideDataProgressIndicatorForAttendee ];
    [self removeStalePDFs:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeStalePDFs:) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}
-(void)hideUserRoleButtonFromToolBar:(BOOL)hide{
    
    if (hide)
        hideUserRoleButton = YES;
    else
        hideUserRoleButton = NO;
    if (toolbar)
        toolbar.items = [self toolbarButtonItems];
}
- (void) connectToPresentation:(id) sender {
    
    if([connectButtonItemTitle isEqualToString:@"Cancel"]) {
        shouldDisconnect = YES;
    } else if([connectButtonItemTitle isEqualToString:@"Disconnect"]) {
        shouldDisconnect = YES;
    } else {
        shouldDisconnect = NO;
    }
    
    [self hideUserRoleButtonFromToolBar:YES];
    if (self.multipeerManager.connected) {
        if ([appDelegate.role isEqualToString:ATTENDEE]) {
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Disconnect from Presenter?"
                                         message:nil
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* cancelButton = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction * action) {
                                           //Handle your yes please button action here
                                           connectButtonItem.enabled = YES;
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                       }];
            
            UIAlertAction* disconnectButton = [UIAlertAction
                                       actionWithTitle:@"Disconnect"
                                       style:UIAlertActionStyleDestructive
                                       handler:^(UIAlertAction * action) {
                                           //Handle your yes please button action here
                                           connectButtonItem.enabled = YES;
                                           
                                           [self removeGettingReadyForPresentationView];
                                           
                                           connectButtonItemTitle = @"Connect";
                                           [self hideUserRoleButtonFromToolBar:NO];
                                           
                                           appDelegate.sessionState = @"0";
                                           
                                           [self.multipeerManager leaveSession];
                                           
                                           //[gkSession disconnectFromAllPeers];
                                           //gkSession.delegate = nil;
                                           presenterPeerId = nil;
                                           //gkSession = nil;
                                           [self hideLoadingIndicator];
                                           [self hideDataProgressIndicatorForAttendee];
                                       }];
            
            [alert addAction:cancelButton];
            [alert addAction:disconnectButton];
            
            [alert setModalPresentationStyle:UIModalPresentationPopover];
            
//            UIPopoverPresentationController *popPresenter = [alert
//                                                             popoverPresentationController];
            //popPresenter.sourceView = connectButtonItem;
            //popPresenter.sourceRect = connectButtonItem.bounds;
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
//            disconnectActionSheet = [[UIActionSheet alloc] initWithTitle:@"Disconnect from Presenter?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Disconnect" otherButtonTitles: nil];
//            disconnectActionSheet.tag = ACTION_PROMPT_DISCONNECT;
//            [disconnectActionSheet showFromBarButtonItem:connectButtonItem animated:YES];
            connectButtonItem.enabled = NO; //for disallowing multiple popups on iPad.
        } else {
            attendeesViewController = [[ESAttendeesViewController alloc] initWithStyle:UITableViewStyleGrouped];
            attendeesViewController.delegate = self;
            //attendeesViewController.session = gkSession;
            attendeesViewController.multipeerManager = self.multipeerManager;
            UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:attendeesViewController];
            navController.modalPresentationStyle = UIModalPresentationFormSheet;
            navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            [self presentViewController:navController animated:YES completion:nil];
            //            [self.navigationController pushViewController:attendeesViewController animated:YES];
        }
    } else {
        
#if X_AUDIENCE
        [self connectAsAttendee];
#else
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSInteger role = [[userDefaults valueForKey:KEY_USER_ROLE] integerValue];
        if (role == ESUserRoleAttendee) {
            [self connectAsAttendee];
        } else {
            [self connectAsPresenter];
        }
#endif
        
    }
}

//- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
//    connectButtonItem.enabled = YES; //for disallowing multiple popups on iPad.
//    if (buttonIndex == actionSheet.cancelButtonIndex) {
//        //        NSLog(@"Action sheet cancelled");
//        return;
//    }
//    
//    if (actionSheet.tag == ACTION_PROMPT_DISCONNECT) {
//        
//        if (actionSheet.destructiveButtonIndex == buttonIndex) {
//            [self removeGettingReadyForPresentationView];
//            
//            connectButtonItemTitle = @"Connect";
//            [self hideUserRoleButtonFromToolBar:NO];
//            
//            appDelegate.sessionState = @"0";
//            
//            [self.multipeerManager leaveSession];
//            
//            //[gkSession disconnectFromAllPeers];
//            //gkSession.delegate = nil;
//            presenterPeerId = nil;
//            //gkSession = nil;
//            [self hideLoadingIndicator];
//            [self hideDataProgressIndicatorForAttendee];
//        }
//    }
//    
//}

#if !X_AUDIENCE
- (void) connectAsPresenter {
    
    appDelegate.role = PRESENTER;
    appDelegate.sessionState = @"1";
    
    [self.multipeerManager joinSessionAsPresenter];
    
    
    //    gkSession = [[GKSession alloc] initWithSessionID:IPROJECTOR_SESSION_ID displayName:@"Presenter" sessionMode:GKSessionModeServer];
    //    [gkSession setDataReceiveHandler:self withContext:nil];
    self.navigationController.delegate = self;
    //    gkSession.delegate = self;
    //    gkSession.available = YES;
    //    connectButtonItem.title = @"0 Connected";
    connectButtonItemTitle =  @"0 Connected";
    [self hideUserRoleButtonFromToolBar:YES];
    
    [self showLoadingIndictorWithMessage:@"Waiting for attendees..."];
    //NSAssert(gkSession != nil, @"Session should not be nil");
}
#endif

- (void) showLoadingIndictorWithMessage:(NSString *) message {
    
    if (activityIndicatorView == nil) {
        activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight;
        [activityIndicatorView startAnimating];
        activityIndicatorView.center = CGPointMake(activityIndicatorView.frame.size.width/2 + 3, toolbar.frame.size.height/2);
        [toolbar addSubview:activityIndicatorView];
        
    }
    if (activityLabel == nil) {
        activityLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        activityLabel.font = [UIFont systemFontOfSize:11.f];
        activityLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight;
        activityLabel.textColor = [UIColor blackColor];
        activityLabel.text = message?:@"Loading...";
        activityLabel.backgroundColor = [UIColor clearColor];
        CGSize size = [activityLabel sizeThatFits:toolbar.frame.size];
        activityLabel .autoresizingMask =
        UIViewAutoresizingFlexibleRightMargin |
        UIViewAutoresizingFlexibleTopMargin |
        UIViewAutoresizingFlexibleHeight |
        UIViewAutoresizingFlexibleWidth;
        
        activityLabel.frame = CGRectMake(activityIndicatorView.frame.size.width + 6, toolbar.frame.size.height/2 - 10, size.width, 20);
        [toolbar addSubview:activityLabel];
        
    }
}

-(void)showDataProgressIndictorForAttendee{
    if (activityIndicatorView == nil) {
        
        activityIndicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityIndicatorView.frame = CGRectMake(0, 0, 20, 20);
        activityIndicatorView.center = CGPointMake(activityIndicatorView.frame.size.width/2 + 3, toolbar.frame.size.height/2);
        [activityIndicatorView startAnimating];
        [toolbar addSubview: activityIndicatorView];
        
    }
    if (progressView == nil) {
        progressView = [[UIProgressView alloc]initWithProgressViewStyle:UIProgressViewStyleDefault];
        progressView.frame = CGRectMake(activityIndicatorView.frame.origin.x+activityIndicatorView.frame.size.width +3, toolbar.frame.size.height/2-5 , 125, 12.f);
        
        progressView .autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |
        UIViewAutoresizingFlexibleRightMargin |
        UIViewAutoresizingFlexibleTopMargin |
        UIViewAutoresizingFlexibleBottomMargin |
        UIViewAutoresizingFlexibleHeight |
        UIViewAutoresizingFlexibleWidth;
        [toolbar addSubview:progressView];
    }
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter) {
        progressView.hidden = YES;
    }else{
        progressView.hidden = NO;
    }
}

-(void)hideDataProgressIndicatorForAttendee{
    receivedData = nil;
    
    [activityIndicatorView stopAnimating];
    progressView.hidden = YES;
    activityIndicatorView.hidden = YES;
    [progressView removeFromSuperview];
    [activityIndicatorView removeFromSuperview];
    progressView = nil;
    activityIndicatorView = nil;
    if (toolbar) {
        toolbar.items = [self toolbarButtonItems];
    }
}

- (void) hideLoadingIndicator {
    [activityIndicatorView stopAnimating];
    [activityIndicatorView removeFromSuperview];
    [activityLabel removeFromSuperview];
    activityIndicatorView = nil;
    activityLabel = nil;
    if (toolbar) {
        toolbar.items = [self toolbarButtonItems];
    }
}

//- (void) session:(GKSession *)session didReceiveConnectionRequestFromPeer:(NSString *)peerID {
//    if (session.sessionMode == GKSessionModeServer) {
//        NSError *error = nil;
//        BOOL success = [session acceptConnectionFromPeer:peerID error:&error];
//        if (!success) {
//            //            NSLog(@"Cannot accept connection from peer, Error: %@",[error localizedDescription]);
//        }
//    } else {
//        //We don't get connection request from server.
//        //        NSLog(@"Client got a connection request");
//    }
//}
//
//- (void)session:(GKSession *)session peer:(NSString *)peerID didChangeState:(GKPeerConnectionState)state {
//
//    if (attendeesViewController) {
//        [attendeesViewController.tableView reloadData];
//    }
//
//    if (session.sessionMode == GKSessionModeClient) {
//        //Don't care about sessions which are not presenter.
//
//
//        if (!presenterPeerId && (state == GKPeerStateUnavailable || state == GKPeerStateDisconnected)) {
//            return;
//        }
//
//        //        NSLog(@"Client: %@ changed state %d",[session displayNameForPeer:peerID],state);
//        if ([[session displayNameForPeer:peerID] isEqualToString:@"Presenter"] && state == GKPeerStateAvailable && presenterPeerId == nil) {
//            //            NSLog(@"Client: Presenter found.. Attempting to connect..");
//            [session connectToPeer:peerID withTimeout:25];
//        } else if (state == GKPeerStateConnected) {
//            [self addGettingReadyForPresentationView ];
//            gkSession.available = NO;
//            connectButtonItemTitle = @"Disconnect";
//            [self hideUserRoleButtonFromToolBar:YES];
//            [self showToastWithText:@"Connected to session"];
//            presenterPeerId = peerID;
//            [self hideLoadingIndicator];
//            [self createSession];
//        } else if (state == GKPeerStateDisconnected) {
//            [self removeGettingReadyForPresentationView];
//            [self hideDataProgressIndicatorForAttendee];
//            connectButtonItemTitle = @"Connect";
//            [self hideUserRoleButtonFromToolBar:NO];
//            [gkSession disconnectFromAllPeers];
//            if (readerViewController) {
//                [[[UIAlertView alloc] initWithTitle:nil message:@"Presentator disconnected from presentation" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
//                [self.navigationController popToRootViewControllerAnimated:YES];
//                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//            }
//            gkSession = nil;
//            presenterPeerId = nil;
//            [self showToastWithText:[NSString stringWithFormat:@"%@ left the session.",[session displayNameForPeer:peerID]]];
//        }
//    } else {
//        //        NSLog(@"Client: %@ changed state %d",[session displayNameForPeer:peerID],state);
//        if (state == GKPeerStateConnected) {
//            [self showToastWithText:[NSString stringWithFormat:@"%@ joined the session.",[session displayNameForPeer:peerID]]];
//            //            [self createSession];
//
//            [self makeAttendeeJoinPresentationForPeerId:peerID];
//
//        } else if (state == GKPeerStateDisconnected ) {
//            [self showToastWithText:[NSString stringWithFormat:@"%@ left the session.",[session displayNameForPeer:peerID]]];
//        }
//        connectButtonItemTitle =  [NSString stringWithFormat:@"%lu Connected",(unsigned long)[session peersWithConnectionState:GKPeerStateConnected].count];
//        [self hideUserRoleButtonFromToolBar:YES];
//        if ([session peersWithConnectionState:GKPeerStateConnected].count > 0) {
//            [self hideLoadingIndicator];
//        }
//
//
//    }
//}
//
//- (void) session:(GKSession *)session connectionWithPeerFailed:(NSString *)peerID withError:(NSError *)error {
//    if (session.sessionMode == GKSessionModeClient) {
//        session.available = YES;
//        connectButtonItem.enabled = YES;
//        connectButtonItemTitle = @"Connect";
//        [self hideUserRoleButtonFromToolBar:NO];
//        gkSession = nil;
//    }
//}
//
//- (void)session:(GKSession *)session didFailWithError:(NSError *)error {
//    //    NSLog(@"You cannot be connected to the presentation. Please try again. Error: %@",[error localizedDescription]);
//    if (error.code == GKSessionCannotEnableError) {
//        [[[UIAlertView alloc] initWithTitle:@"Cannot connect" message:@"Please turn on Bluetooth or WiFi to connect with other devices." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
//    } else if ([error.domain isEqualToString:GKSessionErrorDomain]) {
//        [[[UIAlertView alloc] initWithTitle:nil message:error.description delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
//    }
//    connectButtonItemTitle = @"Cancel";
//    [self hideUserRoleButtonFromToolBar:YES];
//
//    connectButtonItem.enabled = YES;
//    gkSession = nil;
//}

- (void) connectAsAttendee {
    
    if(shouldDisconnect) {
        
        [self removeGettingReadyForPresentationView];
        [self hideDataProgressIndicatorForAttendee];
        connectButtonItemTitle = @"Connect";
        [self hideLoadingIndicator];
        [self hideUserRoleButtonFromToolBar:NO];
        
        appDelegate.sessionState = @"0";
        
        [self.multipeerManager leaveSession];
        
        return;
    }
    
    appDelegate.role = ATTENDEE;
    appDelegate.sessionState = @"1";
    
    [self.multipeerManager joinSessionAsAttendee];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *attendeeName;
    
    if ([[userDefault valueForKey:KEY_USE_DEFAULT_USERNAME] boolValue] == YES) {
        attendeeName = [UIDevice currentDevice].name;
    }else{
        attendeeName = [userDefault valueForKey:KEY_CUSTOM_USERNAME];
    }
    //    gkSession = [[GKSession alloc] initWithSessionID:IPROJECTOR_SESSION_ID displayName: attendeeName sessionMode:GKSessionModeClient];
    //    gkSession.delegate = self;
    //    gkSession.available = YES;
    connectButtonItemTitle = @"Cancel";
    [self hideUserRoleButtonFromToolBar:YES];
    
    [self showLoadingIndictorWithMessage:@"Looking for presenter..."];
    //NSAssert(gkSession != nil, @"Attendee session should not be nil");
}

- (void) attendeesViewControllerCancelled:(ESAttendeesViewController *)attendeesViewController_ {
    [self dismissViewControllerAnimated:YES completion:nil];
    attendeesViewController = nil;
}

- (void) attendeesViewControllerPromptDisconnect:(ESAttendeesViewController *)attendeesViewController_ {
    //don't prompt. just disconnect.
    //    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    attendeesViewController = nil;
    
    appDelegate.sessionState = @"0";
    
    [self.multipeerManager leaveSession];
    
    [self hideLoadingIndicator];
    
    connectButtonItemTitle = @"Connect";
    [self hideUserRoleButtonFromToolBar:NO];
    
}

-(void) showToastWithText:(NSString *)text{
    UILabel *toastLabel = [[UILabel alloc]initWithFrame:CGRectMake(toolbar.frame.origin.x+20, toolbar.frame.origin.y - toolbar.frame.size.height + 20, 300, 40)];
    toastLabel.backgroundColor = [UIColor blackColor];
    toastLabel.textColor = [UIColor whiteColor];
    toastLabel.font = [UIFont systemFontOfSize:14.f];
    toastLabel.text = text;
    [toastLabel sizeToFit];
    [self.view addSubview:toastLabel];
    [UIView animateWithDuration:3.5f animations:^{
        toastLabel.center = CGPointMake(toastLabel.center.x, toastLabel.center.y - 100);
        toastLabel.alpha = 0.0;
        userRoleButton.enabled = NO;
    } completion:^(BOOL finished) {
        userRoleButton.enabled = YES;
        [toastLabel removeFromSuperview];
    }];
}

- (void)settingButtonTouchUpInside:(id) sender
{
    ESSettingListViewController *settingListViewController = [[ESSettingListViewController alloc] initWithNibName:nil bundle:nil];
    settingListViewController.isPeerConnected = self.multipeerManager.connected?YES:NO;
    [self.navigationController pushViewController:settingListViewController  animated:YES];
}
-(IBAction)backButtonTouchUpInside:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    /*
     self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
     UINavigationController * navigationController = [UINavigationController alloc];
     
     // Circle Menu
     CircleMenuViewController * circleMenuViewController;
     circleMenuViewController = [CircleMenuViewController alloc];
     // Set the cricle menu vc as the root vc
     (void)[navigationController initWithRootViewController:circleMenuViewController];
     //[navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
     
     // Setup circle menu with basic configuration
     (void)[circleMenuViewController initWithButtonCount:kKYCCircleMenuButtonsCount
     menuSize:kKYCircleMenuSize
     buttonSize:kKYCircleMenuButtonSize
     buttonImageNameFormat:kKYICircleMenuButtonImageNameFormat
     centerButtonSize:kKYCircleMenuCenterButtonSize
     centerButtonImageName:nil
     centerButtonBackgroundImageName:kKYICircleMenuCenterButtonBackground];
     CGRect myImageRect = CGRectMake(300, -50, 440, 280);
     UIImageView *image = [[UIImageView alloc] initWithFrame:myImageRect];
     [image setImage:[UIImage imageNamed:@"Detailer.png"]];
     [circleMenuViewController.view addSubview:image];
     [circleMenuViewController.view setBackgroundColor:[UIColor whiteColor]];
     // Set navigation controller as the root vc
     [self.window setRootViewController:navigationController];
     [self.window makeKeyAndVisible];
     */
}
- (NSArray *) toolbarButtonItems {
    NSString *userRole;
    // UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(backButtonTouchUpInside:)];
    
    UIBarButtonItem *settingsButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Settings" style:UIBarButtonItemStylePlain target:self action:@selector(settingButtonTouchUpInside:)];
    
    UIBarButtonItem *flexibleBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    connectButtonItem = [[UIBarButtonItem alloc] initWithTitle:connectButtonItemTitle style:UIBarButtonItemStylePlain target:self action:@selector(connectToPresentation:)];
    
#if X_AUDIENCE
    userRole = @"Attendee";
#endif
    
#if X_PRESENTER
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter){
        userRole = @"Presenter";
    }else{
        userRole = @"Attendee";
    }
#endif
    if (hideUserRoleButton) {
        userRoleButton = nil;
    }else{
        userRoleButton =  [[UIBarButtonItem alloc] initWithTitle:userRole style:UIBarButtonItemStylePlain target:self action:@selector(changeUserRole:)];
    }
    if (!hideUserRoleButton){
        return [NSArray arrayWithObjects:flexibleBarButtonItem,userRoleButton, connectButtonItem, settingsButtonItem, nil];
    }else{
        return [NSArray arrayWithObjects:flexibleBarButtonItem, connectButtonItem, settingsButtonItem, nil];
    }
}

- (void) createSession {
    //[gkSession setDataReceiveHandler:self withContext:nil];
    self.navigationController.delegate = self;
}


-(void)sendPacketDataToAllPeers:(NSData *)packetData{
    
    NSError *error = nil;
    
    //BOOL queued = [gkSession sendDataToAllPeers:packetData withDataMode:GKSendDataReliable error:&error];
    BOOL queued = [self.multipeerManager sendData:packetData withMode:MCSessionSendDataReliable error:&error];
    
    if (!queued){
        //        NSLog(@"%s %@", __FUNCTION__, [error localizedDescription]);
    }
}

-(void)sendPacketData:(NSData *)packetData toPeers:(NSArray *) peers {
    
    NSError *error = nil;
    
    //BOOL queued = [gkSession sendData:packetData toPeers:peers withDataMode:GKSendDataReliable error:&error];
    BOOL queued = [self.multipeerManager sendData:packetData toPeers:peers withMode:MCSessionSendDataReliable error:&error];
    
    if (!queued){
        //        NSLog(@"%s %@", __FUNCTION__, [error localizedDescription]);
    }
}

-(void)sendPacketData:(NSData *)packetData toPeerWithId:(MCPeerID *) peerId {
    
    NSError *error = nil;
    
    //BOOL queued = [gkSession sendData:packetData toPeers:[NSArray arrayWithObject:peerId] withDataMode:GKSendDataReliable error:&error];
    BOOL queued = [self.multipeerManager sendData:packetData toPeers:[NSArray arrayWithObject:peerId] withMode:MCSessionSendDataReliable error:&error];
    
    if (!queued){
        //        NSLog(@"%s %@", __FUNCTION__, [error localizedDescription]);
    }
}

//- (void) receiveData:(NSData *)data fromPeer:(NSString *) peer inSession:(GKSession *) session context:(NSString *) context{
//
//    ESAppDelegate *appDelegate = (ESAppDelegate *)[[UIApplication sharedApplication] delegate];
//
//    @try {
//        if ([appDelegate.role isEqualToString:ATTENDEE]) {
//
//            NSObject *object = [NSKeyedUnarchiver unarchiveObjectWithData:data];
//
//            if ([object isKindOfClass:[ESAbstractPacket class]]) {
//                ESAbstractPacket *packet = (ESAbstractPacket *) object;
//                if (packet.packetType == ESPacketTypePDFTranser){
//                    [self didReceivePDFDataFromPresenter:(ESPDFPacket *)object];
//                } else if (packet.packetType == ESPacketTypePresentationInfo) {
//                    [self didReceivePDFInfoRequestFromPresenter:(ESPresentationInfoRequestPacket *) object];
//                } else if (packet.packetType == ESPacketTypeEndPresentation) {
//                    [self didReceiveEndPresentationPacketFromPresenter:(ESEndPresentationPacket *) object];
//                }
//            }
//        } else {
//            NSObject *object = [NSKeyedUnarchiver unarchiveObjectWithData:data];
//            if ([object isKindOfClass:[ESAbstractPacket class]]) {
//                ESAbstractPacket *packet = (ESAbstractPacket *) object;
//                if (packet.packetType == ESPacketTypePresentationInfoResponse) {
//                    [self handlePacket:packet fromPeerWithId:peer];
//                }
//            }
//        }
//    }
//    @catch (NSException *exception) {
//#if DEBUG
//        NSLog(@"Exception: %@",exception);
//#endif
//    }
//}

- (void) didReceiveEndPresentationPacketFromPresenter:(ESEndPresentationPacket *) packet {
    //Presentation ended before the file transfer completed.
    presentationInfoPacket = nil;
    [self hideDataProgressIndicatorForAttendee];
}

-(void) didReceivePDFInfoRequestFromPresenter:(ESPresentationInfoRequestPacket *) packet {
    
    //check if we have the file.
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:packet.fileName];
    
    BOOL excludeBackupSuccessFul=  [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:documentsDirectoryPath]];
    if (!excludeBackupSuccessFul) {
        [self showExcludeBackupFileErrorMessageWithFileName:packet.fileName];
        return;
    }else if ([fileManager fileExistsAtPath:filePath]) {
        //open the file.
        [self openFileWithPacket:packet];
        return;
    }
    
    noOfFilesReceivedFromPresenter = 0;
    presentationInfoPacket = packet;
    
    //    "Attendee does not have the file. Sending the packet back to presenter");
    //request for the PDF file from server otherwise.
    
    //send an PresentationInfo response packet.
    ESPresentationInfoResponsePacket *responsePacket = [ESPresentationInfoResponsePacket packetForFileRequired:packet.fileName];
    
    NSData *packetData = [NSKeyedArchiver archivedDataWithRootObject:responsePacket];
    NSError *error = nil;
    //BOOL success = [gkSession sendData:packetData toPeers:[NSArray arrayWithObject:presenterPeerId] withDataMode:GKSendDataReliable error:&error];
    BOOL success = [self.multipeerManager sendData:packetData toPeers:[NSArray arrayWithObject:presenterPeerId] withMode:MCSessionSendDataReliable error:&error];
    
    if (!success) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Cannot join presentation"
                                     message:[NSString stringWithFormat:@"Unforeseen error. %@", error.localizedDescription]
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        //[[[UIAlertView alloc] initWithTitle:@"Cannot join presentation" message:[NSString stringWithFormat:@"Unforeseen error. %@", error.localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    
}

-(void) didReceivePDFDataFromPresenter:(ESPDFPacket *) packet {
    
    if (presentationInfoPacket == nil) {
        if (!progressView.hidden) {
            [self hideDataProgressIndicatorForAttendee];
            return;
        }
        return;
    }
    
    if (packet.pdfPacketType == ESPDFPacketTypeHeader) {
        [ self showDataProgressIndictorForAttendee];
        //Start Spinner
        progressView.progress = 1.f * noOfFilesReceivedFromPresenter/(1+presentationInfoPacket.videos.count);
        totalData = packet.fileSize.integerValue;
        receivedData = [[NSMutableData alloc] initWithCapacity:packet.fileSize.integerValue];
    } else if (packet.pdfPacketType == ESPDFPacketTypeChunk) {
        [receivedData appendData:packet.chunk];
        progressView.progress = (1.f * noOfFilesReceivedFromPresenter+(float)[receivedData length]/totalData)/(1+presentationInfoPacket.videos.count);
        //        NSLog(@"received chunk: %d%%",100 * [receivedData length]/totalData);
    } else {
        
        //write data to a file with filename and last modification date.
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
        NSString *documentsDirectoryPath = [paths objectAtIndex:0];
        
        NSString *filePath;
        
        if (noOfFilesReceivedFromPresenter == 0) {
            filePath = [documentsDirectoryPath stringByAppendingPathComponent:packet.fileName];
        } else {
            filePath = [documentsDirectoryPath stringByAppendingPathComponent:[presentationInfoPacket.videos objectAtIndex:noOfFilesReceivedFromPresenter-1]];
        }
        BOOL excludeBackupSuccessFul=  [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:documentsDirectoryPath]];
        if (!excludeBackupSuccessFul) {
            [self showExcludeBackupFileErrorMessageWithFileName:packet.fileName];
            return;
        }
        //        BOOL success = [fileManager createFileAtPath:filePath contents:receivedData attributes:[NSDictionary dictionaryWithObjectsAndKeys:packet.lastModifiedAt,NSFileModificationDate, nil]];
        //        NSLog(@"successully written %@ at path: %u", packet.fileName, success);
        
#if X_AUDIENCE
        NSDictionary *dictOfFiles = [[NSUserDefaults standardUserDefaults] dictionaryForKey:KEY_FILES_DOWNLOADED];
        NSMutableDictionary *mutableDictOfFiles = [NSMutableDictionary dictionaryWithDictionary:dictOfFiles];
        [mutableDictOfFiles setValue:[NSDate date] forKey:packet.fileName];
        [[NSUserDefaults standardUserDefaults] setValue:mutableDictOfFiles forKey:KEY_FILES_DOWNLOADED];
#endif
        
        noOfFilesReceivedFromPresenter ++;
        
        if (noOfFilesReceivedFromPresenter < 1+presentationInfoPacket.videos.count) {
            return;
        }
        [self hideDataProgressIndicatorForAttendee];
        
        [self resetData];
        //open the file.
        //        ESPresentationInfoRequestPacket *presentationInfoPacket = [ESPresentationInfoRequestPacket packetWithFileName:packet.fileName];
        //        presentationInfoPacket.attendeeCanAskQuestions = packet.attendeeCanAskQuestions;
        //        presentationInfoPacket.attendeeCanBrowsePages = packet.attendeeCanBrowsePages;
        
        [self openFileWithPacket:presentationInfoPacket];
        
        ESPresentationInfoResponsePacket *responsePacket = [ESPresentationInfoResponsePacket packetForFileAcknowledgement:packet.fileName];
        NSData *packetData = [NSKeyedArchiver archivedDataWithRootObject:responsePacket];
        NSError *error = nil;
        //BOOL success1 = [gkSession sendData:packetData toPeers:[NSArray arrayWithObject:presenterPeerId] withDataMode:GKSendDataReliable error:&error];
        BOOL success1 = [self.multipeerManager sendData:packetData toPeers:[NSArray arrayWithObject:presenterPeerId] withMode:MCSessionSendDataReliable error:&error];
        
        if (!success1) {
            NSLog(@"Error: %@",error);
        }
        
    }
}

- (void) resetData {
    //To be overridden by child view controllers.
}

-(void) openFileName:(NSString *) fileName withMode:(NSString *)mode {
    
    if ([mode isEqualToString:@"Prepare"]) {
        
        [self copyFilesFromBundleToDocumentsWithFileName:fileName];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
        NSString *documentsDirectoryPath = [paths objectAtIndex:0];
        
        NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:fileName];
        
        ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:nil];
        if (document != nil)
        {
            DetailerReaderViewController *readerViewController1 = [[DetailerReaderViewController alloc] initWithReaderDocument:document];
            readerViewController1.delegate = self;
            readerViewController1.readerMode = mode;
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:readerViewController1];
            navController.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:navController animated:YES completion:nil];
        }
        
    }else  if ([mode isEqualToString:@"Practice"]) {
        
        [self copyFilesFromBundleToDocumentsWithFileName:fileName];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
        NSString *documentsDirectoryPath = [paths objectAtIndex:0];
        
        NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:fileName];
        
        ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:nil];
        if (document != nil)
        {
            DetailerReaderViewController *readerViewController1 = [[DetailerReaderViewController alloc] initWithReaderDocument:document];
            readerViewController1.delegate = self;
            //            readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            //            readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
            readerViewController1.readerMode = mode;
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:readerViewController1];
            navController.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:navController animated:YES completion:nil];
            
            
            //            [self presentViewController:readerViewController animated:YES completion:nil];
        }
        
        
    }else{
        ESPresentationInfoRequestPacket *packet = [[ESPresentationInfoRequestPacket alloc] init];
        packet.fileName = fileName;
        packet.attendeeCanAskQuestions = NO;
        // packet.attendeeCanBrowsePages = YES;
        packet.attendeeCanBrowsePages = NO;
        [self openFileWithPacket:packet];
    }
}

-(void) openFileName:(NSString *) fileName {
    
    
    ESPresentationInfoRequestPacket *packet = [[ESPresentationInfoRequestPacket alloc] init];
    packet.fileName = fileName;
    packet.attendeeCanAskQuestions = NO;
    //packet.attendeeCanBrowsePages = YES;
    packet.attendeeCanBrowsePages = NO;
    [self openFileWithPacket:packet];
    
    
}

-(void)copyFilesFromBundleToDocumentsWithFileName:(NSString *)fileName{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    NSString *pathLocal = [documentsDir stringByAppendingPathComponent:fileName];
    
    NSString *pathBundle = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
    
    //    NSArray *fileList = [manager contentsOfDirectoryAtPath:documentsDirectory
    //                                                     error:nil];
    //
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:pathLocal];
    if (!fileExists) {
        NSError *error;
        BOOL success = [fileManager copyItemAtPath:pathBundle toPath:pathLocal error:&error];
        if (!success) {
            NSLog(@"Could not copy file");
        }
    }else{
        NSLog(@"FIle already exsits at path");
    }
    
}


- (void) openFileWithPacket:(ESPresentationInfoRequestPacket *) packet {
    
    [self hideDataProgressIndicatorForAttendee];
    
    if (packet.fileName)
    {
        openedFileName = packet.fileName;
        //        ESPDFNewViewController *vc = [[ESPDFNewViewController alloc] initWithFileName:fileName];
        //        [self.navigationController pushViewController:vc animated:YES];
        
        NSString *phrase = nil; // Document password (for unlocking most encrypted PDF files)
        
        //
        //        NSString *filePath = [pdfs lastObject]; assert(filePath != nil); // Path to last PDF file
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
        NSString *documentsDirectoryPath = [paths objectAtIndex:0];
        
        NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:packet.fileName];
        
        
        //        ReaderDocument *document = [ReaderDocument unarchiveFromFileName:fileName password:phrase];
        ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:phrase];
        if (document != nil) // Must have a valid ReaderDocument object in order to proceed
        {
            readerViewController = [[ESPDFDocumentViewController alloc] initWithReaderDocument:document withSession:self.multipeerManager];
            readerViewController.sessionDataDelegate = self;
            readerViewController.viewTitle = [packet.fileName stringByDeletingPathExtension];
            
            if ([appDelegate.role isEqualToString:ATTENDEE]) {
                if (disconnectActionSheet != nil) {
                    [disconnectActionSheet dismissViewControllerAnimated:YES completion:nil];
                    disconnectActionSheet = nil;
                }
                //                NSLog(@"Opening file on a attendee with attendeeCanBrowsePages: %u, attendeeCanAskQuestions: %u",packet.attendeeCanBrowsePages, packet.attendeeCanAskQuestions);
                readerViewController.presenterPeerId = presenterPeerId;
                readerViewController.multipeerManager = self.multipeerManager;
                readerViewController.attendeeCanBrowse = packet.attendeeCanBrowsePages;
                readerViewController.attendeeCanQuery = packet.attendeeCanAskQuestions;
            } else {
                // readerViewController.attendeeCanBrowse = YES;
                readerViewController.attendeeCanBrowse = NO;
                readerViewController.attendeeCanQuery = NO;
            }
            //            ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
            [self.navigationController pushViewController:readerViewController animated:YES];
            
            BOOL iPad = NO;
#ifdef UI_USER_INTERFACE_IDIOM
            iPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#endif
            
            UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
            
            if (!iPad  & ([[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter) & ((orientation == UIInterfaceOrientationPortrait) || (orientation == UIInterfaceOrientationPortraitUpsideDown))) {
                readerViewController.title = nil;
            }else{
                readerViewController.title = [packet.fileName stringByDeletingPathExtension];
            }
        }
        
    }
}

-(void) addGettingReadyForPresentationView {
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.leftBarButtonItem = nil;
    //    CGRect frame = self.view.bounds;
    //    frame.size.height -= toolbar.frame.size.height;
    presentationLabel = [[UILabel alloc]initWithFrame:self.view.bounds];
    
    presentationLabel.text = @"Getting ready for presentation";
    presentationLabel.backgroundColor = [UIColor colorWithRed:31/255.f green:62/255.f blue:87/255.F alpha:1.0];
    presentationLabel.textAlignment = ESTextAlignmentCenter;
    presentationLabel.textColor = [UIColor whiteColor];
    presentationLabel .autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view insertSubview:presentationLabel belowSubview:toolbar];
}

-(void)removeGettingReadyForPresentationView{
    self.navigationItem.rightBarButtonItem = [self editButtonItem];
    self.navigationItem.leftBarButtonItem = [self importBarButtonItem];
    if (presentationLabel != nil) {
        [presentationLabel removeFromSuperview];
        presentationLabel = nil;
    }
}

- (void) makeAttendeeJoinPresentationForPeerId:(MCPeerID *) peerId {
    
    //A presentation is running.
    
    if (readerViewController) {
        ESPresentationInfoRequestPacket *packet = [ESPresentationInfoRequestPacket packetWithFileName:openedFileName];
        NSData *packetData = [NSKeyedArchiver archivedDataWithRootObject:packet];
        [self sendPacketData:packetData toPeerWithId:peerId];
    }
}

- (void) sendPresentationInfoRequestToAllAttendeesForFile:(NSString *) fileName {
    ESPresentationInfoRequestPacket *presentationPacket = [ESPresentationInfoRequestPacket packetWithFileName:fileName];
    
    presentationPacket.videos = [self parseVideoXML:fileName];
    NSData *packetData = [NSKeyedArchiver archivedDataWithRootObject:presentationPacket];
    [self sendPacketDataToAllPeers:packetData];
    
    //    "Sent presentation info to all attendees");
}

- (NSArray *) parseVideoXML:(NSString *) fileName {
    //parse video.xml file.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];
    
    NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:@"video.xml"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        
        ESVideoParser *videoParser = [[ESVideoParser alloc] init];
        NSData *xmlData = [NSData dataWithContentsOfFile:filePath];
        [videoParser parseData:xmlData forPresentation:fileName];
        
        if (videoParser.videoArray.count > 0) {
            NSMutableArray *videoFiles = [[NSMutableArray alloc] initWithCapacity:videoParser.videoArray.count];
            for (ESVideo *video in videoParser.videoArray) {
                if ([[NSFileManager defaultManager] fileExistsAtPath:[documentsDirectoryPath stringByAppendingPathComponent:video.fileName]]) {
                    [videoFiles addObject:video.fileName];
                }
            }
            //            NSLog(@"%lu video(s) for file %@", (unsigned long)videoFiles.count, fileName);
            return videoFiles;
        }
    } else {
        //        NSLog(@"No video.xml file found in documents");
    }
    return nil;
}

-(void) timerFired:(NSTimer *) sender {
    
    //    NSLog(@"timerFired: %@",sender==resetTimer?@"reset":@"lock");
    
    __block NSString *fileName = sender.userInfo;
    if (sender == lockTimer) {
        peersWeJustSentPresentationData = [NSArray arrayWithArray:peersWhoNeedPresentationData];
        [peersWhoNeedPresentationData removeAllObjects];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self transferPresentationWithName:fileName toPeers:peersWeJustSentPresentationData];
        });
        [self killAllTimers];
        
        ESTimerTarget *timerTarget = [ESTimerTarget new];
        timerTarget.delegate = self;
        resetTimer = [NSTimer scheduledTimerWithTimeInterval:30 target:timerTarget selector:@selector(timerFired:) userInfo:fileName repeats:NO];
    } else {
        [self killAllTimers];
        //may be no body got the file but we should try and send new people who are waiting if any.
        [self triggerDataTransfer:fileName];
    }
}

- (void) killAllTimers {
    
    if (lockTimer) {
        [lockTimer invalidate];
        lockTimer = nil;
    }
    
    if (resetTimer) {
        [resetTimer invalidate];
        resetTimer = nil;
    }
}

- (void) transferPresentationWithFileName:(NSString *)fileName toPeerWithId:(MCPeerID *) peerId {
    
    if(peersWhoNeedPresentationData == nil) {
        peersWhoNeedPresentationData = [[NSMutableArray alloc] init];
    }
    [peersWhoNeedPresentationData addObject:peerId];
    [self triggerDataTransfer:fileName];
}

- (void) triggerDataTransfer:(NSString *) fileName {
    
    if ([peersWhoNeedPresentationData count] == 0) {
        //        NSLog(@"No Peers to transfer data");
        return;
    }
    
    //    NSLog(@"triggered data transfer");
    
    //create a espdfpacket of type start.
    if (lockTimer == nil && resetTimer == nil) {
        //start the timer to fire after 1 sec.
        ESTimerTarget *timerTarget = [[ESTimerTarget alloc] init];
        timerTarget.delegate = self;
        lockTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:timerTarget selector:@selector(timerFired:) userInfo:fileName repeats:NO];
    }
}

- (void) transferPresentationWithName:(NSString *) presentationName toPeers:(NSArray *) peers {
    [self _transferFileWithFileName:presentationName toPeers:peers];
    NSArray *videos = [self parseVideoXML:presentationName];
    
    if (videos.count >0) {
        
        for ( NSString *video in videos) {
            //            NSLog(@"Sending %@ to peers", [videos objectAtIndex:0]);
            [self _transferFileWithFileName:video toPeers:peers];
        }
        
    }
}

- (void) _transferFileWithFileName:(NSString *)fileName toPeers:(NSArray *) peers {
    
    ESPDFPacket *headerPacket = [ESPDFPacket pdfHeaderPacketForFileName:fileName];
    NSData *headerData = [NSKeyedArchiver archivedDataWithRootObject:headerPacket];
    //    [ self showDataProgressIndictorForAttendee];
    
    //    [self sendPacketData:headerData toPeerWithId:peerId];
    [self sendPacketData:headerData toPeers:peers];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];
    
    NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:fileName];
    
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    //    totalData = packet.fileSize.integerValue;
    
    int numItems = ceil((double)data.length / DefaultChunkLength);
    
    //    NSLog(@"%d %d",data.length,DefaultChunkLength);
    for (int i=0; i<numItems; i++){
        NSUInteger start = i * DefaultChunkLength;
        NSUInteger length = DefaultChunkLength;
        NSUInteger end = start + length;
        
        //The chunk position exceeds the range of the source data.
        if (end > data.length){
            length = data.length % DefaultChunkLength;
        }
        NSRange range = {start, length};
        NSData *chunkData = [data subdataWithRange:range];
        ESPDFPacket *pdfPacket = [ESPDFPacket pdfPacketForData:chunkData];
        NSData *packetData= [NSKeyedArchiver archivedDataWithRootObject:pdfPacket];
        //        [self sendPacketData:packetData toPeerWithId:peerId];
        [self sendPacketData:packetData toPeers:peers];
        
    }
    
    ESPDFPacket *footerPacket = [ESPDFPacket pdfFooterPacketForFileName:fileName];
    NSData *footerData = [NSKeyedArchiver archivedDataWithRootObject:footerPacket];
    //    [self hideDataProgressIndicatorForAttendee];
    //    [self sendPacketData:footerData toPeerWithId:peerId];
    [self sendPacketData:footerData toPeers:peers];
    
    //    //"transferred footerData to %@",peers);
}

- (BOOL) handlePacket:(ESAbstractPacket *) packet fromPeerWithId:(MCPeerID *)peerId {
    
    if (packet.packetType == ESPacketTypePresentationInfoResponse) {
        
        ESPresentationInfoResponsePacket *presentationResponsePacket = (ESPresentationInfoResponsePacket *)packet;
        if (!presentationResponsePacket.fileTransferAcknowledgement) {
            [self transferPresentationWithFileName:presentationResponsePacket.fileName toPeerWithId:peerId];
        } else {
            //            if ([peersWeJustSentPresentationData containsObject:peerId]) {
            peersWeJustSentPresentationData = nil;
            [self killAllTimers];
            [self triggerDataTransfer:presentationResponsePacket.fileName];
            //            } else {
            //this is a rare case we got an ack from a sent earlier.
            //            }
        }
        return YES;
    } else {
        NSLog(@"Warning, ESSessionBase can't handle packet with type %zd",packet.packetType);
    }
    return NO;
}

- (void) navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if ([viewController isMemberOfClass:[ESPDFDocumentViewController class]]) {
        //[gkSession setDataReceiveHandler:viewController withContext:nil];
    } else if (viewController == self) {
        //[gkSession setDataReceiveHandler:self withContext:nil];
        if([UIApplication sharedApplication].idleTimerDisabled) {
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
        }
//        [[UIApplication sharedApplication] setStatusBarHidden:NO];
//        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        [self.navigationController setNavigationBarHidden:NO];
        
        [peersWhoNeedPresentationData removeAllObjects];
        peersWhoNeedPresentationData = nil;
        peersWeJustSentPresentationData = nil;
        [self killAllTimers];
    }
}

-(UIBarButtonItem *)importBarButtonItem {
    return nil;
}
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL{
    
    if ([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]) {
        NSError *error = nil;
        
        BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                        
                                      forKey: NSURLIsExcludedFromBackupKey error: &error];
        if(!success){
            
            NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
            
        }
        return success;
    }else{
        return NO;
    }
    
}
-(void)showExcludeBackupFileErrorMessageWithFileName:(NSString *)fileName{
    NSString *message = @"";
    if (fileName) {
        message = [NSString stringWithFormat:@"We are unable to open %@ file at this moment.\n Please retry after sometime",fileName];
    }else{
        message = @"We are unable to open the file  at this moment.\n Please retry after sometime";
    }
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelButton = [UIAlertAction
                               actionWithTitle:@"Dismiss"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    //[[[UIAlertView alloc]initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil]show];
    
}
-(IBAction)changeUserRole:(id)sender{
    
#if X_AUDIENCE
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:@"To use this feature , purchase PresInteract Pro App from AppStore"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* purchaseButton = [UIAlertAction
                                     actionWithTitle:@"Purchase now"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action) {
                                         //Handle your yes please button action here
                                         
    
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Not now"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
    
    
    [alert addAction:purchaseButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];

    
//    
//    UIAlertView *alert=   [[UIAlertView alloc ]initWithTitle:@"" message:@"To use this feature , purchase PresInteract Pro App from AppStore" delegate:self cancelButtonTitle:@"Not now" otherButtonTitles:@"Purchase now", nil];
//    
//    alert.tag = UNAVAILABLE_FEATURE_ALERT_TAG;
//    
//    [alert show];
//    
    return;
    
    
#endif
    
#if X_PRESENTER
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter){
        [userDefaults setValue:[NSNumber numberWithInteger:0] forKey:KEY_USER_ROLE];
        userRoleButton.title = @"Attendee";
        [self showToastWithText:@" Connected as Attendee "];
        
    }else{
        [userDefaults setValue:[NSNumber numberWithInteger:1] forKey:KEY_USER_ROLE];
        userRoleButton.title = @"Presenter";
        [self showToastWithText:@" Connected as Presenter "];
    }
    [userDefaults synchronize];
#endif
}
- (void) dealloc {
    //    [super dealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ES_FILE_DOWNLOADED_NOTIFICATION object:nil];
}
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    
//    if (alertView.tag == UNAVAILABLE_FEATURE_ALERT_TAG) {
//        
//        if (!(alertView.cancelButtonIndex == buttonIndex)) {
//            
//
//        }
//    }
//    return;
//}
#pragma mark - Product view controller delegate methods


#pragma mark - Party Time Delegate

- (void)manager:(ESMultipeerManager *)manager
 didReceiveData:(NSData *)data
       fromPeer:(MCPeerID *)peerID
{
    NSLog(@"Received some data!");
    
    @try {
        if ([appDelegate.role isEqualToString:ATTENDEE]) {
            
            NSObject *object = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            
            if ([object isKindOfClass:[ESAbstractPacket class]]) {
                ESAbstractPacket *packet = (ESAbstractPacket *) object;
                if (packet.packetType == ESPacketTypePDFTranser){
                    [self didReceivePDFDataFromPresenter:(ESPDFPacket *)object];
                } else if (packet.packetType == ESPacketTypePresentationInfo) {
                    [self didReceivePDFInfoRequestFromPresenter:(ESPresentationInfoRequestPacket *) object];
                } else if (packet.packetType == ESPacketTypeEndPresentation) {
                    [self didReceiveEndPresentationPacketFromPresenter:(ESEndPresentationPacket *) object];
                }
            }
        } else {
            NSObject *object = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            if ([object isKindOfClass:[ESAbstractPacket class]]) {
                ESAbstractPacket *packet = (ESAbstractPacket *) object;
                if (packet.packetType == ESPacketTypePresentationInfoResponse) {
                    [self handlePacket:packet fromPeerWithId:peerID];
                }
            }
        }
    }
    @catch (NSException *exception) {
#if DEBUG
        NSLog(@"Exception: %@",exception);
#endif
    }
    
    if (readerViewController) {
        
        [readerViewController manager:manager didReceiveData:data fromPeer:peerID];
        
    }
    
}

- (void)manager:(ESMultipeerManager *)manager
           peer:(MCPeerID *)peer
   changedState:(MCSessionState)state
   currentPeers:(NSArray *)currentPeers
{
    if (attendeesViewController) {
        [attendeesViewController.tableView reloadData];
    }
    
    
    if ([appDelegate.role isEqualToString:ATTENDEE]) {
        if (state == MCSessionStateConnected)
        {
            NSString *message = [NSString stringWithFormat:@"Connected to %@", peer.displayName];
            [self showToastWithText:message];
            NSLog(@"Connected to %@", peer.displayName);
            
            [self addGettingReadyForPresentationView ];
            connectButtonItemTitle = @"Disconnect";
            [self hideUserRoleButtonFromToolBar:YES];
            [self showToastWithText:@"Connected to session"];
            presenterPeerId = peer;
            [self hideLoadingIndicator];
            [self createSession];
        }
        else if (state == MCSessionStateNotConnected)
        {
            
            
            NSString *message = [NSString stringWithFormat:@"Peer disconnected: %@", peer.displayName];
            [self showToastWithText:message];
            
            NSLog(@"Peer disconnected: %@", peer.displayName);
            
            if(shouldDisconnect) {
                
                [self removeGettingReadyForPresentationView];
                [self hideDataProgressIndicatorForAttendee];
                connectButtonItemTitle = @"Connect";
                [self hideLoadingIndicator];
                [self hideUserRoleButtonFromToolBar:NO];
                
                appDelegate.sessionState = @"0";
                
                [self.multipeerManager leaveSession];
                
            } else {
                
                [self.multipeerManager leaveSession];
                
                //                self.multipeerManager = nil;
                //
                //                self.multipeerManager = [[ESMultipeerManager alloc] initWithServiceType:@"iProjector"];
                //                self.multipeerManager.delegate = self;
                //
                [self.multipeerManager joinSessionAsAttendee];
                
            }
            
            if (readerViewController == nil) {
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:nil
                                             message:@"Presentator disconnected from presentation"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* okButton = [UIAlertAction
                                            actionWithTitle:@"OK"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                //Handle your yes please button action here
                                                [self dismissViewControllerAnimated:YES completion:nil];
                                            }];
                
                
                [alert addAction:okButton];
                
                [self presentViewController:alert animated:YES completion:nil];
                
//                
//                [[[UIAlertView alloc] initWithTitle:nil message:@"Presentator disconnected from presentation" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                [self.navigationController popToRootViewControllerAnimated:YES];
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }
            presenterPeerId = nil;
            [self showToastWithText:[NSString stringWithFormat:@"%@ left the session.",peer.displayName]];
        }
    } else {
        presenterPeerId = peer;
        if (state == MCSessionStateConnected) {
            [self showToastWithText:[NSString stringWithFormat:@"%@ joined the session.",peer.displayName]];
            [self makeAttendeeJoinPresentationForPeerId:peer];
            
        } else if (state == MCSessionStateNotConnected ) {
            [self showToastWithText:[NSString stringWithFormat:@"%@ left the session.",peer.displayName]];
        }
        connectButtonItemTitle =  [NSString stringWithFormat:@"%lu Connected",(unsigned long)[self.multipeerManager connectedPeers].count];
        [self hideUserRoleButtonFromToolBar:YES];
        if ([self.multipeerManager connectedPeers].count > 0) {
            [self hideLoadingIndicator];
        }
        
    }
    NSLog(@"Current peers: %@", currentPeers);
    
    
}

- (void)manager:(ESMultipeerManager *)manager failedToJoinSession:(NSError *)error
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Error"
                                  message:[error localizedFailureReason]
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

-(BOOL) prefersStatusBarHidden {
    return NO;
}

@end
