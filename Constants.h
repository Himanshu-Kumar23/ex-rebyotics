//
//  Constants.h
//  KYCircleMenuDemo
//
//  Created by Kaijie Yu on 6/24/12.
//  Copyright (c) 2012 Kjuly. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Basic Configuration - prefix: KYC



#pragma mark -
#pragma mark - View  - prefix: KY

// Button Size
#define kKYButtonInMiniSize   16.f
#define kKYButtonInSmallSize  200.f
#define kKYButtonInNormalSize 260.f

#define kKYButtonInSmallSize_iPhone  100.f
#define kKYButtonInNormalSize_iPhone 120.f

#pragma mark - KYCircleMenu Configuration

// Number of buttons around the circle menu
#define kKYCCircleMenuButtonsCount 3
// Circle Menu
// Basic constants

#define kKYCircleMenuSize_iPhone             320.f
#define kKYCircleMenuButtonSize_iPhone       kKYButtonInNormalSize_iPhone
#define kKYCircleMenuCenterButtonSize_iPhone kKYButtonInNormalSize_iPhone
// Image
#define kKYICircleMenuCenterButton_iPhone           @"KYICircleMenuCenterButton_iPhone.png"
#define kKYICircleMenuCenterButtonBackground_iPhone @"KYICircleMenuCenterButtonBackground_iPhone.png"
#define kKYICircleMenuButtonImageNameFormat_iPhone  @"KYICircleMenuButton%.2d_iPhone.png" // %.2d: 1 - 6




#define kKYCircleMenuSize             780.f
#define kKYCircleMenuButtonSize       kKYButtonInNormalSize
#define kKYCircleMenuCenterButtonSize kKYButtonInNormalSize
// Image
#define kKYICircleMenuCenterButton           @"KYICircleMenuCenterButton.png"
#define kKYICircleMenuCenterButtonBackground @"KYICircleMenuCenterButtonBackground.png"
#define kKYICircleMenuButtonImageNameFormat  @"KYICircleMenuButton%.2d.png" // %.2d: 1 - 6

@interface Constants : NSObject

@end
