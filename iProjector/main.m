//
//  main.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 6/27/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ESAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ESAppDelegate class]));
    }
}
