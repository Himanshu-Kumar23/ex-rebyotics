//
//  waDrawingView.m
//  iProjectorPainting
//
//  Created by Manoj Katragadda on 7/24/13.
//  Copyright (c) 2013 Webileapps. All rights reserved.
//

#import "waDrawingView.h"
#import <QuartzCore/QuartzCore.h>

#define DEFAULT_COLOR [UIColor blackColor]
#define DEFAULT_WIDTH 6.0f
#define DEFAULT_ALPHA 1.0f



@interface waDrawingView ()

#pragma mark Private Helper function

CGPoint midPoint(CGPoint p1, CGPoint p2);

@end
@implementation waDrawingView
@synthesize drawImage;
@synthesize colorPencilIndex;
@synthesize enableDrawing;
@synthesize thicknessPencilIndex;
@synthesize undoArray;

@synthesize lineAlpha;
@synthesize lineColor;
@synthesize lineWidth;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.enableDrawing = NO;
        undoArray=[[NSMutableArray alloc]init];
        self.lineAlpha = DEFAULT_ALPHA;
        colorArray = [[NSMutableArray alloc]init];
        
        //Add Gesture Recognizer 
        UITapGestureRecognizer *singleTapOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap1:)];
        singleTapOne.numberOfTouchesRequired = 1;
        singleTapOne.numberOfTapsRequired = 1; 
        [self addGestureRecognizer:singleTapOne];
        
        UITapGestureRecognizer *doubleTapOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        doubleTapOne.numberOfTouchesRequired = 1;
        doubleTapOne.numberOfTapsRequired = 2;
        [self addGestureRecognizer:doubleTapOne];
        
        UITapGestureRecognizer *doubleTapTwo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        doubleTapTwo.numberOfTouchesRequired = 2; doubleTapTwo.numberOfTapsRequired = 2;
        [self addGestureRecognizer:doubleTapTwo];
        
        [singleTapOne requireGestureRecognizerToFail:doubleTapOne]; // Single tap requires double tap to fail


    }
    return self;
}
-(void)handleDoubleTap:(UITapGestureRecognizer *)g{
    if (self.messageDelegate) {
        [self.messageDelegate addDoubleTap:self withDoubleTapGestureRecognizer:g];
    }

}
-(void)handleSingleTap1:(UITapGestureRecognizer *)g{
    if (self.messageDelegate) {
        [self.messageDelegate addSingleTap:self withSingleTapGestureRecognizer:g];
    }
}
- (void)drawRect:(CGRect)rect
{
    
    // Drawing code
//    [super drawRect:rect];
    switch (colorPencilIndex) {
        case PENCIL_COLOR_RED:
            self.lineColor = [UIColor redColor];
            break;
        case PENCIL_COLOR_YELLOW:
            self.lineColor = [UIColor yellowColor];
            break;
        case PENCIL_COLOR_GREEN:
            self.lineColor = [UIColor greenColor];
            break;
        case PENCIL_COLOR_BLACK:
            self.lineColor = [UIColor blackColor];
            break;
        case PENCIL_COLOR_BLUE:
            self.lineColor = [UIColor blueColor];
            break;
        default:
            self.lineColor = DEFAULT_COLOR;
            break;
    }
    switch (thicknessPencilIndex) {
        case PENCIL_THICKNESS_0:
            self.lineWidth = 2.f;
            break;
        case PENCIL_THICKNESS_1:
            self.lineWidth = 4.f;
            break;
        case PENCIL_THICKNESS_2:
            self.lineWidth = 8.f;
            break;
        case PENCIL_THICKNESS_3:
            self.lineWidth = 12.f;
            break;
        case PENCIL_THICKNESS_4:
            self.lineWidth = 14.0f;
            break;
        case ERASER_BUTTON:
            self.lineWidth = DEFAULT_WIDTH;
            break;
        default:
            break;
    }
    
    switch (drawStep) {
        case DRAW:
        {
            CGPoint mid1 = midPoint(previousPoint1, previousPoint2);
            CGPoint mid2 = midPoint(currentPoint, previousPoint1);
            [myPath moveToPoint:mid1];
            [myPath addQuadCurveToPoint:mid2 controlPoint:previousPoint1];
            [self.lineColor setStroke];
            myPath.lineWidth = self.lineWidth;
            [myPath strokeWithBlendMode:kCGBlendModeNormal alpha:self.lineAlpha];

        }
            break;
        case ERASE:
        {
            CGPoint mid1 = midPoint(previousPoint1, previousPoint2);
            CGPoint mid2 = midPoint(currentPoint, previousPoint1);
            [myPath moveToPoint:mid1];
            [myPath addQuadCurveToPoint:mid2 controlPoint:previousPoint1];
            [self.lineColor setStroke];
            [myPath strokeWithBlendMode:kCGBlendModeClear alpha:self.lineAlpha];

        }
            break;
        case UNDO:
        {
            count =  0;
            for(UIBezierPath *_paths in undoArray)
            {
                UIColor *_color = [colorArray objectAtIndex:count];
                [_color setStroke];
                [_paths strokeWithBlendMode:kCGBlendModeNormal alpha:1.0];
                count ++;
            }
            break;
        }
        case CLEAR:
        {
            //no need to set stoke for clear page
            break;
        }
    }
}

#pragma mark Private Helper function

CGPoint midPoint(CGPoint p1, CGPoint p2)
{
    return CGPointMake((p1.x + p2.x) * 0.5, (p1.y + p2.y) * 0.5);
}

#pragma mark Gesture handle
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    
    myPath=[[UIBezierPath alloc]init];
    myPath.lineWidth=self.lineWidth;
    myPath.lineCapStyle = kCGLineCapRound;
    
    previousPoint1 = [touch locationInView:self];
    previousPoint2 = [touch locationInView:self];
    currentPoint = [touch locationInView:self];
    [self touchesMoved:touches withEvent:event];
    if (self.messageDelegate != nil) {
        [self.messageDelegate drawingViewBegan:self touchesBegan:touches];
    }

}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch  = [touches anyObject];
    previousPoint2  = previousPoint1;
    previousPoint1  = currentPoint;
    currentPoint    = [touch locationInView:self];
    if (self.enableDrawing) {
    if(drawStep != ERASE)
        drawStep = DRAW;
    [self calculateMinImageArea:previousPoint1 :previousPoint2 :currentPoint];
    }
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [colorArray addObject:self.lineColor];
    [undoArray addObject:myPath];
}
#pragma mark Private Helper function


- (void) calculateMinImageArea:(CGPoint)pp1 :(CGPoint)pp2 :(CGPoint)cp
{
    // calculate mid point
    CGPoint mid1    = midPoint(pp1, pp2);
    CGPoint mid2    = midPoint(cp, pp1);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, mid1.x, mid1.y);
    CGPathAddQuadCurveToPoint(path, NULL, pp1.x, pp1.y, mid2.x, mid2.y);
    CGRect bounds = CGPathGetBoundingBox(path);
    CGPathRelease(path);
    
    CGRect drawBox = bounds;
    //Pad our values so the bounding box respects our line width
    drawBox.origin.x        -= self.lineWidth * 1;
    drawBox.origin.y        -= self.lineWidth * 1;
    drawBox.size.width      += self.lineWidth * 2;
    drawBox.size.height     += self.lineWidth * 2;
    
//    UIGraphicsBeginImageContext(drawBox.size);
//    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
//    UIGraphicsEndImageContext();
    [self setNeedsDisplayInRect:drawBox];
    
}
-(void)undoDrawing{
    if([undoArray count]>0 ){
        [colorArray removeLastObject];
        [undoArray removeLastObject];
        drawStep = UNDO;
        [self setNeedsDisplayInRect:self.bounds];
    }
}

-(void)eraseDrawing{
    if(drawStep!=ERASE)
    {
        drawStep = ERASE;
    }
    else
    {
        drawStep = DRAW;
    }
    
}
-(void)clearPage{
    
    drawStep = CLEAR;
    [colorArray removeAllObjects];
    [undoArray removeAllObjects];
    [self setNeedsDisplayInRect:self.bounds];
}
@end
