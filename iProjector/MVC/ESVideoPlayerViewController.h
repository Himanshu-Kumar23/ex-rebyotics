//
//  ESVideoPlayerViewController.h
//  iProjector
//
//  Created by Jayaprada Behera on 21/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

#import <MediaPlayer/MediaPlayer.h>
//#import <GameKit/GameKit.h>
//#import "ESPaintingDoneButtonPacket.h"
//#import "ESSettingListViewController.h"
//#import "ESVideoPausePacket.h"
//#import "ESVideoPlayPacket.h"

#define ES_DEMO_VIDEO_RESOURCE_NAME @"Intro slides video"

@class ESVideoPlayerViewController;

@protocol ESVideoPlayerViewControllerDelegate <NSObject>

//- (void) doneWithESVideoPlayerViewController:(ESVideoPlayerViewController *) viewController forpacket:(ESPaintingDoneButtonPacket *)packet;

- (void) videoPlayerViewController:(ESVideoPlayerViewController *) viewController videoPlayerDidPauseAt:(NSTimeInterval) currentPlaybackTime;
- (void) videoPlayerViewController:(ESVideoPlayerViewController *) viewController videoPlayerDidResumePlayingAt:(NSTimeInterval) currentPlaybackTime;

- (void) videoPlayerViewControllerDone:(ESVideoPlayerViewController *) viewController;

@end

@interface ESVideoPlayerViewController : UIViewController

@property(nonatomic,strong)AVPlayerViewController *movieController;
@property(nonatomic,strong)NSURL *url;
@property(nonatomic,unsafe_unretained) id<ESVideoPlayerViewControllerDelegate> delegate;

@end
