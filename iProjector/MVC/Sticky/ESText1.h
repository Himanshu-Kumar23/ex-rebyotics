//
//  ESText1.h
//  X Detailer
//
//  Created by Jayaprada Behera on 03/03/15.
//  Copyright (c) 2015 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESText1 : NSObject
@property(nonatomic, strong) NSMutableArray *textFrames;
@property(nonatomic) CGFloat fontSize;
@property(nonatomic, strong) UIColor *color;
@property(nonatomic,strong) NSString *textValue;
@property(nonatomic)NSInteger indexValue;
//@property(nonatomic) CGPoint textCenter;
@property(nonatomic) CGRect textFrame;

- (void) addFrame:(CGRect) frame;
@end
