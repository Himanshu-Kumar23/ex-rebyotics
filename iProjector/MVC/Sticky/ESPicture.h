//
//  ESPicture.h
//  iProjector
//
//  Created by Jayaprada Behera on 07/11/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPicture : NSObject

@property(nonatomic, strong) UIImage *picture;
@property(nonatomic)NSInteger pictureIndex;
@property(nonatomic) CGRect pictureFrame;
@property(nonatomic, strong) NSMutableArray *pictureFrames;

- (void) addPictureFrame:(CGRect) frame;

@end
