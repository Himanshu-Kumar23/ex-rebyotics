//
//  ESPicture.m
//  iProjector
//
//  Created by Jayaprada Behera on 07/11/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESPicture.h"

@implementation ESPicture
- (void) addPictureFrame:(CGRect)frame{
    
    if (CGRectIsNull(frame)) {
        self.pictureFrames = [NSMutableArray arrayWithCapacity:10];
    }
    [self.pictureFrames addObject:[NSValue valueWithCGRect:frame]];
}

@end
