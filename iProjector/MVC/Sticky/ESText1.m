//
//  ESText1.m
//  X Detailer
//
//  Created by Jayaprada Behera on 03/03/15.
//  Copyright (c) 2015 EIQ Services. All rights reserved.
//

#import "ESText1.h"

@implementation ESText1
- (void) addFrame:(CGRect)frame{
    
    if (CGRectIsNull(frame)) {
        self.textFrames = [NSMutableArray arrayWithCapacity:10];
    }
    if (![self.textFrames containsObject:[NSValue valueWithCGRect:frame]]) {
        [self.textFrames addObject:[NSValue valueWithCGRect:frame]];
    }
}

@end
