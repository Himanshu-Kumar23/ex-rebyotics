//
//  ESSettingListViewController.m
//  iProjector
//
//  Created by WebileApps on 17/07/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESSettingListViewController.h"
#import "ESDeviceNameViewController.h"


#define TOTAL_SECTIONS 6

#define SECTION_USERNAME 0
#define SECTION_ROLE 1
#define SECTION_ANIMATIONS 2
#define SECTION_TIMER 3
#define SECTION_LASER_POINTER_OPTION 4
#define SECTION_PRESENTATION_SETTINGS 5

#define UNAVAILABLE_FEATURE_ALERT_TAG    1030


@interface ESSettingListViewController ()<UITableViewDataSource, UITableViewDelegate,UIAlertViewDelegate>
{
    UITableViewCell *defaultUserNameSwitchTableCell;
    UITableViewCell *customUserNameTableCell;
    UITableViewCell *timerSwitchTableCell;
    UITableViewCell *slideNumberTableCell;
    NSUserDefaults *userDefaults;
    UITableView *_tableView;
}

@end

@implementation ESSettingListViewController
@synthesize isPeerConnected;
- (void) loadView
{
    [super loadView];
    //This will be inside a UI navigation controller. not necessary to add a navBar.
    
    //Add a tableView.
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [self.view addSubview:_tableView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Settings";
    
    //Create a non reusable cell for timer cell.
    timerSwitchTableCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    timerSwitchTableCell.textLabel.text = @"Show elapsed time";
    timerSwitchTableCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //Attach a switch to timer cell.
    UISwitch *uiSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
    [uiSwitch addTarget:self action:@selector(timerValueChanged:) forControlEvents:UIControlEventValueChanged];
    timerSwitchTableCell.accessoryView = uiSwitch;
    
    slideNumberTableCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    slideNumberTableCell.textLabel.text = @"Show slide number";
    slideNumberTableCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //Attach a switch to timer cell.
    UISwitch *pageSlideSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
    [pageSlideSwitch addTarget:self action:@selector(slideValueChanged:) forControlEvents:UIControlEventValueChanged];
    slideNumberTableCell.accessoryView = pageSlideSwitch;
    
    //Create a non reusable cell for defaultUserName
    defaultUserNameSwitchTableCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    defaultUserNameSwitchTableCell.textLabel.text = @"Use Default";
    defaultUserNameSwitchTableCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //Attach a switch to username cell.
    UISwitch *uiSwitch1 = [[UISwitch alloc] initWithFrame:CGRectZero];
    [uiSwitch1 addTarget:self action:@selector(defaultUsernameValueChanged:) forControlEvents:UIControlEventValueChanged];
    defaultUserNameSwitchTableCell.accessoryView = uiSwitch1;
    
    //User defaults
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    //initialize switches.
    uiSwitch.on = [userDefaults boolForKey:KEY_DISPLAY_TIMER];
    pageSlideSwitch.on = [userDefaults boolForKey:KEY_DISPLAY_SLIDE];
    
    //initialize default username
    if (![userDefaults valueForKey:KEY_USE_DEFAULT_USERNAME]) {
        [userDefaults setValue:[NSNumber numberWithBool:YES] forKey:KEY_USE_DEFAULT_USERNAME];
        [userDefaults synchronize];
    }
    if (![userDefaults valueForKey:KEY_CUSTOM_USERNAME]) {
        [userDefaults setValue:[[UIDevice currentDevice] name] forKey:KEY_CUSTOM_USERNAME];
        [userDefaults synchronize];
    }
    uiSwitch1.on = [[userDefaults valueForKey:KEY_USE_DEFAULT_USERNAME] boolValue];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma UITableviewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return TOTAL_SECTIONS ;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSLog(@"%ld sec",(long)section);
    
    if (section == SECTION_USERNAME) {
        
        if ([[userDefaults valueForKey:KEY_USE_DEFAULT_USERNAME] boolValue]) {
            return 1;
        }
        return 2;
    }
    
    if (section == SECTION_ROLE) {
        return 2;
    }
    
    else if (section == SECTION_ANIMATIONS) {
        return 8;
    } else if (section == SECTION_TIMER) {
        
//        ESUserRole userRole = [[userDefaults valueForKey:KEY_USER_ROLE] integerValue];

//        if (userRole == ESUserRoleAttendee) {
//            return 0;
//        } else {
            return 2;
//        }
    }else if (section == SECTION_LASER_POINTER_OPTION){
        return 2;
    }else if (section == SECTION_PRESENTATION_SETTINGS){
        return 1;
        //return 2;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SECTION_TIMER) {
        switch (indexPath.row) {
            case 0:
                return timerSwitchTableCell;

                break;
            case 1:
                return slideNumberTableCell;

                break;
                
            default:
                break;
        }
    } else if (indexPath.section == SECTION_USERNAME) {

        if (indexPath.row == 0) {
            UISwitch *uiSwitch = (UISwitch *)defaultUserNameSwitchTableCell.accessoryView;
            if (self.isPeerConnected) {
                uiSwitch.enabled = NO;
                defaultUserNameSwitchTableCell.textLabel.textColor = [UIColor grayColor];
            }else{
                uiSwitch.enabled = YES;
                defaultUserNameSwitchTableCell.textLabel.textColor = [UIColor blackColor];
            }
            return defaultUserNameSwitchTableCell;
        } else {
            if (!customUserNameTableCell) {
                //custom name cell.
                customUserNameTableCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
                customUserNameTableCell.textLabel.text = @"Name";
                customUserNameTableCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                customUserNameTableCell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            customUserNameTableCell.detailTextLabel.text = [userDefaults valueForKey:KEY_CUSTOM_USERNAME];
            return customUserNameTableCell;
        }
    }
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (indexPath.section == SECTION_ROLE) {
        cell.accessoryView = nil;
        ESUserRole userRole = [[userDefaults valueForKey:KEY_USER_ROLE] integerValue];
        switch(indexPath.row) {
            default:
            case ESUserRoleAttendee:
                cell.textLabel.text = @"Attendee";
                break;
            case ESUserRolePresenter:
                cell.textLabel.text = @"Presenter";
                break;
        }
        if (userRole == indexPath.row) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        if (self.isPeerConnected) {
            cell.textLabel.textColor = [UIColor grayColor];
            cell.userInteractionEnabled = NO;
        }else{
            cell.userInteractionEnabled = YES;
            cell.textLabel.textColor = [UIColor blackColor];
        }
    }
    if (indexPath.section == SECTION_ANIMATIONS) {
                cell.accessoryView = nil;
        switch (indexPath.row) {
            default:
            case ESPageTransitionAnimationNone:
                cell.textLabel.text = @"Effect None";
                break;
            case ESPageTransitionAnimationCurl:
                cell.textLabel.text = @"Curl";
                break;
            case ESPageTransitionAnimationFade:
                cell.textLabel.text = @"Fade";
                break;
            case ESPageTransitionAnimationFlip:
                cell.textLabel.text = @"Flip";
                break;
            case ESPageTransitionAnimationMoveIn:
                cell.textLabel.text = @"Move In";
                break;
            case ESPageTransitionAnimationPush:
                cell.textLabel.text = @"Push";
                break;
            case ESPageTransitionAnimationReveal:
                cell.textLabel.text = @"Reveal";
                break;
            case ESPageTransitionAnimationRandom:
                cell.textLabel.text = @"Random";
                break;
        }
        ESPageTransitionAnimation transitionAnimation = [[userDefaults valueForKey:KEY_PAGE_TRANSITION_ANIMATION] integerValue];
        if (transitionAnimation == indexPath.row) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        cell.textLabel.textColor = [UIColor blackColor];
        cell.userInteractionEnabled = YES;

    }else if (indexPath.section == SECTION_LASER_POINTER_OPTION) {
                cell.accessoryView = nil;
        switch (indexPath.row) {
            default:
            case ESLaserPointerRed:
                cell.textLabel.text = @"Red";
                break;
            case ESLaserPointerGreen:
                cell.textLabel.text = @"Green";
                break;            
        }
        ESLaserPointer laserPointer = [[userDefaults valueForKey:KEY_LASER_POINTER] integerValue];
        if (laserPointer == indexPath.row) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        cell.textLabel.textColor = [UIColor blackColor];
        cell.userInteractionEnabled = YES;

    } else if (indexPath.section == SECTION_PRESENTATION_SETTINGS) {
        UISwitch *uiSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        cell.accessoryView = uiSwitch;
#if X_AUDIENCE
        
        uiSwitch.userInteractionEnabled = NO;
#endif
        [uiSwitch addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];


       /* if (indexPath.row == 0) {
            cell.textLabel.text = @"Attendees can browse pages";
            uiSwitch.tag = 1001;
            uiSwitch.on = [userDefaults boolForKey:KEY_ATTENDEE_CAN_BROWSE];
            cell.accessoryType = [userDefaults boolForKey:KEY_ATTENDEE_CAN_BROWSE] ? UITableViewCellAccessoryCheckmark: UITableViewCellAccessoryNone;
   //    } else {*/
        if (indexPath.row == 0) {
            uiSwitch.tag = 1002;
            uiSwitch.on = [userDefaults boolForKey:KEY_ATTENDEE_CAN_QUERY];
            cell.textLabel.text = @"Attendees can raise queries";
            cell.accessoryType = [userDefaults boolForKey:KEY_ATTENDEE_CAN_QUERY] ? UITableViewCellAccessoryCheckmark: UITableViewCellAccessoryNone;
       }
    }
    return cell;

}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([self tableView:tableView numberOfRowsInSection:section] == 0) {
        return nil;
    }
    switch (section) {
        case SECTION_USERNAME:
            return @"Change user name";
        case SECTION_ROLE:
            return @"Connect As";
            break;
        case SECTION_ANIMATIONS:
            return @"Page Animations";
        case SECTION_TIMER:
            return @"Presenter Display";
        case SECTION_LASER_POINTER_OPTION:
            return @"Laser Pointer";
        case SECTION_PRESENTATION_SETTINGS:
            return @"During presentation";
        default:
            return nil;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([self tableView:tableView numberOfRowsInSection:section] == 0) {
        return 1.f;
    }
    return 35.f;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if ([self tableView:tableView numberOfRowsInSection:section] == 0) {
        return 1.f;
    }
    return 5.f;
}

#pragma UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case SECTION_ROLE:
#if X_PRESENTER
            [userDefaults setValue:[NSNumber numberWithInteger:indexPath.row] forKey:KEY_USER_ROLE];
            [userDefaults synchronize];
            [tableView reloadData];
#endif
#if X_AUDIENCE
            [self showAlert];
#endif
            break;

        case SECTION_ANIMATIONS:
            [userDefaults setValue:[NSNumber numberWithInteger:indexPath.row] forKey:KEY_PAGE_TRANSITION_ANIMATION];
            [userDefaults synchronize];
            [tableView reloadData];
            break;
        case SECTION_USERNAME:
            if (!self.isPeerConnected && indexPath.row == 1) {          
            ESDeviceNameViewController  *esDeviceNameVC = [[ESDeviceNameViewController alloc]initWithNibName:nil bundle:nil];
            [self.navigationController pushViewController:esDeviceNameVC animated:YES];
            }
        case SECTION_LASER_POINTER_OPTION:
            [userDefaults setValue:[NSNumber numberWithInteger:indexPath.row] forKey:KEY_LASER_POINTER];
            [userDefaults synchronize];
            [tableView reloadData];
            break;
       case SECTION_PRESENTATION_SETTINGS:
            
#if  X_AUDIENCE
            [self showAlert];
#endif

//            switch (indexPath.row) {
//                case 0:
//                    [userDefaults setBool:YES forKey:KEY_ATTENDEE_CAN_BROWSE];
//                    [userDefaults synchronize];
//                    [tableView reloadData];
//                    break;
//                case 1:
//                    [userDefaults setBool:YES forKey:KEY_ATTENDEE_CAN_QUERY];
//                    [userDefaults synchronize];
//                    [tableView reloadData];
//                    break;
//                default:
                    break;
//
        default:
            break;
    }
}

- (void) timerValueChanged:(UISwitch *) uiSwitch {
    [userDefaults setBool:uiSwitch.on forKey:KEY_DISPLAY_TIMER];
    [userDefaults synchronize];
}
-(void)slideValueChanged:(UISwitch *)uiswitch{
    [userDefaults setBool:uiswitch.on forKey:KEY_DISPLAY_SLIDE];
    [userDefaults synchronize];
}
- (void) defaultUsernameValueChanged:(UISwitch *) uiSwitch {
    [userDefaults setValue:[NSNumber numberWithBool:uiSwitch.on] forKey:KEY_USE_DEFAULT_USERNAME];
    [userDefaults synchronize];
    [_tableView beginUpdates];
    if (uiSwitch.on) {
        [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:SECTION_USERNAME]] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else {
        [_tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:SECTION_USERNAME]] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    [_tableView endUpdates];
}
-(void)switchValueChanged:(id)sender{
    
    UISwitch *switchView = sender;
    if (switchView.tag == 1001) {
        [userDefaults setBool:switchView.isOn forKey:KEY_ATTENDEE_CAN_BROWSE];
        [userDefaults synchronize];
    }
    if (switchView.tag == 1002){
        [userDefaults setBool:switchView.isOn forKey:KEY_ATTENDEE_CAN_QUERY];
        [userDefaults synchronize];
    }
}
#pragma Orientations Methods

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}
-(void)showAlert{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:@"To use this feature , purchase PresInteract Pro App from AppStore"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* purchaseButton = [UIAlertAction
                                   actionWithTitle:@"Purchase now"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                       
                                   }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                               actionWithTitle:@"Not now"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    
    [alert addAction:purchaseButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
//    
//    UIAlertView *alert=   [[UIAlertView alloc ]initWithTitle:@"" message:@"To use this feature , purchase PresInteract Pro App from AppStore" delegate:self cancelButtonTitle:@"Not now" otherButtonTitles:@"Purchase now", nil];
//    
//    alert.tag = UNAVAILABLE_FEATURE_ALERT_TAG;
//    
//    [alert show];
    
    return;

}
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if (alertView.tag == UNAVAILABLE_FEATURE_ALERT_TAG) {
//        if (!(alertView.cancelButtonIndex == buttonIndex)) {
//
//        }
//    }
//    return;
//}
#pragma mark - Product view controller delegate methods




@end
