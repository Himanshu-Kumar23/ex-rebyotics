//
//  ESPDFContentView.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 7/31/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ReaderContentView.h"
#import <MessageUI/MessageUI.h>

#import "ESAnnotationsLayer.h"
#if X_PRESENTER 
#import "ESPaintPaletteView.h"
#endif


#import "ESAbstractPagePacket.h"
#import "ESAnnotationPacket.h"
#import "ESLaserPointerPacket.h"
#import "ESZoomTransitionPacket.h"
#import "ESPageTransitionPacket.h"
#import "ESTextPacket.h"
#import "ESPicturePacket.h"

#define DEFAULT_WIDTH 8.0f
#define DEFAULT_ALPHA 1.0f

@class ESPDFContentView;

@protocol ESPDFContentViewPacketDelegate <NSObject>

- (void) transferPacket:(ESAbstractPagePacket *) packet fromContentView:(ESPDFContentView *) contentView;
- (void) transferPacket:(ESAbstractPacket *)packet;
@end

@protocol ESPDFContentViewCurveDelegate <NSObject>

- (void) processAnnotationPacket:(ESAnnotationPacket *) packet;
- (void) processTextWithPacket:(ESTextPacket *)packet;

-(void) processPictureWithPacket:(ESPicturePacket *)packet;

@end

@protocol ESPDFTapGestureRecognizerDelegate <NSObject>

-(void) handleSingleTapDelegateMethod:(UITapGestureRecognizer *)gesture;

#if X_PRESENTER
-(void) handleSingleTapToEditTextDelegateMethod:(UITapGestureRecognizer *)gesture;

-(void) presentTextEditViewFromMenuItemWithIndex:(NSInteger)index ;
-(void) deleteTextFromMenuItemWithPacket:(ESTextPacket *)packet;
-(void) deletePictureFromMenuItemWithpacket: (ESPicturePacket *) packet;
#endif

@end

#if X_PRESENTER
@interface ESPDFContentView : ReaderContentView<ESPaintPaletteDelegate>
#else
@interface ESPDFContentView : ReaderContentView
#endif

@property (nonatomic, unsafe_unretained) id<ESPDFContentViewPacketDelegate> packetDelegate;
@property (nonatomic, unsafe_unretained) id<ESPDFContentViewCurveDelegate> curveDelegate;
@property(nonatomic, unsafe_unretained)  id<ESPDFTapGestureRecognizerDelegate> gestureDelegate;
@property(nonatomic, readonly) UIPanGestureRecognizer * laserPointGestureRecognizer;
@property(nonatomic, readonly) UIPanGestureRecognizer * paintGestureRecognizer;
@property(nonatomic, readonly) UITapGestureRecognizer *singleTapGestureRecognizer;
@property(nonatomic) BOOL enableGestures;
@property(nonatomic) CGRect addStickyFrame;
@property(nonatomic , strong) NSMutableArray *addedLableArray;
@property(nonatomic , strong) NSMutableArray *addedLableIndexArray;

@property(nonatomic , strong) NSMutableArray *addedPictureArray;
- (void) showLaserPointerAtOffset:(CGPoint) point withLaserPointerType:(NSInteger)laserPointerType;

- (void) hideLaserPointer;

-(id) initWithFrame:(CGRect)frame fileURL:(NSURL *)fileURL page:(NSUInteger)page password:(NSString *)phrase;

- (ESAnnotationsLayer *) annotationsLayer;
-(void) addTextToPage:(NSMutableArray *)texts;
-(void) addPictureToPage:(NSMutableArray *)pictures;
-(void)clearText;
-(void) clearPicture;
#if X_PRESENTER 
@property(nonatomic, unsafe_unretained) ESPaintPaletteView *paintPalette;
#endif

- (void) processAnnotation:(ESAnnotationPacket *) annotation;
- (void) processLaserPointerPacket:(ESLaserPointerPacket *) packet;
-(void) processText:(ESTextPacket *)packet;

-(void) processPicture:(ESPicturePacket *)packet;

//-(void)setAnimationTransitions:(ESPageTransitionPacket *)pagePacket;

@end
