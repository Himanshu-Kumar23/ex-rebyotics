//
//  ESDocumentsCollectionViewController.m
//  iProjector
//
//  Created by Manoj Katragadda on 10/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESDocumentsCollectionViewController.h"
#import "ESAppDelegate.h"

#define dDeviceOrientation [[UIDevice currentDevice] orientation]
#define isPortrait  UIDeviceOrientationIsPortrait(dDeviceOrientation)
#define isLandscape UIDeviceOrientationIsLandscape(dDeviceOrientation)

@interface ESDocumentsCollectionViewController ()
{
    UICollectionView *esCollectionView;
    BOOL isEditButtonTapped;
    NSArray *array;

}
@end

@implementation ESDocumentsCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    array = [[NSArray alloc]init];
    isEditButtonTapped = NO;
    CGFloat navigationToolbarFrameHeight = self.navigationController.navigationBar.frame.size.height;
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    CGRect _frame = self.view.bounds;
    //lanscape
    BOOL iPad = NO;
#ifdef UI_USER_INTERFACE_IDIOM
    iPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#endif
    if (isLandscape) {
        if (iPad) {
            _frame.size.height = self.view.bounds.size.height - toolbar.bounds.size.height - navigationToolbarFrameHeight;
        }else
            _frame.size.height = self.view.bounds.size.width - toolbar.bounds.size.height - navigationToolbarFrameHeight;
    }else{
        if (iPad) {
            _frame.size.height = self.view.bounds.size.width - toolbar.bounds.size.height - navigationToolbarFrameHeight;
        }else
            _frame.size.height = self.view.bounds.size.height - toolbar.bounds.size.height - navigationToolbarFrameHeight;
    }
    //portrait
    esCollectionView.frame = _frame;

    esCollectionView = [[UICollectionView alloc]initWithFrame:_frame collectionViewLayout:layout];
    [self.view addSubview:esCollectionView];
//    esCollectionView.backgroundColor = [UIColor colorWithRed:108/255.f green:108/255.f blue:108/255.f alpha:1.0f];
    esCollectionView.backgroundColor = [UIColor whiteColor];
    [esCollectionView registerClass:[ESCollectionCell class] forCellWithReuseIdentifier:@"ESCollectionCell"];

    esCollectionView .autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |
    UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleBottomMargin |
    UIViewAutoresizingFlexibleWidth;

    esCollectionView.dataSource = self;
    esCollectionView.delegate = self;
    self.navigationItem.rightBarButtonItem = [self editBarButtonItem];

	// Do any additional setup after loading the view.
}
#pragma mark - UICollectionView Datasource

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    ESCollectionCell *cell = (ESCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    if ( !isEditButtonTapped) {
    NSString *fileName = [self.pdfFiles objectAtIndex:indexPath.row];
        
    ESAppDelegate *appDelegate = (ESAppDelegate *)[[UIApplication sharedApplication] delegate];
        
    if ([appDelegate.sessionState isEqualToString:@"1"])
    {
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter){
            [self sendPresentationInfoRequestToAllAttendeesForFile:fileName];
        }else {
            return;
        }
    }
    [self openFileName:fileName];
    }else{
        [collectionView reloadData];
    }
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section{
    
    return self.pdfFiles.count;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView{
    // according to file type 
    return 1;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ESCollectionCell *collectionCell=[cv dequeueReusableCellWithReuseIdentifier:@"ESCollectionCell" forIndexPath:indexPath];

    collectionCell.deleteCellDelegate = self;
    collectionCell.textLabel.text =[[self.pdfFiles objectAtIndex:indexPath.row] stringByDeletingPathExtension];
    
    array = [[NSBundle mainBundle] pathsForResourcesOfType:@"pdf" inDirectory:nil];
    UIImage *cellImage = [self buildThumbnailImage:MyGetPDFDocumentRef([array objectAtIndex:indexPath.row])];
    collectionCell.coverImageView.image = cellImage;


    if (isEditButtonTapped) {
        collectionCell.deleteOverLay.hidden = NO;
    }else{
        collectionCell.deleteOverLay.hidden = YES;
    }
    return collectionCell;
}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGSize retval =  CGSizeMake(105, 95);
    return retval;
}

// 
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(50, 20, 50, 20);
}
-(void)showButtonToDeleteCell:(ESCollectionCell *)cell{

    NSIndexPath *indexPath = [esCollectionView indexPathForCell:cell];
    [self removeFile:[self.pdfFiles objectAtIndex:indexPath.row]];
    [self.pdfFiles removeObjectAtIndex:indexPath.row];
    [esCollectionView reloadData];
}
- (UIBarButtonItem *) editBarButtonItem {
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(startEditingCollectionView:)];
}
-(void)startEditingCollectionView:(id)sender{
    isEditButtonTapped = YES;
    self.navigationItem.rightBarButtonItem = [self doneEditBarButtonItem];
    self.navigationItem.leftBarButtonItem = [self deletebarButton];
}
-(UIBarButtonItem *) deletebarButton{
    
    return [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteAllDocuments:)];
    
    UIImage *deleteImage = [UIImage imageNamed:@"document_delete"];
    UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteButton.bounds = CGRectMake( 0, 0, deleteImage.size.width, deleteImage.size.height );
    [deleteButton addTarget:self action:@selector(deleteAllDocuments:) forControlEvents:UIControlEventTouchUpInside];
    [deleteButton setTitle:@"Delete" forState:UIControlStateNormal];
    [deleteButton setBackgroundImage:deleteImage forState:UIControlStateNormal];
    [deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    deleteButton.titleLabel.font = [UIFont boldSystemFontOfSize:12.f];
    deleteButton.titleLabel.shadowOffset = CGSizeMake(1.0, 0.0);
    deleteButton.titleLabel.shadowColor = [UIColor blackColor];
    deleteButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |
    UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleTopMargin |
    UIViewAutoresizingFlexibleBottomMargin |
    UIViewAutoresizingFlexibleHeight;

    return [[UIBarButtonItem alloc]initWithCustomView:deleteButton];
}
- (UIBarButtonItem *) doneEditBarButtonItem {
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditingCollectionView:)];
}
- (void) doneEditingCollectionView:(id) sender {

    isEditButtonTapped = NO;
    [esCollectionView reloadData];
    self.navigationItem.leftBarButtonItem = [self importBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self editBarButtonItem];
}
-(IBAction)deleteAllDocuments:(id)sender{
    
    [self.pdfFiles removeAllObjects];
    [esCollectionView reloadData];
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    CGFloat navigationToolbarFrameHeight = self.navigationController.navigationBar.frame.size.height;
    toolbar.frame = CGRectMake(0, self.view.bounds.size.height - navigationToolbarFrameHeight, self.view.bounds.size.width, navigationToolbarFrameHeight);
    CGRect _frame = self.view.bounds;
   
    _frame.size.height = self.view.bounds.size.height - toolbar.frame.size.height;
    esCollectionView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - toolbar.frame.size.height);
    
}
- (UIImage *)buildThumbnailImage:(CGPDFDocumentRef)pdfDocument
{
    BOOL hasRetinaDisplay = FALSE;  // by default
    CGFloat pixelsPerPoint = 1.0;  // by default (pixelsPerPoint is just the "scale" property of the screen)
    
    if ([UIScreen instancesRespondToSelector:@selector(scale)])  // the "scale" property is only present in iOS 4.0 and later
    {
        // we are running iOS 4.0 or later, so we may be on a Retina display;  we need to check further...
        if ((pixelsPerPoint = [[UIScreen mainScreen] scale]) == 1.0)
            hasRetinaDisplay = FALSE;
        else
            hasRetinaDisplay = TRUE;
    }
    else
    {
        // we are NOT running iOS 4.0 or later, so we can be sure that we are NOT on a Retina display
        pixelsPerPoint = 1.0;
        hasRetinaDisplay = FALSE;
    }
    
    size_t imageWidth = 80;  // width of thumbnail in points
    size_t imageHeight = 60;  // height of thumbnail in points
    
    if (hasRetinaDisplay)
    {
        imageWidth *= pixelsPerPoint;
        imageHeight *= pixelsPerPoint;
    }
    
    size_t bytesPerPixel = 4;  // RGBA
    size_t bitsPerComponent = 8;
    size_t bytesPerRow = bytesPerPixel * imageWidth;
    
    void *bitmapData = malloc(imageWidth * imageHeight * bytesPerPixel);
    
    // in the event that we were unable to mallocate the heap memory for the bitmap,
    // we just abort and preemptively return nil:
    if (bitmapData == NULL)
        return nil;
    
    // remember to zero the buffer before handing it off to the bitmap context:
    bzero(bitmapData, imageWidth * imageHeight * bytesPerPixel);
    
    CGContextRef theContext = CGBitmapContextCreate(bitmapData, imageWidth, imageHeight, bitsPerComponent, bytesPerRow,
                                                    CGColorSpaceCreateDeviceRGB(), (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    
    //CGPDFDocumentRef pdfDocument = MyGetPDFDocumentRef();  // NOTE: you will need to modify this line to supply the CGPDFDocumentRef for your file here...
    CGPDFPageRef pdfPage = CGPDFDocumentGetPage(pdfDocument, 1);  // get the first page for your thumbnail
    
    CGAffineTransform shrinkingTransform =
    CGPDFPageGetDrawingTransform(pdfPage, kCGPDFMediaBox, CGRectMake(0, 0, imageWidth, imageHeight), 0, YES);
    
    CGContextConcatCTM(theContext, shrinkingTransform);
    
    CGContextDrawPDFPage(theContext, pdfPage);  // draw the pdfPage into the bitmap context
    CGPDFDocumentRelease(pdfDocument);
    
    //
    // create the CGImageRef (and thence the UIImage) from the context (with its bitmap of the pdf page):
    //
    CGImageRef theCGImageRef = CGBitmapContextCreateImage(theContext);
    free(CGBitmapContextGetData(theContext));  // this frees the bitmapData we malloc'ed earlier
    CGContextRelease(theContext);
    
    UIImage *theUIImage;
    
    // CAUTION: the method imageWithCGImage:scale:orientation: only exists on iOS 4.0 or later!!!
    if ([UIImage respondsToSelector:@selector(imageWithCGImage:scale:orientation:)])
    {
        theUIImage = [UIImage imageWithCGImage:theCGImageRef scale:pixelsPerPoint orientation:UIImageOrientationUp];
    }
    else
    {
        theUIImage = [UIImage imageWithCGImage:theCGImageRef];
    }
    
    CFRelease(theCGImageRef);
    return theUIImage;
}


CGPDFDocumentRef MyGetPDFDocumentRef(NSString *inputPDFFile)
{
    //NSString *inputPDFFile = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"test.pdf"];
    
    const char *inputPDFFileAsCString = [inputPDFFile cStringUsingEncoding:NSASCIIStringEncoding];
    //NSLog(@"expecting pdf file to exist at this pathname: \"%s\"", inputPDFFileAsCString);
    
    CFStringRef path = CFStringCreateWithCString(NULL, inputPDFFileAsCString, kCFStringEncodingUTF8);
    
    CFURLRef url = CFURLCreateWithFileSystemPath(NULL, path, kCFURLPOSIXPathStyle, 0);
    CFRelease (path);
    
    CGPDFDocumentRef document = CGPDFDocumentCreateWithURL(url);
    CFRelease(url);
    
    if (CGPDFDocumentGetNumberOfPages(document) == 0)
    {
        printf("Warning: No pages in pdf file \"%s\" or pdf file does not exist at this path\n", inputPDFFileAsCString);
        return NULL;
    }
    
    return document;
}

- (void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) removeFile:(NSString *) fileName {
}
#pragma Orientations Methods

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}

@end
