//
//  ESDeviceNameViewController.m
//  iProjector
//
//  Created by Manoj Katragadda on 7/23/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESDeviceNameViewController.h"

@interface ESDeviceNameViewController (){
    NSUserDefaults *userDefaults;
    UITextField *textField;
}

@end

@implementation ESDeviceNameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Name";
    //User defaults
    userDefaults = [NSUserDefaults standardUserDefaults];

    UILabel *enterDeviceName = [[UILabel alloc]initWithFrame:CGRectMake(30, 18, self.view.frame.size.width-50, 35)];
    enterDeviceName.text = @"Change username";
//    enterDeviceName.textColor = [UIColor darkGrayColor];
//    enterDeviceName.font = [UIFont systemFontOfSize:12.f];
    [self.view addSubview:enterDeviceName];
    textField = [[UITextField alloc] initWithFrame:CGRectMake(enterDeviceName.frame.origin.x, enterDeviceName.frame.size.height +enterDeviceName.frame.origin.y, self.view.frame.size.width - 50, 40)];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.font = [UIFont systemFontOfSize:15];
    textField.placeholder = @"Your name";
//    textField.center =  CGPointMake(self.view.center.x, textField.frame.origin.y);//self.view.center;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [textField becomeFirstResponder];
    [self.view addSubview:textField];
    textField.text = [userDefaults valueForKey:KEY_CUSTOM_USERNAME];
    textField .autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |
    UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleTopMargin |
    UIViewAutoresizingFlexibleBottomMargin |
    UIViewAutoresizingFlexibleHeight |
    UIViewAutoresizingFlexibleWidth;

    enterDeviceName .autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |
    UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleTopMargin |
    UIViewAutoresizingFlexibleBottomMargin |
    UIViewAutoresizingFlexibleHeight |
    UIViewAutoresizingFlexibleWidth;

    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(deviceNameChanged:)];
    self.navigationItem.rightBarButtonItem = doneButton;
    
    // Do any additional setup after loading the view.
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

-(void)deviceNameChanged:(id)sender{
    if (textField.text.length == 0) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please enter username"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        //[[[UIAlertView alloc]initWithTitle:nil message:@"Please enter username" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
    }else{
    [textField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
    [userDefaults setValue:textField.text forKey:KEY_CUSTOM_USERNAME];
    [userDefaults synchronize]; 
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma Orientations Methods

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}

@end
