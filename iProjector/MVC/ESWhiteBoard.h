//
//  ESWhiteBoard.h
//  iProjector
//
//  Created by Jayaprada Behera on 19/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESAnnotationsLayer.h"
@interface ESWhiteBoard : UIView
@property(nonatomic,strong)ESAnnotationsLayer *annotationsLayer;

@end
