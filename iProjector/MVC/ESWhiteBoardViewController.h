//
//  ESWhiteBoardViewController.h
//  iProjector
//
//  Created by Jayaprada Behera on 18/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESAnnotationPacket.h"
#import "ESSettingListViewController.h"
#import "ESPaintingDoneButtonPacket.h"
#import "ESTextPacket.h"
#import "ESPicturePacket.h"

@class ESWhiteBoardViewController;

@protocol ESWhiteBoardViewControllerDelegate <NSObject>
//- (void) ESWhiteBoardViewController:(ESWhiteBoardViewController *) viewController doneEditingWithText:(NSString *) text forAttendee:(NSString *)attendeeName;

- (void) doneWithWhiteBoardViewController:(ESWhiteBoardViewController *) viewController forpacket:(ESPaintingDoneButtonPacket *)packet;

-(void)transferPacketForAnnotationFromViewController:(ESWhiteBoardViewController *)viewController forPacket:(ESAnnotationPacket *)packet;

-(void)transferPacketForTextFromViewController:(ESWhiteBoardViewController *)viewController forPacket:(ESTextPacket *)packet;

-(void)transferPacketForPictureFromViewController:(ESWhiteBoardViewController *)viewController forPacket:(ESPicturePacket *)packet;


@end


@interface ESWhiteBoardViewController : UIViewController

@property(nonatomic,unsafe_unretained) id<ESWhiteBoardViewControllerDelegate> delegate;

//
- (void) processAnnotation:(ESAnnotationPacket *) annotation ;
-(void) processText:(ESTextPacket *)packet;
-(void) processPicture:(ESPicturePacket *)packet;
@end


/*
 
 If presenter,
 
 a. Show done button.

 b. Show paint pallete.
 c. Add paint gesture and convert touches to annotations.
 d. transfer packets to all devices.
 
 e. draw the annotations.
 
 If attendee,
 
 f. draw the annotations.
 
 */
