//
//  ESDocumentsListViewController.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 6/27/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "ESSessionBaseViewController.h"

@interface ESDocumentsListViewController : ESSessionBaseViewController<ESMultipeerManagerDelegate>{

}

@property(nonatomic,strong)  NSMutableArray *pdfFiles;
@property(nonatomic, strong) UITableView *tableView;

-(NSString *) pathForFileName:(NSString *) fileName;
-(NSMutableArray *)findFiles:(NSString *)extension ;

@end
