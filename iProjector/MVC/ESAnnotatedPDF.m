//
//  ESAnnotatedPDF.m
//  iProjector
//
//  Created by Jayaprada Behera on 13/11/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAnnotatedPDF.h"
#import "CoreText/CoreText.h"
#import "ESText1.h"
#import "ESAnnotationsLayer.h"
#import "ESPicture.h"

@implementation ESAnnotatedPDF


+(void)drawPDF:(NSString*)fileName  forURL:(NSURL *)fileUrl  withCurveArray:(NSArray *)pagesCurvesArray withTextArray:(NSArray *)pagesAddTextArray withPictureArray:(NSArray *)pagesPictureArray {
    
    CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((__bridge CFURLRef)fileUrl);
    
    const size_t numberOfPages = CGPDFDocumentGetNumberOfPages(pdf);
    
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    
    
    for(size_t page = 0; page < numberOfPages; page++)
    {
        CGContextSaveGState(pdfContext);
        
        //	Get the current page and page frame
        
        CGPDFPageRef pdfPage = CGPDFDocumentGetPage(pdf, page+1);
        const CGRect pageFrame = CGPDFPageGetBoxRect(pdfPage, kCGPDFMediaBox);

        UIGraphicsBeginPDFPageWithInfo(pageFrame, nil);

        //	Draw the page (flipped)
        CGContextSetRGBFillColor(pdfContext, 1.0,1.0,1.0,1.0);
        CGContextScaleCTM(pdfContext, 1, -1);
        CGContextTranslateCTM(pdfContext, 0, -pageFrame.size.height);
        CGContextDrawPDFPage(pdfContext, pdfPage);
        
        CGContextRestoreGState(pdfContext);
        
        ESAnnotationsLayer *layer = [ESAnnotationsLayer layer];
        layer.frame = UIGraphicsGetPDFContextBounds();
        NSMutableArray *curvesArray = [pagesCurvesArray objectAtIndex:page];
        [layer addCurves:curvesArray];
        [layer drawInContext:pdfContext];
//        NSLog(@"Page %zu",page);
        
        NSArray *arr_text = [pagesAddTextArray objectAtIndex:page];
        for (ESText1 *text in arr_text) {
            [self drawLabel:text withPageHeight:pageFrame.size.height];
        }
        NSArray *arr_pic = [pagesPictureArray objectAtIndex:page];
        for (ESPicture *pic in arr_pic) {
//            CGContextTranslateCTM(pdfContext, 0,pageFrame.size.height);
//            CGContextScaleCTM(pdfContext, 1.0, -1.0);

            CGContextDrawImage(pdfContext, pic.pictureFrame, pic.picture.CGImage);
//            CGContextScaleCTM(pdfContext, 1.0, -1.0);
//            CGContextTranslateCTM(pdfContext, 0, -pageFrame.size.height);

//            [self drawPicture:pic];
        }
        
        CGContextSaveGState(pdfContext);

        CGContextRestoreGState(pdfContext);
    }
    UIGraphicsEndPDFContext();

}

+(void)drawImage:(UIImage*)image inRect:(CGRect)rect{
    
    [image drawInRect:rect];
    
}

+(void)drawLabel:(ESText1 *)text withPageHeight:(CGFloat)height{

    
    NSString *textToDraw = text.textValue;
    CGRect frameRect = text.textFrame;
    
    if(textToDraw.length == 0){
        return; //To Avoid crashing
    }
    
    CFStringRef stringRef = (__bridge CFStringRef)textToDraw;
    // Prepare the text using a Core Text Framesetter
    CFAttributedStringRef currentText = CFAttributedStringCreate(NULL, (CFStringRef)stringRef, NULL);

    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(currentText);
    
    CGMutablePathRef framePath = CGPathCreateMutable();
    CGPathAddRect(framePath, NULL, frameRect);
    
    // Get the frame that will do the rendering.
    CFRange currentRange = CFRangeMake(0, 0);
    CTFrameRef frameRef = CTFramesetterCreateFrame(framesetter, currentRange, framePath, NULL);
    CGPathRelease(framePath);
    
    // Get the graphics context.
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();

    // Put the text matrix into a known state. This ensures
    CGContextSetTextMatrix(currentContext, CGAffineTransformMakeScale(1.0, -1.0));
    //set text fontsize and color
	CGContextSelectFont(currentContext, "Helvetica", text.fontSize, kCGEncodingFontSpecific);
    CGContextSetStrokeColorWithColor(currentContext, text.color.CGColor);
	CGContextSetTextDrawingMode(currentContext, kCGTextStroke);
    
    // Core Text draws from the bottom-left corner up, so flip
    // the current transform prior to drawing.
    
    CGContextTranslateCTM(currentContext, 0, height);
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    // Draw the frame.
    CTFrameDraw(frameRef, currentContext);
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    CGContextTranslateCTM(currentContext, 0, (-1)*height);

    CFRelease(frameRef);
    CFRelease(stringRef);
    CFRelease(framesetter);

    
}


+(void)drawPicture:(ESPicture *)picture{
    
    UIImage* logo = picture.picture;
    [self drawImage:logo inRect:picture.pictureFrame];
}


@end
