//
//  ESSettingListViewController.h
//  iProjector
//
//  Created by WebileApps on 17/07/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

#define KEY_USER_ROLE                   @"keyUserRole"
#define KEY_PAGE_TRANSITION_ANIMATION   @"keyPageTransitionAnimation"
#define KEY_DISPLAY_TIMER               @"keyDisplayTimer"
#define KEY_USE_DEFAULT_USERNAME        @"keyUseDefaultUserName"
#define KEY_CUSTOM_USERNAME             @"keyCustomUserName"
#define KEY_LASER_POINTER               @"keyLaserPointer"
#define KEY_ATTENDEE_CAN_QUERY          @"keyAttendeeCanQuery"
#define KEY_ATTENDEE_CAN_BROWSE          @"keyAttendeeCanBrowse"
#define KEY_DISPLAY_SLIDE               @"keyDisplaySlide"
#define X_PRESENTER_APP_ID     915200116

typedef NS_ENUM (NSInteger, ESUserRole){
    ESUserRoleAttendee = 0,
    ESUserRolePresenter = 1
};

typedef NS_ENUM (NSInteger, ESPageTransitionAnimation) {
    ESPageTransitionAnimationNone,
    ESPageTransitionAnimationCurl,
    ESPageTransitionAnimationFlip,
    ESPageTransitionAnimationMoveIn,
    ESPageTransitionAnimationPush,
    ESPageTransitionAnimationReveal,
    ESPageTransitionAnimationFade,
    ESPageTransitionAnimationRandom,
};

typedef NS_ENUM (NSInteger, ESLaserPointer){
    ESLaserPointerGreen,
    ESLaserPointerRed
};

@interface ESSettingListViewController : UIViewController
{
}

@property(nonatomic)BOOL isPeerConnected;

@end
