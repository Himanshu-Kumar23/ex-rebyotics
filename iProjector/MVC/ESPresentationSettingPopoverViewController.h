//
//  ESPresentationSettingPopoverViewController.h
//  iProjector
//
//  Created by Manoj Katragadda on 13/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESSettingListViewController.h"

@protocol ESAttendeeQuestionsDelegate <NSObject>

//- (void) attendeesQueriesTurnedOn:(BOOL) status;
//- (void) attendeesBrowseTurnedOn:(BOOL) status;
-(void) attendeesBrowseTurnedOn:(BOOL)status andQuery:(BOOL)qStatus;
@end

@interface ESPresentationSettingPopoverViewController : UITableViewController
@property(nonatomic, unsafe_unretained) id<ESAttendeeQuestionsDelegate> delegate;

@end
