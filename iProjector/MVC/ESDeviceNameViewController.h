//
//  ESDeviceNameViewController.h
//  iProjector
//
//  Created by Manoj Katragadda on 7/23/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#define KEY_CUSTOM_USERNAME             @"keyCustomUserName"

@interface ESDeviceNameViewController : UIViewController

@end
