//
//  ESPaintPalette.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/3/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>

#define UNDO_BUTTON 5

#define ERASER_BUTTON   5

@class ESPaintPaletteView;
@protocol ESPaintPaletteDelegate <NSObject>

@optional
- (void) undoButtonClickedOnPalette:(ESPaintPaletteView *) palette;

- (void) colorSelectionChangedTo:(UIColor *) newColor onPalette:(ESPaintPaletteView *) palette;
- (void) thicknessSelectionChangedTo:(CGFloat) newThickness onPalette:(ESPaintPaletteView *) palette;
-(void) clearButtonClicked:(ESPaintPaletteView *) pallette;

-(void) addTextButtonClicked:(ESPaintPaletteView *)pallete;

-(void) addPictureButtonClicked:(ESPaintPaletteView *)pallete;

-(void) highlightTextButtonClicked:(ESPaintPaletteView *)pallete;

@end

@interface ESPaintPaletteView : UIView

@property(weak, nonatomic, readonly) UIColor * selectedColor;
@property(nonatomic) CGFloat selectedThickness;

@property(nonatomic, unsafe_unretained) id<ESPaintPaletteDelegate> delegate;

@property (weak, nonatomic, readonly) UIButton *undoButton;
@property (weak, nonatomic, readonly) UIButton *annotationButton;
@property (weak, nonatomic, readonly) UIButton *clearPageButton;
@property (weak, nonatomic, readonly) UIButton *addTextButton;
@property (weak, nonatomic, readonly) UIButton *addPictureButton;
@property (weak, nonatomic, readonly) UIButton *highlightTextButton;

- (id)initWithFrame:(CGRect)frame isiPodPortrait:(BOOL)isPotrait;
-(void)initWithFrame:(CGRect)frame forIsPotrait:(BOOL)isPotrait;
@end
