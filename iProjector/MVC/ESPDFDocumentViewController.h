//
//  ESPDFDocumentViewController.h
//  iProjector
//
//  Created by WebileApps on 30/07/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderViewController.h"
#import <MessageUI/MessageUI.h>
#import "ESPDFContentView.h"
#import "ESPresentationSettingPopoverViewController.h"
#import "ESWhiteBoardViewController.h"
#import "ESVideoPlayerViewController.h"
#import "ESTextPopoverViewController.h"
#import "ESPaintPaletteView.h"

#import "ESMultipeerManager.h"
#import "ESAnswerPacket.h"


#define RENAME_ANNOTATED_DOC_ALERT_TAG                 1050

#define Attendee_Query_Posted     @"ESAttendeeQueryPosted"
#define Attendee_Answer_Posted    @"ESAttendeeAnswerPoster"
//#define X_PRESENTER_APP_ID     915200116 //already defined in baseVC


@protocol ESPDFViewControllerSessionDataDelegate <NSObject>

- (BOOL) handlePacket:(ESAbstractPacket *) packet fromPeerWithId:(MCPeerID *) peerId;

@end

typedef NS_ENUM (NSInteger,pdfDataType) {
    //    kPdfDataScrollViewTouchesForLaserPoint,
    kPdfDataForScrollViewPageChanges,
    kPdfDataForScrollViewZooming,
    kPdfDataForPageControllerChanges,
    kPdfDataForPagetouchEnded
};

struct pdfInfo
{
    CGPoint  scrollViewOffSet;
    CGFloat  ZoomValue;
    NSInteger  headerValue;
    NSInteger page;
    NSInteger deviceType;
    CGFloat  senderMinimumZoomValue;
} ;
#if X_PRESENTER
@interface ESPDFDocumentViewController : ReaderViewController <ESPDFContentViewPacketDelegate,MFMailComposeViewControllerDelegate,ESPDFTapGestureRecognizerDelegate,ESWhiteBoardViewControllerDelegate,ESVideoPlayerViewControllerDelegate,ESAddTextPopoverDelegate,UIPopoverControllerDelegate>
#else
@interface ESPDFDocumentViewController : ReaderViewController <ESPDFContentViewPacketDelegate,MFMailComposeViewControllerDelegate,ESPDFTapGestureRecognizerDelegate,ESWhiteBoardViewControllerDelegate,ESVideoPlayerViewControllerDelegate>

#endif
{
    //An array which holds array of pages.
    NSMutableArray *pagesCurvesArray;
    //UIPopoverController *settingsPopoverVC;
    
    ESPresentationSettingPopoverViewController *settingsPopOverView;
    
    NSMutableArray *pagesAddTextArray;
    
    NSMutableArray *pagesPictureArray;
}

//@property(nonatomic,strong)GKSession *gkSession;
@property(nonatomic, strong) ESMultipeerManager *multipeerManager;
@property(nonatomic,strong) MCPeerID *presenterPeerId;


@property (nonatomic, strong) UILabel *progress;
@property (nonatomic, assign) NSInteger currMinute;
@property (nonatomic, assign) NSInteger currSeconds;
@property (nonatomic, assign) NSInteger secondsSinceStart;
@property (nonatomic, strong) NSString *viewTitle;
@property (nonatomic, strong) NSMutableArray *presenterQuestionsAsked;

//- (id) initWithReaderDocument:(ReaderDocument *)document withSession:(BKSessionController *)session;
- (id) initWithReaderDocument:(ReaderDocument *)document withSession:(ESMultipeerManager *)manager;
- (void)manager:(ESMultipeerManager *)manager didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID;

@property (nonatomic) BOOL attendeeCanQuery;
@property (nonatomic) BOOL attendeeCanBrowse;

@property (nonatomic, assign) id<ESPDFViewControllerSessionDataDelegate> sessionDataDelegate;

@end
