//
//  ESQAPdfDocument.m
//  iProjector
//
//  Created by Manoj Katragadda on 15/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESQAPdfDocument.h"

@implementation ESQAPdfDocument

+(void) drawPDFWithFileName:(NSString *)filename forQuestionsAnswer:(NSString *)QuestionsAnswer{
    
    CFAttributedStringRef currentText = CFAttributedStringCreate(NULL, (CFStringRef)QuestionsAnswer, NULL);
    
    if (currentText) {
        CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(currentText);
        if (framesetter) {
            
            NSString *pdfFileName = filename;
            // Create the PDF context using the default page size of 612 x 792.
            UIGraphicsBeginPDFContextToFile(pdfFileName, CGRectZero, nil);
            
            CFRange currentRange = CFRangeMake(0, 0);
            NSInteger currentPage = 0;
            BOOL done = NO;
            
            do {
                // Mark the beginning of a new page.
                UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 612, 792), nil);
                
                // Draw a page number at the bottom of each page.
                currentPage++;
                [self drawPageNumber:currentPage];
                
                // Render the current page and update the current range to
                // point to the beginning of the next page.
                //                currentRange = [self renderPage:currentPage WithTextRange:currentRange andFramesetter:framesetter];
                currentRange = [self renderPage:currentPage withTextRange:currentRange andFramesetter:framesetter];
                
                // If we're at the end of the text, exit the loop.
                if (currentRange.location == CFAttributedStringGetLength((CFAttributedStringRef)currentText))
                    done = YES;
            } while (!done);
            
            // Close the PDF context and write the contents out.
            UIGraphicsEndPDFContext();
            
            // Release the framewetter.
            CFRelease(framesetter);
            
        } else {
//            NSLog(@"Could not create the framesetter needed to lay out the atrributed string.");
        }
        // Release the attributed string.
        CFRelease(currentText);
    } else {
//        NSLog(@"Could not create the attributed string for the framesetter");
    }
}
// Use Core Text to draw the text in a frame on the page.
+ (CFRange)renderPage:(NSInteger)pageNum withTextRange:(CFRange)currentRange
       andFramesetter:(CTFramesetterRef)framesetter
{
    // Get the graphics context.
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();
    
    // Put the text matrix into a known state. This ensures
    // that no old scaling factors are left in place.
    CGContextSetTextMatrix(currentContext, CGAffineTransformIdentity);
    
    // Create a path object to enclose the text. Use 72 point
    // margins all around the text.
    CGRect    frameRect = CGRectMake(72, 72, 468, 648);
    CGMutablePathRef framePath = CGPathCreateMutable();
    CGPathAddRect(framePath, NULL, frameRect);
    
    // Get the frame that will do the rendering.
    // The currentRange variable specifies only the starting point. The framesetter
    // lays out as much text as will fit into the frame.
    CTFrameRef frameRef = CTFramesetterCreateFrame(framesetter, currentRange, framePath, NULL);
    CGPathRelease(framePath);
    
    // Core Text draws from the bottom-left corner up, so flip
    // the current transform prior to drawing.
    CGContextTranslateCTM(currentContext, 0, 792);
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    
    // Draw the frame.
    CTFrameDraw(frameRef, currentContext);
    
    // Update the current range based on what was drawn.
    currentRange = CTFrameGetVisibleStringRange(frameRef);
    currentRange.location += currentRange.length;
    currentRange.length = 0;
    CFRelease(frameRef);
    
    return currentRange;
}

+ (void)drawPageNumber:(NSInteger)pageNum
{
    NSString *pageString = [NSString stringWithFormat:@"Page %ld", (long)pageNum];
    UIFont *theFont = [UIFont systemFontOfSize:12];
    CGSize maxSize = CGSizeMake(612, 72);
    
//    CGSize pageStringSize = [pageString sizeWithFont:theFont
//                                   constrainedToSize:maxSize
//                                       lineBreakMode:ES_LINE_BREAK_WORD_WRAP];
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:pageString
                                                                         attributes:@{NSFontAttributeName:theFont}];
    
    CGRect rect = [attributedText boundingRectWithSize:maxSize
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize pageStringSize = rect.size;
    
    CGRect stringRect = CGRectMake(((612.0 - pageStringSize.width) / 2.0),
                                   720.0 + ((72.0 - pageStringSize.height) / 2.0),
                                   pageStringSize.width,
                                   pageStringSize.height);
    
    //[pageString drawInRect:stringRect withFont:theFont];
    
    NSDictionary *attributes = @{ NSFontAttributeName: theFont};
    
    [pageString drawInRect:stringRect withAttributes:attributes];
    
    
}
/*
 Question 1. What is your age?
 Type - Multiple Choice
 Options
 1. less than 20
 2. between 20 and 40
 3. greater than 40
 Answers #6
 1. 16.7%
 2. 66.7%
 3. 16.7%
 Question 2. .....
 .....
 .....
 
 */

@end
