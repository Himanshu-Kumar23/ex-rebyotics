//
//  ESWhiteBoardViewController.m
//  iProjector
//
//  Created by Jayaprada Behera on 18/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESWhiteBoardViewController.h"

#import "ESAnnotationsLayer.h"
#import "ESWhiteBoard.h"

#import "ESPaintPaletteView.h"
#import "ESTextPopoverViewController.h"

#import "ESPicturePacket.h"

#define Packect_Frame_size_iPhone    200.f
#define Packect_Frame_size_iPad      320.f

@interface ESWhiteBoardViewController ()<UIPopoverControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,ESAddTextPopoverDelegate,UIActionSheetDelegate, UIPopoverPresentationControllerDelegate>
{
    
    ESPaintPaletteView *paintPalette;
    //UIPopoverController *textPopoverController;
    //UIPopoverController *imagePopover;
    ESTextPopoverViewController *textPopoverView;
    ESTextPopoverViewController *addTextPopoverView;
    UIImagePickerController *imagePicker;
    NSInteger picMenuIndex;
    NSInteger menuIndex;
    
    ESWhiteBoard *whiteBoard;
}
@property(nonatomic , strong) NSMutableArray *addedPictures;
@property(nonatomic , strong) NSMutableArray *addedLabels;
@property(nonatomic , strong) NSMutableArray *lableIndexArray;

@end

@implementation ESWhiteBoardViewController
@synthesize addedLabels;
@synthesize addedPictures;
@synthesize lableIndexArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(CGRect)frameOfPaintPallete{
    CGFloat height = 0;
    
    BOOL iPad = NO;
    BOOL isPotrait = NO;
//    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
#ifdef UI_USER_INTERFACE_IDIOM
    iPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#endif
    if (!iPad) {
        
#ifdef UIDeviceOrientationIsPortrait//(orientation)
        isPotrait = (UIDeviceOrientationIsPortrait(orientation) == UIDeviceOrientationPortrait ||UIDeviceOrientationIsPortrait(orientation) == UIDeviceOrientationPortraitUpsideDown);
#endif
    }
    
    CGFloat width = 0.f;
    if (!iPad){
        if( isPotrait ) {
            height = 130.f;
            width = 250.f;
        }else{
            width = 385.f;
            height =  80.f;
        }
    }else{//for iPAD
        width = 385.f;
        height =  80.f;
    }
    CGRect frame = CGRectMake(self.view.frame.size.width - width, self.view.bounds.size.height - self.navigationController.navigationBar.frame.size.height - height, width, height);
    
    return frame;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.addedPictures = [[NSMutableArray alloc] init];
    self.addedLabels = [[NSMutableArray alloc]init];
    self.lableIndexArray = [[NSMutableArray alloc] init];
    
    whiteBoard = [[ESWhiteBoard alloc] initWithFrame:self.view.bounds];
    whiteBoard.backgroundColor = [UIColor whiteColor];
    whiteBoard .autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:whiteBoard];
    
    if (_delegate) {
        
#if X_PRESENTER
//        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        BOOL isPotrait = NO;
        BOOL iPad = NO;
        
#ifdef UI_USER_INTERFACE_IDIOM
        iPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#endif
        if (!iPad) {
            
#ifdef UIDeviceOrientationIsPortrait//(orientation)
            isPotrait = (UIDeviceOrientationIsPortrait(orientation) == UIDeviceOrientationPortrait ||UIDeviceOrientationIsPortrait(orientation) == UIDeviceOrientationPortraitUpsideDown);
#endif
        }
        
        paintPalette = [[ESPaintPaletteView alloc]initWithFrame:[self frameOfPaintPallete] isiPodPortrait:isPotrait];
        paintPalette.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
        [paintPalette.annotationButton addTarget:self action:@selector(annotationButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [paintPalette.undoButton addTarget:self action:@selector(undoButtonClickedOnPalette:) forControlEvents:UIControlEventTouchUpInside];
        [paintPalette.clearPageButton addTarget:self action:@selector(clearButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [paintPalette.addTextButton addTarget:self action:@selector(textAddButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [paintPalette.addPictureButton addTarget:self action:@selector(pictureAddButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [paintPalette.highlightTextButton addTarget:self action:@selector(highlightTextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:paintPalette];
#endif
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonTapped:)];
        self.navigationItem.rightBarButtonItem = doneButton;
        
        UIPanGestureRecognizer *paintPanGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panGestureRecognizerDidRecognize:)];
        [whiteBoard addGestureRecognizer:paintPanGesture];
    }
    
    self.title = @"White board";
}
#pragma mark - UIButtonActions

#if X_PRESENTER
-(IBAction)annotationButtonClicked:(id)sender{
    [self.view bringSubviewToFront:paintPalette];
}
-(IBAction)doneButtonTapped:(id)sender{
    
    [paintPalette.undoButton removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [paintPalette.clearPageButton removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    ESPaintingDoneButtonPacket *packet = [ESPaintingDoneButtonPacket packectToDismissWhiteBoard];
    [self.delegate doneWithWhiteBoardViewController:self forpacket:packet];
}

- (IBAction)undoButtonClickedOnPalette:(id) undoButton {
    ESAnnotationPacket *annotation = [ESAnnotationPacket undoPacket];
    
    [self processAnnotation:annotation];
    
    [self.delegate transferPacketForAnnotationFromViewController:self forPacket:annotation];
    
}

-(void) clearButtonClicked:(UIButton *)pallette{
    ESAnnotationPacket *annotation = [ESAnnotationPacket clearPacket];
    
    //send packet across.
    if (self.delegate) {
        [self.delegate transferPacketForAnnotationFromViewController:self forPacket:annotation];
    }
    [self processAnnotation:annotation];
    
}

#endif

-(IBAction)highlightTextButtonClicked:(id)sender{
}
-(IBAction)pictureAddButtonTouchUpInside:(id)sender{
    
    UIButton *b = sender;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                          message:nil
                                                   preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                           style:UIAlertActionStyleCancel
                                         handler:^(UIAlertAction *action) {
                                             // do something here
                                         }];
    
    UIAlertAction *libraryAction = [UIAlertAction actionWithTitle:@"Library"
                                           style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action) {
                                             // do something here
                                             if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
                                                 
                                                 imagePicker =[[UIImagePickerController alloc] init];
                                                 imagePicker.delegate = self;
                                                 imagePicker.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
                                                 imagePicker.allowsEditing = NO;
                                                 
                                                 if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                                                     
#if X_PRESENTER
                                                     
                                                     UIPopoverPresentationController * imagePopover = [imagePicker popoverPresentationController];
                                                     imagePopover.sourceRect = CGRectMake(0, 0, 320, 230);
                                                     imagePopover.sourceView = self.view;
                                                     imagePopover.permittedArrowDirections = UIPopoverArrowDirectionAny;
                                                     [self presentViewController:imagePicker animated:YES completion:nil];

//                                                     imagePopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
//                                                     [imagePopover presentPopoverFromRect:CGRectMake(0, 0, 320, 230) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
#endif
                                                     
                                                 } else {
                                                     [self presentViewController:imagePicker animated:YES completion:nil];
                                                 }
                                             }
                                         }];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Camera"
                                           style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action) {
                                             // do something here
                                             if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                                                 
                                                 imagePicker =[[UIImagePickerController alloc] init];
                                                 imagePicker.delegate = self;
                                                 imagePicker.sourceType =UIImagePickerControllerSourceTypeCamera;
                                                 imagePicker.allowsEditing = NO;
                                                 
                                                 if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
#if X_PRESENTER
                                                     UIPopoverPresentationController *imagePopover = [imagePicker popoverPresentationController];
                                                     imagePopover.sourceView = self.view;
                                                     imagePopover.sourceRect = CGRectMake(0, 0, 320, 230);
                                                     imagePopover.permittedArrowDirections = UIPopoverArrowDirectionAny;
                                                     [self presentViewController:imagePicker animated:YES completion:nil];

//                                                     imagePopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
//                                                     [imagePopover presentPopoverFromRect:CGRectMake(0, 0, 320, 230) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
#endif
                                                     
                                                 } else {
                                                     [self presentViewController:imagePicker animated:YES completion:nil];
                                                     
                                                 }
                                             }
                                         }];
    // note: you can control the order buttons are shown, unlike UIActionSheet
    [alertController addAction:cancelAction];
    [alertController addAction:libraryAction];
    [alertController addAction:cameraAction];
    [alertController setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alertController
                                                     popoverPresentationController];
    popPresenter.sourceView = b;
    popPresenter.sourceRect = b.bounds;
    [self presentViewController:alertController animated:YES completion:nil];
    
//    UIActionSheet * addPictureActionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Library",@"Camera", nil];
//    [addPictureActionSheet showInView:self.view];
//    [addPictureActionSheet showFromRect:b.frame inView:self.view animated:YES];
}
-(IBAction)textAddButtonTouchUpInside:(id)sender{
    
    ESTextPacket *packet = [ESTextPacket  packetWithStartState:ESTextStateStartText];
    [self processText:packet];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        
        
        //If the device type is ipad,show a popoverVC
        if (addTextPopoverView == nil) {
            
            addTextPopoverView = [[ESTextPopoverViewController alloc]init];
            
            addTextPopoverView.delegate = self;
            addTextPopoverView.fontSize = 14.f;
            addTextPopoverView.indexOfText = [self.addedLabels indexOfObject:[self.addedLabels lastObject]];
            addTextPopoverView.fromPdf = NO;
            
            UIPopoverPresentationController *textPopoverController = [addTextPopoverView popoverPresentationController];
            textPopoverController.sourceRect = self.addFrame;
            textPopoverController.sourceView = self.view;
            textPopoverController.delegate = self;
            textPopoverController.permittedArrowDirections = UIPopoverArrowDirectionAny;
            
            
//            textPopoverController = [[UIPopoverController alloc] initWithContentViewController:addTextPopoverView];
//            textPopoverController.delegate = self;
//            [textPopoverController setPopoverContentSize:CGSizeMake(280,400.0f)];
//            
//            [textPopoverController presentPopoverFromRect:self.addFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }else{
            //[textPopoverController dismissPopoverAnimated:YES];
            [[addTextPopoverView presentingViewController] dismissViewControllerAnimated:YES completion:nil];
        }
    }else{ //iPhone or iPod
        
        textPopoverView = [[ESTextPopoverViewController alloc]init];
        textPopoverView.indexOfText = [ self.addedLabels indexOfObject:[self.addedLabels lastObject]];
        textPopoverView.delegate = self;
        textPopoverView.fontSize = 14.f;
        textPopoverView.fromPdf = NO;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:textPopoverView];
        [self presentViewController:navController animated:YES completion:nil];
    }
    
}
-(void)cancelAddTextFromPopOver:(ESTextPopoverViewController *)vc {
    
    if (addTextPopoverView != nil) {
        //[textPopoverController dismissPopoverAnimated:YES];
        [[addTextPopoverView presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        addTextPopoverView = nil;
    }
}
-(void)doneWithTextPopover:(ESTextPopoverViewController *)viewController withPacket:(ESTextPacket *)packet{
    [self processText:packet];
    if (self.delegate) {
        [self.delegate transferPacketForTextFromViewController:self forPacket:packet];
    }
    if (textPopoverView != nil) {
        //[textPopoverController dismissPopoverAnimated:YES];
        [[textPopoverView presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        textPopoverView = nil;
    }
}
#pragma mark - Local Methods

- (void) processAnnotation:(ESAnnotationPacket *) annotation {
    switch (annotation.annotationType) {
        case ESAnnotationTypeHighlightStart:
        case ESAnnotationTypeStart:
            [whiteBoard.annotationsLayer startCurveAtPoint:annotation.point withColor:annotation.color andLineWidth:annotation.lineWidth];
            break;
        case ESAnnotationTypeMove:
            [whiteBoard.annotationsLayer moveCurveToPoint:annotation.point];
            break;
        case ESAnnotationTypeEnd:
            [whiteBoard.annotationsLayer endCurveAtPoint:annotation.point];
            break;
        case ESAnnotationTypeUndo:
            [whiteBoard.annotationsLayer removeLastCurve];
            break;
        case ESAnnotationTypeHighlightMove:
            [whiteBoard.annotationsLayer moveHighlightCurveToPoint:annotation.point];
            break;
        case ESAnnotationTypeHighlightEnd:
            [whiteBoard.annotationsLayer endHighlightCurveAtPoint:annotation.point];
            break;
        case ESAnnotationTypeClear:
            [whiteBoard.annotationsLayer clear];
        default:
            break;
    }
}
-(CGRect)addFrame{
    BOOL iPad = NO;
#ifdef UI_USER_INTERFACE_IDIOM
    iPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#endif
    if(iPad){
        return CGRectMake((whiteBoard.frame.size.width - Packect_Frame_size_iPad)/2, whiteBoard.frame.size.height/2 - Packect_Frame_size_iPad/2, Packect_Frame_size_iPad,Packect_Frame_size_iPad);
    }
    return CGRectMake((whiteBoard.frame.size.width - Packect_Frame_size_iPhone)/2, whiteBoard.frame.size.height/2 - Packect_Frame_size_iPhone/2, Packect_Frame_size_iPhone,Packect_Frame_size_iPhone);
    
}

-(void) processText:(ESTextPacket *)packet{
    //put a lable into an array and according to the index of the array make editing of the lable with object
    
    if (packet.textState == ESTextStateStartText) {
        
        UILabel *labelText = [[UILabel alloc] init];
        labelText.text = packet.text;
        labelText.textColor = packet.textColor;
        labelText.font = [UIFont systemFontOfSize:packet.textFontSize];
        if (packet.text.length == 0) {
            labelText.frame = [self addFrame];
            
        }else{
            labelText.frame = packet.textFrame;
        }
        labelText.backgroundColor = [UIColor clearColor];
        labelText.numberOfLines = 0;
        
        labelText.layer.borderColor = [UIColor darkGrayColor].CGColor;
        labelText.layer.borderWidth = 1.5f;
        labelText.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |  UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;

        [self.view addSubview:labelText];
        [self.addedLabels addObject:labelText];
        
#if X_PRESENTER
        
        labelText.userInteractionEnabled = YES;
        UITapGestureRecognizer  *singleTapOnTextLabel = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTapGestureRecognizerOnTextLabel:)];
        singleTapOnTextLabel.numberOfTouchesRequired = 1;
        singleTapOnTextLabel.numberOfTapsRequired = 1;
        
        [labelText addGestureRecognizer:singleTapOnTextLabel];
        
        UIPanGestureRecognizer *panGextureAddTextView = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveText:)];
        panGextureAddTextView.enabled = YES;
        panGextureAddTextView .minimumNumberOfTouches = 1;
        [labelText addGestureRecognizer:panGextureAddTextView];
        
#endif
        
    }else if (packet.textState == ESTextStateTextChange){
        
        UILabel *l = [self.addedLabels objectAtIndex:packet.indexValue];
        l.text = packet.text;
        
    }else if (packet.textState == ESTextStateAddTextDone){//done button tapped
        
        UILabel *l;
        if ([[NSUserDefaults standardUserDefaults] integerForKey:KEY_USER_ROLE] == ESUserRoleAttendee  && packet.session_attendee) {
            if (![self.lableIndexArray containsObject:[NSNumber numberWithInteger:packet.indexValue]] ) {
                [self.lableIndexArray addObject:[NSNumber numberWithInteger:packet.indexValue]];
                l = [[UILabel alloc] init];
                l.backgroundColor = [UIColor clearColor];
                l.numberOfLines = 0;
                l.frame = [self addFrame];
                
                l.layer.borderColor = [UIColor darkGrayColor].CGColor;
                l.layer.borderWidth = 1.5f;
                l.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |  UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
                [self .view addSubview:l];
                [self.addedLabels addObject:l];
            }
        }
        l = [self.addedLabels objectAtIndex:packet.indexValue];
        l.text = packet.text;
        l.textColor = packet.textColor;
        l.font = [UIFont systemFontOfSize:packet.textFontSize];
        //CGSize textSize = [l.text sizeWithFont:l.font constrainedToSize:CGSizeMake(l.frame.size.width, MAXFLOAT) lineBreakMode:l.lineBreakMode];
        
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:l.text
                                        attributes:@{NSFontAttributeName: l.font}];
        CGRect rect = [attributedText boundingRectWithSize:(CGSize){l.frame.size.width, MAXFLOAT}
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                   context:nil];
        CGSize textSize = rect.size;
        
        l.frame = CGRectMake(packet.textFrame.origin.x ,packet.textFrame.origin.y, textSize.width+5, textSize.height+5);
        //        l.center = CGPointMake(packet.translationPoint.x, packet.translationPoint.y);
        
    }else if (packet.textState == ESTextStateRemoveText){
        if ([self.lableIndexArray containsObject:[NSNumber numberWithInteger:packet.indexValue]] ) {
            [self.lableIndexArray removeObject:[NSNumber numberWithInteger:packet.indexValue]];
        }
        UILabel *l = [self.addedLabels objectAtIndex:packet.indexValue];
        
        [self.addedLabels removeObjectAtIndex:packet.indexValue];
        [l removeFromSuperview];
        l = nil;
        
    }else if (packet.textState == ESTextStateTextColorChange){
        
        UILabel *l = [self.addedLabels objectAtIndex:packet.indexValue];
        l.textColor = packet.textColor;
        
    }else if (packet.textState == ESTextStateTextFontChange){
        
        UILabel *l = [self.addedLabels objectAtIndex:packet.indexValue];
        l.font = [UIFont systemFontOfSize:packet.textFontSize];
        //CGSize textSize = [l.text sizeWithFont:l.font constrainedToSize:CGSizeMake([self addFrame].size.width, MAXFLOAT) lineBreakMode:l.lineBreakMode];
        
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:l.text
                                        attributes:@{NSFontAttributeName: l.font}];
        CGRect rect = [attributedText boundingRectWithSize:(CGSize){[self addFrame].size.width, MAXFLOAT}
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                   context:nil];
        CGSize textSize = rect.size;
        
        l.frame = CGRectMake(l.frame.origin.x,l.frame.origin.y, textSize.width, textSize.height);
        
    }else if (packet.textState == ESTextStateMoveText){
        
        UILabel *l = [self.addedLabels objectAtIndex:packet.indexValue];
        l.frame = packet.textFrame;
        
    }else { // state = ESTextStateDismisPopover
        
        UILabel *l;
        if ([[NSUserDefaults standardUserDefaults] integerForKey:KEY_USER_ROLE] == ESUserRoleAttendee) {
            
            l = [[UILabel alloc] init];
            l.backgroundColor = [UIColor clearColor];
            l.numberOfLines = 0;
            
            l.layer.borderColor = [UIColor darkGrayColor].CGColor;
            l.layer.borderWidth = 1.5f;
            l.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |  UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
            [self.view addSubview:l];
            [self.addedLabels addObject:l];
        }
        
        l = [self.addedLabels objectAtIndex:packet.indexValue];
        
//        NSLog(@"presentaddtext with %ld = index,%@ = text centre x= %f y= %f",(long)packet.indexValue,packet.text,packet.textFrame.origin.x ,packet.textFrame.origin.y);
        
        if (packet.text.length == 0) {//if no text is added
            
            [self.addedLabels removeObjectAtIndex:packet.indexValue];
            [l removeFromSuperview];
            l = nil;
            
        }else {
            l.text = packet.text;
            l.textColor = packet.textColor;
            l.font = [UIFont systemFontOfSize:packet.textFontSize];
            l.frame = packet.textFrame;
            
        }
    }
}
-(void)processPicture:(ESPicturePacket *)packet{
    
    if (packet.pictureState == ESStateAddPicture) {
        
        UIImageView *stickyImage = [[UIImageView alloc]init];
        stickyImage.contentMode = UIViewContentModeScaleAspectFit;
        if (CGRectIsEmpty(packet.pictureFrame)) {
            stickyImage.frame = [self addFrame];
        }else {
            stickyImage.frame = packet.pictureFrame;
        }
        [self.view addSubview:stickyImage];
        stickyImage.image = packet.picture;
        [self.addedPictures addObject:stickyImage];
        
        
#if X_PRESENTER
        
        stickyImage.userInteractionEnabled = YES;
        
        UILongPressGestureRecognizer *longPressToDeletePicture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPressToDeletePicture:)];
        longPressToDeletePicture.minimumPressDuration = 0.7f;
        [stickyImage addGestureRecognizer:longPressToDeletePicture];
        
        
        UIPanGestureRecognizer *movePicture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(movePicture:)];
        movePicture.enabled = YES;
        movePicture .minimumNumberOfTouches = 1;
        [stickyImage addGestureRecognizer:movePicture];
        
#endif
        
    }else if(packet.pictureState == ESStateMovePicture){
        
        UIImageView *pic = [self.addedPictures objectAtIndex:packet.pictureIndex];
        pic.frame = packet.pictureFrame;
        
    }else if (packet.pictureState == ESStateRemovePicture){
        
        UIImageView *pic = [self.addedPictures objectAtIndex:packet.pictureIndex];
        [self.addedPictures removeObjectAtIndex:packet.pictureIndex];
        [pic removeFromSuperview];
        pic = nil;
        
    }
    
}

#pragma mark - Gesture Recognizer Methods
-(void)movePicture:(UIPanGestureRecognizer *)gesture{
    
    CGPoint translation = [gesture translationInView:[self view]];
    UIImageView *piece =(UIImageView *) [gesture view];
    
    if ([gesture state] == UIGestureRecognizerStateBegan || [gesture state] == UIGestureRecognizerStateChanged) {
        
        //moving image should come above all the subview
        [[self view] bringSubviewToFront:piece];
        
        [gesture setTranslation:CGPointZero inView:[self view]];
        
        [piece setCenter:CGPointMake([piece center].x + translation.x, [piece center].y+translation.y)];
        
        CGRect frame = piece.frame;
        frame.origin.x = MAX(0, frame.origin.x);
        frame.origin.x = MIN(frame.origin.x, [self view].bounds.size.width - piece.frame.size.width);
        frame.origin.y = MAX(0, frame.origin.y);
        frame.origin.y = MIN(frame.origin.y, [self view].bounds.size.height - piece.frame.size.height);
        frame.size.height = MAX(0, frame.size.height);
        frame.size.width = MAX(0, frame.size.width);
        piece.frame = frame;
        
        
    }else if ([gesture state] == UIGestureRecognizerStateEnded){
        
//        NSLog(@"indexof picture  %lu with arraycount %lu",(unsigned long)[self.addedPictures indexOfObject:piece],(unsigned long)self.addedPictures.count);
        ESPicturePacket *packet = [ESPicturePacket packetToMovePictureToPosition:piece.frame withPictureState:ESStateMovePicture withIndexValue:[self.addedPictures indexOfObject:piece]];
        if (self.delegate) {
            [self.delegate transferPacketForPictureFromViewController:self forPacket:packet];
        }
    }
}
-(void)handleLongPressToDeletePicture:(UILongPressGestureRecognizer *)gesture{
    
    
    picMenuIndex = NSNotFound;
    if (gesture.state == UIGestureRecognizerStateBegan) {
        
    
    [self becomeFirstResponder];
    UIImageView *stickyImage =(UIImageView *) [gesture view];
    picMenuIndex = [self.addedPictures indexOfObject:stickyImage];
    stickyImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    UIMenuItem *delete23 = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(deletePictureFromWhiteBoard:)];
    CGPoint touchPoint = [gesture locationInView: stickyImage];

    UIMenuController *menu =nil;
    menu=[UIMenuController sharedMenuController];
    [menu setMenuItems:[NSArray arrayWithObjects:delete23, nil]];
    [menu setTargetRect:CGRectMake(touchPoint.x,touchPoint.y,0.f,0.f) inView:stickyImage];
    [menu setMenuVisible:YES animated:YES];
    
    NSAssert([self becomeFirstResponder], @"Sorry, UIMenuController will not work with %@ since it cannot become first responder", self);
    }
}
- (void) panGestureRecognizerDidRecognize:(UIPanGestureRecognizer *) panGestureRecognizer {
    
    //Calculate scaled point
    CGPoint point = [panGestureRecognizer locationInView:whiteBoard];
    CGPoint scaledPoint = CGPointMake(point.x/self.view.bounds.size.width, point.y/self.view.bounds.size.height);
    
    ESAnnotationPacket *annotation;
    if (paintPalette.selectedThickness == 20) {
        UIColor *highlightColor = [UIColor colorWithRed:83/255.f green:230/258.f blue:83/255.f alpha:0.4];
        //touchStart
        if (panGestureRecognizer.state == UIGestureRecognizerStateBegan) {
            
            annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint lineWidth:10.0 andColor:highlightColor annotationType:ESAnnotationTypeHighlightStart];
        }
        
        //touchMoved
        else if (panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
            annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint lineWidth:10.0 andColor:highlightColor annotationType:ESAnnotationTypeHighlightMove];
        }
        
        //touch cancelled or ended
        else if (panGestureRecognizer.state == UIGestureRecognizerStateEnded || panGestureRecognizer.state == UIGestureRecognizerStateCancelled) {
            
            annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint lineWidth:10.0 andColor:highlightColor annotationType:ESAnnotationTypeHighlightEnd];
        }
    }else{
        
        //touchStart
        if (panGestureRecognizer.state == UIGestureRecognizerStateBegan) {
            
            annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint lineWidth:paintPalette.selectedThickness andColor:paintPalette.selectedColor annotationType:ESAnnotationTypeStart];
        }
        
        //touchMoved
        else if (panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
            
            annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint lineWidth:paintPalette.selectedThickness andColor:paintPalette.selectedColor annotationType:ESAnnotationTypeMove];
        }
        
        //touch cancelled or ended
        else if (panGestureRecognizer.state == UIGestureRecognizerStateEnded || panGestureRecognizer.state == UIGestureRecognizerStateCancelled) {
            
            annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint lineWidth:paintPalette.selectedThickness andColor:paintPalette.selectedColor annotationType:ESAnnotationTypeEnd];
        }
    }
    
    //draw on our own screen.
    [self processAnnotation:annotation];
    
    //send packet across network
    [self.delegate transferPacketForAnnotationFromViewController:self forPacket:annotation];
}
-(void)handleSingleTapToEditTextDelegateMethod:(UITapGestureRecognizer *)gesture{
    //    if ([[NSUserDefaults standardUserDefaults] integerForKey:KEY_USER_ROLE] == ESUserRolePresenter) {
    //gesture.view
    UILabel *piece =(UILabel *) [gesture view];
    
    BOOL isObjPresent =[self.addedLabels containsObject:piece];
    
    if (isObjPresent) {
        
        textPopoverView = [[ESTextPopoverViewController alloc]init];
        textPopoverView.delegate = self;
        textPopoverView.text = piece.text;
        CGFloat labelFontSize ;
        if (piece.font.pointSize < 14.f) {
            labelFontSize = 14.f;
        }else{
            labelFontSize = piece.font.pointSize;
        }
        textPopoverView.fontSize = labelFontSize;
        textPopoverView.textColor = piece.textColor;
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            
            textPopoverView.indexOfText = [self.addedLabels indexOfObject:piece];
            
            textPopoverView.modalPresentationStyle = UIModalPresentationPopover;
            [self presentViewController:textPopoverView animated:YES completion:nil];
            
            UIPopoverPresentationController *textPopoverController = [textPopoverView popoverPresentationController];
            textPopoverController.delegate = self;
            textPopoverController.sourceView = self.view;
            textPopoverController.sourceRect = piece.frame;
            
            
//            
//            textPopoverController = [[UIPopoverController alloc] initWithContentViewController:textPopoverView];
//            textPopoverController.delegate = self;
//            [textPopoverController setPopoverContentSize:CGSizeMake(280,400.0f)];
//            [textPopoverController presentPopoverFromRect:piece.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }
    //    }
}
-(void)moveText:(UIPanGestureRecognizer *)panGesture{
    
    //    if ( _packetDelegate && [[NSUserDefaults standardUserDefaults] integerForKey:KEY_USER_ROLE] == ESUserRolePresenter) {
    
    CGPoint translation = [panGesture translationInView:[self view]];
    UILabel *piece =(UILabel *) [panGesture view];
    
    if ([panGesture state] == UIGestureRecognizerStateBegan || [panGesture state] == UIGestureRecognizerStateChanged) {
        
        [panGesture setTranslation:CGPointZero inView:[self view]];
        
        [piece setCenter:CGPointMake([piece center].x + translation.x, [piece center].y+translation.y)];
        
        CGRect frame = piece.frame;
        frame.origin.x = MAX(0, frame.origin.x);
        frame.origin.x = MIN(frame.origin.x, [self view].bounds.size.width - piece.frame.size.width);
        frame.origin.y = MAX(0, frame.origin.y);
        frame.origin.y = MIN(frame.origin.y, [self view].bounds.size.height - piece.frame.size.height);
        frame.size.height = MAX(0, frame.size.height);
        frame.size.width = MAX(0, frame.size.width);
        piece.frame = frame;
        
    }else if ([panGesture state] == UIGestureRecognizerStateEnded){
        
        //            }
        ESTextPacket *packet = [ESTextPacket packetWithAddTextMoveToPosition:piece.frame withTextState:ESTextStateMoveText withIndexValue:[self.addedLabels indexOfObject:piece]];
        if (self.delegate) {
            [self.delegate transferPacketForTextFromViewController:self forPacket:packet];
        }
    }
}
-(void)handleSingleTapGestureRecognizerOnTextLabel:(UITapGestureRecognizer *)tapGesture{
    menuIndex = NSNotFound;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        
        [self handleSingleTapToEditTextDelegateMethod:tapGesture];
    }else{
        [self becomeFirstResponder];
        UILabel *piece =(UILabel *) [tapGesture view];
        menuIndex = [self.addedLabels indexOfObject:piece];
        piece.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |  UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        UIMenuItem *edit = [[UIMenuItem alloc] initWithTitle:@"Edit" action:@selector(editTextFromWhiteBoard:)];
        UIMenuItem *delete = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(deleteTextFromWhiteBoard:)];
        
        UIMenuController *menu =nil;
        CGPoint touchPoint = [tapGesture locationInView: piece];

        menu= [UIMenuController sharedMenuController];
        [menu setMenuItems:[NSArray arrayWithObjects:edit, delete, nil]];
        [menu setTargetRect:CGRectMake(touchPoint.x,touchPoint.y, 0.0f, 0.0f) inView:piece];
        [menu setMenuVisible:YES animated:YES];
        
        NSAssert([self becomeFirstResponder], @"Sorry, UIMenuController will not work with %@ since it cannot become first responder", self);
    }
    
}

-(void)deleteTextFromPopover:(ESTextPopoverViewController *)vc withPacket:(ESTextPacket *)packet{
    
    [self processText:packet];
    if (self.delegate) {
        [self.delegate transferPacketForTextFromViewController:self forPacket:packet];
    }
    
    if (textPopoverView != nil) {
        //[textPopoverController dismissPopoverAnimated:YES];
        [[textPopoverView presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        textPopoverView = nil;
    }
    
}

-(void) fontSizeChangeFromPopover:(ESTextPopoverViewController *)vc withPacket:(ESTextPacket *)packet{
    
    [self processText:packet];
    //don't send packect while changing the font size
    //packect will be send only when dissmiss/done/remove text
    
//    if (self.delegate) {
//        [self.delegate transferPacketForTextFromViewController:self forPacket:packet];
//    }
//    
}

-(void) textColorChangeFromPopover:(ESTextPopoverViewController *)vc withPacket:(ESTextPacket *)packet{
    
    [self processText:packet];
    
}

-(void)textChangeFromPopover:(ESTextPopoverViewController *)vc withPacket:(ESTextPacket *)packet{
    
    [self processText:packet];
    
}

//DISMISS POPOVER TAPPING OUTSIDE OF POPOVER
- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    
    NSInteger index = [ self.addedLabels indexOfObject:[self.addedLabels lastObject]];
    //index 2147483647 beyond bounds for empty array'
    
    UILabel *label = [self.addedLabels objectAtIndex:index];
    CGFloat labelFontSize ;
    if (label.font.pointSize < MINIMUM_FONT_VALUE) {
        labelFontSize = MINIMUM_FONT_VALUE;
    }else{
        labelFontSize = label.font.pointSize;
    }
    //CGSize textSize = [label.text sizeWithFont:[UIFont systemFontOfSize:labelFontSize] constrainedToSize:CGSizeMake(320, MAXFLOAT) lineBreakMode:label.lineBreakMode];
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:label.text
                                                                         attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:labelFontSize]}];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){320, MAXFLOAT}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize textSize = rect.size;
    
    label.frame = CGRectMake(label.frame.origin.x ,label.frame.origin.y, textSize.width, textSize.height);
    
    //    NSLog(@"presenter dismisspopover with %ld = index,%@ = text center=%f,%f",(long)index,label.text,label.center.x,label.center.y);
    
    
    ESTextPacket *packet = [ESTextPacket packetToDismissPopoverWithState:ESTextStateDismisPopover indexValue:index withText:label.text withTextColor:label.textColor withFontSize:labelFontSize withCenter:label.frame];
    
    [self processText:packet];
    if (self.delegate) {
        [self.delegate transferPacketForTextFromViewController:self forPacket:packet];
    }
    
    
    
}

//-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
//    
//    
//    NSInteger index = [ self.addedLabels indexOfObject:[self.addedLabels lastObject]];
//    //index 2147483647 beyond bounds for empty array'
//    
//    UILabel *label = [self.addedLabels objectAtIndex:index];
//    CGFloat labelFontSize ;
//    if (label.font.pointSize < MINIMUM_FONT_VALUE) {
//        labelFontSize = MINIMUM_FONT_VALUE;
//    }else{
//        labelFontSize = label.font.pointSize;
//    }
//    //CGSize textSize = [label.text sizeWithFont:[UIFont systemFontOfSize:labelFontSize] constrainedToSize:CGSizeMake(320, MAXFLOAT) lineBreakMode:label.lineBreakMode];
//    
//    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:label.text
//                                    attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:labelFontSize]}];
//    CGRect rect = [attributedText boundingRectWithSize:(CGSize){320, MAXFLOAT}
//                                               options:NSStringDrawingUsesLineFragmentOrigin
//                                               context:nil];
//    CGSize textSize = rect.size;
//    
//    label.frame = CGRectMake(label.frame.origin.x ,label.frame.origin.y, textSize.width, textSize.height);
//    
////    NSLog(@"presenter dismisspopover with %ld = index,%@ = text center=%f,%f",(long)index,label.text,label.center.x,label.center.y);
//    
//    
//    ESTextPacket *packet = [ESTextPacket packetToDismissPopoverWithState:ESTextStateDismisPopover indexValue:index withText:label.text withTextColor:label.textColor withFontSize:labelFontSize withCenter:label.frame];
//    
//    [self processText:packet];
//    if (self.delegate) {
//        [self.delegate transferPacketForTextFromViewController:self forPacket:packet];
//    }
//    
//}
#pragma mark - UIActionSheet Method


//- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
//    if (buttonIndex == 0){
//        
//        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
//            
//            UIImagePickerController *imagePicker =[[UIImagePickerController alloc] init];
//            imagePicker.delegate = self;
//            imagePicker.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
//            imagePicker.allowsEditing = NO;
//            
//            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
//                
//#if X_PRESENTER
//                imagePopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
//                [imagePopover presentPopoverFromRect:CGRectMake(0, 0, 320, 230) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//#endif
//                
//            } else {
//                [self presentViewController:imagePicker animated:YES completion:nil];
//            }
//        }
//    }else if (buttonIndex == 1){
//        
//        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
//            
//            UIImagePickerController *imagePicker =[[UIImagePickerController alloc] init];
//            imagePicker.delegate = self;
//            imagePicker.sourceType =UIImagePickerControllerSourceTypeCamera;
//            imagePicker.allowsEditing = NO;
//            
//            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
//#if X_PRESENTER
//                imagePopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
//                [imagePopover presentPopoverFromRect:CGRectMake(0, 0, 320, 230) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//#endif
//                
//            } else {
//                [self presentViewController:imagePicker animated:YES completion:nil];
//                
//            }
//        }
//    }
//}

#pragma mark - UIMenuController Methods

-(BOOL)canBecomeFirstResponder{
    return YES;
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    
    if (action == @selector(editTextFromWhiteBoard:)) {
        return YES;
    }else if (action == @selector(deleteTextFromWhiteBoard:)){
        return YES;
    }else if (action == @selector(deletePictureFromWhiteBoard:)){
        return YES;
    }
    return NO;
}
-(void)editTextFromWhiteBoard:(id)sender{
//    NSLog(@"%ld = index",(long)menuIndex);
    if (menuIndex != NSNotFound ) {
        [self presentWhiteBoardTextEditViewFromMenuItemWithIndex:menuIndex];
    }
    
}
-(void)deleteTextFromWhiteBoard:(id)sender{
    
//    NSLog(@"%ld = index deleteText",(long)menuIndex);
    ESTextPacket *packet = [ESTextPacket removeTextWithState:ESTextStateRemoveText withIndex:menuIndex];
    [self processText:packet];
    
    if (self.delegate) {
        [self.delegate transferPacketForTextFromViewController:self forPacket:packet];
    }
}
-(void)deletePictureFromWhiteBoard:(id)sender{
    
//    NSLog(@"%ld = index",(long)picMenuIndex);
    ESPicturePacket *packet = [ESPicturePacket removePictureWithState:ESStateRemovePicture withIndex:picMenuIndex];
    [self processPicture:packet];
    
    if (menuIndex != NSNotFound ) {
        if (self.delegate) {
            [self.delegate transferPacketForPictureFromViewController:self forPacket:packet];
        }
    }
}
-(void) deleteWhiteBoardTextFromMenuItemWithPacket:(ESTextPacket *)packet{
    
    [self processText:packet];
    if (self.delegate) {
        [self.delegate transferPacketForTextFromViewController:self forPacket:packet];
    }
}
-(void) deleteWhiteBoardPictureFromMenuItemWithpacket:(ESPicturePacket *)packet{
    
    [self processPicture:packet];
    if (self.delegate) {
        [self.delegate transferPacketForPictureFromViewController:self forPacket:packet];
    }
}

-(void)presentWhiteBoardTextEditViewFromMenuItemWithIndex:(NSInteger )index{
    
    UILabel *l = [self.addedLabels objectAtIndex:index];
    
    addTextPopoverView = [[ESTextPopoverViewController alloc]init];
    addTextPopoverView.delegate = self;
    addTextPopoverView.indexOfText = index;
    addTextPopoverView.text = l.text;
    
//    CGFloat labelFontSize ;
//    if (l.font.pointSize < 14.f) {
//        labelFontSize = 14.f;
//    }else{
//        labelFontSize = l.font.pointSize;
////    }
    addTextPopoverView.fontSize = l.font.pointSize;
    addTextPopoverView.textColor = l.textColor;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:addTextPopoverView];
    
    [self presentViewController:navController animated:YES completion:nil];
    
}
#pragma mark - UIImagePicker Delegate Methods

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *originalImage=[info objectForKey:UIImagePickerControllerOriginalImage];
    
    CGSize size=     CGSizeMake(Packect_Frame_size_iPhone, Packect_Frame_size_iPhone);
    UIGraphicsBeginImageContext(size);
    [originalImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    ESPicturePacket *packet= [ESPicturePacket  packetWithPicture:destImage andPictureframe:CGRectMake([self addFrame].origin.x,[self addFrame].origin.y, size.width, size.height) withState:ESStateAddPicture];
    [self processPicture:packet];
    
    if (self.delegate) {
        [self.delegate transferPacketForPictureFromViewController:self forPacket:packet];
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
#if X_PRESENTER
        if (imagePicker != nil) {
            //[imagePopover dismissPopoverAnimated:YES];
            [[imagePicker presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
            imagePicker = nil;
        }
#endif
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}
#pragma mark - Orientations Methods

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
    BOOL iPad = NO;
    BOOL isPotrait = NO;
#ifdef UI_USER_INTERFACE_IDIOM
    iPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#endif
    
//    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
#ifdef UIDeviceOrientationIsPortrait//(orientation)
    isPotrait = (UIDeviceOrientationIsPortrait(orientation) == UIDeviceOrientationPortrait ||UIDeviceOrientationIsPortrait(orientation) == UIDeviceOrientationPortraitUpsideDown);
#endif
    if (!iPad ){
        [paintPalette initWithFrame:[self frameOfPaintPallete] forIsPotrait:isPotrait];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
