//
//  ESPaintPalette.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/3/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#define PENCIL_COLOR_RED 0
#define PENCIL_COLOR_BLUE 1
#define PENCIL_COLOR_GREEN 2
#define PENCIL_COLOR_YELLOW 3
#define PENCIL_COLOR_BLACK 4

#define PENCIL_THICKNESS_0 0
#define PENCIL_THICKNESS_1 1
#define PENCIL_THICKNESS_2 2
#define PENCIL_THICKNESS_3 3
#define PENCIL_THICKNESS_4 4
#define PENCIL_THICKNESS_ERASER 5
#define HIGHLIGHT_TEXT_BUTTON 20

#import "ESPaintPaletteView.h"
#import <QuartzCore/QuartzCore.h>

@interface ESPaintPaletteColorButton : UIButton

@end

//A custom class that will make sure only one of its child buttons are selected.
@interface ESButtonRadioGroup : UIView

@property(nonatomic) NSInteger selectedIndex;

@end

@interface ESPaintPaletteView () {
    
    //Annotation
    UIButton *annotationButton;
    UIView *paintView;
    
    UIButton *undoButton;
    UIButton *clearPageButton;
    //    UIButton *eraserButton;
    
    ESButtonRadioGroup *thicknessView;
    ESButtonRadioGroup *pencilView;
    
    UIButton *addTextButton;
    UIButton *addPictureButton;
    UIButton *highlightTextButton;
    BOOL isPotraitMode;
}

@end

@implementation ESPaintPaletteView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        isPotraitMode = NO;
        [self _init:isPotraitMode];
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame isiPodPortrait:(BOOL)isPotrait{
    self = [super initWithFrame:frame];
    if (self) {
        isPotraitMode = isPotrait;
        [self _init:isPotrait];
    }
    return self;
    
}
- (UIColor *) selectedColor {
    
    switch (pencilView.selectedIndex) {
        case PENCIL_COLOR_RED:
            return [UIColor redColor];
        case PENCIL_COLOR_BLUE:
            return [UIColor blueColor];
        case PENCIL_COLOR_GREEN:
            return [UIColor greenColor];
        case PENCIL_COLOR_YELLOW:
            return [UIColor yellowColor];
        default:
        case PENCIL_COLOR_BLACK:
            return [UIColor blackColor];
    }
    return nil;
}

- (CGFloat ) selectedThickness {
    if (thicknessView.selectedIndex == HIGHLIGHT_TEXT_BUTTON) {
        return 20.f;
    }else {
        switch (thicknessView.selectedIndex) {
            default:
            case PENCIL_THICKNESS_0:
                return 2.f;
            case PENCIL_THICKNESS_1:
                return 4.f;
            case PENCIL_THICKNESS_2:
                return 6.f;
            case PENCIL_THICKNESS_3:
                return 8.f;
            case PENCIL_THICKNESS_4:
                return 10.f;
            case PENCIL_THICKNESS_ERASER:
                return -10.f;
        }
    }
}


/*
 PaintPallete
 ┗━━━ annotationButton
 ┗━━━ paintView
 ┗━━ pencilView, undoButton
 ┗━━ buttons
 ┗━━ thicknessView, eraserButton
 ┗━━ buttons
 */
-(void)initWithFrame:(CGRect)frame forIsPotrait:(BOOL)isPotrait{
    self.frame = frame;
    isPotraitMode = isPotrait;
//    NSLog(@"isPotraitMode -%d,frame %@ ",isPotraitMode,NSStringFromCGRect(frame));
    [self _init:isPotrait];
}
- (void) _init:(BOOL)isPotrait {
    CGFloat height = 0.f;
    CGFloat thicknessViewOrigin_y=0.f;
    CGFloat pencilViewOrigin_y = 0.f;

    NSArray * colorButtonImages = [NSArray arrayWithObjects:
                                   @"red",
                                   @"blue",
                                   @"green",
                                   @"khakhi",
                                   @"darkgrey", nil];
    
    
    NSArray *thicknessButtonImages = [NSArray arrayWithObjects:
                                      @"blackdot",
                                      @"blackdot01",
                                      @"blackdot02",
                                      @"blackdot03",
                                      @"blackdot04",
                                      @"eraser",
                                      nil];
    

    //Annotation button
    if (annotationButton == nil) {
        
        annotationButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:annotationButton];
        annotationButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
        annotationButton.backgroundColor = [UIColor clearColor];
        [annotationButton setImage:[UIImage imageNamed:@"pencil_2"] forState:UIControlStateNormal];
        [annotationButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateSelected];
        [annotationButton addTarget:self action:@selector(annotationButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        annotationButton.selected = NO;
    }
    
    
    //Clear Button
    if (clearPageButton == nil) {
        
        clearPageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        clearPageButton.backgroundColor = [UIColor whiteColor];
        clearPageButton.hidden = YES;
        clearPageButton.titleLabel.font = [UIFont fontWithName:@"marion" size:16.f];
        clearPageButton.backgroundColor = [UIColor darkGrayColor];
        [clearPageButton setTitle:@"Clear" forState:UIControlStateNormal];
        [clearPageButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [clearPageButton addTarget:self action:@selector(clearPageButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:clearPageButton];
    }
    
    //paint view
    if (paintView == nil) {
        
        paintView = [[UIView alloc]init];
        paintView.backgroundColor = [UIColor colorWithRed:54/255.f green:54/255.f blue:54/255.f alpha:1.0];
        paintView.layer.cornerRadius = 5;
        paintView.layer.masksToBounds = YES;
    }
    paintView.frame = CGRectMake(self.bounds.size.width - 250, 0, 250, self.bounds.size.height);
//    paintView.backgroundColor = [UIColor redColor];//Remove this line
    height  = isPotrait ? paintView.frame.size.height/3 - 10 : paintView.frame.size.height/2 - 10;
    thicknessViewOrigin_y = isPotrait? paintView.frame.size.height-height-5 : paintView.frame.size.height/2 + 5;
    pencilViewOrigin_y = isPotrait ?thicknessViewOrigin_y - height - 5 :5;
    if (pencilView == nil) {
        
        pencilView = [[ESButtonRadioGroup alloc]init];
        pencilView.backgroundColor = [UIColor  clearColor];
        [paintView addSubview:pencilView];
        //add drawing pencils to pencil view
        UIButton *colorbutton;
        for (int i=0; i<5; i++) {
            
            colorbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            
            [colorbutton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_unselected",colorButtonImages[i]]] forState:UIControlStateNormal];
            [colorbutton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",colorButtonImages[i]]] forState:UIControlStateSelected];
            
            [pencilView addSubview:colorbutton];
        }
        
        pencilView.selectedIndex = PENCIL_COLOR_BLACK;

    }
    pencilView.frame = CGRectMake(5, pencilViewOrigin_y, paintView.frame.size.width - 50, height);
    if (undoButton == nil) {
        
        undoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [undoButton setImage:[UIImage imageNamed:@"undo_unselected"] forState:UIControlStateNormal];
        [undoButton addTarget:self action:@selector(undoButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [paintView addSubview:undoButton];
    }
    CGFloat undoButton_origin_Y = isPotrait?pencilView.frame.origin.y-12:pencilView.frame.origin.y;
    
    undoButton.frame = CGRectMake(paintView.bounds.size.width - 40, undoButton_origin_Y, 30, paintView.frame.size.height/2 - 10);
    
    if (thicknessView == nil) {
        
        thicknessView =[[ESButtonRadioGroup alloc]init];
        thicknessView.backgroundColor = [UIColor  clearColor];
        
        [paintView addSubview:thicknessView];
        //add thickness pencils to thickness view
        UIButton *thicknessbutton;
        for (int i=0; i<6; i++) {
            
            thicknessbutton = [UIButton buttonWithType:UIButtonTypeCustom];
            
            [thicknessbutton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_unselected",thicknessButtonImages[i]]] forState:UIControlStateNormal];
            [thicknessbutton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",thicknessButtonImages[i]]] forState:UIControlStateSelected];
            
            [thicknessView addSubview:thicknessbutton];
        }
        
        //    eraserButton = [UIButton buttonWithType:UIButtonTypeCustom];
        //    [pencilView addSubview:eraserButton];
        
        thicknessView.selectedIndex = PENCIL_THICKNESS_0;
    }
    thicknessView.frame = CGRectMake(5, thicknessViewOrigin_y, paintView.frame.size.width - 5,height);
    if(addTextButton == nil){
        addTextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        addTextButton.backgroundColor = [UIColor darkGrayColor];
        addTextButton.hidden = YES;
        addTextButton.titleLabel.font = [UIFont fontWithName:@"marion" size:24.f];
        [addTextButton setTitle:@"A" forState:UIControlStateNormal];
        [addTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [addTextButton addTarget:self action:@selector(addTextButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:addTextButton];
        
    }
    if (addPictureButton == nil) {
        
        addPictureButton = [UIButton buttonWithType:UIButtonTypeCustom];
        addPictureButton.backgroundColor = [UIColor darkGrayColor];
        addPictureButton.hidden = YES;
        addPictureButton.titleLabel.font = [UIFont fontWithName:@"marion" size:24.f];
        [addPictureButton setTitle:@"P" forState:UIControlStateNormal];
        [addPictureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [addPictureButton addTarget:self action:@selector(addPictureButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:addPictureButton];
        
    }
    if (highlightTextButton == nil) {
        
        highlightTextButton  = [UIButton buttonWithType:UIButtonTypeCustom];
        highlightTextButton.backgroundColor = [UIColor darkGrayColor];
        highlightTextButton.hidden = YES;
        highlightTextButton.titleLabel.font = [UIFont fontWithName:@"marion" size:24.f];
        [highlightTextButton setTitle:@"H" forState:UIControlStateNormal];
        [highlightTextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [highlightTextButton addTarget:self action:@selector(highlightTextButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:highlightTextButton];
        
    }
    [self resizeButtonsForPotrait:isPotrait];
}

-(void)resizeButtonsForPotrait:(BOOL)isPotrait{
    
    annotationButton.frame = CGRectZero;
    clearPageButton.frame = CGRectZero;
    annotationButton.frame = CGRectZero;
    addPictureButton.frame = CGRectZero;
    highlightTextButton.frame = CGRectZero;
    
    if (isPotrait) {
        if (annotationButton.selected) {
            annotationButton.frame = CGRectMake(2, 2, 44.f, 44.f);
            self.backgroundColor = [UIColor colorWithRed:54/255.f green:54/255.f blue:54/255.f alpha:1.0];

        }else{
            annotationButton.frame = CGRectMake(self.bounds.size.width- 50, 2, 44.f, 44.f);
            self.backgroundColor = [UIColor clearColor];

        }
        clearPageButton.frame = CGRectMake(annotationButton.frame.origin.x + annotationButton.frame.size.width +5,annotationButton.frame.origin.y, 44.f, 44.f);
        addPictureButton.frame = CGRectMake(clearPageButton.frame.origin.x +clearPageButton.frame.size.width + 5,annotationButton.frame.origin.y , 44.f, 44);
        addTextButton.frame = CGRectMake(addPictureButton.frame.origin.x +addPictureButton.frame.size.width + 5 , annotationButton.frame.origin.y, 44.f, 44);
        highlightTextButton.frame   = CGRectMake(addTextButton.frame.origin.x +addTextButton.frame.size.width + 5 , annotationButton.frame.origin.y, 44.f, 44);
        [self bringSubViewsToFront];

    }else{
        if (annotationButton.selected) {
            self.backgroundColor = [UIColor colorWithRed:54/255.f green:54/255.f blue:54/255.f alpha:1.0];

            annotationButton.frame = CGRectMake(2, 0, 44.f, 39.f);
        }else{
            self.backgroundColor = [UIColor clearColor];

            annotationButton.frame = CGRectMake(self.bounds.size.width - 50, 0, 44.f, 39.f);
        }
        clearPageButton.frame = CGRectMake(annotationButton.frame.origin.x,annotationButton.frame.size.height+annotationButton.frame.origin.y+1, 44.f, 39.f);
        addTextButton.frame = CGRectMake(annotationButton.frame.origin.x+annotationButton.frame.size.width+1, annotationButton.frame.origin.y, 44.f, 39);
        addPictureButton.frame = CGRectMake(addTextButton.frame.origin.x , clearPageButton.frame.origin.y , 44.f,39);
        highlightTextButton.frame   = CGRectMake(addPictureButton.frame.origin.x+addPictureButton.frame.size.width + 1,0 , 42.f, self.bounds.size.height - 1);
    }
    
}
//- (void) eraserButtonTapped:(UIButton *) button {
//    eraserButton.selected = !eraserButton.selected;
//}

- (void) undoButtonTapped:(id) sender {
    if (_delegate && [_delegate respondsToSelector:@selector(undoButtonClickedOnPalette:)]) {
        [_delegate undoButtonClickedOnPalette:self];
    }
}
-(IBAction)addTextButtonTouchUpInside:(id)sender{
    [self paintPalette];
    
    //    if (_delegate && [_delegate respondsToSelector:@selector(addTextButtonClicked:)]) {
    //        [_delegate addTextButtonClicked:self];
    //    }
}
-(IBAction)addPictureButtonTouchUpInside:(id)sender{
    [self paintPalette];
    
    //    if (_delegate && [_delegate respondsToSelector:@selector(addPictureButtonClicked:)]) {
    //        [_delegate addPictureButtonClicked:self];
    //    }
    
}
-(void)bringSubViewsToFront{
    [self bringSubviewToFront:annotationButton];
    [self bringSubviewToFront:clearPageButton];
    [self bringSubviewToFront:highlightTextButton];
    [self bringSubviewToFront:addPictureButton];
    [self bringSubviewToFront:addTextButton];

}
-(void)paintPalette{
    if (paintView == nil) {
        paintView = [[UIView alloc]init];
    }
    [self addSubview:paintView];
    if (annotationButton.selected){
        self.backgroundColor = [UIColor clearColor];

    }else{

        self.backgroundColor = [UIColor colorWithRed:54/255.f green:54/255.f blue:54/255.f alpha:1.0];

    }
    [UIView animateWithDuration:1.0f animations:^{
        //show painting view
        if (!annotationButton.selected){
            paintView.hidden = NO;
            //            annotationButton.backgroundColor = [UIColor redColor];
            annotationButton.selected = YES;
            
            clearPageButton.hidden = NO;
            addPictureButton.hidden = NO;
            addTextButton.hidden = NO;
            highlightTextButton.hidden = NO;
            
            if (isPotraitMode) {
                [self bringSubViewsToFront];
                annotationButton.frame = CGRectMake(2, 2, 44.f, 44.f);
                clearPageButton.frame = CGRectMake(annotationButton.frame.origin.x + annotationButton.frame.size.width +5,annotationButton.frame.origin.y, 44.f, 44.f);
                addPictureButton.frame = CGRectMake(clearPageButton.frame.origin.x +clearPageButton.frame.size.width + 5,annotationButton.frame.origin.y, 44.f, 44);
                addTextButton.frame = CGRectMake(addPictureButton.frame.origin.x +addPictureButton.frame.size.width + 5 , annotationButton.frame.origin.y, 44.f, 44);
                highlightTextButton.frame   = CGRectMake(addTextButton.frame.origin.x +addTextButton.frame.size.width + 5 , annotationButton.frame.origin.y, 44.f, 44);
            }else{
                annotationButton.frame = CGRectOffset(annotationButton.frame, -annotationButton.frame.origin.x + 1, 0);
                clearPageButton.frame = CGRectOffset(clearPageButton.frame, -clearPageButton.frame.origin.x + 1, 0);
                addTextButton.frame = CGRectOffset(addTextButton.frame,  -addTextButton.frame.origin.x + addTextButton.frame.size.width + 2,0);
                addPictureButton.frame = CGRectOffset(addPictureButton.frame,  -addPictureButton.frame.origin.x + addPictureButton.frame.size.width + 2,0);
                highlightTextButton.frame = CGRectOffset(highlightTextButton.frame,  -highlightTextButton.frame.origin.x+90+1, 0);
            }
            
            
        }else{

            clearPageButton.hidden = YES;
            addTextButton.hidden = YES;
            paintView.hidden = YES;
            addPictureButton.hidden = YES;
            highlightTextButton.hidden = YES;
            annotationButton.selected = NO;
            //            annotationButton.backgroundColor = [UIColor greenColor];
            annotationButton.frame = CGRectOffset(annotationButton.frame, self.frame.size.width - annotationButton.frame.size.width, 0);
        }
        
    } completion:^(BOOL finished) {
        
    }];
    
}
-(IBAction)annotationButtonTapped:(id)sender{
#if X_AUDIENCE
    return;
#endif
    [self paintPalette];
    //    clearPageButton.frame = CGRectOffset(clearPageButton.frame, -clearPageButton.frame.origin.x, 0);
    
    //    [self bringSubviewToFront:annotationButton];
    
}
-(IBAction)clearPageButtonTouchUpInside:(id)sender{
    if (_delegate && [_delegate respondsToSelector:@selector(clearButtonClicked:)]) {
        [_delegate clearButtonClicked:self];
    }
    
}
-(IBAction)highlightTextButtonTouchUpInside:(id)sender{
    //    [self paintPalette];
    
    pencilView.selectedIndex = HIGHLIGHT_TEXT_BUTTON;
    thicknessView.selectedIndex = HIGHLIGHT_TEXT_BUTTON;
    
    if (_delegate && [_delegate respondsToSelector:@selector(highlightTextButtonClicked:)]) {
        [_delegate highlightTextButtonClicked:self];
    }
    
}

- (UIButton *) undoButton {
    return undoButton;
}

- (UIButton *) annotationButton {
    return annotationButton;
}

-(UIButton *) clearPageButton{
    return clearPageButton;
}
-(UIButton *) addTextButton{
    return addTextButton;
}
-(UIButton *) addPictureButton{
    return addPictureButton;
}
-(UIButton *) highlightTextButton{
    return highlightTextButton;
}
@end

@implementation ESButtonRadioGroup

- (void) addSubview:(UIView *)view {
    NSAssert([view isMemberOfClass:[UIButton class]], @"Only buttons can be added to the Radio ");
    UIButton *button = (UIButton *) view;
    [super addSubview:button];
    [button addTarget:self action:@selector(childButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
}

- (void) childButtonSelected:(UIButton *) button {
    self.selectedIndex = [self.subviews indexOfObject:button];
}

- (void) setSelectedIndex:(NSInteger) selectedIndex {
    _selectedIndex = selectedIndex;
    //deselect all child buttons
    for (UIButton *childButton in self.subviews) {
        childButton.selected = NO;
    }
    if (selectedIndex == HIGHLIGHT_TEXT_BUTTON) {
        
    }else{
        [(UIButton *)[self.subviews objectAtIndex:selectedIndex] setSelected:YES];
    }
    
}

//layout subviews one beside the other.

- (void) layoutSubviews {
    if (self.subviews.count == 0) {
        return;
    }
    //margin between each child should be as atleast one third of their widths.
    //i.e. widthOfChild = widthOfSelf/((4n - 1)/3)
    int widthOfChild = self.bounds.size.width * 3/(4 *self.subviews.count - 1);
    for (int i=0; i<self.subviews.count; i++) {
        [self.subviews[i] setFrame:CGRectMake(i*widthOfChild * 4/3, 0, widthOfChild, self.bounds.size.height)];
    }
}

@end

//Possible implementation for future when we don't want images for buttons and its selection state.
@implementation ESPaintPaletteColorButton

- (id) initWithColor:(UIColor *) color {
    self = [ESPaintPaletteColorButton buttonWithType:UIButtonTypeRoundedRect];
    return self;
}

@end