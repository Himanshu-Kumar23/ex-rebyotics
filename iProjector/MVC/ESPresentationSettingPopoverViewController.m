//
//  ESPresentationSettingPopoverViewController.m
//  iProjector
//
//  Created by Manoj Katragadda on 13/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESPresentationSettingPopoverViewController.h"

@interface ESPresentationSettingPopoverViewController (){
    NSUserDefaults *userDefaults;
    BOOL querySwitch;
    BOOL browseSwitch;

}

@end

@implementation ESPresentationSettingPopoverViewController
@synthesize delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Change Settings";
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonTapped:)];
    self.navigationItem.rightBarButtonItem = doneButton;

    userDefaults = [NSUserDefaults standardUserDefaults];
    browseSwitch = [userDefaults boolForKey:KEY_ATTENDEE_CAN_BROWSE];
    querySwitch = [userDefaults boolForKey:KEY_ATTENDEE_CAN_QUERY];
	// Do any additional setup after loading the view.
}

-(IBAction)doneButtonTapped:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma UITableviewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UISwitch *uiSwitch;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        uiSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        //    uiSwitch.on = YES; //FIXME: J.P. default value should come from NSUserDefaults.
        cell.accessoryView = uiSwitch;
        cell.selectionStyle = UITableViewCellEditingStyleNone;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16.f];
    }

    [uiSwitch addTarget:self action:@selector(switchTapped:) forControlEvents:UIControlEventValueChanged];
//    if (indexPath.row == 0) {
//        uiSwitch.on = browseSwitch;
//
//        cell.textLabel.text = @"Attendee can browse pages";
//        uiSwitch.tag = 101;
//        cell.accessoryType = [userDefaults boolForKey:KEY_ATTENDEE_CAN_BROWSE] ? UITableViewCellAccessoryCheckmark: UITableViewCellAccessoryNone;
//    }
    if(indexPath.row == 0){
        uiSwitch.on = querySwitch;
        uiSwitch.tag = 102;
        cell.textLabel.text =@"Attendee can ask questions";
//        cell.accessoryType = [userDefaults boolForKey:KEY_ATTENDEE_CAN_QUERY] ? UITableViewCellAccessoryCheckmark: UITableViewCellAccessoryNone;
    }

    return cell;
}
-(void)switchTapped:(id)sender{
    UISwitch *switchView = sender;
    if (switchView.tag == 101) {
        browseSwitch = switchView.isOn;
        [userDefaults setBool:browseSwitch forKey:KEY_ATTENDEE_CAN_BROWSE];
        [userDefaults synchronize];
    }
    if (switchView.tag == 102){
        querySwitch = switchView.isOn;
        [userDefaults setBool:querySwitch forKey:KEY_ATTENDEE_CAN_QUERY];
        [userDefaults synchronize];
    }

    if (self.delegate != nil) {
        [self.delegate attendeesBrowseTurnedOn:browseSwitch andQuery:querySwitch];
    }
}

#pragma Orientations Methods

- (BOOL)shouldAutorotate{
    
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}

@end
