//
//  ESAnnotatedPDF.h
//  iProjector
//
//  Created by Jayaprada Behera on 13/11/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESPDFContentView.h"
@interface ESAnnotatedPDF : NSObject

+(void)drawPDF:(NSString*)fileName forURL:(NSURL *)fileUrl withCurveArray:(NSArray *)pagesCurvesArray withTextArray:(NSArray *)pagesAddTextArray withPictureArray:(NSArray *)pagesPictureArray;

@end
