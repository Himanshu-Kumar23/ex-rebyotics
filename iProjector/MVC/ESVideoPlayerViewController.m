//
//  ESVideoPlayerViewController.m
//  iProjector
//
//  Created by Jayaprada Behera on 21/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESVideoPlayerViewController.h"

@interface ESVideoPlayerViewController ()

@end

@implementation ESVideoPlayerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (![self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.extendedLayoutIncludesOpaqueBars = YES;
        self.navigationController.navigationBar.translucent = YES;
//        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
    }
    
    AVPlayer *player = [[AVPlayer alloc] initWithURL:self.url];
    
    self.movieController = [[AVPlayerViewController alloc] init];
    
    self.movieController.player = player;
    
    //self.movieController = [[MPMoviePlayerController alloc] initWithContentURL:self.url];
    
    [self.movieController.view setFrame:self.view.bounds];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(dismissVideoPage:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:self.movieController];
        
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(moviePlayerPlaybackStateDidChange:)
//                                                     name:MPMoviePlayerPlaybackStateDidChangeNotification
//                                                   object:nil];
//    
    //self.movieController.controlStyle = MPMovieControlStyleNone;
    
    //self.movieController.movieSourceType = MPMovieSourceTypeFile;
    
    self.movieController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self.view addSubview:self.movieController.view];
    
    UIBarButtonItem *dismissBarButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(dismissVideoPage:)];
    self.navigationItem.rightBarButtonItem = dismissBarButton;
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.movieController.player play];
    //[self.movieController setFullscreen:NO animated:NO];
    //self.movieController.controlStyle = MPMovieControlStyleEmbedded;
}

-(IBAction)dismissVideoPage:(id)sender{
    
    [self cleanup];
    [self dismissViewControllerAnimated:YES completion:nil];
    [_delegate videoPlayerViewControllerDone:self];
    
}


-(void) cleanup {
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    //[[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
    
    [self.movieController.player pause];
    [self.movieController.view removeFromSuperview];
    self.movieController = nil;

}

- (void)moviePlayBackDidFinish:(NSNotification *)notification {
    [self cleanup];
}

- (void) dealloc {
    if (self.movieController) {
        [self cleanup];
    }
}

//-(void) moviePlayerPlaybackStateDidChange:(NSNotification *)aNotification{
//    
//    if (self.movieController.playbackState == MPMoviePlaybackStatePaused) {
//        [_delegate videoPlayerViewController:self videoPlayerDidPauseAt:_movieController.currentPlaybackTime];
//    }else  if (self.movieController.playbackState == MPMoviePlaybackStatePlaying) {
//        [_delegate videoPlayerViewController:self videoPlayerDidResumePlayingAt:_movieController.currentPlaybackTime];
//    }
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma Orientations Methods

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskAll;
}

@end
