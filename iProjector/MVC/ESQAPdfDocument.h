//
//  ESQAPdfDocument.h
//  iProjector
//
//  Created by Manoj Katragadda on 15/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreText/CoreText.h>
#import "ESQuestion.h"
@interface ESQAPdfDocument : NSObject

+(void) drawPDFWithFileName:(NSString *)filename forQuestionsAnswer:(NSString *)QuestionsAnswer;

@end
