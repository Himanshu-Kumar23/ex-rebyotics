//
//  ESPDFDocumentViewController.m
//  iProjector
//
//  Created by WebileApps on 30/07/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESPDFDocumentViewController.h"
#import "ESPDFContentView.h"
//#import "ESReaderSliderPageBar.h"
#import "ReaderMainPagebar.h"
#import "ReaderMainToolbar.h"
#import <QuartzCore/CoreAnimation.h>
#import "ESSettingListViewController.h"
#import "ESCurve.h"

#import "ESAnnotationPacket.h"
#import "ESLaserPointerPacket.h"
#import "ESPageTransitionPacket.h"
#import "ESZoomTransitionPacket.h"

#import "ESQuestionsListViewController.h"
#import "ESQuestion.h"
#import "ESQuestionPacket.h"
#import "ESAnswerPacket.h"
#import "ESAttendeeQuestion.h"
#import "ESAttendeeQuestionSwitchPacket.h"
#import "ESQuestionAnswerViewController.h"
#import "ESAttendeeQuestionListViewController.h"
#import "ESQuestionsTypeViewController.h"
#import "ESTextViewController.h"
#import "ESAttendeeQuestionPacket.h"
#import "ESAttendeeCustomAnswerPacket.h"
#import "ESEndPresentationPacket.h"
#import "ESCustomQuestionController.h"
#import "ESCustomBadgeButton.h"
#import "ESQAPdfDocument.h"

#import "ESAnnotationsViewController.h"
#import "ESQuestionnaireParser.h"
#import "ESWhiteBoardPacket.h"
#import "ESPaintingDoneButtonPacket.h"

#import "ESBarView.h"
#import "ESVideoPlayerPacket.h"

#import "ESAnnotatedPDF.h"
#if X_PRESENTER

#import "ESVideo.h"
#import  "ESVideoParser.h"

#endif

#import "ESText1.h"
#import "ESPicture.h"

#import "ESAppDelegate.h"

#define END_PRESENTATION_BUTTON 0
#define EMAIL_QA_ASKED 1
#define EMAIL_ANNOTATED_PDF 2
#define ANSWER_QUERIES 3

#define Q_A_PDF_FILE_NAME                   @"Question&Answer.pdf"

#define SINGLE_TAP_AREA_SIZE 160.0f

#define DONE_ACTIONSHEET_TAG  1001

#define ADD_PICTURE_ACTIONSHEET_TAG 1002

#define UNAVAILABLE_FEATURE_ALERT_TAG    1030

#define PRESENTER @"Presenter"
#define ATTENDEE @"Attendee"



@interface TimerTarget : NSObject
@property(weak, nonatomic) id realTarget;
@end

@implementation TimerTarget

- (void)timerFired:(NSTimer*)theTimer
{
    [self.realTarget performSelector:@selector(timerFired:) withObject:theTimer];
}

@end

@interface ESPDFDocumentViewController () <ESQuestionPickerDelegate, ESQuestionAnswerViewControllerDelegate, ESTextViewControllerDelegate, ESAttendeeQuestionsDelegate, UIActionSheetDelegate, ESPDFContentViewCurveDelegate,ESPDFTapGestureRecognizerDelegate, ESVideoPlayerViewControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate, UIPopoverPresentationControllerDelegate> {
    
    ESAppDelegate *appDelegate;
    
    NSArray *colorButtonImages;
    NSData *presenterData;
    
    ESPaintPaletteView *paintPalette;
    UIAlertController *addPictureActionSheet;
    
    NSMutableArray *attendeeAskQuestionArray;
    NSMutableArray *attendeeCustomAnswerArray;
    
    UITapGestureRecognizer *singleTapGestureRecognizer;
    UITapGestureRecognizer *doubleTapGestureRecognizer;
    
    
#if X_PRESENTER
    UILongPressGestureRecognizer *longPressToPlayVideo;
    ESTextPopoverViewController *addTextPopoverView;
    ESTextPopoverViewController *textPopoverView;
    UIImagePickerController *imagePicker;
    
#endif
    
    BOOL bShowQueriesNavItem;
    UIAlertController *doneButtonActionSheet;
    NSTimer *timer;
    BOOL isPDFDrawn;
    NSString *annotatedFileName;
    UIView *loadingView;
    
}

@property (nonatomic, strong) NSMutableArray *questions;
@property(nonatomic,strong) NSMutableArray *videos;
@end

@implementation ESPDFDocumentViewController

@synthesize progress,currMinute,currSeconds;


#pragma LifeCycle Methods

- (id) initWithReaderDocument:(ReaderDocument *)document withSession:(ESMultipeerManager *)manager {
    self = [super initWithReaderDocument:document];
    if (self) {
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            self.automaticallyAdjustsScrollViewInsets = NO;
        } else {
            self.extendedLayoutIncludesOpaqueBars = YES;
            //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }
        
        appDelegate  = (ESAppDelegate *)[[UIApplication sharedApplication] delegate];
        self.view.backgroundColor=[UIColor whiteColor];
        self.multipeerManager = manager;
        
        pagesCurvesArray = [NSMutableArray arrayWithCapacity:document.pageCount.integerValue];
        pagesAddTextArray = [NSMutableArray arrayWithCapacity:document.pageCount.integerValue];
        pagesPictureArray = [NSMutableArray arrayWithCapacity:document.pageCount.integerValue];
        
        for (int i=0; i<document.pageCount.integerValue; i++) {
            [pagesCurvesArray addObject:[NSMutableArray arrayWithCapacity:5]];
            [pagesAddTextArray addObject:[NSMutableArray arrayWithCapacity:5]];
            [pagesPictureArray addObject: [NSMutableArray arrayWithCapacity:5]];
        }
        singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTapOnContentView:)];
        singleTapGestureRecognizer.numberOfTouchesRequired = 1;
        singleTapGestureRecognizer.numberOfTapsRequired = 1;
        
        doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTapOnContentView:)];
        doubleTapGestureRecognizer.numberOfTouchesRequired = 1;
        doubleTapGestureRecognizer.numberOfTapsRequired = 2;
        
#if X_PRESENTER
        
        longPressToPlayVideo = [[UILongPressGestureRecognizer alloc]
                                initWithTarget:self
                                action:@selector(handleLongPress:)];
        longPressToPlayVideo.minimumPressDuration = 1.0;
#endif
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer]; // Single tap requires double tap to fail
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setupNavigationButtonItems];
    
}

- (void)manager:(ESMultipeerManager *)manager
 didReceiveData:(NSData *)data
       fromPeer:(MCPeerID *)peerID
{
//    if ([appDelegate.role isEqualToString:ATTENDEE] && ( !_presenterPeerId || ![peerID isEqual:_presenterPeerId])) {
//        return; //discard packet as it is from an attendee.
//    }
      NSObject *object = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    ESAbstractPacket *packet;
   if ([object isKindOfClass:[ESAbstractPacket class]]) {
       packet = (ESAbstractPacket *) object;
   }
    
//   if(![appDelegate.role isEqualToString:ATTENDEE] && packet.packetType != ESPacketTypeAttendeeQuestion){
//        return;
//    }
    
    @try {
        NSObject *object = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if ([object isKindOfClass:[ESAbstractPacket class]]) {
            packet = (ESAbstractPacket *) object;
            if (packet.packetType == ESPacketTypeAnnotation) {
                [self didReceiveAnnotationFromPresenter:(ESAnnotationPacket *) object];
            } else if (packet.packetType == ESPacketTypeLaserPointer) {
                [self didReceiveLaserPointerPacketFromPresenter:(ESLaserPointerPacket *) object];
            } else if (packet.packetType == ESPacketTypeZoomAndPan){
                [self didReceiveZoomTransitionPacketFromPreseneter:(ESZoomTransitionPacket *)object];
            } else if (packet.packetType == ESPacketTypeChangePage){
                [self didReceivePageTransitionFromPresenter:(ESPageTransitionPacket *)object];
            } else if (packet.packetType == ESPacketTypeQuestion) {
                [self didreceiveQuestionFromPresenter:(ESQuestionPacket *) object];
            } else if (packet.packetType == ESPacketTypeAnswer) {
                [self didReceiveAnswerFromAttendee:(ESAnswerPacket *) object];
            } else if (packet.packetType == ESPacketTypeAttendeeQuestion) {
                [self didReceiveQuestionFromAttendee:(ESAttendeeQuestionPacket *) object];
            } else if (packet.packetType == ESPacketTypeAttendeeQuestionSwitch){
                [self didRecieveAttendeeQuestionStatusFromPresenter:(ESAttendeeQuestionSwitchPacket *)object];
            } else if (packet.packetType == ESPacketTypeEndPresentation){
                [self didReceiveEndPresentationFromPresenter:(ESEndPresentationPacket *) object];
            } else if (packet.packetType == ESPacketTypeWhiteBoard){
                [self didReceiveToGoWhiteBoardFromPresenter:(ESWhiteBoardPacket *)object];
            } else if (packet.packetType == ESPacketTypeDismissWhiteBoard){
                [self didReceiveToDismissWhiteBoardFromPresenter:(ESPaintingDoneButtonPacket *)object];
            }else if (packet.packetType == ESPacketTypeVideo){
                [self didReceiveVideoPlayerPacketFromPresenter:(ESVideoPlayerPacket *)object];
            }else if (packet.packetType == ESPacketTypeText){
                [self didReceiveTextPacketFromPresenter:(ESTextPacket *)object];
            }else if (packet.packetType == ESPacketTypePicture){
                [self didReceivePicturePacketFromPresenter:(ESPicturePacket *)object];
            }else if(packet.packetType == ESPacketTypeAttendeeCustomAnswer) {
              [self didReceiveCustomAnswerFromAttendee:(ESAttendeeCustomAnswerPacket *)object];
            }
            else if ([_sessionDataDelegate handlePacket:packet fromPeerWithId:peerID]) {
                
            } else {
                NSLog(@"Warning: Packet type undefined: %zd",packet.packetType);
            }
        }
    }
    @catch (NSException *exception) {
#if DEBUG
        NSLog(@"Exception: %@",exception);
#endif
    }
    
}

//- (void) receiveData:(NSData *)data fromPeer:(NSString *) peer inSession:(GKSession *) session context:(NSString *) string {
//    if (self.gkSession.sessionMode == GKSessionModeClient && ( !_presenterPeerId || ![peer isEqualToString:_presenterPeerId.displayName])) {
//        return; //discard packet as it is from an attendee.
//    }
//    @try {
//        NSObject *object = [NSKeyedUnarchiver unarchiveObjectWithData:data];
//        if ([object isKindOfClass:[ESAbstractPacket class]]) {
//            ESAbstractPacket *packet = (ESAbstractPacket *) object;
//            if (packet.packetType == ESPacketTypeAnnotation) {
//                [self didReceiveAnnotationFromPresenter:(ESAnnotationPacket *) object];
//            } else if (packet.packetType == ESPacketTypeLaserPointer) {
//                [self didReceiveLaserPointerPacketFromPresenter:(ESLaserPointerPacket *) object];
//            } else if (packet.packetType == ESPacketTypeZoomAndPan){
//                [self didReceiveZoomTransitionPacketFromPreseneter:(ESZoomTransitionPacket *)object];
//            } else if (packet.packetType == ESPacketTypeChangePage){
//                [self didReceivePageTransitionFromPresenter:(ESPageTransitionPacket *)object];
//            } else if (packet.packetType == ESPacketTypeQuestion) {
//                [self didreceiveQuestionFromPresenter:(ESQuestionPacket *) object];
//            } else if (packet.packetType == ESPacketTypeAnswer) {
//                [self didReceiveAnswerFromAttendee:(ESAnswerPacket *) object];
//            } else if (packet.packetType == ESPacketTypeAttendeeQuestion) {
//                [self didReceiveQuestionFromAttendee:(ESAttendeeQuestionPacket *) object];
//            } else if (packet.packetType == ESPacketTypeAttendeeQuestionSwitch){
//                [self didRecieveAttendeeQuestionStatusFromPresenter:(ESAttendeeQuestionSwitchPacket *)object];
//            } else if (packet.packetType == ESPacketTypeEndPresentation){
//                [self didReceiveEndPresentationFromPresenter:(ESEndPresentationPacket *) object];
//            } else if (packet.packetType == ESPacketTypeWhiteBoard){
//                [self didReceiveToGoWhiteBoardFromPresenter:(ESWhiteBoardPacket *)object];
//            } else if (packet.packetType == ESPacketTypeDismissWhiteBoard){
//                [self didReceiveToDismissWhiteBoardFromPresenter:(ESPaintingDoneButtonPacket *)object];
//            }else if (packet.packetType == ESPacketTypeVideo){
//                [self didReceiveVideoPlayerPacketFromPresenter:(ESVideoPlayerPacket *)object];
//            }else if (packet.packetType == ESPacketTypeText){
//                [self didReceiveTextPacketFromPresenter:(ESTextPacket *)object];
//            }else if (packet.packetType == ESPacketTypePicture){
//                [self didReceivePicturePacketFromPresenter:(ESPicturePacket *)object];
//            }
//            else if ([_sessionDataDelegate handlePacket:packet fromPeerWithId:peer]) {
//
//            } else {
//                NSLog(@"Warning: Packet type undefined: %zd",packet.packetType);
//            }
//        }
//    }
//    @catch (NSException *exception) {
//#if DEBUG
//        NSLog(@"Exception: %@",exception);
//#endif
//    }
//}

-(CGRect)frameOfPaintPallete{
    CGFloat height = 0;
    
    //    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    BOOL iPad = NO;
    BOOL isPotrait = NO;
    
#ifdef UI_USER_INTERFACE_IDIOM
    iPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#endif
    if (!iPad) {
        
#ifdef UIDeviceOrientationIsPortrait//(orientation)
        isPotrait = (UIDeviceOrientationIsPortrait(orientation) == UIDeviceOrientationPortrait ||UIDeviceOrientationIsPortrait(orientation) == UIDeviceOrientationPortraitUpsideDown);
#endif
    }
    CGFloat width = 0.f;
    if (!iPad){
        if( isPotrait) {
            height = 130.f;
            width = 250.f;
        }else{
            width = 385.f;
            height =  80.f;
        }
    }else{//for iPAD
        width = 385.f;
        height =  80.f;
    }
    UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
    CGFloat constantVal=0;
    if (UIDeviceOrientationIsPortrait(orientation)) {
        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
        
        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
            constantVal = 110;
        }
        else
        {
            constantVal = 76;
        }
    }


    CGRect frame = CGRectMake(self.view.bounds.size.width-width, self.view.bounds.size.height-constantVal - height,width , height);
    
    return frame;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    if ([appDelegate.sessionState isEqualToString:@"1"] && [appDelegate.role isEqualToString:ATTENDEE]) {
        [self handleDoubleTap:nil];
    }
    // Do any additional setup after loading the view.
    //    if (!self.sessionController || [[NSUserDefaults standardUserDefaults] integerForKey:KEY_USER_ROLE] == ESUserRolePresenter) {
    
    if (![appDelegate.sessionState isEqualToString:@"1"] || ![appDelegate.role isEqualToString:ATTENDEE]) {
        
        //        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        BOOL isPotrait = NO;
        BOOL iPad = NO;
        
#ifdef UI_USER_INTERFACE_IDIOM
        iPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#endif
        if (!iPad) {
            
#ifdef UIDeviceOrientationIsPortrait
            isPotrait = (UIDeviceOrientationIsPortrait(orientation) == UIDeviceOrientationPortrait ||UIDeviceOrientationIsPortrait(orientation) == UIDeviceOrientationPortraitUpsideDown);
#endif
        }
        
        paintPalette = [[ESPaintPaletteView alloc]initWithFrame:[self frameOfPaintPallete] isiPodPortrait:isPotrait];
        //        paintPalette = [[ESPaintPaletteView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 385, self.view.bounds.size.height - self.navigationController.navigationBar.frame.size.height - 80, 385, 80)];
        paintPalette.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
        [self.view addSubview:paintPalette];
        
        [paintPalette.annotationButton addTarget:self action:@selector(annotationButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [paintPalette.addTextButton addTarget:self action:@selector(addTextButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [paintPalette.addPictureButton addTarget:self action:@selector(addPictureButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    //    [self.mainPagebar removeFromSuperview];
    //    self.mainPagebar = [[ESReaderSliderPageBar alloc] initWithFrame:self.mainPagebar.frame document:self.document];
    //    self.mainPagebar.delegate = self;
    if ([appDelegate.sessionState isEqualToString:@"1"] && [appDelegate.role isEqualToString:ATTENDEE] && !self.attendeeCanBrowse) {
        self.mainPagebar.alpha = 0.f;
    } else {
        self.mainPagebar.alpha = 1.f;
    }
    
    [self.view addSubview:self.mainPagebar];
    
    [self setupNavigationButtonItems];
    //    [self addAnnotation];
    [self addATimer];
    if ([appDelegate.sessionState isEqualToString:@"1"]) {
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
        if (!self.attendeeCanBrowse && [appDelegate.sessionState isEqualToString:@"1"] && ([[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRoleAttendee)) {
            //hide slider
            self.mainPagebar.hidden = YES;
        }
        else{
            //show slider
            self.mainPagebar.hidden = NO;
        }
    }
}

- (void) annotationButtonClicked:(UIButton *) annotationButton {
#if X_AUDIENCE
    [self showAlert];
    return;
#else
    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    contentView.paintPalette = annotationButton.selected ? paintPalette : nil;
    [self.view bringSubviewToFront:paintPalette];
    
#endif
    
    //expand annotation view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma ReciverMethods

-(void)reciveDatawhenPresenterZoomTheScreen:(struct pdfInfo*) info
{
    //    NSLog(@"%s",__FUNCTION__);
    //    CGPoint scrollViewContentOffSet = info->scrollViewOffSet;
    //    NSLog(@"Reciver:%f%f",scrollViewContentOffSet.x,scrollViewContentOffSet.y);
    NSInteger currentPageNumber = info->page;
    //    NSLog(@"Reciver:%ld",(long)currentPageNumber);
    CGFloat zoomValue = info->ZoomValue;
    //    NSLog(@"Reciver:%f",zoomValue);
    NSInteger senderDeviceType = info->deviceType;
    //    NSLog(@"Reciver:%ld",(long)senderDeviceType);
    CGFloat senderMinimumZoomValue = info->senderMinimumZoomValue;
    NSNumber *key = [NSNumber numberWithInteger:currentPageNumber]; // Page number key
    ReaderContentView *targetView = [super.contentViews objectForKey:key];
    CGFloat reciverMinimumZoomValue = [targetView minimumZoomScale];
    //    NSLog(@"reciverMinimumZoomValue:%f",reciverMinimumZoomValue);
    CGSize reciverScreenSize;
    CGSize senderScreenSize;
    NSInteger reciverDeviceType;
    CGFloat finalZoomValue;
    switch (senderDeviceType) {
        case 0:
            senderScreenSize = CGSizeMake(768, 1024);
            break;
        case 1:
            senderScreenSize = CGSizeMake(320, 568);
            break;
        case 2:
            senderScreenSize = CGSizeMake(320, 480);
            break;
            
        default:
            break;
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
    {
        reciverScreenSize = [[UIScreen mainScreen] bounds].size;
        if (reciverScreenSize.height > 480.0f)
        {
            /*Do iPhone 5 stuff here.*/
            reciverDeviceType = 1;
        }
        else
        {
            /*Do iPhone Classic stuff here.*/
            reciverDeviceType = 2;
        }
    }
    else{
        reciverScreenSize = [[UIScreen mainScreen] bounds].size;
        reciverDeviceType = 0;
    }
    
    
    if (reciverDeviceType == senderDeviceType)
    {
        finalZoomValue = zoomValue;
    }
    else
    {
        NSString * zoomvaluestr = [NSString stringWithFormat:@"%.4f",zoomValue];
        NSString * sendervaluestr = [NSString stringWithFormat:@"%.4f",senderMinimumZoomValue];
        if ([zoomvaluestr isEqualToString:sendervaluestr])
        {
            finalZoomValue = reciverMinimumZoomValue;
        }
        else
        {
            
            if (senderMinimumZoomValue>reciverMinimumZoomValue)
            {
                finalZoomValue = (zoomValue/senderMinimumZoomValue)*(reciverMinimumZoomValue+0.37);
            }
            else
            {
                finalZoomValue = (zoomValue/senderMinimumZoomValue)*(reciverMinimumZoomValue-0.22);
            }
            
        }
        
    }
    
    [targetView setZoomScale:finalZoomValue animated:YES];
    
    
}

#pragma private Methods

- (void) handleSingleTapOnContentView:(UITapGestureRecognizer *) gestureRecognizer {
    
    if ([appDelegate.sessionState isEqualToString:@"1"] && [appDelegate.role isEqualToString:ATTENDEE] && self.attendeeCanBrowse == NO) {
        return;
    }
    CGRect viewRect = gestureRecognizer.view.bounds;
    CGRect nextPageRect = viewRect;
    nextPageRect.size.width = SINGLE_TAP_AREA_SIZE;
    nextPageRect.origin.x = (viewRect.size.width - SINGLE_TAP_AREA_SIZE);
    CGPoint point = [gestureRecognizer locationInView:gestureRecognizer.view];
    
    
    if (CGRectContainsPoint(nextPageRect, point)) // page++ area
    {
        //        [self incrementPageNumber];
        //        self.isNextPageValue = YES;
        
        [self showDocumentPage:[super.document.pageNumber integerValue]+1];
        NSLog(@"%li",(long)[super.document.pageNumber integerValue]);
        super.selectedIndexRow=[super.document.pageNumber integerValue]-1;
        [super.colPages reloadData];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:super.selectedIndexRow inSection:0];
        if (indexPath.row < 0)
        {
            return;
        }
        [super.colPages scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:YES];

        return;
    }
    
    CGRect prevPageRect = viewRect;
    prevPageRect.size.width = SINGLE_TAP_AREA_SIZE;
    
    if (CGRectContainsPoint(prevPageRect, point)) // page-- area
    {
        //        [self decrementPageNumber];
        //        self.isNextPageValue = NO;
        
        [self showDocumentPage:[super.document.pageNumber integerValue]-1];
        super.selectedIndexRow=[super.document.pageNumber integerValue]-1;
        [super.colPages reloadData];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:super.selectedIndexRow inSection:0];
        if (indexPath.row < 0)
        {
            return;
        }
        [super.colPages scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:YES];

        return;
    }
    
}

-(void)setAnimationTransitions:(BOOL) advancePage {
    
    NSInteger transitionType = [[[NSUserDefaults standardUserDefaults] valueForKey:KEY_PAGE_TRANSITION_ANIMATION] integerValue];
    //    NSString *str = [self.settingData valueForKey:@"AnimationName"];
    if (transitionType == ESPageTransitionAnimationRandom)
    {
        int randNum = rand() % (6) + 1; //create the random number.
        [self setupNonRandomAnimations:randNum advancePage:advancePage];
    } else {
        [self setupNonRandomAnimations:transitionType advancePage:advancePage];
    }
}

-(void)setupNonRandomAnimations:(ESPageTransitionAnimation) animation advancePage:(BOOL) advancePage
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if (animation == ESPageTransitionAnimationNone)
    {
        [UIView setAnimationTransition: UIViewAnimationTransitionNone
                               forView:self.view cache:YES];
    }
    else if (animation == ESPageTransitionAnimationCurl) {
        if (advancePage) {
            [UIView setAnimationTransition: UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
        } else {
            [UIView setAnimationTransition: UIViewAnimationTransitionCurlDown
                                   forView:self.view cache:YES];
        }
        
    } else if (animation == ESPageTransitionAnimationFlip) {
        if (advancePage) {
            [UIView setAnimationTransition: UIViewAnimationTransitionFlipFromRight
                                   forView:self.view cache:YES];
        } else {
            [UIView setAnimationTransition: UIViewAnimationTransitionFlipFromLeft forView:self.view cache:YES];
        }
    } else {
        CATransition* transition = [CATransition animation];
        transition.startProgress = 0;
        transition.endProgress = 1.0;
        
        if (advancePage) {
            transition.subtype = kCATransitionFromRight;
        }else{
            transition.subtype = kCATransitionFromLeft;
        }
        transition.duration = 0.5;
        
        
        if (animation == ESPageTransitionAnimationMoveIn)
        {
            transition.type = kCATransitionMoveIn;
            
        }
        else if (animation == ESPageTransitionAnimationPush)
        {
            transition.type = kCATransitionPush;
            
        }
        else if (animation == ESPageTransitionAnimationReveal)
        {
            transition.type = kCATransitionReveal;
        }
        else if (animation == ESPageTransitionAnimationFade)
        {
            transition.type = kCATransitionFade;
        }
        
        // Add the transition animation to both layers
        [self.theScrollView.layer addAnimation:transition forKey:@"transition"];
        
    }
    [UIView commitAnimations];
}

- (void)contentView:(ReaderContentView *)contentView touchesBegan:(NSSet *)touches {
    
}

- (void) handleDoubleTapOnContentView:(UITapGestureRecognizer *)recognizer {
    [self handleDoubleTap:recognizer];
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)recognizer
{
    
    BOOL animateIn = self.navigationController.navigationBarHidden;
    
    //[[UIApplication sharedApplication] setStatusBarHidden:!animateIn withAnimation:animateIn?UIStatusBarAnimationNone:UIStatusBarAnimationSlide];
    [self.navigationController setNavigationBarHidden:!animateIn animated:YES];
    
    [super.mainToolbar hideToolbar]; [super.mainPagebar hidePagebar]; // Show
    
    super.colPages.hidden=!super.colPages.hidden;
    BOOL isHide=NO;
    if (super.colPages.hidden) {
        isHide=YES;
    }
    else
    {
        isHide=NO;
    }
    [self.navigationController setNavigationBarHidden:isHide animated:YES];
    
    [super didRotate:nil];

//    [UIView animateWithDuration:0.35 animations:^{
//        super.mainPagebar.center = CGPointMake(super.mainPagebar.center.x, super.mainPagebar.center.y + (animateIn?-1:1) * super.mainPagebar.frame.size.height);
//    } completion:^(BOOL finished){
//        super.mainPagebar.hidden = !self.attendeeCanBrowse ? YES:!animateIn;
//    }];
}

- (void) showDocumentPage:(NSInteger)page {
    
    NSInteger oldCurrentPage = self.currentPage;
    
    [super showDocumentPage:page];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:KEY_DISPLAY_SLIDE]){
        self.mainPagebar.pageNumberView.hidden = NO;
    }else{
        self.mainPagebar.pageNumberView.hidden = YES;
    }
    
    if (self.currentPage != oldCurrentPage) {
        
        
#if X_PRESENTER
        
        ESPDFContentView *oldContentView = [self.contentViews objectForKey:[ NSNumber numberWithInteger:oldCurrentPage]];
        oldContentView.paintPalette = nil;
        
#endif
        ESPDFContentView *contentView = [self.contentViews objectForKey:[ NSNumber numberWithInteger:self.currentPage]];
        
#if X_PRESENTER
        contentView.paintPalette = paintPalette.annotationButton.selected ? paintPalette: nil;
        contentView.paintPalette = paintPalette.addTextButton.selected ? paintPalette:nil;
        if ([appDelegate.sessionState isEqualToString:@"1"] || [appDelegate.role isEqualToString:PRESENTER]) {
            [[contentView containerView] addGestureRecognizer:longPressToPlayVideo];
        }
#endif
        //A gesture recognizer detatches itself from old view if it is added to a new view.
        [[contentView containerView] addGestureRecognizer:singleTapGestureRecognizer];
        [[contentView containerView] addGestureRecognizer:doubleTapGestureRecognizer];
        
        [[contentView annotationsLayer] clear];
        [[contentView annotationsLayer] addCurves:[pagesCurvesArray objectAtIndex:self.currentPage-1]];
        [contentView clearText];
        [contentView addTextToPage:[pagesAddTextArray objectAtIndex:self.currentPage - 1]];
        [[contentView annotationsLayer] setNeedsDisplay];
        
        [contentView clearPicture];
        [contentView addPictureToPage:[pagesPictureArray objectAtIndex:self.currentPage - 1 ]];
        
    }
    
    if (oldCurrentPage <=0 || self.currentPage == oldCurrentPage) {
        //no transitions required or no gestures needed to be added.
        return;
    }
    
    
    
    if ([appDelegate.sessionState isEqualToString:@"1"] && [[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter) {
        //        int transitionType = [[[NSUserDefaults standardUserDefaults] valueForKey:KEY_PAGE_TRANSITION_ANIMATION] integerValue];
        
        ESPageTransitionPacket *packet = [ESPageTransitionPacket packetWithPageNo:self.currentPage];
        [self transferPacket:packet];
    }
    
    if (ABS(self.currentPage - oldCurrentPage) == 1) {
        [self setAnimationTransitions: (self.currentPage-oldCurrentPage>0)];
    }
}

-(UIView *)rightNavButtonView{
    UIView *myView = [[UIView alloc]init];
    myView.frame = CGRectMake(0, 0, 85, 44);
    
    ESCustomBadgeButton *showButton=[[ESCustomBadgeButton alloc] initWithFrame:CGRectMake(0, 10, 35, 30)];
    [showButton setTitle:@"Queries" forState:UIControlStateNormal];
    [showButton setbadgeValue:[NSString stringWithFormat:@"%lu",(unsigned long)[attendeeAskQuestionArray count]]];
    [showButton addTarget:self action:@selector(showAttendeesQuestion:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *questionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    questionButton.frame =CGRectMake(38, 10, 50, 30);
    [questionButton setBackgroundImage:[UIImage imageNamed:@"show-Button"] forState:UIControlStateNormal];
    [questionButton setTitle:@"Question" forState:UIControlStateNormal];
    questionButton.titleLabel.font = [UIFont systemFontOfSize:12.f];
    [questionButton addTarget:self action:@selector(questionButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter) {
        //        if (attendeeAskQuestionArray.count > 0) {
        //            showButton.hidden = NO;
        //
        //        }
        showButton.hidden = NO;
    }else{
        showButton.hidden = YES;
    }
    [myView addSubview:questionButton];
    [myView addSubview:showButton];
    return myView;
}

-(void)setupNavigationButtonItems
{
    NSString *queryButtonTitle = nil;//
    if ([attendeeAskQuestionArray count] == 0) {
        queryButtonTitle =@"?(0)";
    }else{
        queryButtonTitle = [NSString stringWithFormat:@"?(%lu)",(unsigned long)[attendeeAskQuestionArray count]];
    }
    NSString *questionButtonTitle = nil;
    NSArray *rightBarButtonArray = nil;
    //For attendee "Query"
    //For presenter @"Questions" @"?(x)"
    
#if  X_PRESENTER
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter) {
        questionButtonTitle = @"Questions";
    }else{
        questionButtonTitle = @"Query";
    }
#endif
    
#if X_AUDIENCE
    //PRESENTER title can't be questions as it is not able  to create survey questions in real time
    questionButtonTitle = @"Query";
    
#endif
    
    UIBarButtonItem *questionButton = [[UIBarButtonItem alloc] initWithTitle:questionButtonTitle style:UIBarButtonItemStylePlain target:self action:@selector(questionButtonTouchUpInside:)];
    //    UIBarButtonItem *presentationSettingButton = [[UIBarButtonItem alloc]initWithTitle:@"Settings" style:UIBarButtonItemStyleBordered target:self action:@selector(settingsButtonTouchUPInside:)];
    
#if  X_PRESENTER
    UIBarButtonItem *queryButton = [[UIBarButtonItem alloc] initWithTitle:queryButtonTitle style:UIBarButtonItemStylePlain target:self action:@selector(showAttendeesQuestion:)];
    
    UIBarButtonItem *presentationSettingButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(settingsButtonTouchUPInside:)];
#endif
    
    
    UIBarButtonItem *whiteBoardButton = [[UIBarButtonItem alloc]initWithTitle:@"Board" style:UIBarButtonItemStylePlain target:self action:@selector(openWhiteBoardForPresenter:)];
    //if connected
    if ([appDelegate.sessionState isEqualToString:@"1"]) {
        if ([appDelegate.role isEqualToString:PRESENTER]) {
            
#if  X_PRESENTER
            
            rightBarButtonArray = [NSArray arrayWithObjects:questionButton,queryButton,presentationSettingButton,
#if ENABLE_WHITEBOARD
                                   whiteBoardButton,
#endif
                                   nil];
#endif
            
        }else{//GKSessionModeClient
            //Disable whiteBoardButton / presentationSettingButton
            //            NSLog(@"Attendee can query %@",self.attendeeCanQuery?@"YES":@"NO");
            if (self.attendeeCanQuery) {
                rightBarButtonArray = [NSArray arrayWithObjects:questionButton, nil];
            } else {
                rightBarButtonArray = nil;
            }
        }
    }
    //No session
    else{
        
        //show white board for presenter /interactor even if not connected and show alert for audience
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter) {
            rightBarButtonArray = [NSArray arrayWithObjects:questionButton,whiteBoardButton, nil];
        }else{
            rightBarButtonArray = [NSArray arrayWithObjects:whiteBoardButton, nil];
        }
    }
    //#if X_AUDIENCE
    //    if (_gkSession) {
    //        questionButtonTitle = @"Query";
    //        rightBarButtonArray = [NSArray arrayWithObjects:questionButton, nil];
    //    }
    //
    //#endif
    
    [self.navigationItem setRightBarButtonItems:rightBarButtonArray animated:YES];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonTouchUpInside:)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    if ([appDelegate.sessionState isEqualToString:@"1"] && [[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRoleAttendee){
        [self.navigationItem.leftBarButtonItem setEnabled:NO];
    }else{
        [self.navigationItem.leftBarButtonItem setEnabled:YES];
    }
}

-(void)addATimer
{
    CGFloat hieght;
    CGFloat xOrign;
    CGFloat fontSize;
    CGFloat width;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
        CGFloat constantVal=0;
        if (UIDeviceOrientationIsPortrait(orientation)) {
            CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
            
            if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
                constantVal = 34;
            }
            else
            {
                constantVal = 0;
            }
            hieght = self.view.bounds.size.height-super.colPages.bounds.size.height-constantVal;
            xOrign = 10;
        }
        else
        {
            CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
            
            if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
                constantVal = 34;
                xOrign = 115;
            }
            else
            {
                constantVal = 0;
                xOrign = 80;
            }
            hieght = self.view.bounds.size.height-constantVal;
        }
        
        fontSize = 12;
        width = 80;
    }
    else
    {
        UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
        CGFloat constantVal=0;
        if (UIDeviceOrientationIsPortrait(orientation)) {
            hieght = self.view.bounds.size.height-super.colPages.bounds.size.height;
            xOrign = 10;
        }
        else
        {
            hieght = self.view.bounds.size.height;
            xOrign = 80;
        }
        fontSize = 20;
        width = 120;
    }
    
    
    progress=[[UILabel alloc] initWithFrame:CGRectMake(xOrign, hieght-22, width, 22)];
    progress.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
    self.counterValue = 0;
    progress.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    progress.layer.shadowColor = [UIColor colorWithWhite:0.0f alpha:0.6f].CGColor;
    progress.layer.shadowPath = [UIBezierPath bezierPathWithRect:progress.bounds].CGPath;
    progress.layer.shadowRadius = 2.0f; progress.layer.shadowOpacity = 1.0f;
    progress.text = [NSString stringWithFormat:@"Timer:%d", self.counterValue];
    progress.textColor=[UIColor whiteColor];
    progress.textAlignment = ESTextAlignmentCenter;
    [progress setFont:[UIFont systemFontOfSize:fontSize]];
    progress.backgroundColor=[UIColor colorWithWhite:0.0f alpha:0.4f];
    [self.view addSubview:progress];
    [self.view bringSubviewToFront:progress];
    [progress setHidden:YES];
    ;
    //IsTimeRequired
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([userDefaults boolForKey:KEY_DISPLAY_TIMER]){
        
        [progress setHidden:NO];
        TimerTarget *timerTarget = [[TimerTarget alloc] init];
        timerTarget.realTarget = self;
        timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                 target:timerTarget
                                               selector:@selector(timerFired:)
                                               userInfo:nil
                                                repeats:YES];
    }
    else
    {
        [progress setHidden:YES];
    }
}

- (void) saveReceivedFile
{
    
}

-(void)timerFired:(id) theTimer
{
    
    if(self.counterValue >= 24 * 60 * 60){
        [self killTimer];
    }
    
    if (self.counterValue / 3600 > 0) {
        progress.text = [NSString stringWithFormat:@"Timer:%@", [NSString stringWithFormat:@"%02d:%02d:%02d",self.counterValue/3600 , self.counterValue/60, self.counterValue % 60]];
    } else {
        progress.text = [NSString stringWithFormat:@"Timer:%@", [NSString stringWithFormat:@"%02d:%02d",self.counterValue/60, self.counterValue % 60]];
    }
    self.counterValue++;
}


- (void)killTimer{
    if(timer){
        [timer invalidate];
        timer = nil;
    }
}

#pragma Animation Methods


- (IBAction)thumbsButtonTouchUpInside:(id)sender
{
    //    [self tappedInToolbar:super.mainToolbar thumbsButton:sender];
    
}

-(IBAction)showAttendeesQuestion:(id)sender{
    
    
    
#if X_AUDIENCE
    [self showAlert];
    return;
#endif
#if X_PRESENTER
    
    if (settingsPopOverView != nil) {
        //[settingsPopoverVC dismissPopoverAnimated:YES];
        [[settingsPopOverView presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        settingsPopOverView = nil;
    }
    if (doneButtonActionSheet != nil) {
        //[doneButtonActionSheet dismissWithClickedButtonIndex:doneButtonActionSheet.cancelButtonIndex animated:YES];
        [doneButtonActionSheet dismissViewControllerAnimated:YES completion:nil];
    }
    ESAttendeeQuestionListViewController *attendeeQuestionListVC = [[ESAttendeeQuestionListViewController alloc]initWithStyle:UITableViewStylePlain];
    //    attendeeQuestionListVC.delegate = self;
    attendeeQuestionListVC.attendeeAskedQuestionArray = attendeeAskQuestionArray;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:attendeeQuestionListVC];
    navController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:navController animated:YES completion:nil];
#endif
}

-(void )attendeesBrowseTurnedOn:(BOOL)status andQuery:(BOOL)qStatus{
    
    ESAttendeeQuestionSwitchPacket *packet = [ESAttendeeQuestionSwitchPacket packectWithQuerySwichValueStatus:qStatus andBrowseSatuts:status];
    [self transferPacket:packet];
    
}
-(IBAction)questionButtonTouchUpInside:(id)sender
{
#if X_AUDIENCE
    [self showAlert];
    return;
#endif
    if (settingsPopOverView != nil) {
        //[settingsPopoverVC dismissPopoverAnimated:YES];
        [[settingsPopOverView presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        settingsPopOverView = nil;
    }
    if (doneButtonActionSheet != nil) {
        //[doneButtonActionSheet dismissWithClickedButtonIndex:doneButtonActionSheet.cancelButtonIndex animated:YES];
        [doneButtonActionSheet dismissViewControllerAnimated:YES completion:nil];
    }
    
    //            NSString *str = self.sessionController.session.peerID;
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter) {
        
        if (!_questions) {
            BOOL success = [self initializeQuestionnaire];
            if (!success) {
                return;
            }
        }
        
        ESQuestionsTypeViewController *questionTableVC = [[ESQuestionsTypeViewController alloc]initWithStyle:UITableViewStylePlain];
        questionTableVC.questionList = self.questions;
        questionTableVC.delegate = [appDelegate.sessionState isEqualToString:@"1"]?self:nil;
        questionTableVC.delegate = self;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:questionTableVC];
        navController.modalPresentationStyle = UIModalPresentationPageSheet;
        [self presentViewController:navController animated:YES completion:nil];
       // [self.tableView reloadData];
        
        
        
        
        

      
      
//
//        ESQuestionsListViewController *questionTableVC = [[ESQuestionsListViewController alloc]initWithStyle:UITableViewStylePlain];
//        questionTableVC.questionArray = self.questions;
//        questionTableVC.delegate = [appDelegate.sessionState isEqualToString:@"1"]?self:nil;
//
//
//        //FIXME: Restore conditional statement.
//        questionTableVC.delegate = [appDelegate.sessionState isEqualToString:@"1"]?self:nil;
//        questionTableVC.delegate = self;
//
//        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:questionTableVC];
//        navController.modalPresentationStyle = UIModalPresentationPageSheet;
//
//        [self presentViewController:navController animated:YES completion:nil];
        
        
        
        
        //        UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissViewControllerAnimated:completion:)];
        //        questionTableVC.navigationItem.leftBarButtonItem = closeButtonItem;
        
    }else{
        ESTextViewController *textViewController = [[ESTextViewController alloc] initWithTextViewControllerWithText:nil];
        textViewController.delegate = self;
        textViewController.title = @"Describe your query";
        //        textViewController.attendeeName = [self.sessionController.session displayNameForPeer:self.sessionController.session.peerID];
        textViewController.attendeeName = [self.multipeerManager displayName];
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:textViewController];
        //        navController.modalPresentationStyle = UIModalPresentationFormSheet;
        navController.modalPresentationStyle = UIModalPresentationPageSheet;
        [self presentViewController:navController animated:YES completion:nil];
        
        //add a close button item.
        //        UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissViewControllerAnimated:completion:)];
        //        textViewController.navigationItem.leftBarButtonItem = closeButtonItem;
    }
}


- (BOOL) initializeQuestionnaire {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    NSString *pathLocal = [documentsDir stringByAppendingPathComponent:@"questions.xml"];
   // NSString *filePath = [[NSBundle mainBundle] pathForResource:@"questions.xml" ofType:@".xml"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:pathLocal]) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"No stored questions"
                                     message:@"Unable to find questions.xml file in documents. Import it from iTunes or try to create a new question."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        //[[[UIAlertView alloc] initWithTitle:@"No stored questions" message:@"Unable to find questions.xml file in documents. Import it from iTunes or try to create a new question." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return NO;
    }
    NSData *xmlData = [NSData dataWithContentsOfFile:pathLocal];
    
    ESQuestionnaireParser *questionParser = [[ESQuestionnaireParser alloc] init];
    [questionParser parseWithData:xmlData];
    self.questions =  questionParser.questionArray;
    return YES;
   
}

-(IBAction)cancelButtonTouchUpInside:(UIBarButtonItem *)sender
{
    if (settingsPopOverView != nil) {
        [[settingsPopOverView presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        settingsPopOverView = nil;
    }
    if ([appDelegate.sessionState isEqualToString:@"1"] && [[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRoleAttendee) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    
    //Shows an action sheet with the following options.
    
    //end presentation and cancel for x-audience ,no "you can"
    //#if  X_INTERACTOR
    
    doneButtonActionSheet = [UIAlertController alertControllerWithTitle:@"You can"
                                                          message:nil
                                                   preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *endAction = [UIAlertAction actionWithTitle:@"End Presentation"
                                             style:UIAlertActionStyleDestructive
                                           handler:^(UIAlertAction *action) {
                                               // do destructive stuff here
                                               [self goBack:nil];
                                           }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                           style:UIAlertActionStyleCancel
                                         handler:^(UIAlertAction *action) {
                                             // do something here
                                             self.navigationItem.leftBarButtonItem.enabled = YES;
                                         }];
    
    /*UIAlertAction *qaEmailAction = [UIAlertAction actionWithTitle:@"Email Q & A asked"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              // do destructive stuff here
#if X_PRESENTER
                                                              [self emailQuestionsAndAnswers];
#else
                                                              [self showAlert];
                                                              return;
#endif
                                                          }];
    UIAlertAction *pdfEmailAction = [UIAlertAction actionWithTitle:@"Email annotated PDF"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *action) {
                                                            // do something here
#if !X_AUDIENCE
                                                            [self emailAnnotatedPDF];
#else
                                                            [self showAlert];
#endif
                                                        }];*/

    UIAlertAction *answerQueriesAction = [UIAlertAction actionWithTitle:@"Answer Queries"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              // do destructive stuff here
#if X_PRESENTER
                                                              [self showAttendeesQuestion:nil];
#else
                                                              [self showAlert];
                                                              return;
#endif

                                                          }];

    // note: you can control the order buttons are shown, unlike UIActionSheet
    [doneButtonActionSheet addAction:endAction];
    [doneButtonActionSheet addAction:cancelAction];
    //[doneButtonActionSheet addAction:qaEmailAction];
    //[doneButtonActionSheet addAction:pdfEmailAction];
    [doneButtonActionSheet addAction:answerQueriesAction];
    
    [doneButtonActionSheet setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [doneButtonActionSheet
                                                     popoverPresentationController];
    
    //doneButtonActionSheet = [[UIActionSheet alloc] initWithTitle:@"You can" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"End Presentation" otherButtonTitles:@"Email Q & A asked", @"Email annotated PDF",@"Answer Queries", nil];
    //#endif
    //#if X_PRESENTER
    //    doneButtonActionSheet = [[UIActionSheet alloc] initWithTitle:@"You can" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"End Presentation" otherButtonTitles:@"Email annotated PDF", nil];
    //
    //#endif
    //#if X_AUDIENCE
    //    doneButtonActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"End Presentation" otherButtonTitles: nil];
    //
    //#endif
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        //[doneButtonActionSheet showFromBarButtonItem:sender animated:YES];
        popPresenter.barButtonItem = sender;
        [self presentViewController:doneButtonActionSheet animated:YES completion:nil];

    } else {
        //[doneButtonActionSheet showInView:self.view];
        [self presentViewController:doneButtonActionSheet animated:YES completion:nil];

    }
    //doneButtonActionSheet.tag = DONE_ACTIONSHEET_TAG;
    //self.navigationItem.leftBarButtonItem.enabled = NO;
}

//- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
//    
//    if (buttonIndex == actionSheet.cancelButtonIndex) {
//        self.navigationItem.leftBarButtonItem.enabled = YES;
//        return;
//    }
//    
//    if (actionSheet.tag == DONE_ACTIONSHEET_TAG) {
//        
//        self.navigationItem.leftBarButtonItem.enabled = YES;
//        
//        if (buttonIndex == actionSheet.destructiveButtonIndex) {
//            [self goBack:nil];
//        }
//        //#if X_PRESENTER
//        //        else if (buttonIndex == 1) {
//        //            //email annotated PDF.
//        //            [self emailAnnotatedPDF];
//        //        }
//        //#endif
//        //#if X_INTERACTOR
//        
//        else if (buttonIndex == 2) {
//            //email annotated PDF.
//#if !X_AUDIENCE
//            [self emailAnnotatedPDF];
//#else
//            [self showAlert];
//#endif
//        } else if (buttonIndex == 1) {
//            //email Q & A.
//#if X_PRESENTER
//            [self emailQuestionsAndAnswers];
//#else
//            [self showAlert];
//            return;
//#endif
//        } else if (buttonIndex == 3) {
//            //show attendees queries.
//#if X_PRESENTER
//            [self showAttendeesQuestion:nil];
//#else
//            [self showAlert];
//            return;
//#endif
//        }
//        //#endif
//    }else { //Add picture actionsheet
//        
//        if (buttonIndex == 0){
//            
//            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
//                
//                UIImagePickerController *imagePicker =[[UIImagePickerController alloc] init];
//                imagePicker.delegate = self;
//                imagePicker.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
//                imagePicker.allowsEditing = NO;
//                
//                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
//                    
//#if X_PRESENTER
//                    imagePopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
//                    [imagePopover presentPopoverFromRect:CGRectMake(0, 0, 320, 230) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//#endif
//                    
//                } else {
//                    
//                    [self presentViewController:imagePicker animated:YES completion:nil];
//                }
//            }
//        }else if (buttonIndex == 1){
//            
//            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
//                
//                UIImagePickerController *imagePicker =[[UIImagePickerController alloc] init];
//                imagePicker.delegate = self;
//                imagePicker.sourceType =UIImagePickerControllerSourceTypeCamera;
//                imagePicker.allowsEditing = NO;
//                
//                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
//#if X_PRESENTER
//                    imagePopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
//                    [imagePopover presentPopoverFromRect:CGRectMake(0, 0, 320, 230) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//#endif
//                    
//                } else {
//                    [self presentViewController:imagePicker animated:YES completion:nil];
//                    
//                    //                    [self dismissViewControllerAnimated:YES completion:nil];
//                }
//            }
//        }
//    }
//}

- (void) goBack:(id) sender {
    
    if ([appDelegate.sessionState isEqualToString:@"1"]){
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter)
        {
            ESEndPresentationPacket *packet = [ESEndPresentationPacket packet];
            [self transferPacket:packet];
            //            [self.navigationController popToRootViewControllerAnimated:YES];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } else {
        //        [self.navigationController popToRootViewControllerAnimated:YES];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}

- (void) emailQuestionsAndAnswers {
    /*Create a string like the following for all the questions that are selected in the current presentation. i.e. all objects of the array _presenterQuestionsAsked.
     
     Create a string like the following and write it on to a new PDF file. (Don't forget to use \n for next line) (http://developer.apple.com/library/ios/documentation/2ddrawing/conceptual/drawingprintingios/GeneratingPDF/GeneratingPDF.html#//apple_ref/doc/uid/TP40010156-CH10-SW5)
     
     
     Question 1. What is your age?
     Type - Multiple Choice
     
     Options
     1. less than 20
     2. between 20 and 40
     3. greater than 40
     
     Answers #6
     1. 16.7%
     2. 66.7%
     3. 16.7%
     
     Question 2. .....
     .....
     .....
     */
    
    if (_presenterQuestionsAsked.count == 0) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"No questions sent to attendees during this presentation"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        //[[[UIAlertView alloc] initWithTitle:nil message:@"No questions sent to attendees during this presentation" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    
    NSString* fileName = Q_A_PDF_FILE_NAME;
    
    NSArray *arrayPaths =
    NSSearchPathForDirectoriesInDomains(
                                        NSCachesDirectory,
                                        NSUserDomainMask,
                                        YES);
    NSString *path = [arrayPaths objectAtIndex:0];
    NSString* pdfFileName = [path stringByAppendingPathComponent:fileName];
    
    //-------------------
    
    UIGraphicsBeginPDFContextToFile(pdfFileName, CGRectZero, nil);
    
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    // Core Text draws from the bottom-left corner up, so flip
    // the current transform prior to drawing.
    //    CGContextTranslateCTM(currentContext, 0, pageSize.height);
    //    CGContextScaleCTM(currentContext, 1.0, -1.0);
    
    BOOL createNewPage = YES;
    BOOL drawAnswerGraph = NO;
    CGSize pageSize = CGSizeMake(612, 792);
    int i = 0;
    CFRange currentRange = CFRangeMake(0, 0);
    
    ESBarView *barView = [[ESBarView alloc] initWithFrame:CGRectMake(0, 0, pageSize.width-40, 200)];
    //    UIFont *font = [UIFont systemFontOfSize:14.f];
    
    int yOffset = 0;
    
    static int topMargin = 20;
    static int leftRightMargin = 20;
    NSString *string = nil;
    do {
        
        if (createNewPage) {
            // Mark the beginning of a new page.
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, pageSize.width, pageSize.height), nil);
            
            // Put the text matrix into a known state. This ensures
            // that no old scaling factors are left in place.
            CGContextSetTextMatrix(currentContext, CGAffineTransformIdentity);
            
            createNewPage = NO;
            yOffset = topMargin;
        }
        
        if (drawAnswerGraph == NO) {
            
            if (currentRange.location == 0) {//get a new string.
                string = [[_presenterQuestionsAsked objectAtIndex:i] textForDisplayInPDFWithQuestionNumber:i];
            }
            
            //create a string ref.
            CFAttributedStringRef currentText = CFAttributedStringCreate(NULL, (CFStringRef)string, NULL);
            
            //create a framesetter.
            CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(currentText);
            
            CFRange nextRange; //range after drawing the string.
            
            CGSize size = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, currentRange, NULL, CGSizeMake(pageSize.width - leftRightMargin - leftRightMargin, pageSize.height - yOffset - topMargin), &nextRange);
            
            CGRect    frameRect = CGRectMake(leftRightMargin, yOffset, pageSize.width - leftRightMargin - leftRightMargin, pageSize.height - yOffset - topMargin);
            
            CGMutablePathRef framePath = CGPathCreateMutable();
            CGPathAddRect(framePath, NULL, frameRect);
            
            
            // Get the frame that will do the rendering.
            // The currentRange variable specifies only the starting point. The framesetter
            // lays out as much text as will fit into the frame.
            CTFrameRef frameRef = CTFramesetterCreateFrame(framesetter, currentRange, framePath, NULL);
            CGPathRelease(framePath);
            
            // Draw the frame.
            CGContextSaveGState(currentContext);
            
            CGContextScaleCTM(currentContext, 1, -1);
            CGContextTranslateCTM(currentContext, 0, -pageSize.height - (yOffset - 20));
            
            CTFrameDraw(frameRef, currentContext);
            CGContextRestoreGState(currentContext);
            
            if (nextRange.location + nextRange.length < string.length) {
                //continue drawing the rest of the string coz no enough place on this page.
                createNewPage = YES;
                currentRange.location = nextRange.length;
                currentRange.length = 0;
            } else {
                yOffset += size.height;
                drawAnswerGraph = YES; //draw a view next.
                currentRange = CFRangeMake(0, 0);
            }
            
            CFRelease(frameRef);
            
            CFRelease(framesetter);
            CFRelease(currentText);
            
        } else {
            
            //draw a colored rectangle.
            //            CGContextSaveGState(currentContext);
            
            //            int heightOfTheRectangle = 10 * counter % 86 + 20;
            int heightOfTheRectangle = 200;
            
            ESQuestion *question = [_presenterQuestionsAsked objectAtIndex:i];
            
            barView.dataSource = [[ESBarViewDataSourceImpl alloc] initWithIndexSets:question.answersArray andNumberOfBars:question.options.count];
            
            if (pageSize.height - yOffset - topMargin < heightOfTheRectangle) {
                createNewPage = YES;
                continue; //continue drawing on the next page.
            }
            
            createNewPage = NO;
            i++;
            
            //            CGContextSetFillColorWithColor(currentContext, [UIColor blueColor].CGColor);
            //            CGContextFillRect(currentContext, CGRectMake(leftRightMargin, yOffset, pageSize.width - leftRightMargin - leftRightMargin, heightOfTheRectangle));
            
            CGRect rect = CGRectMake(leftRightMargin, yOffset, 320, heightOfTheRectangle);
            barView.frame = rect;
            
            CGContextSaveGState(currentContext);
            
            CGContextTranslateCTM(currentContext, 0, yOffset);
            [barView.layer renderInContext:currentContext];
            
            CGContextRestoreGState(currentContext);
            
            yOffset+= heightOfTheRectangle;
            drawAnswerGraph = NO;
            
        }
        
    } while (i<_presenterQuestionsAsked.count);
    
    UIGraphicsEndPDFContext();
    
    
    //-------------------
    
    NSString *emailTitle = @"Questions and Answers";
    
    NSString *messageBody = @"Here are the questions and audience responses from today's presentation.";
    
    //    NSArray *toRecipents = [NSArray arrayWithObject:@"rajiv@webileapps.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    //    [mc setToRecipients:toRecipents];
    
    // Get the resource path and read the file using NSData
    NSData *fileData = [NSData dataWithContentsOfFile:pdfFileName];
    
    // Add attachment
    [mc addAttachmentData:fileData mimeType:@"application/pdf" fileName:@"Questions&Answers.pdf"];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}

//This will add packet to the curves array.
//curves array is the array for each page.
- (void) processAnnotationPacket:(ESAnnotationPacket *)annotation {
    NSMutableArray *curvesArray = [pagesCurvesArray objectAtIndex:self.currentPage-1];
    if (annotation.annotationType == ESAnnotationTypeStart || annotation.annotationType == ESAnnotationTypeHighlightStart) {
        ESCurve *curve = [ESCurve new];
        [curve addPoint:annotation.point];
        curve.color = annotation.color;
        curve.lineWidth = annotation.lineWidth;
        [curvesArray addObject:curve];
    } else if (annotation.annotationType == ESAnnotationTypeUndo) {
        [curvesArray removeLastObject];
    } else if (annotation.annotationType == ESAnnotationTypeClear) {
        [curvesArray removeAllObjects];
    } else {
        ESCurve *curve = [curvesArray lastObject];
        [curve addPoint:annotation.point];
    }
}

-(void)transferPacketForAnnotationFromViewController:(ESWhiteBoardViewController *)viewController forPacket:(ESAnnotationPacket *)packet{
    [self transferPacket:packet];
}
-(void)transferPacketForTextFromViewController:(ESWhiteBoardViewController *)viewController forPacket:(ESTextPacket *)packet{
    [self transferPacket:packet];
}
-(void)transferPacketForPictureFromViewController:(ESWhiteBoardViewController *)viewController forPacket:(ESPicturePacket *)packet{
    [self transferPacket:packet];
}
-(void)processTextWithPacket:(ESTextPacket *)packet{
    
    NSMutableArray *textsArray = [pagesAddTextArray objectAtIndex:self.currentPage-1];
    
    if (packet.textState == ESTextStateRemoveText) {
        
        [textsArray removeObjectAtIndex:packet.indexValue];
        
    }else if (packet.textState == ESTextStateMoveText){
        
        ESText1 *text = [textsArray objectAtIndex:packet.indexValue];
        //        [text addPoint:packet.translationPoint];
        [text addFrame:packet.textFrame];
        text.indexValue = packet.indexValue;
        text.textFrame = packet.textFrame;
        //        text.textCenter = packet.translationPoint;
        
    }else if(packet.textState == ESTextStateAddTextDone || packet.textState == ESTextStateDismisPopover){
        
        ESText1 *text = [textsArray objectAtIndex:packet.indexValue];
        text.indexValue = packet.indexValue;
        text.fontSize = packet.textFontSize;
        text.color = packet.textColor;
        text.textValue = packet.text;
        text.textFrame = packet.textFrame;
        [text addFrame:packet.textFrame];
        
        if (text.textValue.length == 0) {
            [textsArray removeObjectAtIndex:packet.indexValue];
        }
        
    }else{
        
        ESText1 *text = [ESText1 new];
        [text addFrame:packet.textFrame];
        
        //        [text addPoint:packet.translationPoint];
        text.indexValue = packet.indexValue;
        text.fontSize = packet.textFontSize;
        text.color = packet.textColor;
        text.textValue = packet.text;
        text.textFrame = packet.textFrame;
        //        text.textCenter = packet.translationPoint;
        [textsArray addObject:text];
    }
}
-(void)processPictureWithPacket:(ESPicturePacket *)packet{
    
    NSMutableArray *picturesArray = [pagesPictureArray objectAtIndex:self.currentPage-1];
    
    if (packet.pictureState == ESStateRemovePicture) {
        
        [picturesArray removeObjectAtIndex:packet.pictureIndex];
        
    }else if (packet.pictureState == ESStateMovePicture){
        
        ESPicture *pic = [picturesArray objectAtIndex:packet.pictureIndex];
        pic.pictureIndex = packet.pictureIndex;
        pic.pictureFrame = packet.pictureFrame;
        [pic addPictureFrame:packet.pictureFrame];
        
    }else if(packet.pictureState == ESStateAddPicture){
        
        ESPicture *pic = [ESPicture new];
        pic.pictureIndex = packet.pictureIndex;
        pic.picture = packet.picture;
        pic.pictureFrame = packet.pictureFrame;
        [pic addPictureFrame:packet.pictureFrame];
        [picturesArray addObject:pic];
    }
    
}
-(NSString*)cacheFileName{
    NSArray* cacheDirectories = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask,YES);
    
    NSString* cacheDirectory = [cacheDirectories objectAtIndex:0];
    NSString* cacheFilename = [cacheDirectory stringByAppendingPathComponent:@"Cache.pdf"];
    return cacheFilename;
}
- (void) emailAnnotatedPDF {
    
    //Write cache file in BG
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0),
                   ^{
                       //Create a new PDF with all the annotations.
                       //                       NSLog(@"file started");
                       isPDFDrawn = NO;
                       [self writeToCacheFile:[self cacheFileName]];
                       //                       NSLog(@"file done");
                       isPDFDrawn = YES;
                       //if loading is showing ,present MailCompose VC
                       if (loadingView) {
                           [self loadingShow:NO];
                           [self presentMailComposeVCWithFileName:annotatedFileName];
                       }
                   });
    //show an alert for renaming the doc
    
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:@"Do you want to rename the document before sending it out"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   //UITextField *textField = [[alert textFields] objectAtIndex:0];
                                   
                                   annotatedFileName = @"";
                                   
                                   //Check if PDF drawing is completed or not. if not show an loading
                                   [self presentMailComposeVCWithFileName:annotatedFileName];
                               }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"AnnotatedPDF";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    
    UIAlertAction* renameButton = [UIAlertAction
                                   actionWithTitle:@"Rename"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                       UITextField *textField = [[alert textFields] objectAtIndex:0];
                                       
                                       annotatedFileName = [NSString stringWithFormat:@"%@.pdf",textField.text];
                                       
                                       //Check if PDF drawing is completed or not. if not show an loading
                                       [self presentMailComposeVCWithFileName:annotatedFileName];
                                   }];
    
    
    [alert addAction:noButton];
    [alert addAction:renameButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
//    UIAlertView *renameAlert = [[UIAlertView alloc]initWithTitle:@"" message:@"Do you want to rename the document before sending it out" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"Rename", nil];
//    
//    renameAlert.tag = RENAME_ANNOTATED_DOC_ALERT_TAG;
//    
//    renameAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//    
//    UITextField *textField = [renameAlert textFieldAtIndex:0];
//    
//    textField.placeholder = @"AnnotatedPDF";
//    
//    [renameAlert show];
    
    /*
     MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
     mc.mailComposeDelegate = self;
     [mc setSubject:@"Send email"];
     [mc setMessageBody:@"" isHTML:NO];
     
     NSData *fileData = [NSData dataWithContentsOfFile:cacheFilename];
     // Add attachment
     [mc addAttachmentData:fileData mimeType:@"application/pdf" fileName:@"AnnotatedPDF.pdf"];
     
     NSError *error = nil;
     [[NSFileManager defaultManager] removeItemAtPath:cacheFilename error:&error];
     
     // Present mail view controller on screen
     [self presentViewController:mc animated:YES completion:NULL];
     */
}
-(BOOL) writeToCacheFile:(NSString *) cacheFileName {
    BOOL retFlag =YES;
    
    //    const size_t numberOfPages = CGPDFDocumentGetNumberOfPages(pdf);
    
    retFlag = UIGraphicsBeginPDFContextToFile(cacheFileName, CGRectZero, nil);
    
    if (!retFlag) {
        //        NSLog(@"cannot create a file");
        return NO;
    }
    //    for (int i = 0; i< pagesAddTextArray.count; i++) {
    //        NSArray *b = [pagesAddTextArray objectAtIndex:i];
    //        for (ESText *text in b) {
    //        }
    //    }
    
    [ESAnnotatedPDF drawPDF:cacheFileName  forURL:self.document.fileURL withCurveArray:pagesCurvesArray withTextArray:pagesAddTextArray withPictureArray:pagesPictureArray];
    /*
     
     CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((__bridge CFURLRef)self.document.fileURL);
     
     const size_t numberOfPages = CGPDFDocumentGetNumberOfPages(pdf);
     
     retFlag = UIGraphicsBeginPDFContextToFile(cacheFileName, CGRectZero, nil);
     
     if (!retFlag) {
     NSLog(@"cannot create a file");
     return NO;
     }
     
     CGContextRef pdfContext = UIGraphicsGetCurrentContext();
     
     for(size_t page = 0; page < numberOfPages; page++)
     {
     CGContextSaveGState(pdfContext);
     
     //	Get the current page and page frame
     
     CGPDFPageRef pdfPage = CGPDFDocumentGetPage(pdf, page+1);
     const CGRect pageFrame = CGPDFPageGetBoxRect(pdfPage, kCGPDFMediaBox);
     
     UIGraphicsBeginPDFPageWithInfo(pageFrame, nil);
     
     //	Draw the page (flipped)
     CGContextSetRGBFillColor(pdfContext, 1.0,1.0,1.0,1.0);
     CGContextScaleCTM(pdfContext, 1, -1);
     CGContextTranslateCTM(pdfContext, 0, -pageFrame.size.height);
     
     CGContextDrawPDFPage(pdfContext, pdfPage);
     
     CGContextRestoreGState(pdfContext);
     
     
     CGContextSaveGState(pdfContext);
     //	add annotation to page
     CGContextScaleCTM(pdfContext, 1, -1);
     CGContextTranslateCTM(pdfContext, 0, -pageFrame.size.height);
     
     ESAnnotationsLayer *layer = [ESAnnotationsLayer layer];
     //        ESCustomLayer *layer = [ESCustomLayer layer];
     //        layer.backgroundColor = [UIColor redColor].CGColor;
     layer.frame = UIGraphicsGetPDFContextBounds();
     NSMutableArray *curvesArray = [pagesCurvesArray objectAtIndex:page];
     [layer addCurves:curvesArray];
     [layer drawInContext:pdfContext];
     
     ESPDFContentView *contentView = [super.contentViews objectForKey:[NSNumber numberWithInt:page]];
     NSMutableArray *textArray = [pagesAddTextArray objectAtIndex:page];
     [contentView addTextToPage:textArray];
     
     CGContextRestoreGState(pdfContext);
     }
     
     UIGraphicsEndPDFContext();
     
     */
    
    return retFlag;
    //   http://stackoverflow.com/questions/10058752/duplicate-calayer
    //    http://stackoverflow.com/questions/16069343/uiview-layer-renderincontext-memory-not-getting-released
}


#pragma Orientations Methods

- (BOOL)shouldAutorotate
{
    return YES;
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
//
////    return (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown );
//
//}
//
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
//
//    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight | UIInterfaceOrientationMaskPortrait| UIInterfaceOrientationMaskPortraitUpsideDown ;
//
//}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    UIImage *image = [super.mainPagebar getImageFromPage:1];
    if (image.size.width>image.size.height) {
        return UIInterfaceOrientationMaskLandscape;
    }
    if (image.size.width<image.size.height) {
        return UIInterfaceOrientationMaskPortrait;
    }
    
    return UIInterfaceOrientationMaskAll;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_6_0
- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window
{
    return UIInterfaceOrientationMaskAll; //Getting error here? then update ur xcode to 4.5+
}
#endif

- (ReaderContentView *) contentViewWithFrame:(CGRect ) frame fileURL:(NSURL *)fileURL page:(NSUInteger)number password:(NSString *)phrase {
    
    //    NSLog(@"page : %d", number);
    //    BOOL userRolePresenter = [[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter;
    
    ESPDFContentView *pdfContentView =
    [[ESPDFContentView alloc] initWithFrame:frame fileURL:fileURL page:number password:phrase];
    [[pdfContentView annotationsLayer] clear];
    [[pdfContentView annotationsLayer] addCurves:[pagesCurvesArray objectAtIndex:number-1]];
    [pdfContentView setCurveDelegate:self];
    [pdfContentView setGestureDelegate:self];
    [pdfContentView clearText];
    [pdfContentView addTextToPage:[pagesAddTextArray objectAtIndex:number - 1]];
    [pdfContentView clearPicture];
    [pdfContentView addPictureToPage:[pagesPictureArray objectAtIndex:number - 1]];
    
    //    if (self.sessionController && [[NSUserDefaults standardUserDefaults] integerForKey:KEY_USER_ROLE] == ESUserRolePresenter) {
    if ([appDelegate.sessionState isEqualToString:@"1"]) {
        
        [pdfContentView setPacketDelegate:self];
    }
    //    pdfContentView.enableGestures = !self.sessionController || [[NSUserDefaults standardUserDefaults] integerForKey:KEY_USER_ROLE] == ESUserRolePresenter;
    pdfContentView.enableGestures = ![appDelegate.sessionState isEqualToString:@"1"] || [[NSUserDefaults standardUserDefaults] integerForKey:KEY_USER_ROLE] == ESUserRolePresenter;
    
    //Show laser for Audience
    if (![appDelegate.sessionState isEqualToString:@"1"]) {
#if X_AUDIENCE
        pdfContentView.laserPointGestureRecognizer.enabled = YES;
#endif
    }
    
    //    [pdfContentView setPaintPalette:paintPalette.annotationButton.selected ? paintPalette : nil];
    
    //    [self.view.gestureRecognizers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
    //        UIGestureRecognizer *gestureRecognizer = (UIGestureRecognizer *) obj;
    //        if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
    //            [pdfContentView.paintGestureRecognizer requireGestureRecognizerToFail:gestureRecognizer];
    //            [pdfContentView.laserPointGestureRecognizer requireGestureRecognizerToFail:gestureRecognizer];
    //        }
    //    }];
    return pdfContentView;
}

//Role is attendee

- (void) didReceiveAnnotationFromPresenter:(ESAnnotationPacket *) annotation {
    
    if (annotation.pageNo == 0) {
        UINavigationController *navController = (UINavigationController *)self.presentedViewController;
        
        ESWhiteBoardViewController *whiteboardVC = (ESWhiteBoardViewController *)[navController.viewControllers objectAtIndex:0];
        [whiteboardVC processAnnotation:annotation];
        return;
    }
    
    //Store in our pagesCurvesArray
    NSMutableArray *curvesArray = [pagesCurvesArray objectAtIndex:annotation.pageNo-1];
    if (annotation.annotationType == ESAnnotationTypeStart || annotation.annotationType == ESAnnotationTypeHighlightStart) {
        
        ESCurve *curve = [ESCurve new];
        [curve addPoint:annotation.point];
        curve.lineWidth = annotation.lineWidth;
        curve.color = annotation.color;
        [curvesArray addObject:curve];
        
    } else {
        
        ESCurve *curve = [curvesArray lastObject];
        [curve addPoint:annotation.point];
    }
    
    //draw it on our views.
    if ([super.document.pageNumber integerValue] == annotation.pageNo) { //annotation is for the current page.
        
        ESPDFContentView *contentView = [super.contentViews objectForKey:[NSNumber numberWithInteger:annotation.pageNo]];
        [contentView processAnnotation:annotation];
    }
}

-(void) didReceiveZoomTransitionPacketFromPreseneter:(ESZoomTransitionPacket *)packet{
    //    NSLog(@"didReceiveZoomTransitionPacketFromPreseneter:%@",packet);
    
    ESPDFContentView *contentView = [super.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    
    CGFloat reciverMinimumZoomValue = [contentView minimumZoomScale];
    CGFloat senderMinimumZoomValue = packet.senderMinimumZoomValue;
    
    NSString *zoomvaluestr = [NSString stringWithFormat:@"%.4f",packet.zoomValue];
    NSString *sendervaluestr = [NSString stringWithFormat:@"%.4f",senderMinimumZoomValue];
    CGFloat finalZoomValue;
    CGPoint senderContentOffset = CGPointMake(packet.scrollViewOffSet.x, packet.scrollViewOffSet.y);
    
    //    float p =senderContentOffset.x/contentView.frame.size.width;
    //    float q = senderContentOffset.y/contentView.frame.size.height;
    
    
    if ([zoomvaluestr isEqualToString:sendervaluestr]){
        finalZoomValue = reciverMinimumZoomValue;
    }else{
        
        if (senderMinimumZoomValue>reciverMinimumZoomValue){
            
            finalZoomValue = (packet.zoomValue/senderMinimumZoomValue)*(reciverMinimumZoomValue+0.37);
            contentView.contentOffset = CGPointMake( senderContentOffset.x/3, senderContentOffset.y/3);
            //            contentView.contentOffset = CGPointMake(contentView.frame.size.width *p, contentView.frame.size.height *q);
            
        }else{
            //            finalZoomValue = (packet.zoomValue/packet.senderMinimumZoomValue)*(reciverMinimumZoomValue-0.22);
            finalZoomValue = (packet.zoomValue/packet.senderMinimumZoomValue)*(reciverMinimumZoomValue);
            
            contentView.contentOffset = packet.scrollViewOffSet;
        }
    }
    [contentView setZoomScale:finalZoomValue animated:YES];
    
}

-(void) didReceivePageTransitionFromPresenter:(ESPageTransitionPacket *)packet{
    [self showDocumentPage:packet.pageNo];
}

- (void) didReceiveLaserPointerPacketFromPresenter:(ESLaserPointerPacket *) packet {
    if ([super.document.pageNumber integerValue] == packet.pageNo) { //packet is for the current page.
        ESPDFContentView *contentView = [super.contentViews objectForKey:[NSNumber numberWithInteger:packet.pageNo]];
        [contentView processLaserPointerPacket:packet];
    }
}

- (void) transferPacket:(ESAbstractPacket *)packet {
    if ([appDelegate.sessionState isEqualToString:@"1"]) {
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:packet];
        NSError *error = nil;
       
        
        BOOL didSendData = NO;
        if ([appDelegate.role isEqualToString:ATTENDEE]) {
            //only send to server.
            //didSendData = [self.gkSession sendData:data toPeers:[NSArray arrayWithObject:_presenterPeerId] withDataMode:GKSendDataReliable error:&error];
            didSendData = [self.multipeerManager sendData:data toPeers:[NSArray arrayWithObject:_presenterPeerId] withMode:MCSessionSendDataReliable error:&error];
            
        } else {
            //didSendData= [self.gkSession sendDataToAllPeers:data withDataMode:GKSendDataReliable error:&error];
            didSendData = [self.multipeerManager sendData:data withMode:MCSessionSendDataReliable error:&error];
        }
        if (!didSendData) {
            NSLog(@"Could not send data : %@",[error localizedDescription]);
        }
    }
}

- (void) transferPacket:(ESAbstractPagePacket *)packet fromContentView:(ESPDFContentView *)contentView {
    //    NSLog(@"transferPacketFromContentView");
    //Add page no to page packet.
    for (NSNumber *numberKey in self.contentViews.allKeys) {
        if ([self.contentViews objectForKey:numberKey] == contentView) {
            packet.pageNo = numberKey.integerValue;
            break;
        }
    }
    
    if ([appDelegate.sessionState isEqualToString:@"1"]) {
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:packet];
        NSError *error = nil;
        
        //BOOL didSendData = [self.gkSession sendDataToAllPeers:data withDataMode:GKSendDataReliable error:&error];
        BOOL didSendData = [self.multipeerManager sendData:data withMode:MCSessionSendDataReliable error:&error];
        
        if (!didSendData) {
            NSLog(@"Could not send data : %@",[error localizedDescription]);
        }
    }
}

#pragma mark -
#pragma mark ESQuestionPickerDelegate protocol implementation

- (void) viewController:(UIViewController *)questionRelatedViewController didPickQuestion:(ESQuestion *)question {
    if (_presenterQuestionsAsked == nil) {
        _presenterQuestionsAsked = [[NSMutableArray alloc] initWithCapacity:4];
    }
    
    [_presenterQuestionsAsked addObject:question];
    
    if ([appDelegate.sessionState isEqualToString:@"1"]) {
        question.state = ESQuestionStateAsked;
        [self sendQuestionToAttendees:question];
    }
}

- (void) sendQuestionToAttendees:(ESQuestion *) question {
    //create a packet.
    ESQuestionPacket *packet = [ESQuestionPacket packetWithQuestion:question];
    [self transferPacket:packet];
}

- (void) viewController:(UIViewController *)questionRelatedViewController didShareQuestion:(ESQuestion *)question {
    if ([appDelegate.sessionState isEqualToString:@"1"]) {
        question.state = ESQuestionStateShared;
        [self sendQuestionToAttendees:question];
    }
}

- (void) viewController:(UIViewController *)questionRelatedViewController didStopQuestion:(ESQuestion *)question {
    if ([appDelegate.sessionState isEqualToString:@"1"]) {
        question.state = ESQuestionStateStopped;
        [self sendQuestionToAttendees:question];
    }
}

- (void) viewController:(UIViewController *)questionRelatedViewController didCloseQuestion:(ESQuestion *)question {
    int prevState = question.state;
    if ([appDelegate.sessionState isEqualToString:@"1"]) {
        question.state = ESQuestionStateClosed;
        [self sendQuestionToAttendees:question];
    }
    question.state = prevState;
}

- (void) didreceiveQuestionFromPresenter:(ESQuestionPacket *) questionPacket {
    
    ESQuestion *question = questionPacket.question;
    //    NSLog(@"received question from presenter with state: %zd",question.state);
    
    if (question.state == ESQuestionStateAsked) {
        
        if (!self.presentedViewController) {
    
            
           ESQuestionAnswerViewController *answerViewController = [[ESQuestionAnswerViewController alloc] initWithStyle:UITableViewStyleGrouped];
            answerViewController.question = question;
            answerViewController.delegate = self;
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:answerViewController];
            navController.modalPresentationStyle = UIModalPresentationPageSheet;
            [self presentViewController:navController animated:YES completion:nil];
        } else {
            [self dismissViewControllerAnimated:YES completion:^() {
                ESQuestionAnswerViewController *answerViewController = [[ESQuestionAnswerViewController alloc] initWithStyle:UITableViewStyleGrouped];
                answerViewController.question = question;
                answerViewController.delegate = self;
                
                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:answerViewController];
                navController.modalPresentationStyle = UIModalPresentationPageSheet;
                [self presentViewController:navController animated:YES completion:nil];
            }];
        }
        
    } else {
        
        if (![self.presentedViewController isMemberOfClass:[UINavigationController class]]) {
            return;
        }
        
        UINavigationController *navController = (UINavigationController *)self.presentedViewController;
        if (![[navController.viewControllers objectAtIndex:0] isMemberOfClass:[ESQuestionAnswerViewController class]]) {
            //ignore
            return;
        }
        
        if (question.state == ESQuestionStateClosed) {
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } else if (question.state == ESQuestionStateShared || question.state == ESQuestionStateStopped) {
            
            ESQuestionAnswerViewController *answerViewController = (ESQuestionAnswerViewController *)[navController.viewControllers objectAtIndex:0];
            
            answerViewController.question = question;
        }
    }
    
}

- (void) questionAnswerViewController:(ESQuestionAnswerViewController *)viewController finishedWithOptions:(NSIndexSet *)options {
    
    viewController.question.state = ESQuestionStateAnswered;
    //create an answer packet.
    ESAnswerPacket *packet = [ESAnswerPacket packetWithOptions:options];
    [self transferPacket:packet];
}
- (void) questionCustomAnswerViewController:(ESQuestionAnswerViewController *)viewController doneEditingWithText:(NSString *) text forAttendee:(NSString *)attendeeName {
    
    viewController.question.state = ESQuestionStateAnswered;
    //create an answer packet.ESAttendeeCustomAnswerPacket *packet = [ESAttendeeCustomAnswerPacket packetWithAttendeeCustomAnswerText:text attendeeName:attendeeName];
      ESAttendeeCustomAnswerPacket *packet = [ESAttendeeCustomAnswerPacket packetWithAttendeeCustomAnswerText:text attendeeName:attendeeName];
    [self transferPacket:packet];
    
   
//    ESAttendeeCustomQuestionAnswerPacket *packet = [ESAttendeeCustomAnswerPacket packetWithOptions:options];
//    [self transferPacket:packet];
}

- (void) didReceiveAnswerFromAttendee:(ESAnswerPacket *) packet {

    ESQuestion *question = [self.presenterQuestionsAsked lastObject];
    [question addAnswer:packet.selectedOptions];
    question.state = ESQuestionStateUpdated;
}

-(void)didReceiveQuestionFromAttendee:(ESAttendeeQuestionPacket *)packet {
    
    if (attendeeAskQuestionArray == nil) {
        attendeeAskQuestionArray = [[NSMutableArray alloc] initWithCapacity:3];
    }
    [attendeeAskQuestionArray addObject:packet.question];
    //    AttendeeQueryPosted
    [[NSNotificationCenter defaultCenter] postNotificationName:Attendee_Query_Posted object:attendeeAskQuestionArray];
    [self setupNavigationButtonItems];
}

-(void)didReceiveCustomAnswerFromAttendee:(ESAttendeeCustomAnswerPacket *)packet {

    if (attendeeCustomAnswerArray == nil) {
        attendeeCustomAnswerArray = [[NSMutableArray alloc] initWithCapacity:3];
    }
    ESQuestion *question = [self.presenterQuestionsAsked lastObject];
    [question addCustomAnswer: packet.answer];
    question.state = ESQuestionStateUpdated;
}
- (void) questionCustomAnswerController:(ESQuestionAnswerViewController *) viewController doneEditingWithText:(NSString *) text forAttendee:(NSString *)attendeeName{
    
//    [self dismissViewControllerAnimated:YES completion:nil];
    
    //create a packet
    ESAttendeeCustomAnswerPacket *packet = [ESAttendeeCustomAnswerPacket packetWithAttendeeCustomAnswerText:text attendeeName:attendeeName];
    
    [self transferPacket:packet];
    
}


- (void) textViewController:(ESTextViewController *) viewController doneEditingWithText:(NSString *) text forAttendee:(NSString *)attendeeName{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //create a packet
    ESAttendeeQuestionPacket *packet = [ESAttendeeQuestionPacket packetWithAttendeeQuestionText:text attendeeName:attendeeName];
    
    [self transferPacket:packet];
}

- (void) doneWithWhiteBoardViewController:(ESWhiteBoardViewController *) viewController forpacket:(ESPaintingDoneButtonPacket *)packet{
    //    [self dismissViewControllerAnimated:YES completion:nil];
    [self transferPacket:packet];
    
    
}

- (void) doneWithESVideoPlayerViewController:(ESVideoPlayerViewController *) viewController forpacket:(ESPaintingDoneButtonPacket *)packet{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self transferPacket:packet];
    
}
-(void) didRecieveAttendeeQuestionStatusFromPresenter:(ESAttendeeQuestionSwitchPacket *)packet{
    //    self.attendeeCanQuery = packet.switchStatus;
    self.attendeeCanQuery = packet.queryStatus;
    self.attendeeCanBrowse = packet.browseStatus;
    [self setupNavigationButtonItems];
    //call a method for hiding and unhiding slider
    //    [self hideSlider];
    BOOL navBarHidden = self.navigationController.navigationBarHidden;
    super.mainPagebar.hidden = !self.attendeeCanBrowse?YES:navBarHidden;
}

- (void) setAttendeeCanBrowse:(BOOL)attendeeCanBrowse {
    if ([appDelegate.sessionState isEqualToString:@"1"] && [appDelegate.role isEqualToString:ATTENDEE] && !attendeeCanBrowse) {
        self.mainPagebar.alpha = 0.f;
    } else {
        self.mainPagebar.alpha = 1.f;
    }
    _attendeeCanBrowse = attendeeCanBrowse;
}

//-(void)hideSlider{
//    if (self.gkSession && !self.attendeeCanBrowse) {
//        [super.mainPagebar hidePagebar];
//    }else {
//        [super.mainPagebar showPagebar];
//    }
//}
-(void) didReceiveEndPresentationFromPresenter:(ESEndPresentationPacket *) packet{
    //    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    //    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}


//-(void) didReceivePageSliderFromPresenter:(ESPageSliderPacket *) packet{
//    [self showDocumentPage:packet.pageNum];
//}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *mainPath    = [myPathList  objectAtIndex:0];
    
    mainPath = [mainPath stringByAppendingPathComponent:Q_A_PDF_FILE_NAME];
    
    //    Here mainPath is Full Path of File.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *fileArray = [fileManager contentsOfDirectoryAtPath:mainPath error:nil];
    for (NSString *filename in fileArray)  {
        [fileManager removeItemAtPath:[mainPath stringByAppendingPathComponent:filename] error:NULL];
    }
    BOOL fileExists = [fileManager fileExistsAtPath:mainPath];
    
    if (fileExists)
    {
        BOOL success = [fileManager removeItemAtPath:mainPath error:&error];
        if (!success)
            NSLog(@"Error: %@", [error localizedDescription]);
        
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) handleSingleTapDelegateMethod:(UITapGestureRecognizer *)gesture{
    
    [self handleSingleTapOnContentView:gesture];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer{
    [self handleSingleTapOnContentView:recognizer];
}

- (void) dealloc {
    //    if (SYSTEM_VERSION_LESS_THAN(@"6.0")) {
    //        [super viewDidUnload];
    //    }
    //        [super viewDidUnload];
    
    [self killTimer];
}

-(IBAction)settingsButtonTouchUPInside:(id)sender{
    
#if X_AUDIENCE
    [self showAlert];
    return;
#else
    if (doneButtonActionSheet != nil) {
        //[doneButtonActionSheet dismissWithClickedButtonIndex:doneButtonActionSheet.cancelButtonIndex animated:YES];
        [doneButtonActionSheet dismissViewControllerAnimated:YES completion:nil];
    }
    
    //If the device type is ipad,show a popoverVC
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        //show Popover VC
        if (settingsPopOverView == nil) {
//            settingsPopOverView = [[ESPresentationSettingPopoverViewController alloc] initWithStyle:UITableViewStylePlain];
//            settingsPopOverView.delegate = self;
//            
//            UIPopoverPresentationController *settingsPopoverVC = [settingsPopOverView popoverPresentationController];
//            settingsPopoverVC.permittedArrowDirections = UIPopoverArrowDirectionAny;
//            settingsPopoverVC.barButtonItem = sender;
//            settingsPopoverVC.delegate = self;
            
            //settingsPopoverVC = [[UIPopoverController alloc] initWithContentViewController:settingsPopOverView];
            
            //[settingsPopoverVC setPopoverContentSize:CGSizeMake( 320,120.0f)];
            //[settingsPopoverVC presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            settingsPopOverView= [[ESPresentationSettingPopoverViewController alloc]initWithStyle:UITableViewStylePlain];
            settingsPopOverView.delegate = self;
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:settingsPopOverView];
            
            [self presentViewController:navController animated:YES completion:nil];
            
        }else{
            //[settingsPopoverVC dismissPopoverAnimated:YES];
            [[settingsPopOverView presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
            settingsPopOverView = nil;
            
        }
    }else{
        //Present VC
        
        settingsPopOverView= [[ESPresentationSettingPopoverViewController alloc]initWithStyle:UITableViewStylePlain];
        settingsPopOverView.delegate = self;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:settingsPopOverView];
        
        [self presentViewController:navController animated:YES completion:nil];
        
        
    }
#endif
    
    
}
-(IBAction)openWhiteBoardForPresenter:(id)sender{
#if X_AUDIENCE
    [self showAlert];
    return;
#else
    if (settingsPopOverView != nil) {
        //[settingsPopoverVC dismissPopoverAnimated:YES];
        [[settingsPopOverView presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        settingsPopOverView = nil;
    }
    if (doneButtonActionSheet != nil) {
        //[doneButtonActionSheet dismissWithClickedButtonIndex:doneButtonActionSheet.cancelButtonIndex animated:YES];
        [doneButtonActionSheet dismissViewControllerAnimated:YES completion:nil];
    }
    
    if ([appDelegate.sessionState isEqualToString:@"1"]) {
        ESWhiteBoardPacket *packet = [ESWhiteBoardPacket packectToGoWhiteBoard];
        [self transferPacket:packet];
    }
    ESWhiteBoardViewController *whiteBoardVC = [[ESWhiteBoardViewController alloc]init];
    whiteBoardVC.delegate = self;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:whiteBoardVC];
    navController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:navController animated:YES completion:nil];
#endif
}
-(void) didReceiveToGoWhiteBoardFromPresenter:(ESWhiteBoardPacket *) packet{
    
    ESWhiteBoardViewController *whiteBoardVC = [[ESWhiteBoardViewController alloc]init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:whiteBoardVC];
    navController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:navController animated:YES completion:nil];
    navController.navigationItem.rightBarButtonItem = nil;
    
}

-(void) didReceiveToDismissWhiteBoardFromPresenter:(ESPaintingDoneButtonPacket *) packet{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSMutableArray *)findFiles:(NSString *)extension {
    
    NSMutableArray *matches = [[NSMutableArray alloc]init];
    NSFileManager *fManager = [NSFileManager defaultManager];
    NSString *item;
    
    NSString *documentsDir = [self getDocumentDirectory];
    
    NSArray *contents = [fManager contentsOfDirectoryAtPath:documentsDir error:nil];
    
    // >>> this section here adds all files with the chosen extension to an array
    for (item in contents){
        if ([item isEqualToString:extension]) {
            [matches addObject:item];
        }
    }
    return matches;
}

-(NSString *) getDocumentDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}


#if X_PRESENTER

#pragma mark -
#pragma mark Video Player Additions.
-(void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer {
    
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan){
        
        if ([appDelegate.sessionState isEqualToString:@"1"] && [appDelegate.role isEqualToString:ATTENDEE]) {
            return;
        }
        
        CGPoint point = [gestureRecognizer locationInView:gestureRecognizer.view];
        CGRect viewRect = gestureRecognizer.view.bounds;
        CGRect videoPageRect = viewRect;
        
        //if page matches with video file page number accordingly play video for
        //    NSString *pathLocal = [[NSBundle mainBundle]pathForResource:@"video" ofType:@"xml"];
        
        NSMutableArray *a = [self findFiles:@"video.xml"];
        if (a.count>0) {
            NSString *pathLocal =[NSString stringWithFormat:@"/%@/%@",[self getDocumentDirectory],[a objectAtIndex:0]];
            NSData *xmlData = [NSData dataWithContentsOfFile:pathLocal];
            
            ESVideoParser *videoParser = [[ESVideoParser alloc] init];
            [videoParser parseData:xmlData forPresentation:self.document.fileName];
            
            if ([self.viewTitle isEqualToString:[videoParser.presentationName stringByDeletingPathExtension]]) {
                
                self.videos =  videoParser.videoArray;
                
                for (int i = 0; i < self.videos.count; i ++) {
                    
                    ESVideo *video = [self.videos objectAtIndex:i];
                    
                    if (self.currentPage == video.pageNumber ){
                        
                        NSArray *rectArray = [video.rect componentsSeparatedByString:@","];
                        CGFloat  rect_X = [[rectArray objectAtIndex:0] floatValue];
                        CGFloat rect_Y = [[rectArray objectAtIndex:1] floatValue];
                        CGFloat rect_width = [[rectArray objectAtIndex:2] floatValue];
                        CGFloat rect_height =  [[rectArray objectAtIndex:3] floatValue];
                        
                        videoPageRect.size.width = rect_width * viewRect.size.width;
                        videoPageRect.size.height = rect_height * viewRect.size.height;
                        videoPageRect.origin.x = rect_X * viewRect.size.width;
                        videoPageRect.origin.y = rect_Y * viewRect.size.height;
                        
                        //                NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video.fileName ofType:nil]];
                        
                        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",[self getDocumentDirectory] ,video.fileName]];
                        
                        if (CGRectContainsPoint(videoPageRect, point)){
                            BOOL hasVideoFile= [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/%@",[self getDocumentDirectory] ,video.fileName]];
                            if (!hasVideoFile) {
                                //No video found
                                return;
                            }
                            if (gestureRecognizer.state != UIGestureRecognizerStateBegan) {
                                //            [gestureRecognizer setEnabled:NO];
                                return;
                            }
                            
                            if ([appDelegate.sessionState isEqualToString:@"1"] && [appDelegate.role isEqualToString:PRESENTER]) {
                                
                                ESVideoPlayerPacket *packet = [ESVideoPlayerPacket startPacketWithFileName:video.fileName];
                                [self transferPacket:packet];
                                
                            }
                            if ([appDelegate.sessionState isEqualToString:@"1"] && [appDelegate.role isEqualToString:ATTENDEE]) {
                                
                                //                        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:ES_DEMO_VIDEO_RESOURCE_NAME ofType:@"mp4"]];
                                
                                AVPlayer *player = [[AVPlayer alloc] initWithURL:url];
                                
                                AVPlayerViewController *vc = [[AVPlayerViewController alloc] init];
                                
                                vc.player = player;
                                
                                [self presentViewController:vc animated:YES completion:nil];
                                
                                //MPMoviePlayerViewController *vc = [[MPMoviePlayerViewController alloc] initWithContentURL:url];
                                //vc.moviePlayer.controlStyle = MPMovieControlStyleEmbedded;
                                //[self presentMoviePlayerViewControllerAnimated:vc];
                                
                                
                                
                            } else {
                                
                                
                                ESVideoPlayerViewController *videoPlayerVC = [[ESVideoPlayerViewController alloc]init];
                                videoPlayerVC.delegate = [appDelegate.sessionState isEqualToString:@"1"]?self:nil;
                                videoPlayerVC.url = url;
                                
                                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:videoPlayerVC];
                                navController.modalPresentationStyle = UIModalPresentationFullScreen;
                                [self presentViewController:navController animated:YES completion:nil];
                                
                            }
                        }else{
                            //                    NSLog(@"Longpress rect did not match with Videorect");
                        }
                    }
                }
            }
        }else{
            NSLog(@"Video file not found in directory");
        }
    }
}

#endif

//ESPlayerVideoControllerDelegate

- (void) videoPlayerViewControllerDone:(ESVideoPlayerViewController *)viewController {
    ESVideoPlayerPacket *packet = [ESVideoPlayerPacket endPacket];
    [self transferPacket:packet];
}

- (void) videoPlayerViewController:(ESVideoPlayerViewController *)viewController videoPlayerDidResumePlayingAt:(NSTimeInterval)currentPlaybackTime {
    ESVideoPlayerPacket *packet = [ESVideoPlayerPacket playPacketFromTime:currentPlaybackTime];
    [self transferPacket:packet];
}

- (void) videoPlayerViewController:(ESVideoPlayerViewController *)viewController videoPlayerDidPauseAt:(NSTimeInterval)currentPlaybackTime {
    ESVideoPlayerPacket *packet = [ESVideoPlayerPacket pausePacketAtTime:currentPlaybackTime];
    [self transferPacket:packet];
}

//Attendee receives video player related packets from presenter.

- (void) didReceiveVideoPlayerPacketFromPresenter:(ESVideoPlayerPacket *) packet {
    if (packet.videoPlayerState == ESVideoPlayerStateStart) {
        
        //        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:ES_DEMO_VIDEO_RESOURCE_NAME ofType:@"mp4"]];
        //        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:packet.videoFileName ofType:nil]];
        
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",[self getDocumentDirectory] ,packet.videoFileName]];
        BOOL hasVideoFile= [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/%@",[self getDocumentDirectory] ,packet.videoFileName]];
        if (!hasVideoFile) {
            //No video found
            NSLog(@"Video file doesn't exists in this directory");
        }
        
        //        NSLog(@"video file url : %@",[NSString stringWithFormat:@"%@/%@",[self getDocumentDirectory] ,packet.videoFileName]);
        
        
        AVPlayer *player = [[AVPlayer alloc] initWithURL:url];
        
        AVPlayerViewController *vc = [[AVPlayerViewController alloc] init];
        
        vc.player = player;
        
        [self presentViewController:vc animated:YES completion:nil];
        
        
//        MPMoviePlayerViewController *vc = [[MPMoviePlayerViewController alloc] initWithContentURL:url];
//        vc.moviePlayer.controlStyle = MPMovieControlStyleEmbedded;
//        [self presentMoviePlayerViewControllerAnimated:vc];
        
    } else {
        @try {
            //            NSLog(@"didReceiveVideoPlayerPacketFromPresenter: %@",packet);
            
            
            AVPlayerViewController *playerViewController = (AVPlayerViewController *)self.presentedViewController;
            AVPlayer *player = playerViewController.player;
            
            //MPMoviePlayerViewController *playerViewController = (MPMoviePlayerViewController *)self.presentedViewController;
            //MPMoviePlayerController *moviePlayer = playerViewController.moviePlayer;
            
            if (packet.videoPlayerState == ESVideoPlayerStatePause) {
                //[player seekToTime:CMTimeMake(packet.timeInterval, 1) ];
                //if(player.playbackState != MPMoviePlaybackStatePaused) {
                    [player pause];
                //}
            } else if (packet.videoPlayerState == ESVideoPlayerStatePlay) {
                //[player seekToTime:CMTimeMake(packet.timeInterval, 1) ];
                //player.currentPlaybackTime = packet.timeInterval;
                //if(moviePlayer.playbackState != MPMoviePlaybackStatePlaying) {
                    [player play];
                //}
            } else {
                [player pause];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
        @catch (NSException *exception) {
            [self dismissViewControllerAnimated:YES completion:nil];
            //            NSLog(@"videoPlayerException: %@",exception);
        }
    }
}
//Method not used
-(void)didReceiveToGoVideoPlayerFromPresenter:(ESVideoPlayerPacket *)packet{
    
    ESVideoPlayerViewController *videoPlayerVC = [[ESVideoPlayerViewController alloc]init];
   // videoPlayerVC.movieController.controlStyle = MPMovieControlStyleNone;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:videoPlayerVC];
    navController.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:navController animated:YES completion:nil];
    navController.navigationItem.rightBarButtonItem = nil;
    
}
-(void)didReceiveTextPacketFromPresenter:(ESTextPacket *)packet{
    
    //Store in our pagesCurvesArray
    if (packet.pageNo == 0) {
        UINavigationController *navController = (UINavigationController *)self.presentedViewController;
        
        ESWhiteBoardViewController *whiteboardVC = (ESWhiteBoardViewController *)[navController.viewControllers objectAtIndex:0];
        packet.session_attendee = YES;
        [whiteboardVC processText:packet];
        return;
    }
    
    NSMutableArray *textArray = [pagesAddTextArray objectAtIndex:packet.pageNo-1];
    
    if(packet.textState == ESTextStateAddTextDone || packet.textState == ESTextStateDismisPopover){
        
        
        ESText1 *text;
        if(textArray.count > 0){
            
            if (textArray.count == packet.indexValue) {
                text = [ESText1 new];
                text.indexValue = packet.indexValue;
                text.fontSize = packet.textFontSize;
                text.color = packet.textColor;
                text.textValue = packet.text;
                text.textFrame = packet.textFrame;
                [text addFrame:packet.textFrame];
                
                //Here  if text is nill don't add that label to  the array
                
                if (text.textValue.length > 0) {
                    [textArray addObject:text];
                }
                
            }else{
                text = [textArray objectAtIndex:packet.indexValue];
                text.indexValue = packet.indexValue;
                text.fontSize = packet.textFontSize;
                text.color = packet.textColor;
                text.textValue = packet.text;
                text.textFrame = packet.textFrame;
                
            }
        }else{
            text = [ESText1 new];
            text.indexValue = packet.indexValue;
            text.fontSize = packet.textFontSize;
            text.color = packet.textColor;
            text.textValue = packet.text;
            text.textFrame = packet.textFrame;
            [text addFrame:packet.textFrame];
            
            //Here  if text is nill don't add that label to  the array
            
            if (text.textValue.length > 0) {
                [textArray addObject:text];
            }
            
        }
        
    }else if (packet.textState == ESTextStateRemoveText) {
        
        [textArray removeObjectAtIndex:packet.indexValue];
        
    }else if (packet.textState == ESTextStateMoveText){
        
        ESText1 *text = [textArray objectAtIndex:packet.indexValue];
        text.indexValue = packet.indexValue;
        text.textFrame = packet.textFrame;
        
        
    }
    if ([super.document.pageNumber integerValue] == packet.pageNo) { //packet is for the current page.
        ESPDFContentView *contentView = [super.contentViews objectForKey:[NSNumber numberWithInteger:packet.pageNo]];
        [contentView processText:packet];
        
    }
}

-(void)didReceivePicturePacketFromPresenter:(ESPicturePacket *)packet{
    
    if (packet.pageNo == 0) {
        UINavigationController *navController = (UINavigationController *)self.presentedViewController;
        
        ESWhiteBoardViewController *whiteboardVC = (ESWhiteBoardViewController *)[navController.viewControllers objectAtIndex:0];
        [whiteboardVC processPicture:packet];
        return;
    }
    NSMutableArray *pictureArray = [pagesPictureArray objectAtIndex:packet.pageNo-1];
    
    
    if(packet.pictureState == ESStateAddPicture){
        
        ESPicture *pic = [ESPicture new];
        pic.pictureIndex = packet.pictureIndex;
        pic.pictureFrame = packet.pictureFrame;
        pic.picture = packet.picture;
        [pic addPictureFrame:packet.pictureFrame];
        
        [pictureArray addObject:pic];
        
    }else if (packet.pictureState == ESStateRemovePicture) {
        
        [pictureArray removeObjectAtIndex:packet.pictureIndex];
        
    }else if (packet.pictureState == ESStateMovePicture){
        
        ESPicture *pic = [pictureArray objectAtIndex:packet.pictureIndex];
        pic.pictureIndex = packet.pictureIndex;
        pic.pictureFrame = packet.pictureFrame;
        [pic addPictureFrame:packet.pictureFrame];
    }
    
    if ([super.document.pageNumber integerValue] == packet.pageNo) { //packet is for the current page.
        ESPDFContentView *contentView = [super.contentViews objectForKey:[NSNumber numberWithInteger:packet.pageNo]];
        [contentView processPicture:packet];
    }
}
#pragma mark - Device Orientation
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //    NSLog(@"NSTimeInterval%f",duration);
    //    NSLog(@"Orientation %d",toInterfaceOrientation);
    
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
    BOOL iPad = NO;
    BOOL isPotrait = NO;
#ifdef UI_USER_INTERFACE_IDIOM
    iPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#endif
    
    //[[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_ROLE] integerValue] == ESUserRolePresenter)
    
#if X_PRESENTER
    //    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
#ifdef UIDeviceOrientationIsPortrait//(orientation)
    isPotrait = (UIDeviceOrientationIsPortrait(orientation) == UIDeviceOrientationPortrait ||UIDeviceOrientationIsPortrait(orientation) == UIDeviceOrientationPortraitUpsideDown);
#endif
    
    if (!iPad ){
        if (isPotrait) {
            self.title = nil;
        }
        paintPalette.frame=[self frameOfPaintPallete];
//        [paintPalette initWithFrame:[self frameOfPaintPallete] forIsPotrait:isPotrait];
    }else{//iPad
        self.title = self.viewTitle;
    }
    
    
    CGFloat hieght;
    CGFloat xOrign;
    CGFloat fontSize;
    CGFloat width;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
        CGFloat constantVal=0;
        if (UIDeviceOrientationIsPortrait(orientation)) {
            CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
            
            if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
                constantVal = 34;
            }
            else
            {
                constantVal = 0;
            }
            hieght = self.view.bounds.size.height-super.colPages.bounds.size.height-constantVal;
            xOrign = 10;
        }
        else
        {
            CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
            
            if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
                constantVal = 34;
                xOrign = 115;
            }
            else
            {
                constantVal = 0;
                xOrign = 80;
            }
            hieght = self.view.bounds.size.height-constantVal;
        }
        
        fontSize = 12;
        width = 80;
    }
    else
    {
        UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
        CGFloat constantVal=0;
        if (UIDeviceOrientationIsPortrait(orientation)) {
            hieght = self.view.bounds.size.height-super.colPages.bounds.size.height;
            xOrign = 10;
        }
        else
        {
            hieght = self.view.bounds.size.height;
            xOrign = 80;
        }
        fontSize = 20;
        width = 120;
    }
    
    
    progress.frame=CGRectMake(xOrign, hieght-22, width, 22);

#endif
    
}
#if X_PRESENTER
-(IBAction)addPictureButtonTouchUpInside:(id)sender{
    
    UIButton *b = sender;
    //    if (self.gkSession && self.gkSession.sessionMode == GKSessionModeClient) {
    //        return;
    //    }
    
    if (doneButtonActionSheet != nil) {
        //[doneButtonActionSheet dismissWithClickedButtonIndex:doneButtonActionSheet.cancelButtonIndex animated:YES];
        [doneButtonActionSheet dismissViewControllerAnimated:YES completion:nil];
    }
    
    addPictureActionSheet = [UIAlertController alertControllerWithTitle:nil
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             // do something here
                                                         }];
    
    UIAlertAction *libraryAction = [UIAlertAction actionWithTitle:@"Library"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              // do something here
                                                              if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
                                                                  
                                                                  imagePicker =[[UIImagePickerController alloc] init];
                                                                  imagePicker.delegate = self;
                                                                  imagePicker.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
                                                                  imagePicker.allowsEditing = NO;
                                                                  
                                                                  if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                                                                      
#if X_PRESENTER
                                                                      imagePicker.modalPresentationStyle = UIModalPresentationPopover;
                                                                      [self presentViewController:imagePicker animated:YES completion:nil];
                                                                      
                                                                      UIPopoverPresentationController *imagePopover = [imagePicker popoverPresentationController];
                                                                      imagePopover.delegate = self;
                                                                      imagePopover.sourceView = self.view;
                                                                      imagePopover.sourceRect = CGRectMake(0, 0, 320, 230);
                                                                      imagePopover.permittedArrowDirections = UIPopoverArrowDirectionAny;
                                                                      
                                                                      
//                                                                      imagePopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
//                                                                      [imagePopover presentPopoverFromRect:CGRectMake(0, 0, 320, 230) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
#endif
                                                                      
                                                                  } else {
                                                                      
                                                                      [self presentViewController:imagePicker animated:YES completion:nil];
                                                                  }
                                                              }
                                                          }];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Camera"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             // do something here
                                                             if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                                                                 
                                                                 imagePicker =[[UIImagePickerController alloc] init];
                                                                 imagePicker.delegate = self;
                                                                 imagePicker.sourceType =UIImagePickerControllerSourceTypeCamera;
                                                                 imagePicker.allowsEditing = NO;
                                                                 
                                                                 if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
#if X_PRESENTER
                                                                     
                                                                     [self presentViewController:imagePicker animated:YES completion:nil];
                                                                     
//                                                                     imagePopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
//                                                                     [imagePopover presentPopoverFromRect:CGRectMake(0, 0, 320, 230) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
#endif
                                                                     
                                                                 } else {
                                                                     [self presentViewController:imagePicker animated:YES completion:nil];
                                                                     
                                                                     //                    [self dismissViewControllerAnimated:YES completion:nil];
                                                                 }
                                                             }
                                                         }];
    
    // note: you can control the order buttons are shown, unlike UIActionSheet
    [addPictureActionSheet addAction:cancelAction];
    [addPictureActionSheet addAction:libraryAction];
    [addPictureActionSheet addAction:cameraAction];
    [addPictureActionSheet setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [addPictureActionSheet
                                                     popoverPresentationController];
    popPresenter.sourceView = b;
    popPresenter.sourceRect = b.bounds;
    [self presentViewController:addPictureActionSheet animated:YES completion:nil];
    
//    addPictureActionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Library",@"Camera", nil];
//    [addPictureActionSheet showInView:self.view];
//    [addPictureActionSheet showFromRect:b.frame inView:self.view animated:YES];
//    
//    addPictureActionSheet.tag = ADD_PICTURE_ACTIONSHEET_TAG;
    
    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    
    contentView.paintPalette = paintPalette.annotationButton.selected ? paintPalette : nil;
    
    
}
-(IBAction)addTextButtonTouchUpInside:(id)sender{
    
    //    if (self.gkSession && self.gkSession.sessionMode == GKSessionModeClient) {
    //        return;
    //    }
    
    if (doneButtonActionSheet != nil) {
        //[doneButtonActionSheet dismissWithClickedButtonIndex:doneButtonActionSheet.cancelButtonIndex animated:YES];
        [doneButtonActionSheet dismissViewControllerAnimated:YES completion:nil];
    }
    
    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    ESTextPacket *packet = [ESTextPacket  packetWithStartState:ESTextStateStartText];
    [contentView processText:packet];
    [self processTextWithPacket:packet];
    
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        
        
        //If the device type is ipad,show a popoverVC
        if (addTextPopoverView != nil) {
            
            addTextPopoverView = [[ESTextPopoverViewController alloc]init];
            addTextPopoverView.indexOfText = [ contentView.addedLableArray indexOfObject:[contentView.addedLableArray lastObject]];
            addTextPopoverView.delegate = self;
            addTextPopoverView.fontSize = 14.f;
            addTextPopoverView.fromPdf = YES;
            //            NSLog(@"%@ %ld",contentView.addedLableArray,(long)addTextPopoverView.indexOfText);
            
            addTextPopoverView.modalPresentationStyle = UIModalPresentationPopover;
            [self presentViewController:addTextPopoverView animated:YES completion:nil];
            
            UIPopoverPresentationController *textPopoverController = [addTextPopoverView popoverPresentationController];
            textPopoverController.delegate = self;
            textPopoverController.sourceView = self.view;
            textPopoverController.sourceRect = contentView.addStickyFrame;
            
//            textPopoverController = [[UIPopoverController alloc] initWithContentViewController:addTextPopoverView];
//            textPopoverController.delegate = self;
//            [textPopoverController setPopoverContentSize:CGSizeMake(280,400.0f)];
//            
//            [textPopoverController presentPopoverFromRect:contentView.addStickyFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }else{
            //[textPopoverController dismissPopoverAnimated:YES];
            [[addTextPopoverView presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
            addTextPopoverView = nil;
        }
    }else{ //iPhone or iPod
        
        textPopoverView = [[ESTextPopoverViewController alloc]init];
        textPopoverView.delegate = self;
        textPopoverView.fontSize = 14.f;
        textPopoverView.fromPdf = YES;
        textPopoverView.indexOfText = [ contentView.addedLableArray indexOfObject:[contentView.addedLableArray lastObject]];
        //        NSLog(@"%@ = addedtextArray %ld = index of label",contentView.addedLableArray,(long)textPopoverView.indexOfText);
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:textPopoverView];
        
        [self presentViewController:navController animated:YES completion:nil];
        
    }
    contentView.paintPalette = paintPalette.annotationButton.selected ? paintPalette : nil;
    
}

-(void)cancelAddTextFromPopOver:(ESTextPopoverViewController *)vc {
    
    if (addTextPopoverView != nil) {
        //[textPopoverController dismissPopoverAnimated:YES];
        [[addTextPopoverView presentingViewController] dismissViewControllerAnimated:YES completion:nil];
        addTextPopoverView = nil;
    }
}
-(void)doneWithTextPopover:(ESTextPopoverViewController *)viewController withPacket:(ESTextPacket *)packet{
    
    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    [contentView processText:packet];
    [self processTextWithPacket:packet];
    if (packet.text.length >0) {
        
        [self transferPacket:packet fromContentView:contentView];
    }
    
    
    if (doneButtonActionSheet != nil) {
    //  [doneButtonActionSheet dismissWithClickedButtonIndex:doneButtonActionSheet.cancelButtonIndex animated:YES];
        [doneButtonActionSheet dismissViewControllerAnimated:YES completion:nil];
    }
    if (addTextPopoverView != nil) {
        //[textPopoverController dismissPopoverAnimated:YES];
        [[addTextPopoverView presentingViewController] dismissViewControllerAnimated:YES completion:nil];
        addTextPopoverView = nil;
    }
}

-(void)handleSingleTapToEditTextDelegateMethod:(UITapGestureRecognizer *)gesture{
    //    if (self.gkSession && [[NSUserDefaults standardUserDefaults] integerForKey:KEY_USER_ROLE] == ESUserRolePresenter) {
    //gesture.view
    UILabel *piece =(UILabel *) [gesture view];
    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    
    BOOL isObjPresent =[contentView.addedLableArray containsObject:piece];
    
    if (isObjPresent) {
        
        textPopoverView = [[ESTextPopoverViewController alloc]init];
        textPopoverView.delegate = self;
        textPopoverView.text = piece.text;
        CGFloat labelFontSize ;
        if (piece.font.pointSize < 14.f) {
            labelFontSize = 14.f;
        }else{
            labelFontSize = piece.font.pointSize;
        }
        textPopoverView.fontSize = labelFontSize;
        textPopoverView.textColor = piece.textColor;
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            
            textPopoverView.indexOfText = [contentView.addedLableArray indexOfObject:piece];
            
            textPopoverView.modalPresentationStyle = UIModalPresentationPopover;
            [self presentViewController:textPopoverView animated:YES completion:nil];
            
            UIPopoverPresentationController *textPopoverController = [textPopoverView popoverPresentationController];
            textPopoverController.delegate = self;
            textPopoverController.sourceView = self.view;
            textPopoverController.sourceRect = piece.frame;
            textPopoverController.permittedArrowDirections = UIPopoverArrowDirectionAny;
            
//            textPopoverController = [[UIPopoverController alloc] initWithContentViewController:textPopoverView];
//            textPopoverController.delegate = self;
//            [textPopoverController setPopoverContentSize:CGSizeMake(280,400.0f)];
//            [textPopoverController presentPopoverFromRect:piece.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }
    //    }
}
-(void)presentTextEditViewFromMenuItemWithIndex:(NSInteger)index{
    
    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    UILabel *l = [contentView.addedLableArray objectAtIndex:index];
    
    addTextPopoverView = [[ESTextPopoverViewController alloc]init];
    addTextPopoverView.delegate = self;
    addTextPopoverView.indexOfText = index;
    addTextPopoverView.text = l.text;
    
    CGFloat labelFontSize ;
    if (l.font.pointSize < MINIMUM_FONT_VALUE) {
        labelFontSize = MINIMUM_FONT_VALUE;
    }else{
        labelFontSize = l.font.pointSize;
    }
    addTextPopoverView.fontSize = l.font.pointSize;
    addTextPopoverView.textColor = l.textColor;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:addTextPopoverView];
    
    [self presentViewController:navController animated:YES completion:nil];
    
    
}
-(void)deleteTextFromPopover:(ESTextPopoverViewController *)vc withPacket:(ESTextPacket *)packet{
    
    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    [contentView processText:packet];
    [self transferPacket:packet fromContentView:contentView];
    [self processTextWithPacket:packet];
    
    if (textPopoverView != nil) {
        //[textPopoverController dismissPopoverAnimated:YES];
        [[textPopoverView presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        textPopoverView = nil;
    }
    
}
-(void) deleteTextFromMenuItemWithPacket:(ESTextPacket *)packet{
    
    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    [contentView processText:packet];
    [self transferPacket:packet fromContentView:contentView];
    [self processTextWithPacket:packet];
    
}
-(void) deletePictureFromMenuItemWithpacket:(ESPicturePacket *)packet{
    
    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    [contentView processPicture:packet];
    [self processPictureWithPacket:packet];
    [self transferPacket:packet fromContentView:contentView];
    
}
-(void) fontSizeChangeFromPopover:(ESTextPopoverViewController *)vc withPacket:(ESTextPacket *)packet{
    
    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    [contentView processText:packet];
    
}

-(void) textColorChangeFromPopover:(ESTextPopoverViewController *)vc withPacket:(ESTextPacket *)packet{
    
    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    [contentView processText:packet];
    
}

-(void)textChangeFromPopover:(ESTextPopoverViewController *)vc withPacket:(ESTextPacket *)packet{
    
    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    [contentView processText:packet];
    
}

//DISMISS POPOVER TAPPING OUTSIDE OF POPOVER

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    
    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    if (contentView.addedLableArray.count==0)
        return;
    NSInteger index = [ contentView.addedLableArray indexOfObject:[contentView.addedLableArray lastObject]];
    
    UILabel *label = [contentView.addedLableArray objectAtIndex:index];
    CGFloat labelFontSize ;
    if (label.font.pointSize < MINIMUM_FONT_VALUE) {
        labelFontSize = MINIMUM_FONT_VALUE;
    }else{
        labelFontSize = label.font.pointSize;
    }
    //CGSize textSize = [label.text sizeWithFont:[UIFont systemFontOfSize:labelFontSize] constrainedToSize:CGSizeMake(320, MAXFLOAT) lineBreakMode:label.lineBreakMode];
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:label.text
                                                                         attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:labelFontSize]}];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){320, MAXFLOAT}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize textSize = rect.size;
    
    label.frame = CGRectMake(label.frame.origin.x ,label.frame.origin.y, textSize.width, textSize.height);
    
    //    NSLog(@"presenter dismisspopover with %ld = index,%@ = text center=%f,%f",(long)index,label.text,label.center.x,label.center.y);
    
    
    ESTextPacket *packet = [ESTextPacket packetToDismissPopoverWithState:ESTextStateDismisPopover indexValue:index withText:label.text withTextColor:label.textColor withFontSize:labelFontSize withCenter:label.frame];
    
    [contentView processText:packet];
    [self transferPacket:packet fromContentView:contentView];
    [self processTextWithPacket:packet];
    
}

//-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
//    
//    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
//    if (contentView.addedLableArray.count==0)
//        return;
//    NSInteger index = [ contentView.addedLableArray indexOfObject:[contentView.addedLableArray lastObject]];
//    
//    UILabel *label = [contentView.addedLableArray objectAtIndex:index];
//    CGFloat labelFontSize ;
//    if (label.font.pointSize < MINIMUM_FONT_VALUE) {
//        labelFontSize = MINIMUM_FONT_VALUE;
//    }else{
//        labelFontSize = label.font.pointSize;
//    }
//    //CGSize textSize = [label.text sizeWithFont:[UIFont systemFontOfSize:labelFontSize] constrainedToSize:CGSizeMake(320, MAXFLOAT) lineBreakMode:label.lineBreakMode];
//    
//    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:label.text
//                                    attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:labelFontSize]}];
//    CGRect rect = [attributedText boundingRectWithSize:(CGSize){320, MAXFLOAT}
//                                               options:NSStringDrawingUsesLineFragmentOrigin
//                                               context:nil];
//    CGSize textSize = rect.size;
//    
//    label.frame = CGRectMake(label.frame.origin.x ,label.frame.origin.y, textSize.width, textSize.height);
//    
//    //    NSLog(@"presenter dismisspopover with %ld = index,%@ = text center=%f,%f",(long)index,label.text,label.center.x,label.center.y);
//    
//    
//    ESTextPacket *packet = [ESTextPacket packetToDismissPopoverWithState:ESTextStateDismisPopover indexValue:index withText:label.text withTextColor:label.textColor withFontSize:labelFontSize withCenter:label.frame];
//    
//    [contentView processText:packet];
//    [self transferPacket:packet fromContentView:contentView];
//    [self processTextWithPacket:packet];
//    
//}

#endif

#pragma mark - When Finish Shoot

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *originalImage=[info objectForKey:UIImagePickerControllerOriginalImage];
    
    CGSize size=     CGSizeMake(200.f, 200.f);
    UIGraphicsBeginImageContext(size);
    [originalImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGFloat originY =0.f;
    CGFloat originX =0.f;
    
    BOOL iPad = NO;
#ifdef UI_USER_INTERFACE_IDIOM
    iPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#endif
    if(iPad){
        originY  = 350.f;
        originX =100.f;
    }else{
        originY = 150.f;
        originX =-100.f;
    }
    
    ESPDFContentView *contentView = [self.contentViews objectForKey:[NSNumber numberWithInteger:self.currentPage]];
    ESPicturePacket *packet= [ESPicturePacket  packetWithPicture:destImage andPictureframe:CGRectMake(self.view.center.x-originX,self.view.center.y-originY,200,200) withState:ESStateAddPicture];
    
    [contentView processPicture:packet];
    [self transferPacket:packet fromContentView:contentView];
    [self processPictureWithPacket:packet];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
#if X_PRESENTER
        if (imagePicker != nil) {
            //[imagePopover dismissPopoverAnimated:YES];
            [[imagePicker presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
            imagePicker = nil;
        }
#endif
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

#pragma mark - When Tap Cancel

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)showAlert{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:@"To use this feature , purchase PresInteract Pro App from AppStore"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Not now"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                               }];
    
    UIAlertAction* purchaseButton = [UIAlertAction
                               actionWithTitle:@"Purchase now"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   
                               }];
    
    [alert addAction:okButton];
    [alert addAction:purchaseButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
//    UIAlertView *alert=   [[UIAlertView alloc ]initWithTitle:@"" message:@"To use this feature , purchase PresInteract Pro App from AppStore" delegate:self cancelButtonTitle:@"Not now" otherButtonTitles:@"Purchase now", nil];
//    
//    alert.tag = UNAVAILABLE_FEATURE_ALERT_TAG;
//    
//    [alert show];
    
    return;
    
}
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    
//    if (alertView.tag == UNAVAILABLE_FEATURE_ALERT_TAG) {
//        if (!(alertView.cancelButtonIndex == buttonIndex)) {
//            
//
//        }
//    }else if (alertView.tag ==  RENAME_ANNOTATED_DOC_ALERT_TAG){
//        
//        UITextField *textField = [alertView textFieldAtIndex:0];
//        annotatedFileName = @"";
//        if (alertView.cancelButtonIndex == buttonIndex) {
//            annotatedFileName = [NSString stringWithFormat:@"%@.pdf",@"AnnotatedPDF"];
//        }else{
//            annotatedFileName = [NSString stringWithFormat:@"%@.pdf",textField.text];
//        }
//        //Check if PDF drawing is completed or not. if not show an loading
//        [self presentMailComposeVCWithFileName:annotatedFileName];
//        
//    }
//    return;
//}
-(void)presentMailComposeVCWithFileName:(NSString *)PDFFileName{
    
    if (isPDFDrawn) {
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:@"Send email"];
        [mc setMessageBody:@"" isHTML:NO];
        
        NSData *fileData = [NSData dataWithContentsOfFile:[self cacheFileName]];
        // Add attachment
        [mc addAttachmentData:fileData mimeType:@"application/pdf" fileName:PDFFileName];
        
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:[self cacheFileName] error:&error];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
        [self loadingShow:NO];
        
    }else{
        //show loading
        [self loadingShow:YES];
    }
    
}
-(void)loadingShow:(BOOL)isShow{
    
    if (isShow) {
        if (loadingView == nil) {
            loadingView = [[UIView alloc] initWithFrame:self.navigationController.view.bounds];
        }
        loadingView .backgroundColor = [UIColor clearColor];
        UIView     *viewBack = [[UIView alloc] initWithFrame:CGRectMake(95, 230, 130, 40)];
        viewBack.backgroundColor = [UIColor blackColor];
        viewBack.alpha = 0.7f;
        viewBack.layer.masksToBounds = NO;
        viewBack.layer.cornerRadius = 8;
        
        UIActivityIndicatorView  *spinningWheel = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(5.0, 5.0, 30.0, 30.0)];
        [spinningWheel startAnimating];
        [viewBack addSubview:spinningWheel];
        
        UILabel *lblLoading = [[UILabel alloc] initWithFrame:CGRectMake(23, 6, 110, 25)];
        lblLoading.backgroundColor = [UIColor clearColor];
        lblLoading.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
        lblLoading.textAlignment = NSTextAlignmentCenter;
        lblLoading.textColor = [UIColor whiteColor];
        lblLoading.text = @"Loading...";
        [viewBack addSubview:lblLoading];
        viewBack.center = self.navigationController.view.center;
        [loadingView addSubview:viewBack];
        
        [self.navigationController.view addSubview:loadingView];
        
    }else{
        if (loadingView) {
            [loadingView removeFromSuperview];
            loadingView = nil;
            
        }
    }
}

#pragma mark - Product view controller delegate methods


-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) prefersStatusBarHidden {
    return !self.navigationController.navigationBarHidden;
}

-(UIStatusBarAnimation) preferredStatusBarUpdateAnimation {
    if (self.navigationController.navigationBarHidden) {
        return UIStatusBarAnimationNone;
    } else {
        return UIStatusBarAnimationSlide;
    }
    
}

@end
