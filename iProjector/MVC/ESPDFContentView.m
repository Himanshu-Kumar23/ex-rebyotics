//
//  ESPDFContentView.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 7/31/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESPDFContentView.h"

#import <QuartzCore/QuartzCore.h>

#import "ESCurve.h"
#import "ESLaserPointerPacket.h"
#import "ESZoomTransitionPacket.h"

#import "ESPDFDocumentViewController.h"

#import "ESText1.h"

#import "ESPicture.h"

#define Packect_Frame_size_iPhone    200.f
#define Packect_Frame_size_iPad      320.f

//#import "ESSettingListViewController.h"

@interface ESPDFContentView () {
    UIImageView *laserPointerImageView;
    //    UIView *laserPointerImageView;
    ESAnnotationsLayer *annotationsLayer;
    UIPanGestureRecognizer *paintGestureRecognizer;
    UIPanGestureRecognizer *laserPointGestureRecognizer;
    UITapGestureRecognizer *singleTapGestureRecognizer;
    UITapGestureRecognizer *doubleTapGestureRecognizer;
    
    NSInteger textMenuIndex;
    NSInteger picMenuIndex;

    //    NSInteger pageNum;
}

@end

@implementation ESPDFContentView

@synthesize paintGestureRecognizer = paintGestureRecognizer;
@synthesize laserPointGestureRecognizer = laserPointGestureRecognizer;
@synthesize singleTapGestureRecognizer = singleTapGestureRecognizer;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

-(id) initWithFrame:(CGRect)frame fileURL:(NSURL *)fileURL page:(NSUInteger)page password:(NSString *)phrase {
    
    self = [super initWithFrame:frame fileURL:fileURL page:page password:phrase];
    if (self) {
        
        //Turn off default scrollview panning.
        self.panGestureRecognizer.enabled = NO;
        [self initializeLaserPointer];
        
        self.backgroundColor = [UIColor whiteColor];
        
        annotationsLayer = [ESAnnotationsLayer layer];
        annotationsLayer.frame =[self containerView].layer.bounds;
        
        [[self containerView].layer addSublayer:annotationsLayer];
        
        [self containerView].userInteractionEnabled = YES;
        
        paintGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognizerDidRecognize:)];
        paintGestureRecognizer.enabled = NO;
        [[self containerView] addGestureRecognizer:paintGestureRecognizer];
        
        
        laserPointGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(laserPointPanGestureRecognizerDidRecognize:)];
        [[self containerView] addGestureRecognizer:laserPointGestureRecognizer];
        laserPointGestureRecognizer.enabled = NO;
        
        //        [self.panGestureRecognizer requireGestureRecognizerToFail:laserPointPanGestureRecognizer];
        singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTapGestureRecognizer:)];
        singleTapGestureRecognizer.numberOfTouchesRequired = 1;
        singleTapGestureRecognizer.numberOfTapsRequired = 1;
        [[self containerView] addGestureRecognizer:singleTapGestureRecognizer];
        
        _addedLableArray = [[NSMutableArray alloc]init];
        _addedPictureArray = [[NSMutableArray alloc]init];
        _addedLableIndexArray = [[NSMutableArray alloc]init];
         //        [self addTextOnPage];
    }
    return self;
}

//fix for 3586 on iOS7.
- (void) layoutSubviews {
    self.panGestureRecognizer.enabled = NO;
    [super layoutSubviews];
}

#if X_PRESENTER

-(void)handleSingleTapGestureRecognizerOnTextLabel:(UITapGestureRecognizer *)tapGesture{
    textMenuIndex = NSNotFound;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        
        if (_gestureDelegate) {
            [_gestureDelegate handleSingleTapToEditTextDelegateMethod:tapGesture];
        }
    }else{
        [self becomeFirstResponder];
        UILabel *piece =(UILabel *) [tapGesture view];
        textMenuIndex = [_addedLableArray indexOfObject:piece];
        piece.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        UIMenuItem *edit = [[UIMenuItem alloc] initWithTitle:@"Edit" action:@selector(editText:)];
        UIMenuItem *delete = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(deleteText:)];
        CGPoint p = [tapGesture locationInView:piece];

        UIMenuController *menu =nil;
        menu=[UIMenuController sharedMenuController];
        [menu setMenuItems:[NSArray arrayWithObjects:edit, delete, nil]];
        [menu setTargetRect:CGRectMake(p.x,p.y, 0.0f, 0.0f) inView:piece];
        [menu setMenuVisible:YES animated:YES];
        
        NSAssert([self becomeFirstResponder], @"Sorry, UIMenuController will not work with %@ since it cannot become first responder", self);
    }
    
}
-(BOOL)canBecomeFirstResponder{
    return YES;
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    
    if (action == @selector(editText:)) {
        return YES;
    }else if (action == @selector(deleteText:)){
        return YES;
    }else if (action == @selector(deletePicture:)){
        return YES;
    }
    return NO;
}
-(void)editText:(id)sender{
    NSLog(@"%ld = index",(long)textMenuIndex);
    if (textMenuIndex != NSNotFound && _gestureDelegate) {
        [_gestureDelegate presentTextEditViewFromMenuItemWithIndex:textMenuIndex];
    }
}
-(void)deleteText:(id)sender{
    NSLog(@"%ld = index",(long)textMenuIndex);
    ESTextPacket *packet = [ESTextPacket removeTextWithState:ESTextStateRemoveText withIndex:textMenuIndex];
    
    if (textMenuIndex != NSNotFound && _gestureDelegate) {
        [_gestureDelegate deleteTextFromMenuItemWithPacket:packet];
    }
    
}
-(void)deletePicture:(id)sender{
    
    NSLog(@"%ld = picMenuIndex",(long)picMenuIndex);
    ESPicturePacket *packet = [ESPicturePacket removePictureWithState:ESStateRemovePicture withIndex:picMenuIndex];
    
    if (picMenuIndex != NSNotFound && _gestureDelegate) {
        [_gestureDelegate deletePictureFromMenuItemWithpacket:packet];
    }

}
-(void)moveText:(UIPanGestureRecognizer *)panGesture{
    
//    if ( _packetDelegate && [[NSUserDefaults standardUserDefaults] integerForKey:KEY_USER_ROLE] == ESUserRolePresenter) {
    
        CGPoint translation = [panGesture translationInView:[self containerView]];
        UILabel *piece =(UILabel *) [panGesture view];
        
        if ([panGesture state] == UIGestureRecognizerStateBegan || [panGesture state] == UIGestureRecognizerStateChanged) {
            
            [panGesture setTranslation:CGPointZero inView:[self containerView]];
            
            [piece setCenter:CGPointMake([piece center].x + translation.x, [piece center].y+translation.y)];
            
            CGRect frame = piece.frame;
            frame.origin.x = MAX(0, frame.origin.x);
            frame.origin.x = MIN(frame.origin.x, [self containerView].bounds.size.width - piece.frame.size.width);
            frame.origin.y = MAX(0, frame.origin.y);
            frame.origin.y = MIN(frame.origin.y, [self containerView].bounds.size.height - piece.frame.size.height);
            frame.size.height = MAX(0, frame.size.height);
            frame.size.width = MAX(0, frame.size.width);
            piece.frame = frame;
            
        }else if ([panGesture state] == UIGestureRecognizerStateEnded){
    
            //            }
            ESTextPacket *packet = [ESTextPacket packetWithAddTextMoveToPosition:piece.frame withTextState:ESTextStateMoveText withIndexValue:[_addedLableArray indexOfObject:piece]];
            [_packetDelegate transferPacket:packet fromContentView:self];
            
            if (_curveDelegate) {
                [_curveDelegate processTextWithPacket:packet];
            }
        }
    //    }
}
//presenter trying to draw on the screen.
- (void) panGestureRecognizerDidRecognize:(UIPanGestureRecognizer *) panGestureRecognizer {

    ESAnnotationPacket *annotation;
    //Calculate scaled point

    CGPoint point = [panGestureRecognizer locationInView:[self containerView]];
    CGPoint scaledPoint = CGPointMake(point.x/[self containerView].bounds.size.width, point.y/[self containerView].bounds.size.height);
    if (_paintPalette.selectedThickness == 20) {
       
        UIColor *highlightColor = [UIColor colorWithRed:83/255.f green:230/258.f blue:83/255.f alpha:0.4];
        //touchStart
        if (panGestureRecognizer.state == UIGestureRecognizerStateBegan) {
            
            annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint lineWidth:10.0 andColor:highlightColor annotationType:ESAnnotationTypeHighlightStart];
        }
        
        //touchMoved
        else if (panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
            annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint lineWidth:10.0 andColor:highlightColor annotationType:ESAnnotationTypeHighlightMove];
        }
        
        //touch cancelled or ended
        else if (panGestureRecognizer.state == UIGestureRecognizerStateEnded || panGestureRecognizer.state == UIGestureRecognizerStateCancelled) {
            
            annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint lineWidth:10.0 andColor:highlightColor annotationType:ESAnnotationTypeHighlightEnd];
        }

    }else{
        //touchStart
        if (panGestureRecognizer.state == UIGestureRecognizerStateBegan) {
            
            annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint lineWidth:_paintPalette.selectedThickness andColor:_paintPalette.selectedColor annotationType:ESAnnotationTypeStart];
            //        annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint  annotationType:ESAnnotationTypeStart];
            annotation.lineWidth = _paintPalette.selectedThickness;
            annotation.color = _paintPalette.selectedColor;
            
        }
        
        //touchMoved
        else if (panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
            annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint lineWidth:_paintPalette.selectedThickness andColor:_paintPalette.selectedColor annotationType:ESAnnotationTypeMove];
            
            //            annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint annotationType:ESAnnotationTypeMove];
            
        }
        
        //touch cancelled or ended
        else if (panGestureRecognizer.state == UIGestureRecognizerStateEnded || panGestureRecognizer.state == UIGestureRecognizerStateCancelled) {
            
            annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint lineWidth:_paintPalette.selectedThickness andColor:_paintPalette.selectedColor annotationType:ESAnnotationTypeEnd];
            
            //            annotation = [ESAnnotationPacket packetWithScaledPoint:scaledPoint annotationType:ESAnnotationTypeEnd];
        }
        
        //draw on our own screen.

    }
    [self processAnnotation:annotation];
    
    //send packet across network
    if (_packetDelegate) {
        [_packetDelegate transferPacket:annotation fromContentView:self];
    }
    
    if (_curveDelegate) {
        [_curveDelegate processAnnotationPacket:annotation];
    }
}
#endif

/*
 -(void)scrollViewDidScroll:(UIScrollView *)scrollView{
 
 
 //    CGPoint scrollViewPoint;
 //    scrollViewPoint.x = theContainerView.frame.size.width;
 //    scrollViewPoint.y = theContainerView.frame.size.height;
 
 //method for scrolling
 
 
 }
 - (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view{
 
 }
 */

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    if (_packetDelegate) {
        ESZoomTransitionPacket *zoomPackect;
        zoomPackect = [ESZoomTransitionPacket packetWithZoomValue:scrollView.zoomScale scrollViewOffset:scrollView.contentOffset minimumZoomValue:[self minimumZoomScale]];
        
        [_packetDelegate transferPacket:zoomPackect];
    }
}

- (ESAnnotationsLayer *) annotationsLayer {
    return annotationsLayer;
}

#pragma mark -
#pragma mark LaserPointerStuff

//Presenter trying to modify laser pointer.
- (void) laserPointPanGestureRecognizerDidRecognize:(UIPanGestureRecognizer *) panGestureRecognizer {
    
    if (panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        
        //Calculate scaled point.
        //        CGPoint point = [panGestureRecognizer locationInView:self];
        //        point.y -= 60;
        //        CGPoint scaledPoint = CGPointMake(point.x/self.bounds.size.width, point.y/self.bounds.size.height);
        
        CGPoint point = [panGestureRecognizer locationInView:[self containerView]];
        point.y -= 100;
        CGPoint scaledPoint = CGPointMake(point.x/[self containerView].bounds.size.width, point.y/[self containerView].bounds.size.height);
        
        //Show laser pointer on own screen
        
        NSInteger laserPointer = [[[NSUserDefaults standardUserDefaults] valueForKey:KEY_LASER_POINTER] integerValue];
        
        [self showLaserPointerAtOffset:scaledPoint withLaserPointerType:laserPointer];
        
        //transfer laser pointer packet across the network to be displayed on attendees' screen
        if (_packetDelegate) {
            ESLaserPointerPacket *packet = [ESLaserPointerPacket packetWithPointerVisibleAt:scaledPoint withLaserPointerType:laserPointer];
            [_packetDelegate transferPacket:packet fromContentView:self];
        }
        
    } else if (panGestureRecognizer.state == UIGestureRecognizerStateEnded || panGestureRecognizer.state == UIGestureRecognizerStateCancelled) {
        
        //hide laser pointer on own screen
        [self hideLaserPointer];
        
        //transfer packet across network to hide laser pointer on attendees' screen.
        if (_packetDelegate) {
            ESLaserPointerPacket *packet = [ESLaserPointerPacket packetWithhiddenLaserPointer];
            [_packetDelegate transferPacket:packet fromContentView:self];
        }
    }
}

- (void) processLaserPointerPacket:(ESLaserPointerPacket *) packet {
    if (packet.visible == NO) {
        [self hideLaserPointer];
    } else {
        [self showLaserPointerAtOffset:packet.point withLaserPointerType:packet.laserPointer];
    }
}

- (void) showLaserPointerAtOffset:(CGPoint) scaledPoint withLaserPointerType:(NSInteger)laserPointerType {
    laserPointerImageView.center = CGPointMake(scaledPoint.x  * [self containerView].bounds.size.width, scaledPoint.y * [self containerView].bounds.size.height);
    laserPointerImageView.hidden = NO;
    
    if (laserPointerType == ESLaserPointerRed) {
        laserPointerImageView.image = [UIImage imageNamed:@"Red-Laser"];
    }else{
        laserPointerImageView.image = [UIImage imageNamed:@"Green-Laser"];
    }
    
}

- (void) hideLaserPointer {
    laserPointerImageView.hidden = YES;
}

- (void) initializeLaserPointer {
    // Initialization code
    laserPointerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 12)];
    NSInteger laserPointer = [[[NSUserDefaults standardUserDefaults] valueForKey:KEY_LASER_POINTER] integerValue];
    if (laserPointer == ESLaserPointerRed) {
        laserPointerImageView.image = [UIImage imageNamed:@"Red-Laser"];
    }else{
        laserPointerImageView.image = [UIImage imageNamed:@"Green-Laser"];
    }
    laserPointerImageView.layer.shadowColor = [UIColor whiteColor].CGColor;
    //    [self addSubview:laserPointerImageView];
    [[self containerView] addSubview:laserPointerImageView];
    laserPointerImageView.hidden = YES;
}

#if X_PRESENTER
#pragma mark -
#pragma mark PaintView drawing


- (void) setPaintPalette:(ESPaintPaletteView *)paintPalette {
    if (paintPalette) {
        paintGestureRecognizer.enabled = _enableGestures;
        laserPointGestureRecognizer.enabled = NO;
        if (!_paintPalette) {
            [paintPalette.undoButton addTarget:self action:@selector(undoButtonClickedOnPalette:) forControlEvents:UIControlEventTouchUpInside];
            [paintPalette.clearPageButton addTarget:self action:@selector(clearButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [paintPalette.highlightTextButton addTarget:self action:@selector(highlightTextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"Add targets to contentView: %@", self);
        }
        
    } else {
        if (_paintPalette) {
            [_paintPalette.undoButton removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
            [_paintPalette.clearPageButton removeTarget:self action:NULL forControlEvents:UIControlEventTouchUpInside];
            [_paintPalette.highlightTextButton removeTarget:self action:@selector(highlightTextButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"Remove targets from contentView: %@", self);
        }
        
        paintGestureRecognizer.enabled = NO;
        laserPointGestureRecognizer.enabled = _enableGestures;
        
    }
    
    _paintPalette = paintPalette;
}

- (void) undoButtonClickedOnPalette:(UIButton *) undoButton {
    ESAnnotationPacket *annotation = [ESAnnotationPacket undoPacket];
    
    //send packet across.
    if (_packetDelegate) {
        [_packetDelegate transferPacket:annotation fromContentView:self];
    }
    
    if (_curveDelegate) {
        [_curveDelegate processAnnotationPacket:annotation];
    }
    
    [self processAnnotation:annotation];
}
-(void)highlightTextButtonClicked:(UIButton *)pallete{
    
}
-(void) clearButtonClicked:(UIButton *)pallette{
    ESAnnotationPacket *annotation = [ESAnnotationPacket clearPacket];
    
    //send packet across.
    if (_packetDelegate) {
        [_packetDelegate transferPacket:annotation fromContentView:self];
    }
    
    if (_curveDelegate) {
        [_curveDelegate processAnnotationPacket:annotation];
    }
    
    [self processAnnotation:annotation];
    
}

-(void) addTextButtonClicked:(ESPaintPaletteView *)pallete{
    
}
-(void)addPictureButtonClicked:(ESPaintPaletteView *)pallete{

}
#endif

- (void) processAnnotation:(ESAnnotationPacket *) annotation {
    switch (annotation.annotationType) {
        case ESAnnotationTypeHighlightStart:
        case ESAnnotationTypeStart:
            [annotationsLayer startCurveAtPoint:annotation.point withColor:annotation.color andLineWidth:annotation.lineWidth];
            break;
        case ESAnnotationTypeMove:
            [annotationsLayer moveCurveToPoint:annotation.point];
            break;
        case ESAnnotationTypeEnd:
            [annotationsLayer endCurveAtPoint:annotation.point];
            break;
        case ESAnnotationTypeUndo:
            [annotationsLayer removeLastCurve];
            break;
        case ESAnnotationTypeHighlightMove:
            [annotationsLayer moveHighlightCurveToPoint:annotation.point];
            break;
        case ESAnnotationTypeHighlightEnd:
            [annotationsLayer endHighlightCurveAtPoint:annotation.point];
            break;
        case ESAnnotationTypeClear:
            [annotationsLayer clear];
        default:
            break;
    }
}
-(void)handleSingleTapGestureRecognizer:(UITapGestureRecognizer *) gestureRecognizer {
    if (_gestureDelegate) {
        [_gestureDelegate handleSingleTapDelegateMethod:gestureRecognizer];
    }
}
-(CGRect)addStickyFrame{
    
    BOOL iPad = NO;
#ifdef UI_USER_INTERFACE_IDIOM
    iPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#endif
    if(iPad){
        return CGRectMake(280.f,160.f, Packect_Frame_size_iPhone,Packect_Frame_size_iPhone);
    }
    return CGRectMake(60, 70, Packect_Frame_size_iPhone,Packect_Frame_size_iPhone);
    

}
-(void) processText:(ESTextPacket *)packet{
    
    //put a lable into an array and according to the index of the array make editing of the lable with object
    
    if (packet.textState == ESTextStateStartText) {
        
        UILabel *labelText = [[UILabel alloc] init];
        labelText.text = packet.text;
        labelText.textColor = packet.textColor;
        labelText.font = [UIFont systemFontOfSize:packet.textFontSize];
        if (packet.text.length == 0) {
            labelText.frame = [self addStickyFrame];

        }else{
            labelText.frame = packet.textFrame;
        }
        labelText.backgroundColor = [UIColor clearColor];
        labelText.numberOfLines = 0;
        
        labelText.layer.borderColor = [UIColor darkGrayColor].CGColor;
        labelText.layer.borderWidth = 1.5f;
        labelText.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
        [[self containerView] addSubview:labelText];
        [_addedLableArray addObject:labelText];
#if X_PRESENTER
        
        labelText.userInteractionEnabled = YES;
        UITapGestureRecognizer  *singleTapOnTextLabel = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTapGestureRecognizerOnTextLabel:)];
        singleTapOnTextLabel.numberOfTouchesRequired = 1;
        singleTapOnTextLabel.numberOfTapsRequired = 1;
        [labelText addGestureRecognizer:singleTapOnTextLabel];
        
        UIPanGestureRecognizer *panGextureAddTextView = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveText:)];
        panGextureAddTextView.enabled = YES;
        panGextureAddTextView .minimumNumberOfTouches = 1;
        [labelText addGestureRecognizer:panGextureAddTextView];
        
#endif
        
    }else if (packet.textState == ESTextStateTextChange){
        
        UILabel *l = [_addedLableArray objectAtIndex:packet.indexValue];
        l.text = packet.text;
        
    }else if (packet.textState == ESTextStateAddTextDone){//done button tapped
        
        UILabel *l;
        if ([[NSUserDefaults standardUserDefaults] integerForKey:KEY_USER_ROLE] == ESUserRoleAttendee &&_packetDelegate ) {
            if (![_addedLableIndexArray containsObject:[NSNumber numberWithInteger:packet.indexValue]] ) {
                [_addedLableIndexArray addObject:[NSNumber numberWithInteger:packet.indexValue]];
                l = [[UILabel alloc] init];
                l.backgroundColor = [UIColor clearColor];
                l.numberOfLines = 0;
                l.frame = [self addStickyFrame];
                
                l.layer.borderColor = [UIColor darkGrayColor].CGColor;
                l.layer.borderWidth = 1.5f;
                l.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
                [[self containerView] addSubview:l];
                [_addedLableArray addObject:l];
            }
        }
        l = [_addedLableArray objectAtIndex:packet.indexValue];
        l.text = packet.text;
        l.textColor = packet.textColor;
        l.font = [UIFont systemFontOfSize:packet.textFontSize];
        //CGSize textSize = [l.text sizeWithFont:l.font constrainedToSize:CGSizeMake(l.frame.size.width, MAXFLOAT) lineBreakMode:l.lineBreakMode];
        
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:l.text
                                                                             attributes:@{NSFontAttributeName: l.font}];
        
        CGRect rect = [attributedText boundingRectWithSize:(CGSize){l.frame.size.width, MAXFLOAT}
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                   context:nil];
        CGSize textSize = rect.size;
        
        l.frame = CGRectMake(packet.textFrame.origin.x ,packet.textFrame.origin.y, textSize.width+5, textSize.height+5);
//        l.center = CGPointMake(packet.translationPoint.x, packet.translationPoint.y);
        
    }else if (packet.textState == ESTextStateRemoveText){
        if ([_addedLableIndexArray containsObject:[NSNumber numberWithInteger:packet.indexValue]] ) {
            [_addedLableIndexArray removeObject:[NSNumber numberWithInteger:packet.indexValue]];
        }
        UILabel *l = [_addedLableArray objectAtIndex:packet.indexValue];

        [_addedLableArray removeObjectAtIndex:packet.indexValue];
        [l removeFromSuperview];
        l = nil;
        
    }else if (packet.textState == ESTextStateTextColorChange){
        
        UILabel *l = [_addedLableArray objectAtIndex:packet.indexValue];
        l.textColor = packet.textColor;
        
    }else if (packet.textState == ESTextStateTextFontChange){
        
        UILabel *l = [_addedLableArray objectAtIndex:packet.indexValue];
        l.font = [UIFont systemFontOfSize:packet.textFontSize];
        //CGSize textSize = [l.text sizeWithFont:l.font constrainedToSize:CGSizeMake([self addStickyFrame].size.width, MAXFLOAT) lineBreakMode:l.lineBreakMode];
        if (l.text) {
            NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:l.text
                                                                                 attributes:@{NSFontAttributeName: l.font}];
            
            CGRect rect = [attributedText boundingRectWithSize:(CGSize){[self addStickyFrame].size.width, MAXFLOAT}
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                       context:nil];
            CGSize textSize = rect.size;
            
            l.frame = CGRectMake(l.frame.origin.x,l.frame.origin.y, textSize.width, textSize.height);
        }
        
       
        
    }else if (packet.textState == ESTextStateMoveText){
        
        UILabel *l = [_addedLableArray objectAtIndex:packet.indexValue];
        l.frame = packet.textFrame;
        
    }else { // state = ESTextStateDismisPopover
        
        UILabel *l;
        if ([[NSUserDefaults standardUserDefaults] integerForKey:KEY_USER_ROLE] == ESUserRoleAttendee && _packetDelegate) {
            
            l = [[UILabel alloc] init];
            l.backgroundColor = [UIColor clearColor];
            l.numberOfLines = 0;

            l.layer.borderColor = [UIColor darkGrayColor].CGColor;
            l.layer.borderWidth = 1.5f;
            l.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
            [[self containerView] addSubview:l];
            [_addedLableArray addObject:l];
        }

        l = [_addedLableArray objectAtIndex:packet.indexValue];
        
        NSLog(@"presentaddtext with %ld = index,%@ = text centre x= %f y= %f",(long)packet.indexValue,packet.text,packet.textFrame.origin.x ,packet.textFrame.origin.y);
        
        if (packet.text.length == 0) {//if no text is added
            
            [_addedLableArray removeObjectAtIndex:packet.indexValue];
            [l removeFromSuperview];
            l = nil;
            
        }else {
            l.text = packet.text;
            l.textColor = packet.textColor;
            l.font = [UIFont systemFontOfSize:packet.textFontSize];
            l.frame = packet.textFrame;
            
        }
    }
}

- (void) dealloc {
    //    [super dealloc];
#if X_PRESENTER
    if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(@"7.1")) {
      self.paintPalette = nil;
    }
#endif
}


-(void)processPicture:(ESPicturePacket *)packet{
   
    if (packet.pictureState == ESStateAddPicture) {
        
        UIImageView *stickyImage = [[UIImageView alloc]init];
        stickyImage.contentMode = UIViewContentModeScaleAspectFit;
        if (CGRectIsEmpty(packet.pictureFrame)) {
            stickyImage.frame = [self addStickyFrame];;
        }else {
            stickyImage.frame = packet.pictureFrame;
        }
        [[self containerView] addSubview:stickyImage];
        stickyImage.image = packet.picture;
        [_addedPictureArray addObject:stickyImage];
    
        
#if X_PRESENTER

        stickyImage.userInteractionEnabled = YES;
        
        UILongPressGestureRecognizer *longPressToDeletePicture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPressToDeletePicture:)];
        longPressToDeletePicture.minimumPressDuration = 0.7f;
        [stickyImage addGestureRecognizer:longPressToDeletePicture];

        
        UIPanGestureRecognizer *movePicture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(movePicture:)];
        movePicture.enabled = YES;
        movePicture .minimumNumberOfTouches = 1;
        [stickyImage addGestureRecognizer:movePicture];
        
#endif

    }else if(packet.pictureState == ESStateMovePicture){
        
        UIImageView *pic = [_addedPictureArray objectAtIndex:packet.pictureIndex];
        pic.frame = packet.pictureFrame;
        
    }else if (packet.pictureState == ESStateRemovePicture){
    
        UIImageView *pic = [_addedPictureArray objectAtIndex:packet.pictureIndex];
        [_addedPictureArray removeObjectAtIndex:packet.pictureIndex];
        [pic removeFromSuperview];
        pic = nil;

    }
}

-(void)handleLongPressToDeletePicture:(UILongPressGestureRecognizer *)gesture{
   
    if (gesture.state == UIGestureRecognizerStateBegan) {
        
        picMenuIndex = NSNotFound;
        
        [self becomeFirstResponder];
        UIImageView *stickyImage =(UIImageView *) [gesture view];
        picMenuIndex = [_addedPictureArray indexOfObject:stickyImage];
        stickyImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        UIMenuItem *delete23 = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(deletePicture:)];
        CGPoint touchPoint = [gesture locationInView: stickyImage];

        UIMenuController *menu =nil;
        menu= [UIMenuController sharedMenuController];
        [menu setMenuItems:[NSArray arrayWithObjects:delete23, nil]];
        [menu setTargetRect:CGRectMake(touchPoint.x,touchPoint.y,0.f,0.f) inView:stickyImage];
        [menu setMenuVisible:YES animated:YES];
        
        NSAssert([self becomeFirstResponder], @"Sorry, UIMenuController will not work with %@ since it cannot become first responder", self);
    }

}
-(void)movePicture:(UIPanGestureRecognizer *)gesture{
//    if ( _packetDelegate && [[NSUserDefaults standardUserDefaults] integerForKey:KEY_USER_ROLE] == ESUserRolePresenter) {
    
        CGPoint translation = [gesture translationInView:[self containerView]];
        UIImageView *piece =(UIImageView *) [gesture view];
    
    //FIXME: Make sure that we are not sending a packet for move event. We just need to send the packet for state End.
    //FIXME: Same is the case with text.
        if ([gesture state] == UIGestureRecognizerStateBegan || [gesture state] == UIGestureRecognizerStateChanged) {

            //moving image should come above all the subview
            [[self containerView] bringSubviewToFront:piece];
        
            [gesture setTranslation:CGPointZero inView:[self containerView]];
            
            [piece setCenter:CGPointMake([piece center].x + translation.x, [piece center].y+translation.y)];
            
            //FIXME: Correct this so picture can be placed at the bottom and top edges of the page.
            CGRect frame = piece.frame;
            frame.origin.x = MAX(0, frame.origin.x);
            frame.origin.x = MIN(frame.origin.x, [self containerView].bounds.size.width - piece.frame.size.width);
            frame.origin.y = MAX(0, frame.origin.y);
            frame.origin.y = MIN(frame.origin.y, [self containerView].bounds.size.height - piece.frame.size.height);
            frame.size.height = MAX(0, frame.size.height);
            frame.size.width = MAX(0, frame.size.width);
            piece.frame = frame;
            
            
        }else if ([gesture state] == UIGestureRecognizerStateEnded){
           
            NSLog(@"indexof picture  %lu with arraycount %lu",(unsigned long)[_addedPictureArray indexOfObject:piece],(unsigned long)_addedPictureArray.count);
            ESPicturePacket *packet = [ESPicturePacket packetToMovePictureToPosition:piece.frame withPictureState:ESStateMovePicture withIndexValue:[_addedPictureArray indexOfObject:piece]];
            [_packetDelegate transferPacket:packet fromContentView:self];
            if (_curveDelegate) {
                [_curveDelegate processPictureWithPacket:packet];
            }
        }
}
-(void) addTextToPage:(NSMutableArray *)texts{
    
    for (ESText1 *text in texts) {
        [self addtext:text.textValue withColor:text.color withFontSize:text.fontSize withIndexValue:text.indexValue andFrame:text.textFrame];
    }
}
-(void) addPictureToPage:(NSMutableArray *)pictures{
    
    for (ESPicture *pic in pictures) {
        [self addPicture:pic.picture withFrame:pic.pictureFrame withIndex:pic.pictureIndex];
    }
    
}
-(void) addPicture:(UIImage *)image withFrame:(CGRect )frame withIndex:(NSInteger )index{

    ESPicturePacket *packet = [ESPicturePacket packetToAddPicture:image toPosition:frame withPictureState:ESStateAddPicture withIndexValue:index];
    [self processPicture:packet];
    
}
-(void)addtext:(NSString *)text withColor:(UIColor *)color withFontSize:(CGFloat )fontSize withIndexValue:(NSInteger )index andFrame:(CGRect)frame{
    
    ESTextPacket *packet= [ESTextPacket packetWithString:text textColor:color andTextFont:fontSize withTextState:ESTextStateStartText andWithIndex:index andFrame:frame];
    [self processText:packet];
    
}
-(void)clearText{
    
    if (_addedLableArray.count>0) {
        for (int k = 0; k < _addedLableArray.count; k++) {
            [[_addedLableArray objectAtIndex:k] removeFromSuperview];
        }
        [_addedLableArray removeAllObjects];
    }
}
-(void)clearPicture{
    
    if (_addedPictureArray.count>0) {
        for (int k = 0; k < _addedPictureArray.count; k++) {
            [[_addedPictureArray objectAtIndex:k] removeFromSuperview];
        }
        [_addedPictureArray removeAllObjects];
    }
}
@end
