//
//  ESDocumentsCollectionViewController.h
//  iProjector
//
//  Created by Manoj Katragadda on 10/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESSessionBaseViewController.h"
#import "ESDocumentsListViewController.h"
#import "ESSettingListViewController.h"
#import "ESCollectionCell.h"
#import "ESSessionBaseViewController.h"
@interface ESDocumentsCollectionViewController : ESDocumentsListViewController<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,GKSessionDelegate,DeleteCollectionCellDelegate>

@property (nonatomic, strong) NSMutableArray *pdfFiles;
- (void) removeFile:(NSString *) fileName;
@end
