//
//  ESWhiteBoard.m
//  iProjector
//
//  Created by Jayaprada Behera on 19/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESWhiteBoard.h"

@implementation ESWhiteBoard
@synthesize annotationsLayer;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        annotationsLayer = [ESAnnotationsLayer layer];
        annotationsLayer.frame = self.layer.bounds;
        [self.layer addSublayer:annotationsLayer];
        self.contentMode = UIViewContentModeScaleAspectFit;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)layoutSubviews{
    [super layoutSubviews];
    annotationsLayer.frame = self.bounds;
}
@end
