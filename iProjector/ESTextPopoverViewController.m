//
//  ESAddTextPopoverViewController.m
//  iProjector
//
//  Created by Jayaprada Behera on 12/10/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESTextPopoverViewController.h"

#define PENCIL_COLOR_RED 0
#define PENCIL_COLOR_BLUE 1
#define PENCIL_COLOR_GREEN 2
#define PENCIL_COLOR_YELLOW 3
#define PENCIL_COLOR_BLACK 4

//#define FONT_INCREASE_BUTTON_TAG  101
//#define FONT_DECREASE_BUTTON_TAG  102


#define Packect_Frame_size_iPhone    200.f
#define Packect_Frame_size_iPad      320.f
#define Place_holder_text            @"Enter your text"

@interface ESRadioButtonGroup : UIView

@property(nonatomic) NSInteger selectedIndex;

@end

@interface ESTextPopoverViewController ()<UITextViewDelegate>{
    
    UITableViewCell *textViewCell;
    UITableViewCell *deleteCell;
    UITableViewCell *textColorCell;
    UITableViewCell *fontCell;
    
    UITextView *editTextView;
    
    ESRadioButtonGroup *colorView;
    
    UITableView *textTableView;
}

@end

@implementation ESTextPopoverViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        _fontSize = 14.f;
    }
    return self;
}

-(void)loadMyTableView{
    
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done " style:UIBarButtonItemStyleDone target:self action:@selector(popOverDoneButtonTapped:)];

    CGRect tableViewFrame;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        
        UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,44)];
        toolBar.barStyle = UIBarStyleBlack;
        [toolBar setItems:[NSArray arrayWithObject:doneButton]];
        [self.view addSubview:toolBar];
        tableViewFrame = CGRectMake(0, toolBar.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    }else{
        tableViewFrame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
        self.navigationItem.leftBarButtonItem = doneButton;
    }

    textTableView  = [[UITableView alloc]initWithFrame:tableViewFrame style:UITableViewStyleGrouped];
    textTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin;
    [self.view addSubview: textTableView];
    textTableView.delegate = self;
    textTableView.dataSource = self;
    
    //UITableViewCells
    textColorCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    textColorCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [textColorCell.contentView addSubview:[self textColorView]];
    
    fontCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    fontCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [fontCell.contentView addSubview:[self fontButton]];
    
    deleteCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    deleteCell.textLabel.text = @"DELETE";
    deleteCell.textLabel.textColor = [UIColor redColor];
    deleteCell.textLabel.textAlignment = NSTextAlignmentCenter;
    deleteCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    textViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    textViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    editTextView = [[UITextView alloc] initWithFrame:CGRectInset(textViewCell.contentView.bounds, 5, 5) ];
    editTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    editTextView.backgroundColor = [UIColor clearColor];
    if (self.text.length>0) {
        
        editTextView.text = self.text;
        editTextView.font = [UIFont systemFontOfSize:_fontSize];
        editTextView.textColor = self.textColor;
    }else{
        editTextView.text =Place_holder_text;
        editTextView.font = [UIFont systemFontOfSize:14.f];
        editTextView.textColor = [UIColor grayColor];
    }
    editTextView.delegate = self;

    UIToolbar *accessoryToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 30)];
    [accessoryToolbar setItems:[NSArray arrayWithObjects:
                                [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Down_arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(resignEditTextView:)],
                                [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]
                                ,nil]];
    accessoryToolbar.tintColor = [UIColor colorWithRed:115/259.f green:124/259.f blue:137/259.f alpha:1.0 ];//115,124,137
    editTextView.inputAccessoryView = accessoryToolbar;
    
    textViewCell.contentView.backgroundColor = [UIColor clearColor];
    [textViewCell.contentView addSubview:editTextView];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadMyTableView];

}
-(IBAction)resignEditTextView:(id)sender{
    
    [editTextView resignFirstResponder];
    
}
-(CGRect)addFrame{
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    BOOL isPotrait = NO;
    BOOL iPad = NO;
    
#ifdef UI_USER_INTERFACE_IDIOM
    iPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#endif
    
#ifdef UIDeviceOrientationIsPortrait
    isPotrait = (UIDeviceOrientationIsPortrait(orientation) == UIDeviceOrientationPortrait ||UIDeviceOrientationIsPortrait(orientation) == UIDeviceOrientationPortraitUpsideDown);
#endif
    CGFloat iPadWidth = 0.f;
    CGFloat iPadHeight = 0.f;
    CGFloat iPhoneWidth = 0.f;
    CGFloat iPhoneHeight = 0.f;

    if(iPad){
//        if (isPotrait) {
//            iPadWidth=576.f;
//            iPadHeight = 1048.f;
//        }else{
            iPadWidth=1048.f;
            iPadHeight = 576.f;

//        }
        if (self.fromPdf) {

        return CGRectMake((iPadWidth - Packect_Frame_size_iPad)/2 - 60, (iPadHeight- Packect_Frame_size_iPad)-60, Packect_Frame_size_iPhone,Packect_Frame_size_iPhone);
        }else{
            return CGRectMake((iPadWidth - Packect_Frame_size_iPad)/2 , (iPadHeight- Packect_Frame_size_iPad)+90, Packect_Frame_size_iPhone,Packect_Frame_size_iPhone);
            
        }
    }else{
        if (isPotrait) {
             iPhoneWidth = 320.f;
             iPhoneHeight = 480.f;

        }else{
             iPhoneWidth = 480.f;
             iPhoneHeight = 320.f;

        }
        if (self.fromPdf) {
            
            return CGRectMake((iPhoneWidth - Packect_Frame_size_iPhone)+130, (iPhoneHeight)/2, Packect_Frame_size_iPhone,Packect_Frame_size_iPhone);
        }else{
            return CGRectMake((iPhoneWidth - Packect_Frame_size_iPhone)-50, (iPhoneHeight)/2, Packect_Frame_size_iPhone,Packect_Frame_size_iPhone);

        }
    }
    
    
}
-(IBAction)popOverDoneButtonTapped:(id)sender{
    
//transfer all the value to label
    
    CGSize textSize = [editTextView.text sizeWithFont:[UIFont systemFontOfSize:_fontSize] constrainedToSize:CGSizeMake(320, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    CGRect frame = CGRectMake([self addFrame].origin.x ,[self addFrame].origin.y, textSize.width, textSize.height);
    
    ESTextPacket *packet;
    if (editTextView.text.length>0) {
        packet = [ESTextPacket packetWithString:editTextView.text textColor:[self selectedColor] andTextFont:_fontSize withTextState:ESTextStateAddTextDone andWithIndex:_indexOfText andFrame:frame];
    }else {
        packet = [ESTextPacket packetWithString:editTextView.text textColor:[self selectedColor] andTextFont:_fontSize withTextState:ESTextStateRemoveText andWithIndex:_indexOfText andFrame:frame];
    }
    
    if (_delegate) {
        [_delegate doneWithTextPopover:self withPacket:packet];
    }
    
    if (! UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return TOTAL_SECTION_NUMBER;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SECTION_EDIT_TEXT) {
        return  textViewCell;
    }else if (indexPath.section == SECTION_FONT_SIZE){
        return fontCell;
    }else if ( indexPath.section == SECTION_COLOR){
        return textColorCell;
    }else{
        return deleteCell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == SECTION_EDIT_TEXT) {
        return 120.f;
    }
    return 44.f;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == SECTION_DELETE) {
        
        ESTextPacket *packet = [ESTextPacket removeTextWithState:ESTextStateRemoveText withIndex:_indexOfText];
        
        if (_delegate) {
            [_delegate deleteTextFromPopover:self withPacket:packet];
        }
        
        if (! UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){

            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
   
    if (section == SECTION_FONT_SIZE) {
        return @"Font Size";
    }else if (section == SECTION_COLOR){
        return @"Text Color";
    }else{
        return nil;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    if (section == SECTION_FONT_SIZE || section == SECTION_COLOR) {
        return 20.f;
    }
    return 1.0f;
}

-(UIView *) fontButton{

    UIView *buttonContentView = [[UIButton alloc]initWithFrame:fontCell.frame];
    
    UIButton *fontIncreaseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [fontIncreaseButton setTitle:@"A+" forState:UIControlStateNormal];
    [buttonContentView addSubview:fontIncreaseButton];
    
    UIButton *fontDecreaseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [fontDecreaseButton setTitle:@"A-" forState:UIControlStateNormal];
    [buttonContentView addSubview:fontDecreaseButton];
    
    fontDecreaseButton.frame = CGRectMake(0, 0, 44, 44);
    fontIncreaseButton.frame = CGRectMake(75, 0, 44, 44);
    
    [fontDecreaseButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [fontIncreaseButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [fontDecreaseButton addTarget:self action:@selector(increaseFontButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    [fontIncreaseButton addTarget:self action:@selector(decreaseFontButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    return buttonContentView;
}
-(IBAction)decreaseFontButtonTouchUpInside:(id)sender{
    if (_fontSize == MAXIMUM_FONT_VALUE) {
        return;
    }
    _fontSize++;
    [self fontSizeChangeFromTextPopoverWithPacket];

}
-(IBAction)increaseFontButtonTouchUpInside:(id)sender{
   
    if (_fontSize == MINIMUM_FONT_VALUE ) {
        
        return;
    }
    _fontSize--;
    
    [self fontSizeChangeFromTextPopoverWithPacket];
    
}
-(void)fontSizeChangeFromTextPopoverWithPacket{

    editTextView.font = [UIFont systemFontOfSize:_fontSize];
    
    ESTextPacket *packet  = [ESTextPacket packetWithTextFont:_fontSize withState:ESTextStateTextFontChange withIndex:_indexOfText];
    if (_delegate) {
        [_delegate fontSizeChangeFromPopover:self withPacket:packet];
    }
    
}
-(UIView *) textColorView{

    colorView = [[ESRadioButtonGroup alloc]initWithFrame:textColorCell.frame];
    
    NSArray * colorButtonImages = [NSArray arrayWithObjects:
                                   @"red",
                                   @"blue",
                                   @"green",
                                   @"khakhi",
                                   @"darkgrey", nil];
    
    //add drawing pencils to pencil view
    UIButton *colorbutton;
    for (int i=0; i<5; i++) {
        
        colorbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        [colorbutton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_unselected",colorButtonImages[i]]] forState:UIControlStateNormal];
        [colorbutton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",colorButtonImages[i]]] forState:UIControlStateSelected];
        [colorView addSubview:colorbutton];
        [colorbutton addTarget:self action:@selector(colorButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
    }
    //check the text color

    if (self.textColor) {
        
        if (CGColorEqualToColor(self.textColor.CGColor,[UIColor redColor].CGColor))
        {
            colorView.selectedIndex = PENCIL_COLOR_RED;
        }else if (CGColorEqualToColor(self.textColor.CGColor,[UIColor greenColor].CGColor))
        {
            colorView.selectedIndex = PENCIL_COLOR_GREEN;
        }else if (CGColorEqualToColor(self.textColor.CGColor,[UIColor yellowColor].CGColor))
        {
            colorView.selectedIndex = PENCIL_COLOR_YELLOW;
        }else if (CGColorEqualToColor(self.textColor.CGColor,[UIColor blueColor].CGColor))
        {
            colorView.selectedIndex = PENCIL_COLOR_BLUE;
        }else
        {
            colorView.selectedIndex = PENCIL_COLOR_BLACK;
        }
    }else{
        colorView.selectedIndex = PENCIL_COLOR_BLACK;
    }
    return colorView;
}
- (void) colorButtonSelected:(id) sender {
    
    UIButton *button = sender;
    colorView.selectedIndex = [colorView.subviews indexOfObject:button];
    editTextView.textColor = [self selectedColor];
//    ESTextPacket *packet = [ESTextPacket packetWithTextColor:[self selectedColor] withIndex:_indexOfText];
    ESTextPacket *packet = [ESTextPacket packetWithTextColor:[self selectedColor] withState:ESTextStateTextColorChange withIndex:_indexOfText];
    if (_delegate) {
        [_delegate textColorChangeFromPopover:self withPacket:packet];
    }
}

- (UIColor *) selectedColor {
    
    switch (colorView.selectedIndex) {
            
        case PENCIL_COLOR_RED:
            return [UIColor redColor];
        case PENCIL_COLOR_BLUE:
            return [UIColor blueColor];
        case PENCIL_COLOR_GREEN:
            return [UIColor greenColor];
        case PENCIL_COLOR_YELLOW:
            return [UIColor yellowColor];
        default:
        case PENCIL_COLOR_BLACK:
            return [UIColor blackColor];
    }
    return nil;
}

#pragma
#pragma mark - UITEXTVIEW DELEGATE

-(void) textViewDidChange:(UITextView *)textView{

    ESTextPacket *packet = [ESTextPacket packetWithText:textView.text withState:ESTextStateTextChange withIndex:_indexOfText];
    if (_delegate) {
        [_delegate textChangeFromPopover:self withPacket:packet];
    }
}
-(void)textViewDidBeginEditing:(UITextView *)textView{

    if ([textView.text isEqualToString:Place_holder_text]&& CGColorEqualToColor(textView.textColor.CGColor, [UIColor grayColor].CGColor)) {
        
        textView.text = @"";
        textView.textColor = [self selectedColor];
    }
    [textView becomeFirstResponder];
}
@end

@implementation ESRadioButtonGroup

- (void) addSubview:(UIView *)view {
    
    NSAssert([view isMemberOfClass:[UIButton class]], @"Only buttons can be added to the Radio ");
    UIButton *button = (UIButton *) view;
    [super addSubview:button];
}


- (void) setSelectedIndex:(NSInteger) selectedIndex {
    _selectedIndex = selectedIndex;
    //deselect all child buttons
    for (UIButton *childButton in self.subviews) {
        childButton.selected = NO;
    }
    [(UIButton *)[self.subviews objectAtIndex:selectedIndex] setSelected:YES];
}

//layout subviews one beside the other.

- (void) layoutSubviews {
    if (self.subviews.count == 0) {
        return;
    }
    //margin between each child should be as atleast one third of their widths.
    //i.e. widthOfChild = widthOfSelf/((4n - 1)/3)
    
    int widthOfChild = self.bounds.size.width * 3/(5 *self.subviews.count - 1);
    for (int i=0; i<self.subviews.count; i++) {
        [self.subviews[i] setFrame:CGRectMake(i*widthOfChild * 4/3, 0, widthOfChild, self.bounds.size.height)];
    }
}

@end
