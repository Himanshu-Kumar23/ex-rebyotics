//
//  ESAppDelegate.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 6/27/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MessageUI/MessageUI.h>


@interface ESAppDelegate : UIResponder <UIApplicationDelegate,UIAlertViewDelegate,MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *role;
@property (strong, nonatomic) NSString *sessionState;
//@property (strong, nonatomic) ESDocumentsListViewController *documentListVC;

@end
