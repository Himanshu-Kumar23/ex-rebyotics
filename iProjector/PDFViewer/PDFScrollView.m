//    File: PDFScrollView.m
//Abstract: UIScrollView subclass that handles the user input to zoom the PDF page.  This class handles swapping the TiledPDFViews when the zoom level changes.
// Version: 1.0
//
//Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
//Inc. ("Apple") in consideration of your agreement to the following
//terms, and your use, installation, modification or redistribution of
//this Apple software constitutes acceptance of these terms.  If you do
//not agree with these terms, please do not use, install, modify or
//redistribute this Apple software.
//
//In consideration of your agreement to abide by the following terms, and
//subject to these terms, Apple grants you a personal, non-exclusive
//license, under Apple's copyrights in this original Apple software (the
//"Apple Software"), to use, reproduce, modify and redistribute the Apple
//Software, with or without modifications, in source and/or binary forms;
//provided that if you redistribute the Apple Software in its entirety and
//without modifications, you must retain this notice and the following
//text and disclaimers in all such redistributions of the Apple Software.
//Neither the name, trademarks, service marks or logos of Apple Inc. may
//be used to endorse or promote products derived from the Apple Software
//without specific prior written permission from Apple.  Except as
//expressly stated in this notice, no other rights or licenses, express or
//implied, are granted by Apple herein, including but not limited to any
//patent rights that may be infringed by your derivative works or by other
//works in which the Apple Software may be incorporated.
//
//The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
//MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
//THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
//FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
//OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
//
//IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
//OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
//MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
//AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
//STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
//POSSIBILITY OF SUCH DAMAGE.
//
//Copyright (C) 2010 Apple Inc. All Rights Reserved.
//

#import "PDFScrollView.h"
#import "TiledPDFView.h"
#import <QuartzCore/QuartzCore.h>

#define PLACEHOLDER_IMAGE_TAG 0x1FFF
@implementation PDFScrollView{
    CGFloat currentPosition;
    CGFloat prevPosition;
    NSTimeInterval currentTime;
    NSTimeInterval prevTime;
    BOOL monitorVelocity;
    CGPoint locationViewPoint;
}

@synthesize containerView;
@synthesize pdfScale;

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
		
		// Set up the UIScrollView
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.bouncesZoom = YES;
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        self.delegate = self;
		[self setBackgroundColor:[UIColor grayColor]];
		self.maximumZoomScale = 5.0;
		self.minimumZoomScale = .5;
        self.canCancelContentTouches = NO;
        self.pagingEnabled =YES;
    }
    return self;
}

-(void) setPDFURL:(NSURL *) pdfURL {
    
    // Open the PDF document
    pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfURL);
    NSInteger pageCount = CGPDFDocumentGetNumberOfPages(pdf);
    
    // Get the PDF Page that we will be drawing
    CGPDFPageRef firstPage = CGPDFDocumentGetPage(pdf, 1);
    //		CGPDFPageRetain(page);
    
    // determine the size of the PDF page
    CGRect pageRect = CGPDFPageGetBoxRect(firstPage, kCGPDFMediaBox);
    NSLog(@"PageRect size:%@",NSStringFromCGRect(pageRect));

    pdfScale = self.frame.size.width/pageRect.size.width;
//    pageRect.size = CGSizeMake(pageRect.size.width*pdfScale, pageRect.size.height*pdfScale);
    
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, pageRect.size.width*pageCount, pageRect.size.height+80)];
    self.contentSize = containerView.bounds.size;
    
    [self addSubview:containerView];		

    
        
    // Create the TiledPDFView based on the size of the PDF page and scale it to fit the view.
    for (int i=0; i<pageCount; i++) {
        CGPDFPageRef page = CGPDFDocumentGetPage(pdf, i+1);
        CGRect pageFrame = CGRectMake(i*pageRect.size.width, 0, pageRect.size.width, pageRect.size.height);
        pageFrame = CGRectInset(pageFrame, 4, 4);
        TiledPDFView *pdfView = [[TiledPDFView alloc] initWithFrame:pageFrame andScale:1.0];
        [pdfView setPage:page];
        
//        Add  a placeHolder Image before PDF creation
        UIGraphicsBeginImageContext(pageRect.size); 
        CGContextRef context = UIGraphicsGetCurrentContext();
        [pdfView drawLayer:nil inContext:context];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        [imageView setTag:PLACEHOLDER_IMAGE_TAG];
        [imageView setFrame:pageFrame];
        [imageView setUserInteractionEnabled:NO];
        [containerView addSubview:imageView];
        [imageView release];
        UIGraphicsEndImageContext();
//        [containerView sendSubviewToBack:imageView];

        [containerView addSubview:pdfView];            
        [pdfView release];
    }
    self.zoomScale = pdfScale;
}

//- (UIImage *) getImageForPage:(int) page {
    
//}
-(void) addElement:(UIView *) elem {
//    elem.transform = CGAffineTransformMakeScale(0.5, 0.5);
    [containerView addSubview:elem];
    elementView = [elem retain];
}

- (void)dealloc
{
	// Clean up
//    [pdfView release];
//	[backgroundImageView release];
    [containerView release];
//	CGPDFPageRelease(page);
	CGPDFDocumentRelease(pdf);
    [super dealloc];
}

#pragma mark -
#pragma mark Override layoutSubviews to center content

// We use layoutSubviews to center the PDF page in the view
- (void)layoutSubviews 
{
    [super layoutSubviews];
    
    // center the image as it becomes smaller than the size of the screen
	
    CGSize boundsSize = self.bounds.size;
    CGRect frameToCenter = containerView.frame;    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width)
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    else
        frameToCenter.origin.x = 0;
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height)
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    else
        frameToCenter.origin.y = 0;
    
    containerView.frame = frameToCenter;
//	backgroundImageView.frame = frameToCenter;
    
	// to handle the interaction between CATiledLayer and high resolution screens, we need to manually set the
	// tiling view's contentScaleFactor to 1.0. (If we omitted this, it would be 2.0 on high resolution screens,
	// which would cause the CATiledLayer to ask us for tiles of the wrong scales.)
	containerView.contentScaleFactor = 1.0;
}

#pragma mark -
#pragma mark UIScrollView delegate methods

// A UIScrollView delegate callback, called when the user starts zooming. 
// We return our current TiledPDFView.
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return containerView;
}


// A UIScrollView delegate callback, called when the user stops zooming.  When the user stops zooming
// we create a new TiledPDFView based on the new zoom level and draw it on top of the old TiledPDFView.
//- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
//{
	// set the new scale factor for the TiledPDFView
//	pdfScale *=scale;
	
	// Calculate the new frame for the new TiledPDFView
//	CGRect pageRect = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
//	pageRect.size = CGSizeMake(pageRect.size.width*pdfScale, pageRect.size.height*pdfScale);
//	containerView.frame = pageRect;
	// Create a new TiledPDFView based on new frame and scaling.
//	pdfView = [[TiledPDFView alloc] initWithFrame:containerView.bounds andScale:pdfScale];
//	[pdfView setPage:page];
	
	// Add the new TiledPDFView to the PDFScrollView.
//	[containerView addSubview:pdfView];
//    [containerView bringSubviewToFront:elementView];
//}

// A UIScrollView delegate callback, called when the user begins zooming.  When the user begins zooming
// we remove the old TiledPDFView and set the current TiledPDFView to be the old view so we can create a
// a new TiledPDFView when the zooming ends.
//- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
//{
	// Remove back tiled view.
//	[oldPDFView removeFromSuperview];
//	[oldPDFView release];
	
	// Set the current TiledPDFView to be the old view.
//	oldPDFView = pdfView;
//	[containerView addSubview:oldPDFView];
//    [containerView bringSubviewToFront:elementView];
//}

#pragma mark Generating PDF views.
-(void) savePDF:(NSArray *)containerViews
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pdfFileName = [documentsDirectory stringByAppendingPathComponent:@"GenPDF.pdf"];
    
    NSLog(@"File path: %@",pdfFileName);
    // Create the PDF context using the default page size of 612 x 792.
    BOOL result = UIGraphicsBeginPDFContextToFile(pdfFileName, CGRectZero, nil);
    CGContextRef context = UIGraphicsGetCurrentContext();
//	CGContextSaveGState(context);

    NSLog(@"created pdf file: %@",result?@"Yes":@"NO");
    NSInteger pageCount = CGPDFDocumentGetNumberOfPages(pdf);

    for (int i=0; i<pageCount; i++) {
        CGContextSaveGState(context);
        CGPDFPageRef page = CGPDFDocumentGetPage(pdf, i+1);
        CGRect pageRect = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
//        CGRect cropBox = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
//        CGRect bleedBox = CGPDFPageGetBoxRect(page, kCGPDFBleedBox);
//        CGRect trimBox = CGPDFPageGetBoxRect(page, kCGPDFTrimBox);
//        CGRect artBox = CGPDFPageGetBoxRect(page, kCGPDFArtBox);
//        CGFloat leftMargin = CGRectGetMinX(cropBox) - CGRectGetMinX(pageRect);
//        CGFloat topMargin = CGRectGetMinY(cropBox) - CGRectGetMinY(pageRect);
//        CGFloat leftMargin = 0;
        UIGraphicsBeginPDFPageWithInfo(pageRect, nil);
        CGContextSetRGBFillColor(context, 1.0,1.0,1.0,1.0);
        CGContextFillRect(context,self.bounds);
        CGContextTranslateCTM(context, 0.0, pageRect.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextDrawPDFPage(context, page);
        CGContextRestoreGState(context);

//        CGContextScaleCTM(context, 1.0, -1.0);
        //draw shapes belonging to that page.
        for (UIView *overlay in containerViews) {
//            CGPoint center = overlay.center;

            CGPoint center = CGPointMake(overlay.center.x-2, overlay.center.y - i* pageRect.size.height);
//                       CGPoint center = CGPointMake(overlay.center.x-2, overlay.center.y - i* pageRect.size.height - 2);
            NSLog(@"Screen height is %f and overlay center is %f", pageCount*pageRect.size.height, center.y);
            if (center.y < 0 || center.y > pageCount*pageRect.size.height) {
                continue;
            }
             // so we must first apply the layer's geometry to the graphics context
             CGContextSaveGState(context);


            NSLog(@"overlay center: %@",NSStringFromCGPoint(overlay.center));
             // Center the context around the window's anchor point
             CGContextTranslateCTM(context, center.x- overlay.bounds.size.width, pageRect.size.height - center.y);
//            CGContextTranslateCTM(context, center.x/pdfScale - overlay.bounds.size.width - 210, pageRect.size.height + 167 - center.y/pdfScale );
            CGAffineTransform transform_ = CGContextGetCTM(context);
            NSLog(@"transform :%@",NSStringFromCGAffineTransform(transform_));

             // Apply the window's transform about the anchor point
             CGContextConcatCTM(context, [overlay transform]);
             // Offset by the portion of the bounds left of and above the anchor point
             CGContextTranslateCTM(context,
             [overlay bounds].size.width * [[overlay layer] anchorPoint].x,
             [overlay bounds].size.height * [[overlay layer] anchorPoint].y);

//            CGContextScaleCTM(context, 1.0/pdfScale, -1.0/pdfScale);
            CGContextScaleCTM(context, 1.0, -1.0);
            NSLog(@"before render transform :%@",NSStringFromCGAffineTransform(CGContextGetCTM(context)));

             // Render the layer hierarchy to the current context
             [[overlay layer] renderInContext:context];
             
             // Restore the context
             CGContextRestoreGState(context);
        }
//        CGContextEndPage(context);
    }
    
    UIGraphicsEndPDFContext();            
}

- (void)drawPageNumber:(NSInteger)pageNum
{
    NSString* pageString = [NSString stringWithFormat:@"Page %d", pageNum];
    UIFont* theFont = [UIFont systemFontOfSize:12];
    CGSize maxSize = CGSizeMake(612, 72);
    
    CGSize pageStringSize = [pageString sizeWithFont:theFont
                                   constrainedToSize:maxSize
                                       lineBreakMode:UILineBreakModeClip];
    CGRect stringRect = CGRectMake(((612.0 - pageStringSize.width) / 2.0),
                                   self.frame.size.height - 20 + ((72.0 - pageStringSize.height) / 2.0) ,
                                   pageStringSize.width,
                                   pageStringSize.height);
    
    [pageString drawInRect:stringRect withFont:theFont];
}

//-(void) scrollViewWillBeginDragging:(UIScrollView *)scrollView  {
//    CATransition *animation = [CATransition animation];
//    [animation setDelegate:self];
//    [animation setDuration:1.0f];
//    animation.startProgress = 0;
//    animation.endProgress   = 1;
//    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//    animation.type = @"pageCurl";
//    animation.subtype=@"fromRight";
//    animation.fillMode = kCAFillModeForwards;
//    
//    [animation setRemovedOnCompletion:NO];
//    [animation setFillMode: @"extended"];
//    [animation setRemovedOnCompletion: NO];
//    [self.layer addAnimation:animation forKey:@"WebPageCurl"];

//}
//-(void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
//    
//}

- (UIPanGestureRecognizer *) defaultPanGestureRecognizer {
    for (UIGestureRecognizer *gestureRecognizer in self.gestureRecognizers) {
        if ([gestureRecognizer isMemberOfClass:[UIPanGestureRecognizer class]]) {
            return (UIPanGestureRecognizer *)gestureRecognizer;
        }
    } 
    return nil;
}
#pragma UITouchDelegate

//user for get the touch point when user touches began

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    UITouch *touch = [[event allTouches] anyObject];
    locationViewPoint =  [touch locationInView:self.superview];
    //convert CGpoint to NSValue
    NSValue *val = [NSValue valueWithCGPoint:locationViewPoint];
    //notification for passing CGpoint value to require controller
    [[NSNotificationCenter defaultCenter] postNotificationName:GET_LOCATIONPOINT_USER_TOUCH_BEGAN_WITH_NOTIFICATION object:val];
     [super touchesBegan:touches withEvent:event];
    
}
//user for get the touch point when user touches moves
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
       UITouch *touch = [[event allTouches] anyObject];
    locationViewPoint = [touch locationInView:self.superview];
    NSValue *val = [NSValue valueWithCGPoint:locationViewPoint];
    [[NSNotificationCenter defaultCenter] postNotificationName:GET_LOCATIONPOINT_USER_TOUCH_MOVE_WITH_NOTIFICATION object:val];
    [super touchesMoved:touches withEvent:event];
    
}
//user for get the touch point when user touches Ends
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[NSNotificationCenter defaultCenter] postNotificationName:GET_LOCATIONPOINT_USER_TOUCH_END_WITH_NOTIFICATION object:nil];
    
    [super touchesEnded:touches withEvent:event];
}

@end
