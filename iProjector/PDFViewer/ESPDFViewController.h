//
//  ESPDFViewController.h
//  iProjector
//
//  Created by Katragadda, Manoj (external - Project) on 7/2/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDFScrollView.h"
#import "BKSessionController.h"

@class TiledPDFView;

@interface ESPDFViewController : UIViewController<UIScrollViewDelegate>

@property (nonatomic, strong) PDFScrollView *sv;
@property (nonatomic,strong) NSURL *pdfURL;
@property (nonatomic, retain) BKSessionController *sessionController;

-(id) initWithPDFURL:(NSURL *) url ;
@end
