//
//  ESReaderSliderPageBar.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 7/10/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderDocument.h"
#import "ESAbstractPacket.h"
//#import "ESPageSliderPacket.h"

@class ESReaderSliderPageBar;
@protocol ESReaderSliderPageBarDelegate <NSObject>

@required // Delegate protocols

- (void)pagebar:(ESReaderSliderPageBar *)pagebar gotoPage:(NSInteger)page;
- (void) transferPacket:(ESAbstractPacket *)packet;
@end

@interface ESReaderSliderPageBar : UIView

@property (nonatomic, unsafe_unretained, readwrite) id <ESReaderSliderPageBarDelegate> delegate;
@property(nonatomic,strong) UISlider *trackControl;
- (id)initWithFrame:(CGRect)frame document:(ReaderDocument *)object;

- (void)updatePagebar;

- (void) showPagebar;
- (void) hidePagebar;

@end
