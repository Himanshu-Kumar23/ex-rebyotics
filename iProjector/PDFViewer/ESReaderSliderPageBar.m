//
//  ESReaderSliderPageBar.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 7/10/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESReaderSliderPageBar.h"
#import <QuartzCore/QuartzCore.h>

#define PAGE_NUMBER_WIDTH 96.0f
#define PAGE_NUMBER_HEIGHT 30.0f
#define PAGE_NUMBER_SPACE 20.0f

@interface ESReaderSliderPageBar () {
    UILabel *pageNumberLabel;
    
    ReaderDocument *document;
    
	UIView *pageNumberView;
    
    
    NSTimer *trackTimer;
    NSTimer *enableTimer;
}

@end

@implementation ESReaderSliderPageBar
@synthesize trackControl;

+ (Class)layerClass{
    
	return [CAGradientLayer class];
}

- (id)initWithFrame:(CGRect)frame document:(ReaderDocument *)object {
    self = [super initWithFrame:frame];
    if (self) {
        document = object;
        [self _init];
    }
    return self;
}

- (void) _init {
    
    CAGradientLayer *layer = (CAGradientLayer *)self.layer;
    UIColor *liteColor = [UIColor colorWithWhite:0.82f alpha:0.8f];
    UIColor *darkColor = [UIColor colorWithWhite:0.03f alpha:0.8f];
    layer.colors = [NSArray arrayWithObjects:(id)liteColor.CGColor, (id)darkColor.CGColor, nil];

    self.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;

    CGFloat numberY = (0.0f - (PAGE_NUMBER_HEIGHT + PAGE_NUMBER_SPACE));
    CGFloat numberX = ((self.bounds.size.width - PAGE_NUMBER_WIDTH) / 2.0f);
    CGRect numberRect = CGRectMake(numberX, numberY, PAGE_NUMBER_WIDTH, PAGE_NUMBER_HEIGHT);

    trackControl = [[UISlider alloc] initWithFrame:CGRectMake(self.bounds.size.width/8, self.bounds.size.height/2 - 10, self.bounds.size.width * 3/4, 20)];
    trackControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [trackControl addTarget:self action:@selector(trackViewTouchDown:) forControlEvents:UIControlEventTouchDown];
    [trackControl addTarget:self action:@selector(trackViewValueChanged:) forControlEvents:UIControlEventValueChanged];
    [trackControl addTarget:self action:@selector(trackViewTouchUp:) forControlEvents:UIControlEventTouchUpOutside];
    [trackControl addTarget:self action:@selector(trackViewTouchUp:) forControlEvents:UIControlEventTouchUpInside];

    [self addSubview:trackControl];
    
    pageNumberView = [[UIView alloc] initWithFrame:numberRect]; // Page numbers view
    
    pageNumberView.autoresizesSubviews = NO;
    pageNumberView.userInteractionEnabled = NO;
    pageNumberView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    pageNumberView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.4f];
    
    pageNumberView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    pageNumberView.layer.shadowColor = [UIColor colorWithWhite:0.0f alpha:0.6f].CGColor;
    pageNumberView.layer.shadowPath = [UIBezierPath bezierPathWithRect:pageNumberView.bounds].CGPath;
    pageNumberView.layer.shadowRadius = 2.0f; pageNumberView.layer.shadowOpacity = 1.0f;
    
    CGRect textRect = CGRectInset(pageNumberView.bounds, 4.0f, 2.0f); // Inset the text a bit
    
    pageNumberLabel = [[UILabel alloc] initWithFrame:textRect]; // Page numbers label
    
    pageNumberLabel.autoresizesSubviews = NO;
    pageNumberLabel.autoresizingMask = UIViewAutoresizingNone;
    pageNumberLabel.textAlignment = ESTextAlignmentCenter;
    pageNumberLabel.backgroundColor = [UIColor clearColor];
    pageNumberLabel.textColor = [UIColor whiteColor];
    pageNumberLabel.font = [UIFont systemFontOfSize:16.0f];
    pageNumberLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
    pageNumberLabel.shadowColor = [UIColor blackColor];
    pageNumberLabel.adjustsFontSizeToFitWidth = YES;
    pageNumberLabel.minimumFontSize = 12.0f;
    
    [pageNumberView addSubview:pageNumberLabel]; // Add label view
    
    [self addSubview:pageNumberView]; // Add page numbers display view
}

- (void)hidePagebar{
    
	if (self.hidden == NO){ // Only if visible
	
		[UIView animateWithDuration:0.25 delay:0.0
                            options:UIViewAnimationOptionCurveEaseIn | UIViewAnimationOptionAllowUserInteraction
                         animations:^(void){
             self.alpha = 0.0f;
             self.frame = CGRectOffset(self.frame, 0, self.frame.size.height);
         }
                         completion:^(BOOL finished){
             self.hidden = YES;
         }];
	}
}

- (void)showPagebar{
    
	if (self.hidden == YES){ // Only if hidden
	
		[self updatePagebarViews]; // Update views first
        
		[UIView animateWithDuration:0.25 delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionAllowUserInteraction
                         animations:^(void){
             self.hidden = NO;
             self.alpha = 1.0f;
             self.frame = CGRectOffset(self.frame, 0, -self.frame.size.height);
         }
                         completion:NULL
         ];
    }
}

- (void) updatePagebarViews {
	NSInteger page = [document.pageNumber integerValue]; // #
    
    [trackControl setValue:((float)page)/document.pageCount.integerValue animated:NO];
	[self updatePageNumberText:page]; // Update page number text
}

- (void)updatePagebar{
    
	if (self.hidden == NO){ // Only if visible
	
		[self updatePagebarViews]; // Update views
	}
}

- (void)updatePageNumberText:(NSInteger)page{
    
	if (page != pageNumberLabel.tag){ // Only if page number changed
	
		NSInteger pages = [document.pageCount integerValue]; // Total pages
        
		NSString *format = NSLocalizedString(@"%d of %d", @"format"); // Format
        if (page>pages){
            page = pages;
        }
        
		NSString *number = [NSString stringWithFormat:format, page, pages]; // Text
        
		pageNumberLabel.text = number; // Update the page number label text
        
		pageNumberLabel.tag = page; // Update the last page number tag
	}
}

- (NSInteger)trackViewPageNumber:(UISlider *)trackView{
    
	CGFloat controlWidth = trackView.bounds.size.width; // View width
    
	CGFloat stride = (controlWidth / [document.pageCount integerValue]);
    
	NSInteger page = (trackView.value * controlWidth / stride); // Integer page number
    
	return (page + 1); // + 1
}

- (void)trackTimerFired:(NSTimer *)timer{
    
	[trackTimer invalidate]; trackTimer = nil; // Cleanup timer
    
	if (trackControl.tag != [document.pageNumber integerValue]){ // Only if different
		[self.delegate pagebar:self gotoPage:trackControl.tag]; // Go to document page
	}
}

- (void)restartTrackTimer{
    
	if (trackTimer != nil) { [trackTimer invalidate]; trackTimer = nil; } // Invalidate and release previous timer
    
	trackTimer = [NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(trackTimerFired:) userInfo:nil repeats:NO];
}

- (void)enableTimerFired:(NSTimer *)timer{
    
	[enableTimer invalidate]; enableTimer = nil; // Cleanup timer
    
	trackControl.userInteractionEnabled = YES; // Enable track control interaction
}

- (void)startEnableTimer{
    
	if (enableTimer != nil) { [enableTimer invalidate]; enableTimer = nil; } // Invalidate and release previous timer
    
	enableTimer = [NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(enableTimerFired:) userInfo:nil repeats:NO];
}

- (void)trackViewTouchDown:(UISlider *)trackView
{
	NSInteger page = [self trackViewPageNumber:trackView]; // Page
    
	if (page != [document.pageNumber integerValue]){ // Only if different
	
		[self updatePageNumberText:page]; // Update page number text
        
		[self restartTrackTimer]; // Start the track timer
	}
    
	trackView.tag = page; // Start page tracking
}

- (void)trackViewValueChanged:(UISlider *)trackView{
    
	NSInteger page = [self trackViewPageNumber:trackView]; // Page
    
	if (page != trackView.tag){ // Only if the page number has changed
	
		[self updatePageNumberText:page]; // Update page number text
                
		trackView.tag = page; // Update the page tracking tag
        
		[self restartTrackTimer]; // Restart the track timer
	}
//    ESPageSliderPacket *packet =[ESPageSliderPacket packectWithSliderPageValue:page];
//    [self.delegate transferPacket:packet];
    
//    NSString *pageObject = [NSString stringWithFormat:@"%d",page];
//     [[NSNotificationCenter defaultCenter] postNotificationName:@"PageControllerwithNotification" object:pageObject];
    
}

- (void)trackViewTouchUp:(UISlider *)trackView{
    
	[trackTimer invalidate]; trackTimer = nil; // Cleanup
    
	if (trackView.tag != [document.pageNumber integerValue]){ // Only if different
	
		trackView.userInteractionEnabled = NO; // Disable track control interaction
        
		[self.delegate pagebar:self gotoPage:trackView.tag]; // Go to document page
        
		[self startEnableTimer]; // Start track control enable timer
	}
    
	trackView.tag = 0; // Reset page tracking
}

@end
