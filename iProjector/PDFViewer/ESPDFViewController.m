//
//  ESPDFViewController.m
//  iProjector
//
//  Created by Katragadda, Manoj (external - Project) on 7/2/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESPDFViewController.h"
#import <QuartzCore/QuartzCore.h>


#define TOOLBAR_FRAME CGRectMake(0, self.view.bounds.size.height - 44, self.view.bounds.size.width, 44.f)
#define SLIDER_MIN_VALUE 0.0f
#define SLIDER_MAX_VALUE 100.0f
#define CURRENT_PAGE_LABEL_WIDTH 80.0f
#define SLIDER_WIDTH 300.0f
#define CURRENT_PAGE_LABEL_WIDTH 80.0f

typedef enum sendingDataHeaderTypes
{
    ksendingDataHeaderWhileScrolling,
    ksendingDataHeaderWhileZooming,
    ksendingDataHeaderWhileSliderMoving,
    ksendingDataHeaderWhileLaserPointMoving
} sendingDataHeaderType;

@interface ESPDFViewController ()
{
    UITapGestureRecognizer *tapGestureRecognizer;
    CGFloat currentPage;
    CGFloat defaultZoomScale;
    CGFloat totalpages;
    CGFloat scrollviewConentSizeWidth;

}

@property(nonatomic, strong) UIToolbar *toolbar;
@property(nonatomic, strong) UILabel *textLabel;
@property(nonatomic, strong) UILabel *totalPageLabel;
@property(nonatomic, strong) UIView *toolbarOverlay;
@property(nonatomic, strong) UISlider *pdfSlider;
@property(nonatomic,assign) NSInteger sendingDataHeaderType;
 


@end

@implementation ESPDFViewController
@synthesize pdfURL, sv;



#pragma LifeCycle Methods

-(id) initWithPDFURL:(NSURL *) url {
    if (self = [super init]) {
        pdfURL = url;
    }
    return self;
}
// Implement loadView to create a view hierarchy programmatically, without using a nib.

- (void)loadView
{
	[super loadView];
	// Create our PDFScrollView and add it to the view controller.
    self.sv = [[PDFScrollView alloc] initWithFrame:[[self view] bounds]];
    [sv setPagingEnabled:YES];
    sv.scrollEnabled=YES;
    self.sv = [[PDFScrollView alloc] initWithFrame:self.view.bounds];
    [self.sv setPDFURL:self.pdfURL];
    sv.delegate = self;
    [[self view] addSubview:self.sv];
    [self.view addSubview:self.sv];
    sv.delegate = self;
    defaultZoomScale = [sv zoomScale];
    [sv setContentOffset:CGPointMake(0, 0) animated:YES];
    UIBarButtonItem *cancelButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(doCancel:)];
    self.navigationItem.leftBarButtonItem = cancelButtonItem;
       //TODO: Add tap gesture to the container view.
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGestureRecognizer.numberOfTapsRequired = 2;
    tapGestureRecognizer.numberOfTouchesRequired =1;
    [sv.containerView addGestureRecognizer:tapGestureRecognizer];
   
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setupNotifications];
    scrollviewConentSizeWidth = sv.contentSize.width;
    scrollviewConentSizeWidth = sv.contentSize.width;
    [self setupToolBar];
    [self setHideToolBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

#pragma Notification Method

// for display laser pointer based on user touch scrollview.

-(void)receiveLocationNotification:(NSNotification *) aNotification
{
    [self removeSubViews:self.view];
    NSValue *val = [aNotification object];
    CGPoint locationViewPoint = [val CGPointValue];
    CGPoint scrollLocationPoint = self.view.bounds.origin;
    UIImageView *laserPointImageView = nil;
    if ([sv zoomScale]!= defaultZoomScale)
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
        {
            scrollLocationPoint.x = locationViewPoint.x;
            
            scrollLocationPoint.y = locationViewPoint.y-25;
            laserPointImageView = [[UIImageView alloc]initWithFrame:CGRectMake( scrollLocationPoint.x , scrollLocationPoint.y, 6, 6)];
        }
        else
        {
            scrollLocationPoint.x = locationViewPoint.x;
            
            scrollLocationPoint.y = locationViewPoint.y-25;
            laserPointImageView = [[UIImageView alloc]initWithFrame:CGRectMake( scrollLocationPoint.x , scrollLocationPoint.y, 8, 8)];
        }
               
    }
    else
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
        {
        
            scrollLocationPoint.x = locationViewPoint.x;
            scrollLocationPoint.y = locationViewPoint.y-10;
            laserPointImageView = [[UIImageView alloc]initWithFrame:CGRectMake( scrollLocationPoint.x , scrollLocationPoint.y, 4, 4)];
            
        }
        else
        {
            scrollLocationPoint.x = locationViewPoint.x;
            
            scrollLocationPoint.y = locationViewPoint.y-25;
            
            laserPointImageView = [[UIImageView alloc]initWithFrame:CGRectMake( scrollLocationPoint.x , scrollLocationPoint.y, 6, 6)];
        }
        
    }
    NSValue *val1 = [NSValue valueWithCGPoint:scrollLocationPoint];
    NSData *data = [self dataWithValue:val1];
    NSLog(@"data%@",data);
    [laserPointImageView setAlpha:0.3];
    [laserPointImageView setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:laserPointImageView];
    [self.view bringSubviewToFront:laserPointImageView];
     self.sendingDataHeaderType = ksendingDataHeaderWhileLaserPointMoving;
    //[self transferData:data];
    NSValue *resultValue = [self valueWithData:data];
    CGPoint resultPoint = [resultValue CGPointValue];
    NSLog(@"%f,%f",resultPoint.x,resultPoint.y);
    
}
-(void)receiveTouchMovesNotification:(NSNotification *) aNotification
{
    [self removeSubViews:self.view];
    NSValue *val = [aNotification object];
    CGPoint locationViewPoint = [val CGPointValue];
     CGPoint scrollLocationPoint = self.view.bounds.origin;
    UIImageView *laserPointImageView = nil;
    if ([sv zoomScale]!= defaultZoomScale)
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
        {
            scrollLocationPoint.x = locationViewPoint.x;
            
            scrollLocationPoint.y = locationViewPoint.y-25;
            laserPointImageView = [[UIImageView alloc]initWithFrame:CGRectMake( scrollLocationPoint.x , scrollLocationPoint.y, 6, 6)];
        }
        else
        {
            scrollLocationPoint.x = locationViewPoint.x;
            
            scrollLocationPoint.y = locationViewPoint.y-25;
            laserPointImageView = [[UIImageView alloc]initWithFrame:CGRectMake( scrollLocationPoint.x , scrollLocationPoint.y, 8, 8)];
        }
        
    }
    else
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
        {
            
            scrollLocationPoint.x = locationViewPoint.x;
            scrollLocationPoint.y = locationViewPoint.y-10;
            laserPointImageView = [[UIImageView alloc]initWithFrame:CGRectMake( scrollLocationPoint.x , scrollLocationPoint.y, 4, 4)];
            
        }
        else
        {
            scrollLocationPoint.x = locationViewPoint.x;
            
            scrollLocationPoint.y = locationViewPoint.y-25;
            
            laserPointImageView = [[UIImageView alloc]initWithFrame:CGRectMake( scrollLocationPoint.x , scrollLocationPoint.y, 6, 6)];
        }
        
    }
    
    [laserPointImageView setAlpha:0.3];
    [laserPointImageView setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:laserPointImageView];
    [self.view bringSubviewToFront:laserPointImageView];
    NSValue *val1 = [NSValue valueWithCGPoint:scrollLocationPoint];
    NSData *data = [self dataWithValue:val1];
    NSLog(@"data%@",data);
    self.sendingDataHeaderType = ksendingDataHeaderWhileLaserPointMoving;
    //[self transferData:data];

    
}
-(void)receiveTouchEndedNotification
{
    [self removeSubViews:self.view];
}


#pragma Private Methods

//for obserview the required notifications

-(void)setupNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLocationNotification:)
                                                 name:GET_LOCATIONPOINT_USER_TOUCH_BEGAN_WITH_NOTIFICATION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTouchEndedNotification)
                                                 name:GET_LOCATIONPOINT_USER_TOUCH_END_WITH_NOTIFICATION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTouchMovesNotification:)
                                                 name:GET_LOCATIONPOINT_USER_TOUCH_MOVE_WITH_NOTIFICATION
                                               object:nil];
    
}


//for setup toolbar as for our requirements
-(void) setupToolBar
{
    // toolbar bar setup
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:TOOLBAR_FRAME];
    toolBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:toolBar];
    self.toolbar = toolBar;
    //toolbar background view setup
    UIView *toolbarOverlay = [[UIView alloc] initWithFrame:toolBar.frame];
    toolbarOverlay.backgroundColor = [UIColor clearColor];
    toolbarOverlay.autoresizingMask = toolBar.autoresizingMask;
    [self.view addSubview:toolbarOverlay];
    self.toolbarOverlay = toolbarOverlay;
    //calculate the slider frame and setup the slidebar
    CGFloat sliderWidth = self.view.bounds.size.width/2;
    if (sliderWidth<SLIDER_WIDTH)
    {
        sliderWidth = SLIDER_WIDTH;
    }
    CGFloat sliderX = self.view.bounds.size.width-sliderWidth;
    self.pdfSlider = [[UISlider alloc] initWithFrame:CGRectMake(sliderX/2, 10.f, sliderWidth, 30.f)];
    [self.pdfSlider setMinimumValue:SLIDER_MIN_VALUE];
    [self.pdfSlider setMaximumValue:SLIDER_MAX_VALUE];
    [self.pdfSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [toolbarOverlay addSubview:self.pdfSlider];
       //to find the total pages in pdf
    totalpages = (sv.contentSize.width/self.view.bounds.size.width);
    CGFloat currentPageX = self.view.bounds.size.width-CURRENT_PAGE_LABEL_WIDTH;
    //calculate the currentpage label frame and setup the currentpage label
    UILabel * textLabel = [[UILabel alloc] initWithFrame:CGRectMake((currentPageX/2), self.view.bounds.size.height - 119, CURRENT_PAGE_LABEL_WIDTH, 33.f)];
    textLabel.backgroundColor = [UIColor blackColor];
    textLabel.textAlignment = UITextAlignmentCenter;
    textLabel.textColor = [UIColor whiteColor];
    textLabel.font = [UIFont boldSystemFontOfSize:14.f];
    [textLabel setText:[self getLabelText]];
    self.textLabel = textLabel;
    [self.view addSubview:self.textLabel];
    [self.view bringSubviewToFront:self.textLabel];
         //to find the total pages in pdf
       
}
//for showing or hidding the slider and set the scrollview zoom value as a default value
-(void)showPdfSlider
{
      sv.zoomScale =defaultZoomScale;
    
    if ([self.toolbarOverlay isHidden])
    {;
        [self setShowToolBar];
    }
    else
    {
        [self setHideToolBar];
    }
    
    
}
-(void)setHideToolBar
{
    [self.toolbarOverlay setHidden:YES];
    [self.toolbar setHidden:YES];
    [self.textLabel setHidden:YES];
}
-(void)setShowToolBar
{
    [self.toolbarOverlay setHidden:NO];
    [self.toolbar setHidden:NO];
    [self.textLabel setHidden:NO];
}
// for getting current page text to display on textLabel
-(NSString *)getLabelText
{
    if (currentPage<1)
    {
        currentPage = 1;
    }
    NSMutableString *str = [[NSMutableString alloc] init];
    NSString *currentPageText = [NSString stringWithFormat:@"%d",(int) currentPage];
    [str appendString:currentPageText];
    [str appendString:@" of "];
    NSString *totalPageText = [NSString stringWithFormat:@"%d",(int) totalpages];
    [str appendString:totalPageText];
    return str;
    
}
//remove laserpointer image from scrollview superview;
-(void)removeSubViews:(UIView *) scrollView
{
    for (UIView *v in scrollView.subviews)
    {
        if ([v isKindOfClass:[UIImageView class]]) {
            [v removeFromSuperview];
        }
    }
}
- (void) transferData:(NSData *) data
{
    if (self.sessionController == nil || self.sessionController.session == nil) {
        return;
    }
    else{
        [self.sessionController sendDataToAllPeers:data];
    }
}
-(NSData*) dataWithValue:(NSValue*)value
{
    NSUInteger size;
    const char* encoding = [value objCType];
    NSGetSizeAndAlignment(encoding, &size, NULL);
    
    void* ptr = malloc(size);
    [value getValue:ptr];
    NSData* data = [NSData dataWithBytes:ptr length:size];
    free(ptr);
    
    return data;
}
-(NSData*) dataWithNumber:(NSNumber*)number
{
    return [self dataWithValue:(NSValue*)number];
}
- (NSValue *)valueWithData:(NSData *)data
{
   
    return (NSValue *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
}
#pragma Button Action Methods

//for scroll the pdf based on slider value
-(void)sliderValueChanged:(UISlider *) slider
{
    // we are setting scrollview zoom value here.
    sv.zoomScale =defaultZoomScale;
   
    //current page
     currentPage =  slider.value/(SLIDER_MAX_VALUE/totalpages);
    if (currentPage<1)
    {
        currentPage = 1;
    }
   
   // calculate offset x coordinate
    CGPoint offset = sv.contentOffset;
    offset.x = (int)(currentPage-1) * self.view.frame.size.width;

    //set label to
    [self.textLabel setText:[self getLabelText]];
    [sv setContentOffset:offset animated:YES];
    NSValue *val = [NSValue valueWithCGPoint:sv.contentOffset];
    NSData *data = [self dataWithValue:val];
    NSLog(@"data%@",data);
    self.sendingDataHeaderType = ksendingDataHeaderWhileSliderMoving;
    //[self transferData:data];

    
}
/// for pop to previous VC.
-(void)doCancel:(id)sender
{

    [self.navigationController popViewControllerAnimated:YES];
}

// when user tap the scrollview contant we this method will call using this method we are hidding or showing the sider as per requirements

-(void) handleTapGesture:(UITapGestureRecognizer *) aTapGestureRecognizer
{
    
    if (aTapGestureRecognizer.state != UIGestureRecognizerStateEnded)
    {
        return;
        
    }
    else
    {
        [self showPdfSlider];
    }
    
}


#pragma ScrollView Delegate Methods

//it is return the contentview size after zooming.
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
//    NSNumber * zoomNumber = [NSNumber numberWithFloat:[scrollView zoomScale]];
//   NSData *zoomData =  [self dataWithNumber:zoomNumber];
//    NSLog(@"%@",zoomData);
    self.sendingDataHeaderType = ksendingDataHeaderWhileZooming;
	return sv.containerView;
    
}
//when user scrolling or zooming time this methd will cal. using this method we are chcking ispdf is zooming or not and based on scrolling we are calculating page number and slider using this we are updating slider value
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
   //checking pdf is zooming or not
    if ([sv zoomScale]!= defaultZoomScale)
    {
        [self setHideToolBar];

    }
    else
    {
        // we are setting scrollview zoom value here.
        sv.zoomScale =defaultZoomScale;
        CGPoint offset = scrollView.contentOffset;
        currentPage = offset.x/self.view.frame.size.width;
        currentPage = currentPage+1;
        self.pdfSlider.value = (currentPage-1) * SLIDER_MAX_VALUE/totalpages;
        if (currentPage<1)
        {
            currentPage = 1;
        }
        [self.textLabel setText:[self getLabelText]];
    }

}

-(BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
// pre-iOS 6 support
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
