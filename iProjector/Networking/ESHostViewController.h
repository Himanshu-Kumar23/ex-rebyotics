//
//  ESHostViewController.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 6/28/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>

@class ESHostViewController;

@protocol ESHostViewControllerDelegate <NSObject>

- (void)hostViewControllerDidCancel:(ESHostViewController *)controller;
- (void)hostViewController:(ESHostViewController *)controller didEndSessionWithReason:(QuitReason)reason;
- (void)hostViewController:(ESHostViewController *)controller startGameWithSession:(GKSession *)session playerName:(NSString *)name clients:(NSArray *)clients;

@end

@interface ESHostViewController : UITableViewController

@property (nonatomic, unsafe_unretained) id <ESHostViewControllerDelegate> delegate;

@end
