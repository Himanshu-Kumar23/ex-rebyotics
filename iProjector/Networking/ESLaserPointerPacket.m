//
//  ESLaserPointerPacket.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/2/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESLaserPointerPacket.h"

@implementation ESLaserPointerPacket

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeBool:self.visible forKey:@"kESLaserPointerVisible"];
    if (self.visible) {
        [aCoder encodeCGPoint:self.point forKey:@"kESLaserPointCoordinates"];
        [aCoder encodeInteger:self.laserPointer forKey:@"kESLaserPointerType"];
    }
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.visible = [aDecoder decodeBoolForKey:@"kESLaserPointerVisible"];
        if (self.visible) {
            self.point = [aDecoder decodeCGPointForKey:@"kESLaserPointCoordinates"];
            self.laserPointer = [aDecoder decodeIntegerForKey:@"kESLaserPointerType"];
        }
    }
    return self;
}

+ (ESLaserPointerPacket *) packetWithhiddenLaserPointer {

    ESLaserPointerPacket *packet = [[ESLaserPointerPacket alloc] init];
    packet.packetType = ESPacketTypeLaserPointer;
    packet.point = CGPointZero;
    packet.pageNo = -1;
    packet.visible = NO;
    return packet;
}

+ (ESLaserPointerPacket *) packetWithPointerVisibleAt:(CGPoint) point withLaserPointerType:(NSInteger)laserPointer{
    
    ESLaserPointerPacket *packet = [[ESLaserPointerPacket alloc] init];
    packet.packetType = ESPacketTypeLaserPointer;
    packet.point = point;
    packet.visible = YES;
    packet.laserPointer = laserPointer;
    return packet;
}

@end
