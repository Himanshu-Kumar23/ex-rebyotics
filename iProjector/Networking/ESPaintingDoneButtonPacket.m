//
//  ESPaintingDoneButtonPacket.m
//  iProjector
//
//  Created by Jayaprada Behera on 18/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESPaintingDoneButtonPacket.h"

@implementation ESPaintingDoneButtonPacket
- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypeDismissWhiteBoard;
    }
    return self;
}
- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}


+(ESPaintingDoneButtonPacket *) packectToDismissWhiteBoard{
    ESPaintingDoneButtonPacket *packect = [[ESPaintingDoneButtonPacket alloc] init];
    return packect;
}

@end
