//
//  ESAnswerPacket.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/12/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAnswerPacket.h"

@implementation ESAnswerPacket

- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypeAnswer;
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.selectedOptions = [aDecoder decodeObjectForKey:@"kESSelectedOptions"];
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.selectedOptions forKey:@"kESSelectedOptions"];
}

+ (ESAnswerPacket *) packetWithOptions:(NSIndexSet *) options {
    ESAnswerPacket *answerPacket = [[ESAnswerPacket alloc] init];
    answerPacket.selectedOptions = options;
    return answerPacket;
}

@end
