//
//  ESPageTransitionPacket.m
//  iProjector
//
//  Created by Manoj Katragadda on 05/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESPageTransitionPacket.h"

@implementation ESPageTransitionPacket

- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypeChangePage;
    }
    return self;
}

+ (ESPageTransitionPacket *) packetWithPageNo:(NSInteger) pageNo {
    ESPageTransitionPacket *packet = [[ESPageTransitionPacket alloc] init];
    packet.pageNo = pageNo;
    return packet;
}

@end
