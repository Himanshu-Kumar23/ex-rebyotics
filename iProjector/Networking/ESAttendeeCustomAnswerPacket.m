//
//  ESAttendeeCustomAnswerPacket.m
//  X Detailer
//
//  Created by Himanshukumar on 14/11/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//

#import "ESAttendeeCustomAnswerPacket.h"

@implementation ESAttendeeCustomAnswerPacket

- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypeAttendeeCustomAnswer;
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.answer forKey:@"kESAttendeeCustomAnswer"];
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.answer = [aDecoder decodeObjectForKey:@"kESAttendeeCustomAnswer"];
    }
    return self;
}

+ (ESAttendeeCustomAnswerPacket *) packetWithCustomAnswer:(ESAttendeeCustomAnswer *) answer {
    ESAttendeeCustomAnswerPacket *packet = [[ESAttendeeCustomAnswerPacket alloc] init];
    packet.answer = answer;
    return packet;
}

+ (ESAttendeeCustomAnswerPacket *) packetWithAttendeeCustomAnswerText:(NSString *) answer attendeeName:(NSString *)attendeeName{
    ESAttendeeCustomAnswer *obj = [[ESAttendeeCustomAnswer alloc] init];
    obj.aAnswerDescription = answer;
    obj.attendeeName = attendeeName;
    return [ESAttendeeCustomAnswerPacket packetWithCustomAnswer:obj];
}

@end
