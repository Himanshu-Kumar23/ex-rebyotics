//
//  ESPDFInfoRequestPacket.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 9/11/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAbstractPacket.h"

@interface ESPresentationInfoRequestPacket : ESAbstractPacket

@property(nonatomic, strong) NSString *fileName;
@property(nonatomic, strong) NSDate *lastModifiedAt;
//@property(nonatomic, strong) NSNumber *fileSize;

//Other settings to get the presentation started.
@property(nonatomic) BOOL attendeeCanAskQuestions;
@property(nonatomic) BOOL attendeeCanBrowsePages;
@property(nonatomic) NSInteger currentPage;

//If the presentation has videos.
@property(nonatomic) NSArray *videos;
@property(nonatomic) NSArray *questions;

+ (ESPresentationInfoRequestPacket *) packetWithFileName:(NSString *) fileName;

@end
