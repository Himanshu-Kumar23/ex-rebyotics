//
//  ESPresentationInfoResponsePacket.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 10/9/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESPresentationInfoResponsePacket.h"

@implementation ESPresentationInfoResponsePacket

- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypePresentationInfoResponse;
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.fileRequired = [aDecoder decodeBoolForKey:@"kESFileRequired"];
        self.fileName = [aDecoder decodeObjectForKey:@"kESFileName"];
        self.fileTransferAcknowledgement = [aDecoder decodeBoolForKey:@"kESFileTransferAcknowledgement"];
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.fileName forKey:@"kESFileName"];
    [aCoder encodeBool:self.fileRequired forKey:@"kESFileRequired"];
    [aCoder encodeBool:self.fileTransferAcknowledgement forKey:@"kESFileTransferAcknowledgement"];
}

+ (ESPresentationInfoResponsePacket *) packetForFileRequired:(NSString *) fileName {
    ESPresentationInfoResponsePacket *packet = [[ESPresentationInfoResponsePacket alloc] init];
    packet.fileName = fileName;
    packet.fileRequired = YES;
    packet.fileTransferAcknowledgement = NO;
    return packet;
}

+ (ESPresentationInfoResponsePacket *) packetForFileAcknowledgement:(NSString *) fileName {
    ESPresentationInfoResponsePacket *packet = [[ESPresentationInfoResponsePacket alloc] init];
    packet.fileName = fileName;
    packet.fileRequired = YES;
    packet.fileTransferAcknowledgement = YES;
    return packet;
}

@end
