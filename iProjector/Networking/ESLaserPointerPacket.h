//
//  ESLaserPointerPacket.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/2/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAbstractPagePacket.h"

@interface ESLaserPointerPacket : ESAbstractPagePacket

@property(nonatomic) CGPoint point;
@property(nonatomic) BOOL visible;
@property(nonatomic)NSInteger laserPointer;
+ (ESLaserPointerPacket *) packetWithPointerVisibleAt:(CGPoint) point withLaserPointerType:(NSInteger)laserPointer;
+ (ESLaserPointerPacket *) packetWithhiddenLaserPointer;

@end
