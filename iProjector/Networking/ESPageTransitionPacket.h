//
//  ESPageTransitionPacket.h
//  iProjector
//
//  Created by Manoj Katragadda on 05/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESAbstractPagePacket.h"

@interface ESPageTransitionPacket : ESAbstractPagePacket{

}

+ (ESPageTransitionPacket *) packetWithPageNo:(NSInteger) pageNo;

@end
