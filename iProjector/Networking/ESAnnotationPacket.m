//
//  ESAnnotationPacket.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/2/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAnnotationPacket.h"

@implementation ESAnnotationPacket

- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypeAnnotation;
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeCGPoint:self.point forKey:@"kESAnnotationPoint"];
    [aCoder encodeInteger:self.annotationType forKey:@"kESAnnotationType"];
    if (self.annotationType == ESAnnotationTypeStart || self.annotationType == ESAnnotationTypeHighlightStart) {
        [aCoder encodeObject:self.color forKey:@"kESAnnotationColor"];
        [aCoder encodeInteger:self.lineWidth forKey:@"kESAnnotationLineWidth"];
    }
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.point = [aDecoder decodeCGPointForKey:@"kESAnnotationPoint"];
        self.annotationType = [aDecoder decodeIntegerForKey:@"kESAnnotationType"];
        if (self.annotationType == ESAnnotationTypeStart || self.annotationType == ESAnnotationTypeHighlightStart) {
            self.lineWidth = [aDecoder decodeIntegerForKey:@"kESAnnotationLineWidth"];
            self.color = (UIColor *)[aDecoder decodeObjectForKey:@"kESAnnotationColor"];
        }
    }
    return self;
}

- (NSString *) description {
    return [NSString stringWithFormat:@"point : %@, type: %zd, pageNo: %ld, lineWidth: %ld, color: %@",NSStringFromCGPoint(self.point), self.annotationType, (long)self.pageNo, (long)self.lineWidth, self.color];
}

+ (ESAnnotationPacket *) packetWithScaledPoint:(CGPoint) point  lineWidth:(NSInteger)lineWidth andColor:(UIColor *)color annotationType:(ESAnnotationType) annotationType {
    ESAnnotationPacket *packet = [[ESAnnotationPacket alloc] init];
    packet.annotationType = annotationType;
    packet.point = point;
    packet.lineWidth = lineWidth;
    packet.color = color;
    return packet;
}

+ (ESAnnotationPacket *) undoPacket {
    ESAnnotationPacket *packet = [[ESAnnotationPacket alloc] init];
    packet.annotationType = ESAnnotationTypeUndo;
    return packet;
}
+ (ESAnnotationPacket *) clearPacket{
    ESAnnotationPacket *packet = [[ESAnnotationPacket alloc] init];
    packet.annotationType = ESAnnotationTypeClear;
    return packet;

}
@end
