//
//  ESWhiteBoardPacket.m
//  iProjector
//
//  Created by Jayaprada Behera on 18/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESWhiteBoardPacket.h"

@implementation ESWhiteBoardPacket
- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypeWhiteBoard;
    }
    return self;
}
- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}


+(ESWhiteBoardPacket *) packectToGoWhiteBoard{
    ESWhiteBoardPacket *packect = [[ESWhiteBoardPacket alloc] init];
    return packect;
}

@end
