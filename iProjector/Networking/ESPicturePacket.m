//
//  ESPicturePacket.m
//  iProjector
//
//  Created by Jayaprada Behera on 07/11/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESPicturePacket.h"

@implementation ESPicturePacket


- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypePicture;
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    
    [super encodeWithCoder:aCoder];
    
    [aCoder encodeObject:self.picture forKey:@"kESPicture"];
    [aCoder encodeCGRect:self.pictureFrame forKey:@"kESPictureFrame"];
    [aCoder encodeInteger:self.pictureIndex forKey:@"kESPictureIndex"];
    [aCoder encodeInteger:self.pictureState forKey:@"kESPictureState"];

}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        self.picture = (UIImage *)[aDecoder decodeObjectForKey:@"kESPicture"];
        self.pictureFrame = [aDecoder decodeCGRectForKey:@"kESPictureFrame"];
        self.pictureIndex = [aDecoder decodeIntegerForKey:@"kESPictureIndex"];
        self.pictureState = [aDecoder decodeIntegerForKey:@"kESPictureState"];
    }
    return self;
}

+(ESPicturePacket *) packetWithPicture:(UIImage *)picture  andPictureframe:(CGRect )picFrame withState:(ESPictureState)state {
    
    ESPicturePacket *packet = [[ESPicturePacket alloc] init];
    packet.picture = picture;
    packet.pictureState = state;
    packet.pictureFrame = picFrame;
    return packet;


}

+(ESPicturePacket *) removePictureWithState:(ESPictureState ) state withIndex:(NSInteger )index{

    ESPicturePacket *packet = [[ESPicturePacket alloc] init];
    packet.pictureState = state;
    packet.pictureIndex = index;

    return packet;
}

+(ESPicturePacket *) packetToMovePictureToPosition:(CGRect ) pictureFrame withPictureState:(ESPictureState) state withIndexValue:(NSInteger)index{
    
    ESPicturePacket *packet = [[ESPicturePacket alloc] init];
    packet.pictureState = state;
    packet.pictureIndex = index;
    packet.pictureFrame = pictureFrame;

    return packet;

}
+(ESPicturePacket *) packetToAddPicture:(UIImage *)image toPosition:(CGRect ) pictureFrame withPictureState:(ESPictureState) state withIndexValue:(NSInteger)index{
    
    ESPicturePacket *packet = [[ESPicturePacket alloc] init];
    packet.pictureState = state;
    packet.pictureIndex = index;
    packet.pictureFrame = pictureFrame;
    packet.picture = image;
    
    return packet;

}

@end
