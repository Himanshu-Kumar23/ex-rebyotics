//
//  Header.h
//  X Detailer
//
//  Created by Himanshukumar on 14/11/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESAbstractPacket.h"
#import "ESAttendeeCustomAnswer.h"

@interface ESAttendeeCustomAnswerPacket : ESAbstractPacket

@property (nonatomic, strong) ESAttendeeCustomAnswer *answer;

+ (ESAttendeeCustomAnswerPacket *) packetWithCustomAnswer:(ESAttendeeCustomAnswer *) answer;
+ (ESAttendeeCustomAnswerPacket *) packetWithAttendeeCustomAnswerText:(NSString *) answer attendeeName:(NSString *)attendeeName;

@end
