//
//  ESAnswerPacket.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/12/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAbstractPacket.h"

@interface ESAnswerPacket : ESAbstractPacket

@property(nonatomic, strong) NSIndexSet *selectedOptions;

+ (ESAnswerPacket *) packetWithOptions:(NSIndexSet *) indexSet;

@end
