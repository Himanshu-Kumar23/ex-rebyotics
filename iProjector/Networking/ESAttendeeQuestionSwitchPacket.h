//
//  ESAttendeeQuestionSwitchPacket.h
//  iProjector
//
//  Created by Manoj Katragadda on 14/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESAbstractPacket.h"
@interface ESAttendeeQuestionSwitchPacket : ESAbstractPacket

//@property(nonatomic) BOOL switchStatus;
@property(nonatomic) BOOL browseStatus;
@property(nonatomic) BOOL queryStatus;
//+(ESAttendeeQuestionSwitchPacket *) packectWithSwichValue:(BOOL)status;
//+(ESAttendeeQuestionSwitchPacket *) packectWithBrowseSwitchValueStaus:(BOOL)status ;
//+(ESAttendeeQuestionSwitchPacket *) packectWithQuerySwichValueStatus:(BOOL)queryStatus;
+(ESAttendeeQuestionSwitchPacket *) packectWithQuerySwichValueStatus:(BOOL)queryStatus andBrowseSatuts:(BOOL) bStatus;
@end
