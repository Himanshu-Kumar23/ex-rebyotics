//
//  ESPicturePacket.h
//  iProjector
//
//  Created by Jayaprada Behera on 07/11/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESAbstractPagePacket.h"


typedef NS_ENUM (NSInteger,ESPictureState) {
    
    ESStateAddPicture,
    ESStateMovePicture,
    ESStateRemovePicture,
    
} ;


@interface ESPicturePacket : ESAbstractPagePacket

@property(nonatomic) ESPictureState pictureState;
@property(nonatomic)CGRect pictureFrame; //
@property(nonatomic) NSInteger pictureIndex;
@property(nonatomic,strong) UIImage *picture;

+(ESPicturePacket *) packetWithPicture:(UIImage *)picture  andPictureframe:(CGRect )picFrame withState:(ESPictureState)state ;
+(ESPicturePacket *) removePictureWithState:(ESPictureState )pictureState withIndex:(NSInteger )index;

+(ESPicturePacket *) packetToMovePictureToPosition:(CGRect ) pictureFrame withPictureState:(ESPictureState) pictureState withIndexValue:(NSInteger)index;

+(ESPicturePacket *) packetToAddPicture:(UIImage *)image toPosition:(CGRect ) pictureFrame withPictureState:(ESPictureState) pictureState withIndexValue:(NSInteger)index;

@end
