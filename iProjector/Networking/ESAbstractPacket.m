//
//  ESAbstractPacket.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/2/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAbstractPacket.h"

@implementation ESAbstractPacket

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInteger:self.packetType forKey:@"kESPacketType"];
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.packetType = [aDecoder decodeIntegerForKey:@"kESPacketType"];
    }
    return self;
}

@end
