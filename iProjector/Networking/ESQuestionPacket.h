//
//  ESQuestionPacket.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/12/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAbstractPacket.h"
#import "ESQuestion.h"

@interface ESQuestionPacket : ESAbstractPacket

@property (nonatomic, strong) ESQuestion *question;

+ (ESQuestionPacket *) packetWithQuestion:(ESQuestion *) question;

@end
