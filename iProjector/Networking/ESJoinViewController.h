
#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>

#import "MatchmakingClient.h"

@class ESJoinViewController;

@protocol ESJoinViewControllerDelegate <NSObject>

- (void)joinViewControllerDidCancel:(ESJoinViewController *)controller;
- (void)joinViewController:(ESJoinViewController *)controller didDisconnectWithReason:(QuitReason)reason;
- (void)joinViewController:(ESJoinViewController *)controller startGameWithSession:(GKSession *)session playerName:(NSString *)name server:(NSString *)peerID;

@end

@interface ESJoinViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, MatchmakingClientDelegate>

@property (nonatomic, unsafe_unretained) id <ESJoinViewControllerDelegate> delegate;

@end
