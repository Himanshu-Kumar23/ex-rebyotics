//
//  ESPDFData.h
//  iProjector
//
//  Created by WebileApps on 10/07/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESPDFData : NSObject
{
    
}

@property(nonatomic,assign) NSInteger pdfHeaderType;
@property(nonatomic,assign) NSInteger currentPage;
@property(nonatomic,assign) CGPoint pdfScrollViewOffSet;
@property(nonatomic,assign) CGFloat zoomValue;

@end
