//
//  ESVideoPlayerPacket.m
//  iProjector
//
//  Created by Jayaprada Behera on 21/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESVideoPlayerPacket.h"

@implementation ESVideoPlayerPacket

- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypeVideo;
        self.timeInterval = 0;
    }
    return self;
}
- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeInteger:self.videoPlayerState forKey:@"kESVideoPlayerState"];
    if (self.videoPlayerState == ESVideoPlayerStatePause || self.videoPlayerState == ESVideoPlayerStatePlay) {
        [aCoder encodeDouble:self.timeInterval forKey:@"kESVideoPlaybackTime"];
    }
    [aCoder encodeObject:self.videoFileName forKey:@"kESVideoFileName"];
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.videoPlayerState = [aDecoder decodeIntegerForKey:@"kESVideoPlayerState"];
        if (self.videoPlayerState == ESVideoPlayerStatePause || self.videoPlayerState == ESVideoPlayerStatePlay) {
            self.timeInterval = [aDecoder decodeDoubleForKey:@"kESVideoPlaybackTime"];
        }
        self.videoFileName = [aDecoder decodeObjectForKey:@"kESVideoFileName"];
    }
    return self;
}

- (NSString *) description {
    return [NSString stringWithFormat:@"playerstate: %zd, timeInterval: %f", _videoPlayerState, _timeInterval];
}

+(ESVideoPlayerPacket *) startPacketWithFileName:(NSString *)videoFileName {
    ESVideoPlayerPacket *packet = [[ESVideoPlayerPacket alloc] init];
    packet.videoPlayerState = ESVideoPlayerStateStart;
    packet.videoFileName = videoFileName;
    return packet;
}

+(ESVideoPlayerPacket *) endPacket {
    ESVideoPlayerPacket *packet = [[ESVideoPlayerPacket alloc] init];
    packet.videoPlayerState = ESVideoPlayerStateClose;
    return packet;
}

+(ESVideoPlayerPacket *) pausePacketAtTime:(NSTimeInterval) timeInterval {
    ESVideoPlayerPacket *packet = [[ESVideoPlayerPacket alloc] init];
    packet.videoPlayerState = ESVideoPlayerStatePause;
    packet.timeInterval = timeInterval;
//    NSLog(@"pausePacketAtTime: %@",packet);
    return packet;
}

+(ESVideoPlayerPacket *) playPacketFromTime:(NSTimeInterval) timeInterval {
    ESVideoPlayerPacket *packet = [[ESVideoPlayerPacket alloc] init];
    packet.videoPlayerState = ESVideoPlayerStatePlay;
    packet.timeInterval = timeInterval;
//    NSLog(@"playPacketFromTime: %@",packet);
    return packet;
}

@end
