//
//  ESZoomTransitionPacket.m
//  iProjector
//
//  Created by Manoj Katragadda on 05/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESZoomTransitionPacket.h"

@implementation ESZoomTransitionPacket
@synthesize zoomValue;
@synthesize scrollViewOffSet;
@synthesize senderMinimumZoomValue;

- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypeZoomAndPan;
    }
    return self;
}
- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeFloat:self.zoomValue forKey:@"kESZoomValue"];
    [aCoder encodeFloat:self.senderMinimumZoomValue forKey:@"kESSenderMinimumZoomValue"];
    [aCoder encodeCGPoint:self.scrollViewOffSet forKey:@"kESScrollViewOffSet"];
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.zoomValue  = [aDecoder decodeFloatForKey:@"kESZoomValue"];
        self.senderMinimumZoomValue  = [aDecoder decodeFloatForKey:@"kESSenderMinimumZoomValue"];
        self.scrollViewOffSet = [aDecoder decodeCGPointForKey:@"kESScrollViewOffSet"];
    }
    return self;
}

+ (ESZoomTransitionPacket *) packetWithZoomValue:(CGFloat) zoomValue scrollViewOffset:(CGPoint)offSet minimumZoomValue:(CGFloat )minimumZoomValue {
   
    ESZoomTransitionPacket *packet = [[ESZoomTransitionPacket alloc] init];
    packet.zoomValue = zoomValue;
    packet.senderMinimumZoomValue = minimumZoomValue;
    packet.scrollViewOffSet = offSet;
    return packet;
}
@end
