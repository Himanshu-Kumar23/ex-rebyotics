//
//  ESAttendeeQuestionSwitchPacket.m
//  iProjector
//
//  Created by Manoj Katragadda on 14/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAttendeeQuestionSwitchPacket.h"

@implementation ESAttendeeQuestionSwitchPacket
//@synthesize switchStatus;
@synthesize queryStatus;
- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypeAttendeeQuestionSwitch;
    }
    return self;
}
- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
//    [aCoder encodeBool:self.switchStatus forKey:@"kESAttendeeSwitchStatus"];
     [aCoder encodeBool:self.queryStatus forKey:@"kESAttendeeQuerySwitchStatus"];
    [aCoder encodeBool:self.browseStatus forKey:@"kESAttendeeBrowseSwitchStatus"];
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
//        self.switchStatus = [aDecoder decodeBoolForKey:@"kESAttendeeSwitchStatus"];
        self.queryStatus = [aDecoder decodeBoolForKey:@"kESAttendeeQuerySwitchStatus"];
        self.browseStatus = [aDecoder decodeBoolForKey:@"kESAttendeeBrowseSwitchStatus"];
           }
    return self;
}

//+(ESAttendeeQuestionSwitchPacket *) packectWithSwichValue:(BOOL)status{
//    ESAttendeeQuestionSwitchPacket *packet = [[ESAttendeeQuestionSwitchPacket alloc] init];
//    packet.switchStatus = status;
//    return packet;
//}
//+(ESAttendeeQuestionSwitchPacket *) packectWithBrowseSwitchValueStaus:(BOOL)status {
//    ESAttendeeQuestionSwitchPacket *packet = [[ESAttendeeQuestionSwitchPacket alloc] init];
//    packet.browseStatus = status;
//    return packet;
//}
//+(ESAttendeeQuestionSwitchPacket *) packectWithQuerySwichValueStatus:(BOOL)queryStatus{
//    ESAttendeeQuestionSwitchPacket *packet = [[ESAttendeeQuestionSwitchPacket alloc] init];
//    packet.queryStatus = queryStatus;
//    return packet;
//}
+(ESAttendeeQuestionSwitchPacket *) packectWithQuerySwichValueStatus:(BOOL)queryStatus andBrowseSatuts:(BOOL) bStatus{
    
    ESAttendeeQuestionSwitchPacket *packet = [[ESAttendeeQuestionSwitchPacket alloc] init];
    packet.queryStatus = queryStatus;
    packet.browseStatus = bStatus;
    return packet;

}

@end
