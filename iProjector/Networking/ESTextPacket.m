//
//  ESTextPacket.m
//  iProjector
//
//  Created by Jayaprada Behera on 12/10/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESTextPacket.h"

@implementation ESTextPacket

- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypeText;
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    
    [super encodeWithCoder:aCoder];
    
    [aCoder encodeObject:self.textColor forKey:@"kESTextColor"];
    [aCoder encodeObject:self.text forKey:@"kESText"];
    [aCoder encodeFloat:self.textFontSize forKey:@"kESTextFont"];
    [aCoder encodeInteger:self.textState forKey:@"kESTextState"];
    [aCoder encodeInteger:self.indexValue forKey:@"kESIndexValue"];
    [aCoder encodeCGRect:self.textFrame forKey:@"kESTextFrame"];
    [aCoder encodeBool:self.session_attendee forKey:@"kESSessionIsAttendee"];


}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.textColor = (UIColor *)[aDecoder decodeObjectForKey:@"kESTextColor"];
        self.text = [aDecoder decodeObjectForKey:@"kESText"];
        self.textFontSize = [aDecoder decodeFloatForKey:@"kESTextFont"];
        self.textState = [aDecoder decodeIntegerForKey:@"kESTextState"];
        self.indexValue = [aDecoder decodeIntegerForKey:@"kESIndexValue"];
//        self.translationPoint = [ aDecoder decodeCGPointForKey:@"kESTranslationPoint"];
        self.textFrame = [ aDecoder decodeCGRectForKey:@"kESTextFrame"];
        self.session_attendee = [aDecoder decodeBoolForKey:@"kESSessionIsAttendee"];

        
    }
    return self;
}


+(ESTextPacket *) packetWithString:(NSString *)text textColor:(UIColor *)textColor andTextFont:(CGFloat)fontSize withTextState :(ESTextState )textState andWithIndex:(NSInteger)index andFrame:(CGRect)textFrame{
    
    ESTextPacket *packet = [[ESTextPacket alloc] init];
    packet.textColor = textColor;
    packet.text = text;
    packet.indexValue = index;
    packet. textState = textState;
    packet.textFontSize = fontSize;
//    packet.textFrame = textCenter;
    packet.textFrame = textFrame;
    return packet;
    
}
+(ESTextPacket *) packetWithText:(NSString *)editedText withState:(ESTextState)state withIndex:(NSInteger )index{
    
    ESTextPacket *packet = [[ESTextPacket alloc] init];
    packet.text = editedText;
    packet.textState = state;
    packet.indexValue = index;
    return packet;
    
}

+(ESTextPacket *) packetWithTextColor:(UIColor *)color withState:(ESTextState)state withIndex:(NSInteger )index{
    
    ESTextPacket *packet = [[ESTextPacket alloc] init];
    packet.textColor = color;
    packet.textState = state;
    packet.indexValue = index;
    return packet;
}

+(ESTextPacket *) packetWithTextFont:(CGFloat  )textFontSize withState:(ESTextState)state  withIndex:(NSInteger )index{
    
    ESTextPacket *packet = [[ESTextPacket alloc] init];
    packet.textFontSize = textFontSize;
    packet.textState = state;
    packet.indexValue = index;
    return packet;
}

+(ESTextPacket *) packetWithAddTextMoveToPosition:(CGRect ) textFrame withTextState:(ESTextState) textState withIndexValue:(NSInteger)index{
    
    ESTextPacket *packet = [[ESTextPacket alloc] init];
    packet.textState = textState;
    packet.indexValue = index;
    //    packet.translationPoint = translationPoint;
    packet.textFrame = textFrame;
    return packet;
    
}

+(ESTextPacket *) removeTextWithState:(ESTextState )state withIndex:(NSInteger )index{
    
    ESTextPacket *packet = [[ESTextPacket alloc] init];
    packet.textState = state;
    packet.indexValue = index;
    return packet;
    
}

+(ESTextPacket *) packetWithStartState:(ESTextState )textState{
    
    ESTextPacket *packet = [[ESTextPacket alloc] init];
    packet.textState = textState;
    return packet;
    
}
+(ESTextPacket *) packetToDismissPopoverWithState:(ESTextState)state  indexValue:(NSInteger )index withText:(NSString *)text withTextColor:(UIColor *)color withFontSize:(CGFloat )fontSize withCenter:(CGRect)textFrame{

    ESTextPacket *packet = [[ESTextPacket alloc] init];
    packet.textState = state;
    packet.indexValue = index;
    packet.text = text;
    packet.textColor = color;
    packet.textFontSize = fontSize;
//    packet.translationPoint = center;
    packet.textFrame = textFrame;
    return packet;
}
@end
