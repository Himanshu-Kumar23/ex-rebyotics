//
//  ESPresentationInfoResponsePacket.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 10/9/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAbstractPacket.h"

@interface ESPresentationInfoResponsePacket : ESAbstractPacket

@property(nonatomic, strong) NSString *fileName;
@property(nonatomic) BOOL fileRequired;
@property(nonatomic) BOOL fileTransferAcknowledgement;

+ (ESPresentationInfoResponsePacket *) packetForFileAcknowledgement:(NSString *) fileName;
+ (ESPresentationInfoResponsePacket *) packetForFileRequired:(NSString *) fileName;

@end
