//
//  ESTextPacket.h
//  iProjector
//
//  Created by Jayaprada Behera on 12/10/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESAbstractPagePacket.h"


typedef NS_ENUM (NSInteger,ESTextState) {
    
    ESTextStateAddTextDone,
    ESTextStateRemoveText,

    
    ESTextStateMoveText,
    ESTextStateStartText,
    ESTextStateTextChange,
    ESTextStateTextColorChange,
    ESTextStateTextFontChange,
    ESTextStateDismisPopover,
    
} ;

@interface ESTextPacket : ESAbstractPagePacket

@property(nonatomic, strong) UIColor *textColor;
@property(nonatomic,strong) NSString *text;
@property(nonatomic) CGFloat textFontSize;
@property(nonatomic) ESTextState textState;
//@property(nonatomic)CGPoint translationPoint;
@property(nonatomic)CGRect textFrame; //
@property(nonatomic) NSInteger indexValue;
@property(nonatomic)BOOL session_attendee; //

+(ESTextPacket *) packetWithText:(NSString *)editedText withState:(ESTextState)state withIndex:(NSInteger )index;

+(ESTextPacket *) packetWithTextColor:(UIColor *)color withState:(ESTextState)state withIndex:(NSInteger )index;

+(ESTextPacket *) packetWithTextFont:(CGFloat  )textFontSize withState:(ESTextState)state  withIndex:(NSInteger )index;

+(ESTextPacket *) removeTextWithState:(ESTextState )textState withIndex:(NSInteger )index;

+(ESTextPacket *) packetWithString:(NSString *)text textColor:(UIColor *)textColor andTextFont:(CGFloat)fontSize withTextState :(ESTextState )textState andWithIndex:(NSInteger)index andFrame:(CGRect)textFrame;

+(ESTextPacket *) packetWithAddTextMoveToPosition:(CGRect ) textFrame withTextState:(ESTextState) textState withIndexValue:(NSInteger)index;

+(ESTextPacket *) packetWithStartState:(ESTextState )textState;

+(ESTextPacket *) packetToDismissPopoverWithState:(ESTextState)state  indexValue:(NSInteger )index withText:(NSString *)text withTextColor:(UIColor *)color withFontSize:(CGFloat )fontSize withCenter:(CGRect)textFrame;



@end
