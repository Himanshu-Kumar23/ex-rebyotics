//
//  ESAttendeeQuestionPacket.h
//  iProjector
//
//  Created by Manoj Katragadda on 13/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESAbstractPacket.h"
#import "ESAttendeeQuestion.h"

@interface ESAttendeeQuestionPacket : ESAbstractPacket

@property (nonatomic, strong) ESAttendeeQuestion *question;

+ (ESAttendeeQuestionPacket *) packetWithQuestion:(ESAttendeeQuestion *) question;
+ (ESAttendeeQuestionPacket *) packetWithAttendeeQuestionText:(NSString *) question attendeeName:(NSString *)attendeeName;

@end
