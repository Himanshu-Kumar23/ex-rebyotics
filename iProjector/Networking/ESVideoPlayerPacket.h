//
//  ESVideoPlayerPacket.h
//  iProjector
//
//  Created by Jayaprada Behera on 21/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESAbstractPacket.h"

typedef NS_ENUM (NSInteger,ESVideoPlayerState) {
    ESVideoPlayerStateStart,
    ESVideoPlayerStatePause,
    ESVideoPlayerStatePlay,
    ESVideoPlayerStateClose,
} ;

@interface ESVideoPlayerPacket : ESAbstractPacket

@property(nonatomic) ESVideoPlayerState videoPlayerState;
@property(nonatomic) NSTimeInterval timeInterval;
@property(nonatomic) NSString *videoFileName;

//To be used later on.
//@property(nonatomic, strong) NSString *videoName;

+(ESVideoPlayerPacket *) startPacketWithFileName:(NSString *)videoFileName;
+(ESVideoPlayerPacket *) endPacket;
+(ESVideoPlayerPacket *) pausePacketAtTime:(NSTimeInterval) timeInterval;
+(ESVideoPlayerPacket *) playPacketFromTime:(NSTimeInterval) timeInterval;

@end
