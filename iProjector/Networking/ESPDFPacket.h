//
//  ESPDFPacket.h
//  iProjector
//
//  Created by Manoj Katragadda on 30/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESAbstractPacket.h"

typedef NS_ENUM (NSInteger,ESPDFPacketType){
    ESPDFPacketTypeChunk = 0,
    ESPDFPacketTypeHeader,
    ESPDFPacketTypeFooter,
};


@interface ESPDFPacket : ESAbstractPacket{
}

@property(nonatomic,strong) NSNumber *fileSize; //used when packet type is header

@property(nonatomic,strong) NSDate *lastModifiedAt; //used when packet type is footer
@property(nonatomic,strong) NSString *fileName; //used when packet type is footer

@property(nonatomic,strong) NSData *chunk;
@property(nonatomic) ESPDFPacketType pdfPacketType;

@property(nonatomic) BOOL attendeeCanBrowsePages;
@property(nonatomic) BOOL attendeeCanAskQuestions;

+(ESPDFPacket *) pdfHeaderPacketForFileName:(NSString *) fileName;
+ (ESPDFPacket *) pdfPacketForData:(NSData *)chunkData;
+ (ESPDFPacket *) pdfFooterPacketForFileName:(NSString *) fileName;

@end
