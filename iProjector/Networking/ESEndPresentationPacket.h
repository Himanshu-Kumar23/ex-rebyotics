//
//  ESCancelButtonPacket.h
//  iProjector
//
//  Created by Manoj Katragadda on 15/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESAbstractPacket.h"
@interface ESEndPresentationPacket : ESAbstractPacket

+(ESEndPresentationPacket *) packet;

@end
