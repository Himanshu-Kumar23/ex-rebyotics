//
//  ESCancelButtonPacket.m
//  iProjector
//
//  Created by Manoj Katragadda on 15/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESEndPresentationPacket.h"

@implementation ESEndPresentationPacket
- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypeEndPresentation;
    }
    return self;
}
- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}


+(ESEndPresentationPacket *) packet{
    ESEndPresentationPacket *packect = [[ESEndPresentationPacket alloc] init];
    return packect;
}


@end
