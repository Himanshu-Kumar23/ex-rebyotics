//
//  ESAttendeeQuestionPacket.m
//  iProjector
//
//  Created by Manoj Katragadda on 13/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAttendeeQuestionPacket.h"

@implementation ESAttendeeQuestionPacket

- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypeAttendeeQuestion;
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.question forKey:@"kESAttendeeQuestion"];
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.question = [aDecoder decodeObjectForKey:@"kESAttendeeQuestion"];
    }
    return self;
}

+ (ESAttendeeQuestionPacket *) packetWithQuestion:(ESAttendeeQuestion *) question {
    ESAttendeeQuestionPacket *packet = [[ESAttendeeQuestionPacket alloc] init];
    packet.question = question;
    return packet;
}

+ (ESAttendeeQuestionPacket *) packetWithAttendeeQuestionText:(NSString *) question attendeeName:(NSString *)attendeeName{
    ESAttendeeQuestion *obj = [[ESAttendeeQuestion alloc] init];
    obj.aQuestDescription = question;
    obj.attendeeName = attendeeName;
    return [ESAttendeeQuestionPacket packetWithQuestion:obj];
}

@end
