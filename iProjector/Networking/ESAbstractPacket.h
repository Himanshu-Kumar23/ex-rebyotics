//
//  ESAbstractPacket.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/2/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM (NSInteger,ESPacketType){
    ESPacketTypeUnknown = 0,
    
    ESPacketTypeAnnotation,
    ESPacketTypeLaserPointer,
    ESPacketTypeChangePage,
    ESPacketTypeZoomAndPan,
    
    ESPacketTypeQuestion,
    ESPacketTypeAnswer,
    ESPacketTypeAttendeeQuestion,
    ESPacketTypeAttendeeCustomAnswer,
    ESPacketTypeAttendeeQuestionSwitch,
    
    ESPacketTypeEndPresentation,
    ESPacketTypePDFTranser,
    ESPacketTypeWhiteBoard,
    ESPacketTypeDismissWhiteBoard,
    
    ESPacketTypeVideo,
    ESPacketTypePresentationInfo,
    ESPacketTypePresentationInfoResponse,
    ESPacketTypeText,
    ESPacketTypePicture

};

@interface ESAbstractPacket : NSObject<NSCoding>

@property(nonatomic) ESPacketType packetType;
@property(nonatomic) NSInteger packetNumber;

@end
