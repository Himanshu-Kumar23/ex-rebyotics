//
//  ESZoomTransitionPacket.h
//  iProjector
//
//  Created by Manoj Katragadda on 05/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESAbstractPagePacket.h"

@interface ESZoomTransitionPacket : ESAbstractPagePacket
{
    CGPoint  scrollViewOffSet;
    CGFloat  zoomValue;
    CGFloat  senderMinimumZoomValue;

}
@property(nonatomic)CGPoint  scrollViewOffSet;
@property(nonatomic)CGFloat  zoomValue;
@property(nonatomic)CGFloat  senderMinimumZoomValue;

+ (ESZoomTransitionPacket *) packetWithZoomValue:(CGFloat) zoomValue scrollViewOffset:(CGPoint)offSet minimumZoomValue:(CGFloat )minimumZoomValue;
@end
