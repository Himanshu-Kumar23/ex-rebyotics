//
//  ESPDFInfoRequestPacket.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 9/11/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESPresentationInfoRequestPacket.h"
#import "ESSettingListViewController.h"

@implementation ESPresentationInfoRequestPacket

- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypePresentationInfo;
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.fileName = [aDecoder decodeObjectForKey:@"kESFileName"];
        self.questions = [aDecoder decodeObjectForKey:@"kESQuestions"];
//        self.fileSize = [aDecoder decodeObjectForKey:@"kESFileSize"];
        self.lastModifiedAt = [aDecoder decodeObjectForKey:@"kESModifiedAt"];
        self.attendeeCanBrowsePages = [aDecoder decodeBoolForKey:@"kESAttendeeCanBrowsePages"];
        self.attendeeCanAskQuestions = [aDecoder decodeBoolForKey:@"kESAttendeeCanAskQuestions"];
        self.currentPage = [aDecoder decodeIntegerForKey:@"kESCurrentPage"];
        self.videos = [aDecoder decodeObjectForKey:@"kESVideos"];
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.fileName forKey:@"kESFileName"];
    [aCoder encodeObject:self.lastModifiedAt forKey:@"kESModifiedAt"];
//    [aCoder encodeObject:self.fileSize forKey:@"kESFileSize"];

    [aCoder encodeInteger:self.currentPage forKey:@"kESCurrentPage"];
    [aCoder encodeBool:self.attendeeCanBrowsePages forKey:@"kESAttendeeCanBrowsePages"];
    [aCoder encodeBool:self.attendeeCanAskQuestions forKey:@"kESAttendeeCanAskQuestions"];
    
    if (self.videos) {
        [aCoder encodeObject:self.videos forKey:@"kESVideos"];
    }
}


+ (ESPresentationInfoRequestPacket *) packetWithFileName:(NSString *) fileName {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];
    
    NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:fileName];
    
    if (![fileManager fileExistsAtPath:filePath]) {
        [NSException raise:@"FileNotFoundException" format:@"Cannot find %@ under documents.",fileName];
    }
    
    ESPresentationInfoRequestPacket *packet = [[ESPresentationInfoRequestPacket alloc] init];
    packet.fileName = fileName;
    
    //    NSError *error = nil;
    NSDictionary *fileAttributes = [fileManager attributesOfItemAtPath:filePath error:nil];
    //    packet.fileSize = [fileAttributes valueForKey:NSFileSize];
    packet.lastModifiedAt = [fileAttributes valueForKey:NSFileModificationDate];
    packet.attendeeCanAskQuestions = [[NSUserDefaults standardUserDefaults] boolForKey:KEY_ATTENDEE_CAN_QUERY];
    packet.attendeeCanBrowsePages = [[NSUserDefaults standardUserDefaults] boolForKey:KEY_ATTENDEE_CAN_BROWSE];
    
    return packet;
}

- (NSString *) description {
    return [NSString stringWithFormat:@"fileName: %@, attendeeCanAskQuestions: %@, attendeeCanBrowsePages: %@", self.fileName, self.attendeeCanAskQuestions?@"YES":@"NO", self.attendeeCanBrowsePages?@"YES":@"NO"];
}
@end
