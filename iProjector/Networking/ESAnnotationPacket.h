//
//  ESAnnotationPacket.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/2/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAbstractPagePacket.h"

typedef NS_ENUM (NSInteger,ESAnnotationType){
    ESAnnotationTypeStart = 0,
    ESAnnotationTypeMove,
    ESAnnotationTypeEnd,
    ESAnnotationTypeUndo,
    ESAnnotationTypeClear,
    ESAnnotationTypeHighlightStart,
    ESAnnotationTypeHighlightMove,
    ESAnnotationTypeHighlightEnd,
} ;

@interface ESAnnotationPacket : ESAbstractPagePacket

@property(nonatomic) ESAnnotationType annotationType;
@property(nonatomic) CGPoint point;
@property(nonatomic) NSInteger lineWidth;
@property(nonatomic, strong) UIColor *color;

+ (ESAnnotationPacket *) packetWithScaledPoint:(CGPoint) point lineWidth:(NSInteger)lineWidth andColor:(UIColor *)color annotationType:(ESAnnotationType) annotationType;
+ (ESAnnotationPacket *) undoPacket;
+ (ESAnnotationPacket *) clearPacket;
@end