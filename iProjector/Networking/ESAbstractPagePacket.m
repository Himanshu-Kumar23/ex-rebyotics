//
//  ESAbstractPagePacket.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/5/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAbstractPagePacket.h"

@implementation ESAbstractPagePacket

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.pageNo = [aDecoder decodeIntegerForKey:@"kESPagePacketPageNo"];
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeInteger:self.pageNo forKey:@"kESPagePacketPageNo"];
}

@end
