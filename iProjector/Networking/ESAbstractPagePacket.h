//
//  ESAbstractPagePacket.h
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/5/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAbstractPacket.h"

@interface ESAbstractPagePacket : ESAbstractPacket

@property (nonatomic) NSInteger pageNo;

@end
