//
//  ESPDFPacket.m
//  iProjector
//
//  Created by Manoj Katragadda on 30/08/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESPDFPacket.h"
#import "ESSettingListViewController.h"
//Private

@implementation ESPDFPacket

- (void) encodeWithCoder:(NSCoder *)aCoder {
    
    [super encodeWithCoder:aCoder];
    [aCoder encodeInteger:self.pdfPacketType forKey:@"kESPdfPacketType"];
    
    if (self.pdfPacketType == ESPDFPacketTypeHeader) {
        [aCoder encodeObject:self.fileSize forKey:@"kESFileSize"];
    } else if (self.pdfPacketType == ESPDFPacketTypeChunk){
        [aCoder encodeObject:self.chunk forKey:@"kESPDFChunk"];
    } else {
        [aCoder encodeObject:self.fileName forKey:@"kESFileName"];
        [aCoder encodeObject:self.lastModifiedAt forKey:@"kESLastModifiedAt"];

        [aCoder encodeBool:self.attendeeCanBrowsePages forKey:@"kESAttendeeCanBrowsePages"];
        [aCoder encodeBool:self.attendeeCanAskQuestions forKey:@"kESAttendeeCanAskQuestions"];
    }
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.pdfPacketType = [aDecoder decodeIntegerForKey:@"kESPdfPacketType"];
        if (self.pdfPacketType == ESPDFPacketTypeHeader) {
            self.fileSize = [aDecoder decodeObjectForKey:@"kESFileSize"];
        } else if (self.pdfPacketType == ESPDFPacketTypeChunk){
            self.chunk = [aDecoder decodeObjectForKey:@"kESPDFChunk"];
        } else {
            self.fileName = [aDecoder decodeObjectForKey:@"kESFileName"];
            self.lastModifiedAt = [aDecoder decodeObjectForKey:@"kESLastModifiedAt"];
            self.attendeeCanBrowsePages = [aDecoder decodeBoolForKey:@"kESAttendeeCanBrowsePages"];
            self.attendeeCanAskQuestions = [aDecoder decodeBoolForKey:@"kESAttendeeCanAskQuestions"];
        }
    }
    return self;
}

- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypePDFTranser;
    }
    return self;
}

+ (ESPDFPacket *) pdfPacketForData:(NSData *)chunkData {
    
    ESPDFPacket *packet = [[ESPDFPacket alloc] init];
    packet.packetType = ESPacketTypePDFTranser;
    packet.chunk = chunkData;
    return packet;
}

+(ESPDFPacket *) pdfHeaderPacketForFileName:(NSString *) fileName {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];

    NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:fileName];
    
    if (![fileManager fileExistsAtPath:filePath]) {
        [NSException raise:@"FileNotFoundException" format:@"Cannot find %@ under documents.",fileName];
    }
    
    ESPDFPacket *packet = [[ESPDFPacket alloc] init];
    packet.pdfPacketType = ESPDFPacketTypeHeader;
    packet.fileName = fileName;
    
//    NSError *error = nil;
    NSDictionary *fileAttributes = [fileManager attributesOfItemAtPath:filePath error:nil];
    packet.fileSize = [fileAttributes valueForKey:NSFileSize];
    packet.lastModifiedAt = [fileAttributes valueForKey:NSFileModificationDate];
    return packet;
}

+ (ESPDFPacket *) pdfFooterPacketForFileName:(NSString *) fileName {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];
    
    NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:fileName];
    
    if (![fileManager fileExistsAtPath:filePath]) {
        [NSException raise:@"FileNotFoundException" format:@"Cannot find %@ under documents.",fileName];
    }
    
    ESPDFPacket *packet = [[ESPDFPacket alloc] init];
    packet.pdfPacketType = ESPDFPacketTypeFooter;
    packet.fileName = fileName;
    
    packet.attendeeCanAskQuestions = [[NSUserDefaults standardUserDefaults] boolForKey:KEY_ATTENDEE_CAN_QUERY];
    packet.attendeeCanBrowsePages = [[NSUserDefaults standardUserDefaults] boolForKey:KEY_ATTENDEE_CAN_BROWSE];
    
    //    NSError *error = nil;
    NSDictionary *fileAttributes = [fileManager attributesOfItemAtPath:filePath error:nil];
    packet.fileSize = [fileAttributes valueForKey:NSFileSize];
    packet.lastModifiedAt = [fileAttributes valueForKey:NSFileModificationDate];
    return packet;
}

/*
-(void)_createChunkData{
	
	int numItems = ceil((double)chunk.length / _chunkLength);
	_chunkDataContainer = [[NSMutableArray alloc] initWithCapacity:numItems];
	
	for (int i=0; i<numItems; i++){
		NSUInteger start = i * _chunkLength;
		NSUInteger length = _chunkLength;
		NSUInteger end = start + length;
		
		//The chunk position exceeds the range of the source data.
		if (end > chunk.length){
			length = chunk.length % _chunkLength;
		}
		
		NSRange range = {start, length};
		NSData *chunkData = [chunk subdataWithRange:range];
		
		[_chunkDataContainer addObject:chunkData];
	}
}

-(NSInteger)chunkDataCount{
    
    return [_chunkDataContainer count];
}
-(NSData *)chunkDataAtIndex:(NSUInteger)index{
	return (NSData *)[_chunkDataContainer objectAtIndex:index];
}
*/
@end
