//
//  ESWhiteBoardPacket.h
//  iProjector
//
//  Created by Jayaprada Behera on 18/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESAbstractPacket.h"
@interface ESWhiteBoardPacket : ESAbstractPacket
+(ESWhiteBoardPacket *) packectToGoWhiteBoard;
@end
