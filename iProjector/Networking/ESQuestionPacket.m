//
//  ESQuestionPacket.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 8/12/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESQuestionPacket.h"

@implementation ESQuestionPacket

- (id) init {
    self = [super init];
    if (self) {
        self.packetType = ESPacketTypeQuestion;
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.question forKey:@"kESQuestion"];
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.question = [aDecoder decodeObjectForKey:@"kESQuestion"];
    }
    return self;
}

+ (ESQuestionPacket *) packetWithQuestion:(ESQuestion *) question {
    ESQuestionPacket *packet = [[ESQuestionPacket alloc] init];
    packet.question = question;
    return packet;
}

@end
