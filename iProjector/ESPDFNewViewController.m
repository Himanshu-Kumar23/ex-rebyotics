//
//  ESPDFNewViewController.m
//  iProjector
//
//  Created by WebileApps on 09/07/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESPDFNewViewController.h"
#import "ReaderViewController.h"

@interface ESPDFNewViewController () <ReaderViewControllerDelegate>

@end

@implementation ESPDFNewViewController
@synthesize fileName,readerViewController;



-(id) initWithFileName:(NSString *) aFileName
{
    if (self = [super init])
    {
        self.fileName = aFileName;
        [self setupReaderDocument];
    }
    return self;
}

-(void)setupReaderDocument
{
    NSString *phrase = nil; // Document password (for unlocking most encrypted PDF files)
    
	NSArray *pdfs = [[NSBundle mainBundle] pathsForResourcesOfType:@"pdf" inDirectory:nil];
    
	NSString *filePath = [pdfs lastObject]; assert(filePath != nil); // Path to last PDF file
    
	ReaderDocument *document = [ReaderDocument unarchiveFromFileName:fileName password:phrase];
    if (document != nil) // Must have a valid ReaderDocument object in order to proceed
	{
		readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        
		readerViewController.delegate = self;
        [[readerViewController view] setFrame:self.view.frame];
        [self.view addSubview:readerViewController.view];
        
        //Loading first page of PDF
        [readerViewController showDocumentPage:1];
    }
}

-(void)thumbButtonTouchUpInside:(id) sender
{
    
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setupthumbsForNavigationBar];
}

-(void)setupthumbsForNavigationBar
{
    UIBarButtonItem *thumbsButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(thumbsButtonTouchUpInside:)];
    [thumbsButton setTitle:@"Thumbs"];
    self.navigationController.navigationItem.rightBarButtonItem = thumbsButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)thumbsButtonTouchUpInside:(UIButton *)button
{
	//[delegate tappedInToolbar:self thumbsButton:button];
}


@end
