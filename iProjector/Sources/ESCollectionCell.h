//
//  ESCollectionCell.h
//  iProjector
//
//  Created by Manoj Katragadda on 10/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class ESCollectionCell;

@protocol DeleteCollectionCellDelegate <NSObject>

- (void) showButtonToDeleteCell:(ESCollectionCell *) collectionCell;

@end


@interface ESCollectionCell : UICollectionViewCell

@property(nonatomic,strong) IBOutlet UIButton *deleteButton;
@property(nonatomic,strong) IBOutlet UILabel *textLabel;
@property(nonatomic,strong) IBOutlet UIImageView *coverImageView;
@property(nonatomic,strong) IBOutlet UIView *deleteOverLay;

@property(nonatomic, unsafe_unretained) id<DeleteCollectionCellDelegate> deleteCellDelegate;

-(IBAction)deleteCell:(id)sender;
-(IBAction)overlayDeleteButtonTouchInside:(id)sender;
@end
