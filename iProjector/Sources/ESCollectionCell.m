//
//  ESCollectionCell.m
//  iProjector
//
//  Created by Manoj Katragadda on 10/09/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESCollectionCell.h"

@implementation ESCollectionCell
@synthesize textLabel;
@synthesize deleteButton;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        self.backgroundColor = [UIColor colorWithRed:39/255.f green:154/255.f blue:224/255.f alpha:1.0f];
        self.textLabel.backgroundColor = [UIColor clearColor];
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"ESCollectionCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];

        // Initialization code
    }
    return self;
}
-(IBAction)deleteCell:(id)sender{
    if (self.deleteCellDelegate) {
        [self.deleteCellDelegate showButtonToDeleteCell:self];
    }
}

-(IBAction)overlayDeleteButtonTouchInside:(id)sender{
    if (self.deleteCellDelegate) {
        [self.deleteCellDelegate showButtonToDeleteCell:self];
    }
}
-(void)awakeFromNib{
    [super awakeFromNib];
    CALayer * layer = [self.coverImageView layer];
    
    // border radius
    [layer setCornerRadius:6.0f];
    
    // border
    [layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [layer setBorderWidth:1.0f];
    
    // drop shadow
    [layer setShadowColor:[UIColor redColor].CGColor];
    [layer setShadowOpacity:0.8];
    [layer setShadowRadius:3.0];
    [layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
