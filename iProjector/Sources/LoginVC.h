//
//  LoginVC.h
//  X Detailer
//
//  Created by Gorilla Expense on 3/24/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnderlinedTextField.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginVC : UIViewController
{
    IBOutlet UnderlinedTextField *textField_email;
    IBOutlet UnderlinedTextField *textField_password;
    
}
@property (strong, nonatomic) UIWindow *window;

- (IBAction)SignInButtonAction:(id)sender;
- (IBAction)signUpButtonAction:(id)sender;


@end

NS_ASSUME_NONNULL_END
