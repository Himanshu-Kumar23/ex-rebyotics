//
//	ReaderViewController.h
//	Reader v2.6.0
//
//	Created by Julius Oklamcak on 2011-07-01.
//	Copyright © 2011-2013 Julius Oklamcak. All rights reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files (the "Software"), to deal
//	in the Software without restriction, including without limitation the rights to
//	use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
//	of the Software, and to permit persons to whom the Software is furnished to
//	do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in all
//	copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
//	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//	CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#define TAP_AREA_SIZE 160.0f

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>
#import <QuartzCore/QuartzCore.h>

#import "ReaderDocument.h"
#import "ReaderMainPagebar.h"
#import "ESSettingListViewController.h"
@class ReaderViewController;

@protocol ReaderViewControllerDelegate <NSObject>

@optional // Delegate protocols

- (void)dismissReaderViewController:(ReaderViewController *)viewController;

@end

@class ReaderMainToolbar;
@class ESReaderSliderPageBar;

@interface ReaderViewController : UIViewController<UIScrollViewDelegate,GKPeerPickerControllerDelegate>
{
    BOOL isOriented;
}

@property (nonatomic, unsafe_unretained, readwrite) id <ReaderViewControllerDelegate> delegate;

@property (nonatomic, strong) ReaderDocument *document;

@property (nonatomic, strong) UIScrollView *theScrollView;

@property (nonatomic, strong) ReaderMainToolbar *mainToolbar;

@property (nonatomic, strong) ReaderMainPagebar *mainPagebar;

@property (nonatomic, strong) NSMutableDictionary *contentViews;

@property (nonatomic, strong) UIPrintInteractionController *printInteraction;

@property (nonatomic, assign) NSInteger currentPage;

@property (nonatomic, assign) CGSize lastAppearSize;

@property (nonatomic, strong) NSDate *lastHideTime;

@property (nonatomic, assign) BOOL isVisible;

@property (nonatomic, strong) UICollectionView *colPages;

@property long selectedIndexRow;


@property (weak, nonatomic) IBOutlet UILabel *counterLabel;
@property int counterValue;
//@property (nonatomic)BOOL isNextPageValue;

- (id)initWithReaderDocument:(ReaderDocument *)object;
- (void)showDocumentPage:(NSInteger)page;


- (void)incrementPageNumber;
- (void)decrementPageNumber;

-(void)handleSingleTap:(UITapGestureRecognizer *)gesture;
-(void)didRotate:(NSNotification*)notification;

@end
