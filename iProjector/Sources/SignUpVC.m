//
//  SignUpVC.m
//  X Detailer
//
//  Created by Vikramjeet Singh on 24/03/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//

#import "SignUpVC.h"

@interface SignUpVC ()

@end

@implementation SignUpVC

- (void)viewDidLoad {
    
    self.navigationController.navigationBarHidden = NO;
    
    self.title = @"SIGN UP";

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


- (IBAction)SubmitButtonAction:(id)sender
{
    if ([textField_username.text isEqualToString:@""] || [textfield_phoneNumber.text isEqualToString:@""] || [textfield_email.text isEqualToString:@""] || [textfield_password.text isEqualToString:@""] || [textfield_confirmpassword.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Empty fields" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if (![textfield_password.text isEqualToString:textfield_confirmpassword.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Password should match to confirm password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }


}
@end
