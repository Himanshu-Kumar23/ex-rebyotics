//
//  LoginVC.m
//  X Detailer
//
//  Created by Gorilla Expense on 3/24/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//

#import "LoginVC.h"
#import "SignUpVC.h"
#import "CircleMenuViewController.h"
@interface LoginVC ()

@end

@implementation LoginVC

- (void)viewDidLoad {
    
    self.navigationController.navigationBarHidden = NO;
    
    self.title = @"LOGIN";

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (BOOL)shouldAutorotate{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    //        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
    //            return UIInterfaceOrientationMaskLandscape;
    //        }else{
    //    return UIInterfaceOrientationMaskAll;
    return UIInterfaceOrientationMaskLandscape;
    //        }
    
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        return UIInterfaceOrientationLandscapeRight;
    }
    return UIInterfaceOrientationPortrait;
    
}


- (IBAction)SignInButtonAction:(id)sender
{
//    if ([textField_email.text isEqualToString:@""] || [textField_password.text isEqualToString:@""])
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Empty fields" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//        return;
//    }
    
    CircleMenuViewController * circleMenuViewController;
    circleMenuViewController = [[CircleMenuViewController alloc] init];
    // Set the cricle menu vc as the root vc
    //    UINavigationController * navigationController = [[UINavigationController alloc] init];
    //   (void) [navigationController initWithRootViewController:circleMenuViewController];
    //[navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
    
    NSString *deviceType = [UIDevice currentDevice].model;
    CGRect myImageRect;
    UIImageView *image ;
    
    if([deviceType isEqualToString:@"iPad Simulator"]||[deviceType isEqualToString:@"iPad"]){
        //        (void) [circleMenuViewController initWithButtonCount:kKYCCircleMenuButtonsCount
        //                                                    menuSize:kKYCircleMenuSize
        //                                                  buttonSize:kKYCircleMenuButtonSize
        //                                       buttonImageNameFormat:kKYICircleMenuButtonImageNameFormat
        //                                            centerButtonSize:kKYCircleMenuCenterButtonSize
        //                                       centerButtonImageName:nil
        //                             centerButtonBackgroundImageName:kKYICircleMenuCenterButtonBackground];
        //        myImageRect = CGRectMake(300, 0, 440, 280);
        //        image= [[UIImageView alloc] initWithFrame:myImageRect];
        //        [image setImage:[UIImage imageNamed:@"Detailer.png"]];
        
        
    }else{
        // Setup circle menu with basic configuration
        //        (void) [circleMenuViewController initWithButtonCount:kKYCCircleMenuButtonsCount
        //                                                    menuSize:kKYCircleMenuSize_iPhone
        //                                                  buttonSize:kKYCircleMenuButtonSize_iPhone
        //                                       buttonImageNameFormat:kKYICircleMenuButtonImageNameFormat_iPhone
        //                                            centerButtonSize:kKYCircleMenuCenterButtonSize_iPhone
        //                                       centerButtonImageName:nil
        //                             centerButtonBackgroundImageName:kKYICircleMenuCenterButtonBackground_iPhone];
        //        CGFloat  x_Origin;
        //
        //        if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(@"7.1") && ([machineName() isEqualToString:@"iPhone3,1"]||[machineName() isEqualToString:@"iPhone4,1"])) {
        //
        //            x_Origin =([UIScreen mainScreen].bounds.size.width-230)/2;
        //        }else{
        //            x_Origin =([UIScreen mainScreen].bounds.size.height-230)/2;
        //        }
        //        myImageRect = CGRectMake(x_Origin, 0, 230, 120);
        //        image = [[UIImageView alloc] initWithFrame:myImageRect];
        //        [image setImage:[UIImage imageNamed:@"Detailer_iPhone.png"]];
        
    }
    [circleMenuViewController.view addSubview:image];
    [circleMenuViewController.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationController pushViewController:circleMenuViewController animated:YES];
}

- (IBAction)signUpButtonAction:(id)sender
{
    SignUpVC *signVC;
    NSString *device = [UIDevice currentDevice].model;
    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"])
    {
        signVC =[[SignUpVC alloc] initWithNibName:@"SignUpVC_iPad" bundle:nil];
    }
    else
    {
        signVC =[[SignUpVC alloc] initWithNibName:@"SignUpVC" bundle:nil];
    }

    [self.navigationController pushViewController:signVC animated:YES];

}
@end
