//
//	ReaderViewController.m
//	Reader v2.7.2
//
//	Created by Julius Oklamcak on 2011-07-01.
//	Copyright © 2011-2013 Julius Oklamcak. All rights reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files (the "Software"), to deal
//	in the Software without restriction, including without limitation the rights to
//	use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
//	of the Software, and to permit persons to whom the Software is furnished to
//	do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in all
//	copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
//	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//	CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import "ReaderConstants.h"
#import "ReaderViewController.h"
#import "ThumbsViewController.h"
#import "ReaderMainToolbar.h"
#import "ReaderMainPagebar.h"
#import "ReaderContentView.h"
#import "ReaderThumbCache.h"
#import "ReaderThumbQueue.h"
#import "UIImage+PDF.h"

#import <MessageUI/MessageUI.h>

@interface ReaderViewController () <UIScrollViewDelegate, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate,
									ReaderMainToolbarDelegate, ReaderMainPagebarDelegate, ReaderContentViewDelegate, ThumbsViewControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSString *OrientationName;
    BOOL isHide;
    BOOL isViewLoaded;

}
@end

@implementation ReaderViewController

#pragma mark Constants

#define PAGING_VIEWS 3

#define TOOLBAR_HEIGHT 44.0f
#define PAGEBAR_HEIGHT 76.0f

#define MAX_VALUE     10000

#pragma mark Properties

@synthesize delegate;
@synthesize document,theScrollView,mainPagebar,mainToolbar,contentViews,printInteraction;
@synthesize currentPage,lastAppearSize,lastHideTime,isVisible,colPages,selectedIndexRow;
#pragma mark Support methods

- (void)updateScrollViewContentSize
{
	NSInteger count = [document.pageCount integerValue];

	if (count > PAGING_VIEWS) count = PAGING_VIEWS; // Limit

	CGFloat contentHeight = theScrollView.bounds.size.height;

	CGFloat contentWidth = (theScrollView.bounds.size.width * count);

	theScrollView.contentSize = CGSizeMake(contentWidth, contentHeight);
}

- (void)updateScrollViewContentViews
{
	[self updateScrollViewContentSize]; // Update the content size

	NSMutableIndexSet *pageSet = [NSMutableIndexSet indexSet]; // Page set

	[contentViews enumerateKeysAndObjectsUsingBlock: // Enumerate content views
		^(id key, id object, BOOL *stop)
		{
			ReaderContentView *contentView = object; [pageSet addIndex:contentView.tag];
		}
	];

	__block CGRect viewRect = CGRectZero; viewRect.size = theScrollView.bounds.size;

	__block CGPoint contentOffset = CGPointZero; NSInteger page = [document.pageNumber integerValue];

	[pageSet enumerateIndexesUsingBlock: // Enumerate page number set
		^(NSUInteger number, BOOL *stop)
		{
			NSNumber *key = [NSNumber numberWithInteger:number]; // # key

			ReaderContentView *contentView = [contentViews objectForKey:key];

			contentView.frame = viewRect; if (page == number) contentOffset = viewRect.origin;

			viewRect.origin.x += viewRect.size.width; // Next view frame position
		}
	];

	if (CGPointEqualToPoint(theScrollView.contentOffset, contentOffset) == false)
	{
		theScrollView.contentOffset = contentOffset; // Update content offset
	}
}

- (void)updateToolbarBookmarkIcon
{
	NSInteger page = [document.pageNumber integerValue];

	BOOL bookmarked = [document.bookmarks containsIndex:page];

	[mainToolbar setBookmarkState:bookmarked]; // Update
}

- (void)showDocumentPage:(NSInteger)page
{
	if (page != currentPage) // Only if different
	{
        [colPages reloadData];
		NSInteger minValue; NSInteger maxValue;
		NSInteger maxPage = [document.pageCount integerValue];
		NSInteger minPage = 1;

		if ((page < minPage) || (page > maxPage)) return;

		if (maxPage <= PAGING_VIEWS) // Few pages
		{
			minValue = minPage;
			maxValue = maxPage;
		}
		else // Handle more pages
		{
			minValue = (page - 1);
			maxValue = (page + 1);

			if (minValue < minPage)
				{minValue++; maxValue++;}
			else
				if (maxValue > maxPage)
					{minValue--; maxValue--;}
		}

		NSMutableIndexSet *newPageSet = [NSMutableIndexSet new];

		NSMutableDictionary *unusedViews = [contentViews mutableCopy];

		CGRect viewRect = CGRectZero; viewRect.size = theScrollView.bounds.size;

		for (NSInteger number = minValue; number <= maxValue; number++)
		{
			NSNumber *key = [NSNumber numberWithInteger:number]; // # key

			ReaderContentView *contentView = [contentViews objectForKey:key];

			if (contentView == nil) // Create a brand new document content view
			{
				NSURL *fileURL = document.fileURL; NSString *phrase = document.password; // Document properties

                contentView = [self contentViewWithFrame:viewRect fileURL:fileURL page:number password:phrase];
//				contentView = [[ReaderContentView alloc] initWithFrame:viewRect fileURL:fileURL page:number password:phrase];

				[theScrollView addSubview:contentView]; [contentViews setObject:contentView forKey:key];

				contentView.message = self; [newPageSet addIndex:number];
			}
			else // Reposition the existing content view
			{
				contentView.frame = viewRect; [contentView zoomReset];

				[unusedViews removeObjectForKey:key];
			}

			viewRect.origin.x += viewRect.size.width;
		}

		[unusedViews enumerateKeysAndObjectsUsingBlock: // Remove unused views
			^(id key, id object, BOOL *stop)
			{
				[contentViews removeObjectForKey:key];

				ReaderContentView *contentView = object;

				[contentView removeFromSuperview];
			}
		];

		unusedViews = nil; // Release unused views

		CGFloat viewWidthX1 = viewRect.size.width;
		CGFloat viewWidthX2 = (viewWidthX1 * 2.0f);

		CGPoint contentOffset = CGPointZero;

		if (maxPage >= PAGING_VIEWS)
		{
			if (page == maxPage)
				contentOffset.x = viewWidthX2;
			else
				if (page != minPage)
					contentOffset.x = viewWidthX1;
		}
		else
			if (page == (PAGING_VIEWS - 1))
				contentOffset.x = viewWidthX1;

		if (CGPointEqualToPoint(theScrollView.contentOffset, contentOffset) == false)
		{
			theScrollView.contentOffset = contentOffset; // Update content offset
		}

		if ([document.pageNumber integerValue] != page) // Only if different
		{
			document.pageNumber = [NSNumber numberWithInteger:page]; // Update page number
		}

		NSURL *fileURL = document.fileURL; NSString *phrase = document.password; NSString *guid = document.guid;

		if ([newPageSet containsIndex:page] == YES) // Preview visible page first
		{
			NSNumber *key = [NSNumber numberWithInteger:page]; // # key

			ReaderContentView *targetView = [contentViews objectForKey:key];

			[targetView showPageThumb:fileURL page:page password:phrase guid:guid];

			[newPageSet removeIndex:page]; // Remove visible page from set
		}

		[newPageSet enumerateIndexesWithOptions:NSEnumerationReverse usingBlock: // Show previews
			^(NSUInteger number, BOOL *stop)
			{
				NSNumber *key = [NSNumber numberWithInteger:number]; // # key

				ReaderContentView *targetView = [contentViews objectForKey:key];

				[targetView showPageThumb:fileURL page:number password:phrase guid:guid];
			}
		];

		newPageSet = nil; // Release new page set

		[mainPagebar updatePagebar]; // Update the pagebar display

		[self updateToolbarBookmarkIcon]; // Update bookmark

        UIImage *image = [mainPagebar getImageFromPage:1];
        UIDeviceOrientation orientation= [[UIDevice currentDevice] orientation];
        if(!isOriented && !UIDeviceOrientationIsLandscape(orientation) && (image.size.width>image.size.height))
        {
            currentPage=1;
            isOriented=YES;
            OrientationName=@"Landscape";
            [[UIDevice currentDevice] setValue:@(UIDeviceOrientationLandscapeLeft) forKey:@"orientation"];
            [UINavigationController attemptRotationToDeviceOrientation];
        }
        else if (!isOriented && !UIDeviceOrientationIsPortrait(orientation) &&  image.size.width<image.size.height) {
            currentPage=1;
            isOriented=YES;
            OrientationName=@"Portrait";
            [[UIDevice currentDevice] setValue:@(UIDeviceOrientationPortrait) forKey:@"orientation"];
            [UINavigationController attemptRotationToDeviceOrientation];
        }
        else
        {
            
        }
        

        currentPage = page; // Track current page number
	}
}

- (void)showDocument:(id)object
{
	[self updateScrollViewContentSize]; // Set content size

	[self showDocumentPage:[document.pageNumber integerValue]];

	document.lastOpen = [NSDate date]; // Update last opened date

	isVisible = YES; // iOS present modal bodge
}

#pragma mark UIViewController methods

- (id)initWithReaderDocument:(ReaderDocument *)object
{
	id reader = nil; // ReaderViewController object

	if ((object != nil) && ([object isKindOfClass:[ReaderDocument class]]))
	{
		if ((self = [super initWithNibName:nil bundle:nil])) // Designated initializer
		{
			NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];

			[notificationCenter addObserver:self selector:@selector(applicationWill:) name:UIApplicationWillTerminateNotification object:nil];

			[notificationCenter addObserver:self selector:@selector(applicationWill:) name:UIApplicationWillResignActiveNotification object:nil];

			[object updateProperties]; document = object; // Retain the supplied ReaderDocument object for our use

			[ReaderThumbCache touchThumbCacheWithGUID:object.guid]; // Touch the document thumb cache directory

			reader = self; // Return an initialized ReaderViewController object
		}
	}

	return reader;
}
-(void)ISViewLoaded
{
    isViewLoaded = YES;
    
}

-(void)didRotate:(NSNotification*)notification
{
    if (isViewLoaded == YES)
    {
        return;
    }

    UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
    CGFloat constantVal=0;
    if (UIDeviceOrientationIsPortrait(orientation)) {
        constantVal = isHide?0:PAGEBAR_HEIGHT;
    }

    CGRect scrollViewRect;
    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                            (self.navigationController.navigationBar.frame.size.height ?: 0.0));

    NSString *deviceType = [UIDevice currentDevice].model;

    if (UIDeviceOrientationIsPortrait(orientation)) {
        
        if([deviceType isEqualToString:@"iPhone Simulator"]|| [deviceType isEqualToString:@"iPhone"] || [deviceType isEqualToString:@"iPod touch"]){
            
            scrollViewRect = CGRectMake(0,topbarHeight, self.view.bounds.size.width,self.view.bounds.size.height-constantVal-topbarHeight);
        }else {
            scrollViewRect =  CGRectMake(0,topbarHeight, self.view.bounds.size.width,self.view.bounds.size.height-constantVal-topbarHeight);
        }
    }
    else
    {
        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
            scrollViewRect = CGRectMake(PAGEBAR_HEIGHT+30,topbarHeight, self.view.bounds.size.width-(2*76),self.view.bounds.size.height-topbarHeight);
        }
        else
        {
            scrollViewRect = CGRectMake(PAGEBAR_HEIGHT,topbarHeight, self.view.bounds.size.width-(2*76),self.view.bounds.size.height-topbarHeight);
        }

    }

    theScrollView.frame=scrollViewRect;
    
    CGRect toolbarRect = scrollViewRect; // Toolbar frame
    toolbarRect.size.height = TOOLBAR_HEIGHT; // Default toolbar height
    mainToolbar.frame=toolbarRect;
    
    CGRect pagebarRect = scrollViewRect; // Pagebar frame
    pagebarRect.origin.y = (scrollViewRect.size.height - PAGEBAR_HEIGHT);
    pagebarRect.size.height = PAGEBAR_HEIGHT; // Default pagebar height
    mainPagebar.frame=pagebarRect;

    
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)[colPages collectionViewLayout];
    
    if (UIDeviceOrientationIsPortrait(orientation) && [OrientationName isEqualToString:@"Portrait"]) {
        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
            colPages.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-PAGEBAR_HEIGHT-34, [UIScreen mainScreen].bounds.size.width,PAGEBAR_HEIGHT);
        }
        else
        {
            colPages.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-PAGEBAR_HEIGHT, [UIScreen mainScreen].bounds.size.width,PAGEBAR_HEIGHT);
        }
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    else if (UIDeviceOrientationIsLandscape(orientation) && [OrientationName isEqualToString:@"Landscape"])
    {
        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
            colPages.frame=CGRectMake(30, topbarHeight,PAGEBAR_HEIGHT,[UIScreen mainScreen].bounds.size.height-topbarHeight);
            NSLog(@"%f,%f,%f,%f",colPages.frame.size.height,colPages.frame.size.width,colPages.frame.origin.x,colPages.frame.origin.x);
        }
        else
        {
            colPages.frame=CGRectMake(0, topbarHeight,PAGEBAR_HEIGHT,[UIScreen mainScreen].bounds.size.height-topbarHeight);
            NSLog(@"landscape : %f,%f,%f,%f",colPages.frame.size.height,colPages.frame.size.width,colPages.frame.origin.x,colPages.frame.origin.x);
        }
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }

    [mainToolbar hideToolbar];
    [mainPagebar hidePagebar];

    [self updateScrollViewContentViews]; // Update content views

    [self showDocumentPage:currentPage];
}
- (void)viewDidLoad
{
	[super viewDidLoad];
    
    isOriented=NO;
    isViewLoaded = NO;
    [self performSelector:@selector(ISViewLoaded) withObject:self afterDelay:5.0];

	assert(document != nil); // Must have a valid ReaderDocument

	self.view.backgroundColor = [UIColor grayColor]; // Neutral gray

	CGRect scrollViewRect = self.view.bounds;
//    UIView *fakeStatusBar = nil;

//	if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) // iOS 7+
//	{
//		if ([self prefersStatusBarHidden] == NO) // Visible status bar
//		{
//			CGRect statusBarRect = self.view.bounds; // Status bar frame
//			statusBarRect.size.height = STATUS_HEIGHT; // Default status height
//			fakeStatusBar = [[UIView alloc] initWithFrame:statusBarRect]; // UIView
//			fakeStatusBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//			fakeStatusBar.backgroundColor = [UIColor blackColor];
//			fakeStatusBar.contentMode = UIViewContentModeRedraw;
//			fakeStatusBar.userInteractionEnabled = NO;
//
//			scrollViewRect.origin.y += STATUS_HEIGHT; scrollViewRect.size.height -= STATUS_HEIGHT;
//		}
//	}

    UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];

    NSString *deviceType = [UIDevice currentDevice].model;
    
    CGFloat constantVal=0;
    if (UIDeviceOrientationIsPortrait(orientation)) {
        constantVal = isHide?0:PAGEBAR_HEIGHT;
    }

    if (UIDeviceOrientationIsPortrait(orientation)) {

    if([deviceType isEqualToString:@"iPhone Simulator"]|| [deviceType isEqualToString:@"iPhone"] || [deviceType isEqualToString:@"iPod touch"]){
        
        scrollViewRect = CGRectMake(0,0, self.view.bounds.size.width,self.view.bounds.size.height-constantVal);
    }else {
        scrollViewRect =  CGRectMake(0,0, self.view.bounds.size.width,self.view.bounds.size.height-constantVal);
    }
    }
    else
    {
        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
            scrollViewRect = CGRectMake(PAGEBAR_HEIGHT+30,0, self.view.bounds.size.width-(2*76),self.view.bounds.size.height);
        }
        else
        {
            scrollViewRect = CGRectMake(PAGEBAR_HEIGHT,0, self.view.bounds.size.width-(2*76),self.view.bounds.size.height);
        }
    }
    
        
	theScrollView = [[UIScrollView alloc] initWithFrame:scrollViewRect]; // UIScrollView
	theScrollView.autoresizesSubviews = NO;
    theScrollView.contentMode = UIViewContentModeRedraw;
	theScrollView.showsHorizontalScrollIndicator = NO;
    theScrollView.showsVerticalScrollIndicator = NO;
	theScrollView.scrollsToTop = NO;
    theScrollView.delaysContentTouches = NO;
    theScrollView.pagingEnabled = YES;
	theScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	theScrollView.backgroundColor = [UIColor clearColor];
    theScrollView.delegate = self;
    theScrollView.userInteractionEnabled = NO;
    theScrollView.scrollEnabled = NO;
//    theScrollView.userInteractionEnabled = YES;
//    theScrollView.scrollEnabled = YES;
	[self.view addSubview:theScrollView];

	CGRect toolbarRect = scrollViewRect; // Toolbar frame
	toolbarRect.size.height = TOOLBAR_HEIGHT; // Default toolbar height
	mainToolbar = [[ReaderMainToolbar alloc] initWithFrame:toolbarRect document:document]; // ReaderMainToolbar
	mainToolbar.delegate = self; // ReaderMainToolbarDelegate
//	[self.view addSubview:mainToolbar];

	CGRect pagebarRect = scrollViewRect; // Pagebar frame
	pagebarRect.origin.y = (scrollViewRect.size.height - PAGEBAR_HEIGHT);
	pagebarRect.size.height = PAGEBAR_HEIGHT; // Default pagebar height
	mainPagebar = [[ReaderMainPagebar alloc] initWithFrame:pagebarRect document:document]; // ReaderMainPagebar
	mainPagebar.delegate = self; // ReaderMainPagebarDelegate
	[self.view addSubview:mainPagebar];

//	if (fakeStatusBar != nil) [self.view addSubview:fakeStatusBar]; // Add status bar background view

	UITapGestureRecognizer *singleTapOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
	singleTapOne.numberOfTouchesRequired = 1; singleTapOne.numberOfTapsRequired = 1; singleTapOne.delegate = self;
	[self.view addGestureRecognizer:singleTapOne];

	UITapGestureRecognizer *doubleTapOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
	doubleTapOne.numberOfTouchesRequired = 1; doubleTapOne.numberOfTapsRequired = 2; doubleTapOne.delegate = self;
	[self.view addGestureRecognizer:doubleTapOne];

	UITapGestureRecognizer *doubleTapTwo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
	doubleTapTwo.numberOfTouchesRequired = 2; doubleTapTwo.numberOfTapsRequired = 2; doubleTapTwo.delegate = self;
	[self.view addGestureRecognizer:doubleTapTwo];

	[singleTapOne requireGestureRecognizerToFail:doubleTapOne]; // Single tap requires double tap to fail

    
    [self addPagesTableView];
    [mainToolbar hideToolbar];
    [mainPagebar hidePagebar];

	contentViews = [NSMutableDictionary new]; lastHideTime = [NSDate date];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:UIDeviceOrientationDidChangeNotification object:nil];

}


-(void)addPagesTableView {
    //    CGRect rect=[UIScreen mainScreen].bounds;
    //    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
    //                            (self.navigationController.navigationBar.frame.size.height ?: 0.0));
    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(32, 42);
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    colPages  = [[UICollectionView alloc] initWithFrame:CGRectMake(4, [UIScreen mainScreen].bounds.size.height-((IS_IPHONE)?84:124), [UIScreen mainScreen].bounds.size.width-8,PAGEBAR_HEIGHT) collectionViewLayout:flowLayout];
    colPages.backgroundColor=[UIColor colorWithRed:250/255.f green:250/255.f blue:250/255.f alpha:1.0];
    //    UICollectionView *colPages = [[UICollectionView alloc] initWithFrame:CGRectMake(4, topbarHeight, 100,[UIScreen mainScreen].bounds.size.height-topbarHeight)collectionViewLayout:];
    [colPages registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    [colPages setShowsHorizontalScrollIndicator:NO];
    [colPages setShowsVerticalScrollIndicator:NO];
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        if ([[UIScreen mainScreen] nativeBounds].size.height == 2436 || [[UIScreen mainScreen] nativeBounds].size.height ==2688 || [[UIScreen mainScreen] nativeBounds].size.height == 1792) {
            colPages.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-PAGEBAR_HEIGHT, [UIScreen mainScreen].bounds.size.width,PAGEBAR_HEIGHT);
        }
        else
        {
            colPages.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-PAGEBAR_HEIGHT, [UIScreen mainScreen].bounds.size.width,PAGEBAR_HEIGHT);
        }
    }
    
    NSString *device = [UIDevice currentDevice].model;
    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
        colPages.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-PAGEBAR_HEIGHT, [UIScreen mainScreen].bounds.size.width,PAGEBAR_HEIGHT);
    }
    
    UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
    
    
    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                            (self.navigationController.navigationBar.frame.size.height ?: 0.0));
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)[colPages collectionViewLayout];
    
    if (UIDeviceOrientationIsPortrait(orientation)) {
        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
            colPages.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-PAGEBAR_HEIGHT-34, [UIScreen mainScreen].bounds.size.width,PAGEBAR_HEIGHT);
        }
        else
        {
            colPages.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-PAGEBAR_HEIGHT, [UIScreen mainScreen].bounds.size.width,PAGEBAR_HEIGHT);
        }
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    else if (UIDeviceOrientationIsLandscape(orientation))
    {
        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
            colPages.frame=CGRectMake(30, topbarHeight,PAGEBAR_HEIGHT,[UIScreen mainScreen].bounds.size.height-topbarHeight);
            NSLog(@"%f,%f,%f,%f",colPages.frame.size.height,colPages.frame.size.width,colPages.frame.origin.x,colPages.frame.origin.x);
        }
        else
        {
            colPages.frame=CGRectMake(0, topbarHeight,PAGEBAR_HEIGHT,[UIScreen mainScreen].bounds.size.height-topbarHeight);
            NSLog(@"%f,%f,%f,%f",colPages.frame.size.height,colPages.frame.size.width,colPages.frame.origin.x,colPages.frame.origin.x);
        }
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }

    
    
    [colPages setDataSource:self];
    [colPages setDelegate:self];
    [colPages setAllowsMultipleSelection:NO];
    [self.view addSubview:colPages];
    [colPages reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    selectedIndexRow=indexPath.row;
    //    UITableViewCell *tcell = [colPages cellForItemAtIndexPath:indexPath];
    //    [tcell setSelected:YES];
    //    tblPages = [[UITableView alloc] initWithFrame:CGRectMake(4, topbarHeight, 100,[UIScreen mainScreen].bounds.size.height-topbarHeight) style:UITableViewStylePlain];
    //
    //    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    //        if ([[UIScreen mainScreen] nativeBounds].size.height == 2436 || [[UIScreen mainScreen] nativeBounds].size.height ==2688 || [[UIScreen mainScreen] nativeBounds].size.height == 1792) {
    //            tblPages = [[UITableView alloc] initWithFrame:CGRectMake(34, topbarHeight, 100,[UIScreen mainScreen].bounds.size.height-topbarHeight) style:UITableViewStylePlain];
    //        }
    //        else
    //        {
    //            tblPages = [[UITableView alloc] initWithFrame:CGRectMake(4, topbarHeight, 100,[UIScreen mainScreen].bounds.size.height-topbarHeight) style:UITableViewStylePlain];
    //        }
    //    }
    //
    //    NSString *device = [UIDevice currentDevice].model;
    //    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
    //        [tblPages setFrame:CGRectMake(tblPages.frame.origin.x, tblPages.frame.origin.y+20, tblPages.frame.size.width, tblPages.frame.size.height-40)];
    //    }
    //
    //
    //
    //    tblPages.layer.cornerRadius = 10.0f;
    //    tblPages.layer.borderWidth = 5.0f;
    //    tblPages.layer.borderColor = [[UIColor grayColor] CGColor];
    //    tblPages.layer.cornerRadius = 10.0f;
    //    tblPages.layer.borderWidth = 5.0f;
    //    tblPages.layer.borderColor = [[UIColor grayColor] CGColor];
    //    [tblPages setDataSource:self];
    //    [tblPages setDelegate:self];
    //    [tblPages setBackgroundColor:[UIColor clearColor]];
    //    [tblPages setAllowsMultipleSelection:NO];
    //    [self.view addSubview:tblPages];
    //    [tblPages reloadData];
    //
    //    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    //    UITableViewCell *tcell = [tblPages cellForRowAtIndexPath:indexPath];
    //    [tcell setSelected:YES];
    
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setTranslucent:YES];
    
	if (CGSizeEqualToSize(lastAppearSize, CGSizeZero) == false)
	{
		if (CGSizeEqualToSize(lastAppearSize, self.view.bounds.size) == false)
		{
			[self updateScrollViewContentViews]; // Update content views
		}

		lastAppearSize = CGSizeZero; // Reset view size tracking
	}
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
    //[self showDocumentPage:1];
	if (CGSizeEqualToSize(theScrollView.contentSize, CGSizeZero)) // First time
	{
		[self performSelector:@selector(showDocument:) withObject:nil afterDelay:1.5];
	}

    [self didRotate:nil];
#if (READER_DISABLE_IDLE == TRUE) // Option

	[UIApplication sharedApplication].idleTimerDisabled = YES;

#endif // end of READER_DISABLE_IDLE Option
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self.navigationController.navigationBar setTranslucent:NO];
	lastAppearSize = self.view.bounds.size; // Track view size

#if (READER_DISABLE_IDLE == TRUE) // Option

	[UIApplication sharedApplication].idleTimerDisabled = NO;

#endif // end of READER_DISABLE_IDLE Option
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (void)viewDidUnload
{
#ifdef DEBUG
	NSLog(@"%s", __FUNCTION__);
#endif

	mainToolbar = nil; mainPagebar = nil;

	theScrollView = nil; contentViews = nil; lastHideTime = nil;

	lastAppearSize = CGSizeZero; currentPage = 0;

	[super viewDidUnload];
}

//- (BOOL)prefersStatusBarHidden
//{
//	return YES;
//}

//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//	return UIStatusBarStyleLightContent;
//}

- (BOOL)shouldAutorotate
{
	return YES;
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    UIImage *image = [mainPagebar getImageFromPage:1];
    if (image.size.width>image.size.height) {
        OrientationName=@"Landscape";
        return UIInterfaceOrientationMaskLandscape;
    }
    if (image.size.width<image.size.height) {
        OrientationName=@"Portrait";
        return UIInterfaceOrientationMaskPortrait;
    }
    
    return UIInterfaceOrientationMaskAll;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	if (isVisible == NO) return; // iOS present modal bodge

	if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
	{
		if (printInteraction != nil) [printInteraction dismissAnimated:NO];
	}
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
	if (isVisible == NO) return; // iOS present modal bodge

	[self updateScrollViewContentViews]; // Update content views

	lastAppearSize = CGSizeZero; // Reset view size tracking
}

/*
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	//if (isVisible == NO) return; // iOS present modal bodge

	//if (fromInterfaceOrientation == self.interfaceOrientation) return;
}
*/

- (void)didReceiveMemoryWarning
{
#ifdef DEBUG
	NSLog(@"%s", __FUNCTION__);
#endif

	[super didReceiveMemoryWarning];
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark UIScrollViewDelegate methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    if (![scrollView isKindOfClass:[UICollectionView class]]) {
        __block NSInteger page = 0;
        
        CGFloat contentOffsetX = scrollView.contentOffset.x;
        
        [contentViews enumerateKeysAndObjectsUsingBlock: // Enumerate content views
         ^(id key, id object, BOOL *stop)
         {
             ReaderContentView *contentView = object;
             
             if (contentView.frame.origin.x == contentOffsetX)
             {
                 page = contentView.tag; *stop = YES;
             }
         }
         ];
        
        if (page != 0) [self showDocumentPage:page]; // Show the page
        
        //        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:page-1 inSection:0];
        //        //UITableViewCell *tcell = [tableView_ cellForRowAtIndexPath:indexPath];
        //        selectedIndexRow=indexPath.row;
        //        UICollectionViewCell *tcell = [colPages cellForItemAtIndexPath:indexPath];
        //        [tcell setSelected:NO];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (![scrollView isKindOfClass:[UICollectionView class]]) {

	__block NSInteger page = 0;

	CGFloat contentOffsetX = scrollView.contentOffset.x;

	[contentViews enumerateKeysAndObjectsUsingBlock: // Enumerate content views
		^(id key, id object, BOOL *stop)
		{
			ReaderContentView *contentView = object;

			if (contentView.frame.origin.x == contentOffsetX)
			{
				page = contentView.tag; *stop = YES;
			}
		}
	];
//For slider changes
//    if (page != 0) [self showDocumentPage:page]; // Show the page
//    
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:page-1 inSection:0];
//    if (indexPath.row < 0)
//    {
//        return;
//    }
//    [colPages scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
//    
//    //UITableViewCell *tcell = [tableView_ cellForRowAtIndexPath:indexPath];
//    selectedIndexRow=indexPath.row;
//    [colPages reloadData];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (![scrollView isKindOfClass:[UICollectionView class]]) {

	[self showDocumentPage:theScrollView.tag]; // Show page

	theScrollView.tag = 0; // Clear page number tag
    }
}

#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)recognizer shouldReceiveTouch:(UITouch *)touch
{
	if ([touch.view isKindOfClass:[UIScrollView class]]) return YES;

	return NO;
}

#pragma mark UIGestureRecognizer action methods

- (void)decrementPageNumber
{
	if (theScrollView.tag == 0) // Scroll view did end
	{
		NSInteger page = [document.pageNumber integerValue];
		NSInteger maxPage = [document.pageCount integerValue];
		NSInteger minPage = 1; // Minimum

		if ((maxPage > minPage) && (page != minPage))
		{
			CGPoint contentOffset = theScrollView.contentOffset;

			contentOffset.x -= theScrollView.bounds.size.width; // -= 1

			[theScrollView setContentOffset:contentOffset animated:YES];

			theScrollView.tag = (page - 1); // Decrement page number
		}
	}
}

- (void)incrementPageNumber
{
	if (theScrollView.tag == 0) // Scroll view did end
	{
		NSInteger page = [document.pageNumber integerValue];
		NSInteger maxPage = [document.pageCount integerValue];
		NSInteger minPage = 1; // Minimum

		if ((maxPage > minPage) && (page != maxPage))
		{
			CGPoint contentOffset = theScrollView.contentOffset;

			contentOffset.x += theScrollView.bounds.size.width; // += 1

			[theScrollView setContentOffset:contentOffset animated:YES];

			theScrollView.tag = (page + 1); // Increment page number
		}
	}
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
	if (recognizer.state == UIGestureRecognizerStateRecognized)
	{
		CGRect viewRect = recognizer.view.bounds; // View bounds

		CGPoint point = [recognizer locationInView:recognizer.view];

		CGRect areaRect = CGRectInset(viewRect, TAP_AREA_SIZE, 0.0f); // Area

		if (CGRectContainsPoint(areaRect, point)) // Single tap is inside the area
		{
			NSInteger page = [document.pageNumber integerValue]; // Current page #

			NSNumber *key = [NSNumber numberWithInteger:page]; // Page number key

			ReaderContentView *targetView = [contentViews objectForKey:key];

			id target = [targetView processSingleTap:recognizer]; // Target

			if (target != nil) // Handle the returned target object
			{
				if ([target isKindOfClass:[NSURL class]]) // Open a URL
				{
					NSURL *url = (NSURL *)target; // Cast to a NSURL object

					if (url.scheme == nil) // Handle a missing URL scheme
					{
						NSString *www = url.absoluteString; // Get URL string

						if ([www hasPrefix:@"www"] == YES) // Check for 'www' prefix
						{
							NSString *http = [NSString stringWithFormat:@"http://%@", www];

							url = [NSURL URLWithString:http]; // Proper http-based URL
						}
					}

					if ([[UIApplication sharedApplication] openURL:url] == NO)
					{
						#ifdef DEBUG
							NSLog(@"%s '%@'", __FUNCTION__, url); // Bad or unknown URL
						#endif
					}
				}
				else // Not a URL, so check for other possible object type
				{
					if ([target isKindOfClass:[NSNumber class]]) // Goto page
					{
						NSInteger value = [target integerValue]; // Number

						[self showDocumentPage:value]; // Show the page
					}
				}
			}
			else // Nothing active tapped in the target content view
			{
				if ([lastHideTime timeIntervalSinceNow] < -0.75) // Delay since hide
				{
					if ((mainToolbar.hidden == YES) || (mainPagebar.hidden == YES))
					{
                        [mainToolbar hideToolbar]; [mainPagebar hidePagebar]; // Show
                        
                        colPages.hidden=!colPages.hidden;
                        if (colPages.hidden) {
                            isHide=YES;
                        }
                        else
                        {
                            isHide=NO;
                        }
                        [self.navigationController setNavigationBarHidden:isHide animated:YES];
                        
                        [self didRotate:nil];
					}
				}
			}

			return;
		}

		CGRect nextPageRect = viewRect;
		nextPageRect.size.width = TAP_AREA_SIZE;
		nextPageRect.origin.x = (viewRect.size.width - TAP_AREA_SIZE);

		if (CGRectContainsPoint(nextPageRect, point)) // page++ area
		{
			[self incrementPageNumber]; return;
		}

		CGRect prevPageRect = viewRect;
		prevPageRect.size.width = TAP_AREA_SIZE;

		if (CGRectContainsPoint(prevPageRect, point)) // page-- area
		{
			[self decrementPageNumber]; return;
		}
	}
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)recognizer
{
	if (recognizer.state == UIGestureRecognizerStateRecognized)
	{
		CGRect viewRect = recognizer.view.bounds; // View bounds

		CGPoint point = [recognizer locationInView:recognizer.view];

		CGRect zoomArea = CGRectInset(viewRect, TAP_AREA_SIZE, TAP_AREA_SIZE);

		if (CGRectContainsPoint(zoomArea, point)) // Double tap is in the zoom area
		{
			NSInteger page = [document.pageNumber integerValue]; // Current page #

			NSNumber *key = [NSNumber numberWithInteger:page]; // Page number key

			ReaderContentView *targetView = [contentViews objectForKey:key];

			switch (recognizer.numberOfTouchesRequired) // Touches count
			{
				case 1: // One finger double tap: zoom ++
				{
					[targetView zoomIncrement]; break;
				}

				case 2: // Two finger double tap: zoom --
				{
					[targetView zoomDecrement]; break;
				}
			}

			return;
		}

		CGRect nextPageRect = viewRect;
		nextPageRect.size.width = TAP_AREA_SIZE;
		nextPageRect.origin.x = (viewRect.size.width - TAP_AREA_SIZE);

		if (CGRectContainsPoint(nextPageRect, point)) // page++ area
		{
			[self incrementPageNumber]; return;
		}

		CGRect prevPageRect = viewRect;
		prevPageRect.size.width = TAP_AREA_SIZE;

		if (CGRectContainsPoint(prevPageRect, point)) // page-- area
		{
			[self decrementPageNumber]; return;
		}
	}
}

#pragma mark ReaderContentViewDelegate methods

- (void)contentView:(ReaderContentView *)contentView touchesBegan:(NSSet *)touches
{
	if ((mainToolbar.hidden == NO) || (mainPagebar.hidden == NO))
	{
		if (touches.count == 1) // Single touches only
		{
			UITouch *touch = [touches anyObject]; // Touch info

			CGPoint point = [touch locationInView:self.view]; // Touch location

			CGRect areaRect = CGRectInset(self.view.bounds, TAP_AREA_SIZE, TAP_AREA_SIZE);

			if (CGRectContainsPoint(areaRect, point) == false) return;
		}

		[mainToolbar hideToolbar]; [mainPagebar hidePagebar]; // Hide

		lastHideTime = [NSDate date];
	}
}

#pragma mark ReaderMainToolbarDelegate methods

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar doneButton:(UIButton *)button
{
#if (READER_STANDALONE == FALSE) // Option

	[document saveReaderDocument]; // Save any ReaderDocument object changes

	[[ReaderThumbQueue sharedInstance] cancelOperationsWithGUID:document.guid];

	[[ReaderThumbCache sharedInstance] removeAllObjects]; // Empty the thumb cache

	if (printInteraction != nil) [printInteraction dismissAnimated:NO]; // Dismiss

	if ([delegate respondsToSelector:@selector(dismissReaderViewController:)] == YES)
	{
		[delegate dismissReaderViewController:self]; // Dismiss the ReaderViewController
	}
	else // We have a "Delegate must respond to -dismissReaderViewController: error"
	{
		NSAssert(NO, @"Delegate must respond to -dismissReaderViewController:");
	}

#endif // end of READER_STANDALONE Option
}

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar thumbsButton:(UIButton *)button
{
	if (printInteraction != nil) [printInteraction dismissAnimated:NO]; // Dismiss

	ThumbsViewController *thumbsViewController = [[ThumbsViewController alloc] initWithReaderDocument:document];

	thumbsViewController.delegate = self; thumbsViewController.title = self.title;

	thumbsViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	thumbsViewController.modalPresentationStyle = UIModalPresentationFullScreen;

	[self presentViewController:thumbsViewController animated:NO completion:NULL];
}

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar printButton:(UIButton *)button
{
#if (READER_ENABLE_PRINT == TRUE) // Option

	Class printInteractionController = NSClassFromString(@"UIPrintInteractionController");

	if ((printInteractionController != nil) && [printInteractionController isPrintingAvailable])
	{
		NSURL *fileURL = document.fileURL; // Document file URL

		printInteraction = [printInteractionController sharedPrintController];

		if ([printInteractionController canPrintURL:fileURL] == YES) // Check first
		{
			UIPrintInfo *printInfo = [NSClassFromString(@"UIPrintInfo") printInfo];

			printInfo.duplex = UIPrintInfoDuplexLongEdge;
			printInfo.outputType = UIPrintInfoOutputGeneral;
			printInfo.jobName = document.fileName;

			printInteraction.printInfo = printInfo;
			printInteraction.printingItem = fileURL;
			printInteraction.showsPageRange = YES;

			if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
			{
				[printInteraction presentFromRect:button.bounds inView:button animated:YES completionHandler:
					^(UIPrintInteractionController *pic, BOOL completed, NSError *error)
					{
						#ifdef DEBUG
							if ((completed == NO) && (error != nil)) NSLog(@"%s %@", __FUNCTION__, error);
						#endif
					}
				];
			}
			else // Presume UIUserInterfaceIdiomPhone
			{
				[printInteraction presentAnimated:YES completionHandler:
					^(UIPrintInteractionController *pic, BOOL completed, NSError *error)
					{
						#ifdef DEBUG
							if ((completed == NO) && (error != nil)) NSLog(@"%s %@", __FUNCTION__, error);
						#endif
					}
				];
			}
		}
	}

#endif // end of READER_ENABLE_PRINT Option
}

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar emailButton:(UIButton *)button
{
#if (READER_ENABLE_MAIL == TRUE) // Option

	if ([MFMailComposeViewController canSendMail] == NO) return;

	if (printInteraction != nil) [printInteraction dismissAnimated:YES];

	unsigned long long fileSize = [document.fileSize unsignedLongLongValue];

	if (fileSize < (unsigned long long)15728640) // Check attachment size limit (15MB)
	{
		NSURL *fileURL = document.fileURL; NSString *fileName = document.fileName; // Document

		NSData *attachment = [NSData dataWithContentsOfURL:fileURL options:(NSDataReadingMapped|NSDataReadingUncached) error:nil];

		if (attachment != nil) // Ensure that we have valid document file attachment data
		{
			MFMailComposeViewController *mailComposer = [MFMailComposeViewController new];

			[mailComposer addAttachmentData:attachment mimeType:@"application/pdf" fileName:fileName];

			[mailComposer setSubject:fileName]; // Use the document file name for the subject

			mailComposer.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
			mailComposer.modalPresentationStyle = UIModalPresentationFormSheet;

			mailComposer.mailComposeDelegate = self; // Set the delegate

			[self presentViewController:mailComposer animated:YES completion:NULL];
		}
	}

#endif // end of READER_ENABLE_MAIL Option
}

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar markButton:(UIButton *)button
{
	if (printInteraction != nil) [printInteraction dismissAnimated:YES];

	NSInteger page = [document.pageNumber integerValue];

	if ([document.bookmarks containsIndex:page]) // Remove bookmark
	{
		[mainToolbar setBookmarkState:NO]; [document.bookmarks removeIndex:page];
	}
	else // Add the bookmarked page index to the bookmarks set
	{
		[mainToolbar setBookmarkState:YES]; [document.bookmarks addIndex:page];
	}
}

#pragma mark MFMailComposeViewControllerDelegate methods

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
	#ifdef DEBUG
		if ((result == MFMailComposeResultFailed) && (error != NULL)) NSLog(@"%@", error);
	#endif

	[self dismissViewControllerAnimated:YES completion:NULL]; // Dismiss
}

#pragma mark ThumbsViewControllerDelegate methods

- (void)dismissThumbsViewController:(ThumbsViewController *)viewController
{
	[self updateToolbarBookmarkIcon]; // Update bookmark icon

	[self dismissViewControllerAnimated:YES completion:NULL]; // Dismiss
}

- (void)thumbsViewController:(ThumbsViewController *)viewController gotoPage:(NSInteger)page
{
	[self showDocumentPage:page]; // Show the page
}

#pragma mark ReaderMainPagebarDelegate methods

- (void)pagebar:(ReaderMainPagebar *)pagebar gotoPage:(NSInteger)page
{
	[self showDocumentPage:page]; // Show the page
}

#pragma mark UIApplication notification methods

- (void)applicationWill:(NSNotification *)notification
{
	[document saveReaderDocument]; // Save any ReaderDocument object changes

	if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
	{
		if (printInteraction != nil) [printInteraction dismissAnimated:NO];
	}
}
-(ReaderContentView *)contentViewWithFrame:(CGRect)frame fileURL:(NSURL *)fileURL page:(NSUInteger)number password:(NSString *)phrase{
    return [[ReaderContentView alloc]initWithFrame:frame fileURL:fileURL page:number password:phrase];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return document.pageCount.integerValue;
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.contentView.layer.borderColor = [[UIColor grayColor] CGColor];
    cell.contentView.layer.borderWidth = 0.5;
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    //    CGRect rect=cell.frame;
    //    rect.origin.y=0;
    //    cell.frame=rect;
    UIImageView *imagethumb=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, 70, 70)];
    imagethumb.backgroundColor=[UIColor whiteColor];
    [cell.contentView addSubview:imagethumb];
    
    
    if (indexPath.row==selectedIndexRow) {
        
        cell.layer.borderColor=self.navigationController.navigationBar.tintColor.CGColor;
        cell.layer.borderWidth=2.0;
        //        UIView *viewthumb=[[UIView alloc] initWithFrame:CGRectMake(0,0, 32, 42)];
        //        viewthumb.backgroundColor=[UIColor grayColor];
        //        viewthumb.alpha=0.6;
        //        [cell.contentView addSubview:viewthumb];
    }
    else
    {
        cell.layer.borderColor=[UIColor clearColor].CGColor;
    }
    NSURL *fileUrl = document.fileURL;
    long getImageAtIndex = indexPath.row + 1;
    int intIndexValue = (int)getImageAtIndex;
    UIImage *image = [ UIImage imageWithPDFURL:fileUrl atSize:CGSizeMake(140, 140) atPage:intIndexValue];
    
    if (image) {
        imagethumb.image = image;
    }else{
        if (indexPath.row < document.pageCount.integerValue) {
            NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
            [colPages reloadItemsAtIndexPaths:indexPaths];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                self->selectedIndexRow=indexPath.row;
            });
        }
    }
    
    if (indexPath.row < document.pageCount.integerValue-1) {
    }
    
    return cell;
    
}

//Size of item is defined here
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(70,70);
}
// Margins around the image
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
// Inter item spacing
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}
//Inter section spacing
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    selectedIndexRow=indexPath.row;
    if ([mainPagebar getPageNumber]!=indexPath.row+1) {
        [self showDocumentPage:indexPath.row+1];
    }
    
}

@end
