//
//  SignUpVC.h
//  X Detailer
//
//  Created by Vikramjeet Singh on 24/03/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnderlinedTextField.h"

NS_ASSUME_NONNULL_BEGIN

@interface SignUpVC : UIViewController
{
    
    IBOutlet UnderlinedTextField *textField_username;
    IBOutlet UnderlinedTextField *textfield_phoneNumber;
    IBOutlet UnderlinedTextField *textfield_email;
    IBOutlet UnderlinedTextField *textfield_password;
    IBOutlet UnderlinedTextField *textfield_confirmpassword;
    
}
- (IBAction)SubmitButtonAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
