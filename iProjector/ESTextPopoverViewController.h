//
//  ESAddTextPopoverViewController.h
//  iProjector
//
//  Created by Jayaprada Behera on 12/10/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ESTextPacket.h"

#define SECTION_EDIT_TEXT 0
#define SECTION_FONT_SIZE 1
#define SECTION_COLOR 2
#define SECTION_DELETE 3

#define TOTAL_SECTION_NUMBER 4

#define MINIMUM_FONT_VALUE   10
#define MAXIMUM_FONT_VALUE   24

@class ESTextPopoverViewController;

@protocol ESAddTextPopoverDelegate <NSObject>

-(void) cancelAddTextFromPopOver:(ESTextPopoverViewController *)vc ;

-(void) deleteTextFromPopover:(ESTextPopoverViewController *)vc withPacket:(ESTextPacket *)packet;

-(void) fontSizeChangeFromPopover:(ESTextPopoverViewController *)vc withPacket:(ESTextPacket *)packet;

-(void) textColorChangeFromPopover:(ESTextPopoverViewController *)vc withPacket:(ESTextPacket *)packet;

-(void) textChangeFromPopover :(ESTextPopoverViewController *)vc withPacket:(ESTextPacket *)packet;

@optional
-(void) doneWithTextPopover:(ESTextPopoverViewController *)viewController withPacket:(ESTextPacket *)packet;


@end


//@interface ESAddTextPopoverViewController : UITableViewController
@interface ESTextPopoverViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,unsafe_unretained)id <ESAddTextPopoverDelegate> delegate;
@property(nonatomic,strong)NSString *text;
@property(nonatomic) CGFloat fontSize ;
@property(nonatomic) NSInteger indexOfText;
@property(nonatomic) BOOL fromPdf;
@property(nonatomic)UIColor *textColor;

@end
