//
//  ESPDFNewViewController.h
//  iProjector
//
//  Created by WebileApps on 09/07/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReaderViewController;
@interface ESPDFNewViewController : UIViewController

{
}
@property (nonatomic,strong) NSString *fileName;
@property (nonatomic,strong) ReaderViewController *readerViewController;
-(id) initWithFileName:(NSString *) aFileName;

@end
