//
//  ESAppDelegate.m
//  iProjector
//
//  Created by Rajiv Narayana Singaseni on 6/27/13.
//  Copyright (c) 2013 EIQ Services. All rights reserved.
//

#import "ESAppDelegate.h"
#import "ESSettingListViewController.h"
#import "ESSessionBaseViewController.h"

#import "ESDocumentsListViewController.h"

#import "ESDocumentsCollectionViewController.h"
#import "ESWebViewController.h"
#import "CircleMenuViewController.h"
#import "Constants.h"
#import "LoginVC.h"
#import "Flurry.h"
#import <sys/utsname.h>
#import <IQKeyboardManager/IQKeyboardManager.h>
//#import "Flurry.h"
@interface ESAppDelegate (){
    NSString *filePath;
    NSData *data;
}

@end

@implementation ESAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    [self copyFilesFromBundleToDocuments];
#if X_AUDIENCE
#if DEBUG
    [Flurry startSession:@"S8RT8BB7Z7Z5NGNQFN5V"];
#else
    [Flurry startSession:@"GVNG9Z8KP8G46WFSHXPG"];
#endif
    
#else
    //X_PRESENTER
#if DEBUG
    [Flurry startSession:@"F7JCKQKJ2H9FSVHBZCGF"];
#else
    [Flurry startSession:@"DXJB3RP3WQ2TRRXC9G75"];
#endif
    
#endif
    
    [Flurry setCrashReportingEnabled:YES];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //set user_role to attendee for a first time user.
    if (![userDefaults valueForKey:KEY_USER_ROLE]) {
        [userDefaults setValue:[NSNumber numberWithInt:ESUserRoleAttendee] forKey:KEY_USER_ROLE];
        [userDefaults setBool:NO forKey:KEY_ATTENDEE_CAN_BROWSE];
        [userDefaults setBool:YES forKey:KEY_ATTENDEE_CAN_QUERY];
        [userDefaults setBool:YES forKey:KEY_DISPLAY_TIMER];
    }
    [userDefaults synchronize];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    LoginVC *loginVc;
    NSString *device = [UIDevice currentDevice].model;
    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"])
    {
       loginVc=[[LoginVC alloc] initWithNibName:@"LoginVC_iPad" bundle:nil];
    }
    else
    {        loginVc=[[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
    }

    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:loginVc];
 [self.window setRootViewController:navigationController];
  [self.window makeKeyAndVisible];
     return YES;
    
    
    
   // LoginVC * loginVc;
   // loginVc = [[LoginVC alloc]initWithNibName:@"LoginVC" bundle:nil];
   // UINavigationController * navigationController = [[UINavigationController alloc]initWithRootViewController:loginVc];

    CircleMenuViewController * circleMenuViewController;
 //  circleMenuViewController = [[CircleMenuViewController alloc] initWithNibName:@"CircleMenuViewController" bundle:nil];
    // Set the cricle menu vc as the root vc
   // UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:circleMenuViewController];
//    UINavigationController * navigationController = [[UINavigationController alloc] init];
//   (void) [navigationController initWithRootViewController:circleMenuViewController];
    //[navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];

    NSString *deviceType = [UIDevice currentDevice].model;
    CGRect myImageRect;
    UIImageView *image ;

    if([deviceType isEqualToString:@"iPad Simulator"]||[deviceType isEqualToString:@"iPad"]){
//        (void) [circleMenuViewController initWithButtonCount:kKYCCircleMenuButtonsCount
//                                                    menuSize:kKYCircleMenuSize
//                                                  buttonSize:kKYCircleMenuButtonSize
//                                       buttonImageNameFormat:kKYICircleMenuButtonImageNameFormat
//                                            centerButtonSize:kKYCircleMenuCenterButtonSize
//                                       centerButtonImageName:nil
//                             centerButtonBackgroundImageName:kKYICircleMenuCenterButtonBackground];
//        myImageRect = CGRectMake(300, 0, 440, 280);
//        image= [[UIImageView alloc] initWithFrame:myImageRect];
//        [image setImage:[UIImage imageNamed:@"Detailer.png"]];


    }else{
    // Setup circle menu with basic configuration
//        (void) [circleMenuViewController initWithButtonCount:kKYCCircleMenuButtonsCount
//                                                    menuSize:kKYCircleMenuSize_iPhone
//                                                  buttonSize:kKYCircleMenuButtonSize_iPhone
//                                       buttonImageNameFormat:kKYICircleMenuButtonImageNameFormat_iPhone
//                                            centerButtonSize:kKYCircleMenuCenterButtonSize_iPhone
//                                       centerButtonImageName:nil
//                             centerButtonBackgroundImageName:kKYICircleMenuCenterButtonBackground_iPhone];
//        CGFloat  x_Origin;
//
//        if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(@"7.1") && ([machineName() isEqualToString:@"iPhone3,1"]||[machineName() isEqualToString:@"iPhone4,1"])) {
//
//            x_Origin =([UIScreen mainScreen].bounds.size.width-230)/2;
//        }else{
//            x_Origin =([UIScreen mainScreen].bounds.size.height-230)/2;
//        }
//        myImageRect = CGRectMake(x_Origin, 0, 230, 120);
//        image = [[UIImageView alloc] initWithFrame:myImageRect];
//        [image setImage:[UIImage imageNamed:@"Detailer_iPhone.png"]];

    }
    [circleMenuViewController.view addSubview:image];
    [circleMenuViewController.view setBackgroundColor:[UIColor whiteColor]];
    // Set navigation controller as the root vc
    [self.window setRootViewController:navigationController];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) copyFilesFromBundleToDocuments {
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"kESFDownloadedFiles"]) {
        return;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDir error:nil];
    
    if (contents.count >= 1) {
        return;
    }

    
    NSString *pathLocal = [documentsDir stringByAppendingPathComponent:@"Rebyota.pdf"];
    
    NSString *pathBundle = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Rebyota.pdf"];
    NSLog(@"Could not copy Rebyota.pdf");
    NSError *error;
    BOOL success = [fileManager copyItemAtPath:pathBundle toPath:pathLocal error:&error];
    if (!success) {
        NSLog(@"Could not copy Rebyota.pdf");
    }
    
    pathLocal = [documentsDir stringByAppendingPathComponent:@"objectionHandling.plist"];
    
    pathBundle = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"objectionHandling.plist"];
    NSLog(@"Could not copy objectionHandling.plist");
    success = [fileManager copyItemAtPath:pathBundle toPath:pathLocal error:&error];
    if (!success) {
        NSLog(@"Could not copy objectionHandling.plist");
    }
    
    NSLog(@"Path local: %@", pathLocal);
    
    pathLocal = [documentsDir stringByAppendingPathComponent:@"AudioVideoFiles.plist"];
    
    pathBundle = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"AudioVideoFiles.plist"];
    NSLog(@"Could not copy AudioVideoFiles.plist");
    success = [fileManager copyItemAtPath:pathBundle toPath:pathLocal error:&error];
    if (!success) {
        success = [fileManager removeItemAtPath:pathLocal error:&error];
        if (!success) {NSLog(@"remove error");}
        success = [fileManager copyItemAtPath:pathBundle toPath:pathLocal error:&error];
        if (!success) {NSLog(@"copy error");}
        NSLog(@"Could not copy AudioVideoFiles.plist");
    }
    
    NSLog(@"Path local: %@", pathLocal);
    
    /*pathLocal = [documentsDir stringByAppendingPathComponent:@"Final eIQ Presenataion -Aegerion RT1 Oct 9 2013 v2 JL.pdf"];
    
    pathBundle = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Final eIQ Presenataion -Aegerion RT1 Oct 9 2013 v2 JL.pdf"];
    NSLog(@"Could not copy eXpertIntelligence.pdf");
    
    success = [fileManager copyItemAtPath:pathBundle toPath:pathLocal error:&error];
    if (!success) {
        NSLog(@"Could not copy eXpertIntelligence.pdf");
    }*/
    
        
    /*pathLocal = [documentsDir stringByAppendingPathComponent:@"eIQ-Mallinckrodt.pdf"];
    
    pathBundle = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"eIQ-Mallinckrodt.pdf"];
    NSLog(@"Could not copy eIQ-Mallinckrodt.pdf");
    
    success = [fileManager copyItemAtPath:pathBundle toPath:pathLocal error:&error];
    if (!success) {
        NSLog(@"Could not copy eIQ-Mallinckrodt.pdf");
    }*/

    
    //Intro slides video.mp4 file to document dir

    pathLocal = [documentsDir stringByAppendingPathComponent:@"Intro slides video.mp4"];
    
    pathBundle = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Intro slides video.mp4"];
    
    error = nil;
    success = [fileManager copyItemAtPath:pathBundle toPath:pathLocal error:&error];
    if (!success) {
        NSLog(@"Could not copy Intro video");
    }else{
        
        NSLog(@"Intro video successfully copied to doc");
    }
    //Copy C_diff_Draft_05_10_2013.mov file to document dir
    pathLocal = [documentsDir stringByAppendingPathComponent:@"C_diff_Draft_05_10_2013.mov"];
    
    pathBundle = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"C_diff_Draft_05_10_2013.mov"];
    
    error = nil;
    success = [fileManager copyItemAtPath:pathBundle toPath:pathLocal error:&error];
    if (!success) {
        NSLog(@"Could not copy Intro video");
    }else{
        NSLog(@"C_diff_Draft_05_10_2013.mov successfully copied to doc");
        
    }
    //Copy FINAL_V2-1-quicktime.mov file to document dir
    pathLocal = [documentsDir stringByAppendingPathComponent:@"FINAL_V2-1-quicktime.mov"];
    
    pathBundle = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"FINAL_V2-1-quicktime.mov"];
    
    error = nil;
    success = [fileManager copyItemAtPath:pathBundle toPath:pathLocal error:&error];
    if (!success) {
        NSLog(@"Could not copy FINAL_V2-1-quicktime.mov");
    }else{
        NSLog(@"FINAL_V2-1-quicktime.mov successfully copied to doc");
        
    }
#if X_PRESENTER
    //Copy question.xml file to document dir
    pathLocal = [documentsDir stringByAppendingPathComponent:@"questions.xml"];
    
    pathBundle = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"questions.xml"];
    
    error = nil;
    success = [fileManager copyItemAtPath:pathBundle toPath:pathLocal error:&error];
    if (!success) {
        NSLog(@"Could not copy Questions.xml");
    }
        //Copy video.xml file to document dir
    
    pathLocal =[documentsDir stringByAppendingPathComponent:@"video.xml"];
    pathBundle = [[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:@"video.xml"];
    error = nil;
    success = [fileManager copyItemAtPath:pathBundle toPath:pathLocal error:&error];
    if (!success) {
        NSLog(@"Could not copy video.xml");
    }else{
        NSLog(@"video.xml successfully copied to doc");

    }
    
#endif
    
    
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    //Url = file:///private/var/mobile/Applications/849CA8F0-284A-46D2-B862-62A11B3A91AD/Documents/Inbox/MemoryMgmt.pdf
    
    //    Printing description of sourceApplication:
    //    com.apple.mobilemail
    //    [self fileExists:url];
    //
    
    data = [NSData dataWithContentsOfURL:url];
    filePath = [[self getDocumentDirectory] stringByAppendingPathComponent:[url lastPathComponent]];
    
    BOOL excludeBackupSuccessFul=  [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:[self getDocumentDirectory]]];
    //
    if (!excludeBackupSuccessFul) {
        NSString *message = @"";
        if ([url lastPathComponent]) {
            message = [NSString stringWithFormat:@"We are unable to open %@ file at this moment.\n Please retry after sometime",[url lastPathComponent]];
        }else{
            message = @"We are unable to open the file  at this moment.\n Please retry after sometime";
        }
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"Dismiss"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                       
                                       NSFileManager* fm = [NSFileManager defaultManager];
                                       
                                       NSString *documentsDir = [self getDocumentDirectory];
                                       
                                       NSArray *contents = [fm contentsOfDirectoryAtPath:documentsDir error:nil];
                                       
                                       //        NSString *extension = @"pdf";
                                       
                                       NSString *item;
                                       
                                       // >>> this section here adds all files with the chosen extension to an array
                                       for (item in contents){
                                           
                                           //            if ([[item pathExtension] isEqualToString:extension]) {
                                           if ([[[filePath componentsSeparatedByString:@"/"] lastObject] isEqualToString:item]) {
                                               
                                               
                                               //rename old file with timestamp and keep new file
                                               
                                               
                                               NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                               [formatter setTimeStyle:NSDateFormatterMediumStyle];
                                               
                                               NSDictionary* attrs = [fm attributesOfItemAtPath:filePath error:nil];
                                               
                                               if (attrs != nil) {
                                                   NSDate *date = (NSDate*)[attrs objectForKey: NSFileModificationDate];
                                                   NSString *lms = [NSString stringWithFormat:@"%@",[formatter stringFromDate:date]];
                                                   NSString *s = [[[[filePath componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"." ] objectAtIndex:0];
                                                   
                                                   NSString *modifiedFileName = [NSString stringWithFormat:@"%@-%@.pdf",s,lms];
                                                   
                                                   NSString *filePath1 =  [[self getDocumentDirectory] stringByAppendingPathComponent:modifiedFileName];
                                                   NSError *err;
                                                   
                                                   BOOL result =  [fm moveItemAtPath:filePath toPath:filePath1 error:&err];
                                                   if (result) {
                                                       //                        NSLog(@"Successfull");
                                                       [data writeToFile:filePath atomically:YES];
                                                       
                                                   }else {
                                                       //                        NSLog(@"%@",err);
                                                   }
                                               }
                                           }
                                       }
                                       //rename exsiting file -1
                                       //        how to-mmddyy .pdf
                                       
                                       [self pushToDocumentListVC];
                                       
                                   }];
        
        
        [alert addAction:okButton];
        
        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
        
        //[[[UIAlertView alloc]initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil]show];
        return NO;
    }
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) { // if file is not exist, create it.
        [data writeToFile:filePath atomically:YES];
        [self pushToDocumentListVC];
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"A document with the same name exists already. Do you wish to replace it?"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Move existing"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                       
                                       NSFileManager* fm = [NSFileManager defaultManager];
                                       
                                       NSString *documentsDir = [self getDocumentDirectory];
                                       
                                       NSArray *contents = [fm contentsOfDirectoryAtPath:documentsDir error:nil];
                                       
                                       //        NSString *extension = @"pdf";
                                       
                                       NSString *item;
                                       
                                       // >>> this section here adds all files with the chosen extension to an array
                                       for (item in contents){
                                           
                                           //            if ([[item pathExtension] isEqualToString:extension]) {
                                           if ([[[filePath componentsSeparatedByString:@"/"] lastObject] isEqualToString:item]) {
                                               
                                               
                                               //rename old file with timestamp and keep new file
                                               
                                               
                                               NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                               [formatter setTimeStyle:NSDateFormatterMediumStyle];
                                               
                                               NSDictionary* attrs = [fm attributesOfItemAtPath:filePath error:nil];
                                               
                                               if (attrs != nil) {
                                                   NSDate *date = (NSDate*)[attrs objectForKey: NSFileModificationDate];
                                                   NSString *lms = [NSString stringWithFormat:@"%@",[formatter stringFromDate:date]];
                                                   NSString *s = [[[[filePath componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"." ] objectAtIndex:0];
                                                   
                                                   NSString *modifiedFileName = [NSString stringWithFormat:@"%@-%@.pdf",s,lms];
                                                   
                                                   NSString *filePath1 =  [[self getDocumentDirectory] stringByAppendingPathComponent:modifiedFileName];
                                                   NSError *err;
                                                   
                                                   BOOL result =  [fm moveItemAtPath:filePath toPath:filePath1 error:&err];
                                                   if (result) {
                                                       //                        NSLog(@"Successfull");
                                                       [data writeToFile:filePath atomically:YES];
                                                       
                                                   }else {
                                                       //                        NSLog(@"%@",err);
                                                   }
                                               }
                                           }
                                       }
                                       //rename exsiting file -1
                                       //        how to-mmddyy .pdf
                                       
                                       [self pushToDocumentListVC];
                                       
                                   }];
        
        UIAlertAction* overwriteButton = [UIAlertAction
                                   actionWithTitle:@"Overwrite"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                       
                                       NSFileManager *fileMgr = [NSFileManager defaultManager];
                                       NSError *error;
                                       if ([fileMgr fileExistsAtPath:filePath]) {
                                           BOOL success = [fileMgr removeItemAtPath:filePath error:&error];
                                           
                                           if (!success) {
                                               NSLog(@"error:%@", error);
                                           }else {
                                               [data writeToFile:filePath atomically:YES];
                                           }
                                           
                                       }
                                       
                                       [self pushToDocumentListVC];
                                       
                                   }];
        
        
        [alert addAction:cancelButton];
        [alert addAction:overwriteButton];
        
        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
        
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"A document with the same name exists already. Do you wish to replace it?" delegate:self cancelButtonTitle:@"Move existing" otherButtonTitles:@"Overwrite", nil];
//        [alertView show];
        
    }
    //    NSLog(@"filePath %@", filePath);
    
    
    return YES;
}
-(void)pushToDocumentListVC{
    
    
    NSString *path = [self pathForFileName:@"Inbox"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    //    NSLog(@"Documents directory before: %@", [fileManager contentsOfDirectoryAtPath:path error:&error]);
    NSArray *files = [fileManager contentsOfDirectoryAtPath:path error:&error];
    if (files && files.count > 0) {
        for (NSString *file  in files) {
            NSString *pathToFile = [path stringByAppendingPathComponent:file];
            BOOL success = [fileManager removeItemAtPath:pathToFile error:&error];
            if (!success) {
                NSLog(@"error:%@", error);
            }
        }
    }
    //    NSLog(@"Documents directory after: %@", [fileManager contentsOfDirectoryAtPath:path error:&error]);
    
    ESDocumentsListViewController *_documentListVC = [[ESDocumentsListViewController alloc] init];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:_documentListVC];
    
    self.window.rootViewController = navController;

    _documentListVC.pdfFiles = [_documentListVC findFiles:@"pdf"];
    [_documentListVC.tableView reloadData];
    
}
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    
//    if (alertView.cancelButtonIndex == buttonIndex) {
//        
//        NSFileManager* fm = [NSFileManager defaultManager];
//        
//        NSString *documentsDir = [self getDocumentDirectory];
//        
//        NSArray *contents = [fm contentsOfDirectoryAtPath:documentsDir error:nil];
//        
//        //        NSString *extension = @"pdf";
//        
//        NSString *item;
//        
//        // >>> this section here adds all files with the chosen extension to an array
//        for (item in contents){
//            
//            //            if ([[item pathExtension] isEqualToString:extension]) {
//            if ([[[filePath componentsSeparatedByString:@"/"] lastObject] isEqualToString:item]) {
//                
//                
//                //rename old file with timestamp and keep new file
//                
//                
//                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//                [formatter setTimeStyle:NSDateFormatterMediumStyle];
//                
//                NSDictionary* attrs = [fm attributesOfItemAtPath:filePath error:nil];
//                
//                if (attrs != nil) {
//                    NSDate *date = (NSDate*)[attrs objectForKey: NSFileModificationDate];
//                    NSString *lms = [NSString stringWithFormat:@"%@",[formatter stringFromDate:date]];
//                    NSString *s = [[[[filePath componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"." ] objectAtIndex:0];
//                    
//                    NSString *modifiedFileName = [NSString stringWithFormat:@"%@-%@.pdf",s,lms];
//                    
//                    NSString *filePath1 =  [[self getDocumentDirectory] stringByAppendingPathComponent:modifiedFileName];
//                    NSError *err;
//                    
//                    BOOL result =  [fm moveItemAtPath:filePath toPath:filePath1 error:&err];
//                    if (result) {
//                        //                        NSLog(@"Successfull");
//                        [data writeToFile:filePath atomically:YES];
//                        
//                    }else {
//                        //                        NSLog(@"%@",err);
//                    }
//                }
//            }
//        }
//        //rename exsiting file -1
//        //        how to-mmddyy .pdf
//    }else{
//        NSFileManager *fileMgr = [NSFileManager defaultManager];
//        NSError *error;
//        if ([fileMgr fileExistsAtPath:filePath]) {
//            BOOL success = [fileMgr removeItemAtPath:filePath error:&error];
//            
//            if (!success) {
//                NSLog(@"error:%@", error);
//            }else {
//                [data writeToFile:filePath atomically:YES];
//            }
//            
//        }
//    }
//    [self pushToDocumentListVC];
//}

-(NSString *) getDocumentDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}
-(NSString *) pathForFileName:(NSString *) fileName {
    NSFileManager *fManager = [NSFileManager defaultManager];
    
    NSString *documentsDir = [self getDocumentDirectory];
    NSString *path = [documentsDir stringByAppendingPathComponent:fileName];
    if([fManager fileExistsAtPath:path] == YES)
    {
        return path;
    }
    return @"";
}
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL{
    
    if ([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]) {
        NSError *error = nil;
        
        BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                        
                                      forKey: NSURLIsExcludedFromBackupKey error: &error];
        if(!success){
            
            NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
            
        }
        return success;
    }else{
        return NO;
    }
    
}






NSString *machineName(){
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}
/*
 @"i386"      on the simulator
 @"iPod1,1"   on iPod Touch
 @"iPod2,1"   on iPod Touch Second Generation
 @"iPod3,1"   on iPod Touch Third Generation
 @"iPod4,1"   on iPod Touch Fourth Generation
 @"iPhone1,1" on iPhone
 @"iPhone1,2" on iPhone 3G
 @"iPhone2,1" on iPhone 3GS
 @"iPad1,1"   on iPad
 @"iPad2,1"   on iPad 2
 @"iPad3,1"   on iPad 3 (aka new iPad)
 @"iPhone3,1" on iPhone 4
 @"iPhone4,1" on iPhone 4S
 @"iPhone5,1" on iPhone 5
 @"iPhone5,2" on iPhone 5
 */
@end
