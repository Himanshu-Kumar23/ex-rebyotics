//
//  ViewController.m
//  ZLSwipeableViewDemo
//
//  Created by Zhixuan Lai on 11/1/14.
//  Copyright (c) 2014 Zhixuan Lai. All rights reserved.
//

#import "ZLSwipeableViewController.h"
#import "UIColor+FlatColors.h"
#import "CardView.h"

@interface ZLSwipeableViewController ()

@property (nonatomic, strong) NSArray *colors;
@property (nonatomic) NSUInteger colorIndex;
@property (nonatomic) NSUInteger questionIndex;

@property (nonatomic) BOOL loadCardFromXib;

@property (nonatomic, strong) UIBarButtonItem *reloadBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *leftBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *upBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *rightBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *downBarButtonItem;


@end

@implementation ZLSwipeableViewController

@synthesize objectionHandlingQuestions;

ZLSwipeableView *swipeableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.navigationController.toolbarHidden = NO;
    self.view.clipsToBounds = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Do any additional setup after loading the view, typically from a nib.
    self.colorIndex = 0;
    self.questionIndex = 0;
    self.colors = @[
                    @"Green Sea",
                    @"Emerald",
                    @"Nephritis",
                    @"Peter River",
                    @"Belize Hole",
                    @"Amethyst",
                    @"Wisteria",
                    @"Wet Asphalt",
                    @"Midnight Blue",
                    @"Sun Flower",
                    @"Orange",
                    @"Carrot",
                    @"Pumpkin",
                    @"Alizarin",
                    @"Pomegranate",
                    @"Concrete",
                    @"Asbestos"
                    ];
    
    self.reloadBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Reload"
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(handleReload:)];
    self.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"←"
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:@selector(handleLeft:)];
    self.upBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"↑"
                                                            style:UIBarButtonItemStylePlain
                                                           target:self
                                                           action:@selector(handleUp:)];
    self.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"→"
                                                               style:UIBarButtonItemStylePlain
                                                              target:self
                                                              action:@selector(handleRight:)];
    self.downBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"↓"
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:@selector(handleDown:)];
    
    UIBarButtonItem *fixedSpace =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                  target:nil
                                                  action:nil];
    UIBarButtonItem *flexibleSpace =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                  target:nil
                                                  action:nil];
    self.toolbarItems = @[
                          fixedSpace,
                          _reloadBarButtonItem,
                          flexibleSpace,
                          _leftBarButtonItem,
                          flexibleSpace,
                          _upBarButtonItem,
                          flexibleSpace,
                          _rightBarButtonItem,
                          flexibleSpace,
                          _downBarButtonItem,
                          fixedSpace
                          ];
    
    swipeableView = [[ZLSwipeableView alloc] initWithFrame:CGRectZero];
    self.swipeableView = swipeableView;
    [self.view addSubview:self.swipeableView];
    
    // Required Data Source
    self.swipeableView.dataSource = self;
    
    // Optional Delegate
    self.swipeableView.delegate = self;
    
    self.swipeableView.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    
    /*NSDictionary *metrics = @{};
    
    [self.view addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"|-50-[swipeableView]-50-|"
                               options:0
                               metrics:metrics
                               views:NSDictionaryOfVariableBindings(
                                                                    swipeableView)]];
    
    [self.view addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:|-120-[swipeableView]-100-|"
                               options:0
                               metrics:metrics
                               views:NSDictionaryOfVariableBindings(
                                                                    swipeableView)]];*/
    
     UIView *subView=swipeableView;
     UIView *parent=self.view;
     
     subView.translatesAutoresizingMaskIntoConstraints = NO;
    float topMargin;
    float sideMargin;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        topMargin = 100;
        sideMargin = 100;
    }
     
     //Trailing
     NSLayoutConstraint *trailing =[NSLayoutConstraint
     constraintWithItem:subView
     attribute:NSLayoutAttributeTrailing
     relatedBy:NSLayoutRelationEqual
     toItem:parent
     attribute:NSLayoutAttributeTrailing
     multiplier:1.0f
     constant:-50.f];
     
     //Leading
     
     NSLayoutConstraint *leading = [NSLayoutConstraint
     constraintWithItem:subView
     attribute:NSLayoutAttributeLeading
     relatedBy:NSLayoutRelationEqual
     toItem:parent
     attribute:NSLayoutAttributeLeading
     multiplier:1.0f
     constant:50.f];
     
     //Bottom
     NSLayoutConstraint *bottom =[NSLayoutConstraint
     constraintWithItem:subView
     attribute:NSLayoutAttributeBottom
     relatedBy:NSLayoutRelationEqual
     toItem:parent
     attribute:NSLayoutAttributeBottom
     multiplier:1.0f
     constant:-100.f];
     
     //Height to be fixed for SubView same as AdHeight
     NSLayoutConstraint *top = [NSLayoutConstraint
     constraintWithItem:subView
     attribute:NSLayoutAttributeTop
     relatedBy:NSLayoutRelationEqual
     toItem:parent
     attribute:NSLayoutAttributeTop
     multiplier:1.0f
     constant:100.f];
     
     //Add constraints to the Parent
     [parent addConstraint:trailing];
     [parent addConstraint:bottom];
     [parent addConstraint:leading];
     
     //Add top constraint to the subview, as subview owns it.
     [parent addConstraint:top];
}

- (BOOL)shouldAutorotate{
    return NO;
}

    
- (void)viewDidLayoutSubviews {
    [self.swipeableView loadViewsIfNeeded];
}

- (void)layoutSubviews {
    [self.swipeableView layoutSubviews];
}
#pragma mark - Action

- (void)handleLeft:(UIBarButtonItem *)sender {
    [self.swipeableView swipeTopViewToLeft];
}

- (void)handleRight:(UIBarButtonItem *)sender {
    [self.swipeableView swipeTopViewToRight];
}

- (void)handleUp:(UIBarButtonItem *)sender {
    [self.swipeableView swipeTopViewToUp];
}

- (void)handleDown:(UIBarButtonItem *)sender {
    [self.swipeableView swipeTopViewToDown];
}

- (void)handleReload:(UIBarButtonItem *)sender {
    UIActionSheet *actionSheet =
    [[UIActionSheet alloc] initWithTitle:@"Load Cards"
                                delegate:self
                       cancelButtonTitle:@"Cancel"
                  destructiveButtonTitle:nil
                       otherButtonTitles:@"Programmatically", @"From Xib", nil];
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    self.loadCardFromXib = buttonIndex == 1;
    self.colorIndex = 0;
    self.questionIndex = 0;
    [self.swipeableView discardAllViews];
    [self.swipeableView loadViewsIfNeeded];
}

#pragma mark - ZLSwipeableViewDelegate

- (void)swipeableView:(ZLSwipeableView *)swipeableView
         didSwipeView:(UIView *)view
          inDirection:(ZLSwipeableViewDirection)direction {
    NSLog(@"did swipe in direction: %zd", direction);
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView didCancelSwipe:(UIView *)view {
    NSLog(@"did cancel swipe");
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
  didStartSwipingView:(UIView *)view
           atLocation:(CGPoint)location {
    
    NSLog(@"did start swiping at location: x %f, y %f", location.x, location.y);
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
          swipingView:(UIView *)view
           atLocation:(CGPoint)location
          translation:(CGPoint)translation {
    NSLog(@"swiping at location: x %f, y %f, translation: x %f, y %f", location.x, location.y,
          translation.x, translation.y);
}

- (void)swipeableView:(ZLSwipeableView *)swipeableView
    didEndSwipingView:(UIView *)view
           atLocation:(CGPoint)location {
    NSLog(@"did end swiping at location: x %f, y %f", location.x, location.y);
}

#pragma mark - ZLSwipeableViewDataSource

- (UIView *)nextViewForSwipeableView:(ZLSwipeableView *)swipeableView {
    if (self.colorIndex >= self.colors.count) {
        self.colorIndex = 0;
    }
    
    if (self.questionIndex >= self.objectionHandlingQuestions.count){
        self.questionIndex = 0;
    }
    
    CardView *view = [[CardView alloc] initWithFrame:swipeableView.bounds];
    view.flipped = NO;
    
    view.question = [[UILabel alloc]initWithFrame:CGRectMake(view.frame.origin.x + 50, view.frame.origin.y + 50, view.frame.size.width - 100, view.frame.size.height - 100)];
    NSString *question = [[objectionHandlingQuestions allKeys] objectAtIndex:self.questionIndex];
    view.question.text = question;
    view.question.textColor = [UIColor whiteColor];
    view.question.lineBreakMode = NSLineBreakByWordWrapping;
    view.question.numberOfLines = 0;
    [view.question setFont:[UIFont systemFontOfSize:35.0]];
    view.question.minimumScaleFactor=0.5;
    view.question.adjustsFontSizeToFitWidth = YES;
    
    [view addSubview:view.question];
    
    view.answer = [[UILabel alloc]initWithFrame:CGRectMake(view.frame.origin.x + 50, view.frame.origin.y + 50, view.frame.size.width - 100, view.frame.size.height - 100)];
    NSString *answer = [objectionHandlingQuestions objectForKey:[[objectionHandlingQuestions allKeys] objectAtIndex:self.questionIndex]];
    view.answer.text = answer;
    view.answer.textColor = [UIColor whiteColor];
    view.answer.lineBreakMode = NSLineBreakByClipping;
    view.answer.numberOfLines = 0;
    [view.answer setFont:[UIFont systemFontOfSize:35.0]];
    view.answer.adjustsFontSizeToFitWidth = YES;
    view.answer.minimumScaleFactor=.4;
    view.answer.hidden = YES;
    
    
    [view addSubview:view.answer];
    
    view.translatesAutoresizingMaskIntoConstraints = NO;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [view addGestureRecognizer:tapRecognizer];
    
    
    
    view.backgroundColor = [self colorForName:self.colors[self.colorIndex]];
    
    
    self.colorIndex++;
    self.questionIndex++;
    
       
    return view;
}

- (void)tapAction:(UITapGestureRecognizer *)tap
{
    NSLog(@"tap recognized");
    [self.swipeableView tapTopView];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    
    [self layoutSubviews];
    [self.swipeableView layoutSubviews];

}

#pragma mark - ()


- (UIColor *)colorForName:(NSString *)name {
    NSString *sanitizedName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *selectorString = [NSString stringWithFormat:@"flat%@Color", sanitizedName];
    Class colorClass = [UIColor class];
    return [colorClass performSelector:NSSelectorFromString(selectorString)];
}

@end
