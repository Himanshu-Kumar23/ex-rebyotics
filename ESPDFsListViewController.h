//
//  ESPDFsListViewController.h
//  X Detailer
//
//  Created by Vikramjeet Singh on 02/04/19.
//  Copyright © 2019 EIQ Services. All rights reserved.
//

#import "ESSessionBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ESPDFsListViewController : ESSessionBaseViewController<ESMultipeerManagerDelegate>{
    
}

@property(nonatomic,strong)  NSMutableArray *pdfFiles;
@property(nonatomic, strong) UITableView *tableView;

-(NSString *) pathForFileName:(NSString *) fileName;
-(NSMutableArray *)findFiles:(NSString *)extension ;

@end

NS_ASSUME_NONNULL_END
