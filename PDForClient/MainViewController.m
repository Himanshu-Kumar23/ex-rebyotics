//
//  MainViewController.m
//  PDForClient
//
//  Created by Nagavardhan IV on 24/05/13.
//  Copyright (c) 2013 Nagavardhan IV. All rights reserved.
//

#import "MainViewController.h"
#import "CircleMenuViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController
@synthesize mode;

-(IBAction)practice:(id)sender{
    
    mode = @"Practice";
    NSString *file = [[NSBundle mainBundle] pathForResource:@"Document" ofType:@"pdf"];
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:file password:nil];
    if (document != nil)
    {
        DetailerReaderViewController *readerViewController = [[DetailerReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.delegate = self;
        readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        readerViewController.readerMode = mode;
        [self presentViewController:readerViewController animated:YES completion:nil];
    }

}

-(IBAction)prepare:(id)sender{
    
    
    mode = @"Prepare";
    NSString *file = [[NSBundle mainBundle] pathForResource:@"Document" ofType:@"pdf"];
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:file password:nil];
    if (document != nil)
    {
        DetailerReaderViewController *readerViewController = [[DetailerReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.delegate = self;
        readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        readerViewController.readerMode = mode;
        [self presentViewController:readerViewController animated:YES completion:nil];
    }
    
}

-(IBAction)present:(id)sender{
    
    
    mode = @"Present";
    NSString *file = [[NSBundle mainBundle] pathForResource:@"Document" ofType:@"pdf"];
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:file password:nil];
    if (document != nil)
    {
        DetailerReaderViewController *readerViewController = [[DetailerReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.delegate = self;
        readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        readerViewController.readerMode = mode;
        [self presentViewController:readerViewController animated:YES completion:nil];
    }
    
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    CircleMenuViewController *cvc = [[CircleMenuViewController alloc] initWithNibName:@"CircleViewController" bundle:nil];
    [self presentViewController:cvc animated:YES completion:nil];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    mode = [[NSString alloc]init];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
    
}*/


@end
