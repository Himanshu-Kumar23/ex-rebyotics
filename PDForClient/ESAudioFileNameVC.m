//
//  ESAudioFileNameVC.m
//  X Detailer
//
//  Created by Jayaprada Behera on 26/02/15.
//  Copyright (c) 2015 EIQ Services. All rights reserved.
//

#import "ESAudioFileNameVC.h"
#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
#define DOCUMENTS_FOLDER3 [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/"]

@interface ESAudioFileNameVC ()<UIActionSheetDelegate,UITableViewDelegate,UITableViewDataSource,UINavigationBarDelegate,MFMailComposeViewControllerDelegate,UINavigationControllerDelegate>
{
    UITableView *tableView_;
    NSArray *fileNames;
    NSString *cellValue;
    AVPlayerViewController *moviePlayerControllerforAudio;

}
@end

@implementation ESAudioFileNameVC

- (void)viewDidLoad {
    [super viewDidLoad];

    

    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Dismiss" style:UIBarButtonItemStylePlain target:self action:@selector(dismissView:)];
    
    self.navigationItem.leftBarButtonItem = cancelButton;
    [self populateAudioFileNamesArray];
    
    tableView_ = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) style:UITableViewStylePlain];
    tableView_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tableView_.dataSource = self;
    tableView_.delegate = self;
    [self.view addSubview:tableView_];
}
-(IBAction)dismissView:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void) populateAudioFileNamesArray {
    
//#warning logic meaning ??????.Two times an array is allocated.
    fileNames = [[NSArray alloc]init];
    fileNames = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:DOCUMENTS_FOLDER  error:nil];
    
    NSError *error;
    fileNames = [[NSArray alloc]init];
    fileNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:DOCUMENTS_FOLDER error:nil];
    if(error){
        //Handle error
    }
    else{
        NSMutableArray *tempArray=[NSMutableArray new];
        
        for (NSString *str in fileNames) {
            if ([str containsString:@"_audio"] || [str containsString:@"_video"]) {
                [tempArray addObject:str];
            }
        }
        
        fileNames = tempArray;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
        return [fileNames count];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
   UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (tableViewCell == nil) {
        tableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    tableViewCell.textLabel.text = [fileNames objectAtIndex:indexPath.row];
    
    NSString *filename = [fileNames objectAtIndex:indexPath.row];
    if ([filename containsString:@"audio"])
    {
        tableViewCell.imageView.image = [UIImage imageNamed:@"audioIcon.png"];
    }
    else
    {
        tableViewCell.imageView.image = [UIImage imageNamed:@"videoIcon.png"];
    }
    
    tableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return tableViewCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   cellValue = [fileNames objectAtIndex:indexPath.row];
    [self alertViewOpen];
}

-(void) alertViewOpen{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Please select an action"
                                 message:nil
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* playButton = [UIAlertAction
                               actionWithTitle:@"Play"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   NSString *path = [DOCUMENTS_FOLDER stringByAppendingPathComponent:cellValue];
                                   
                                   NSURL *movieURL = [NSURL fileURLWithPath:path];
                                   
                                   AVPlayer *player = [[AVPlayer alloc] initWithURL:movieURL];
                                   
                                   moviePlayerControllerforAudio = [[AVPlayerViewController alloc] init];
                                   
                                   moviePlayerControllerforAudio.player = player;
                                   
                                   
                                   //moviePlayerControllerforAudio = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
                                   
                                   
                                   //              moviePlayerControllerforAudio.fullscreen = YES;
                                   //              moviePlayerControllerforAudio.controlStyle = MPMovieControlStyleFullscreen;
                                   //              moviePlayerControllerforAudio.fullscreen = YES;
                                   //              moviePlayerControllerforAudio.controlStyle = MPMovieControlStyleFullscreen;
                                   //              moviePlayerControllerforAudio.scalingMode = MPMovieScalingModeAspectFill;
                                   //              [moviePlayerControllerforAudio setFullscreen:YES animated:YES];
                                   [[self->moviePlayerControllerforAudio view] setAutoresizesSubviews:YES];
                                   //[self.view addSubview:moviePlayerControllerforAudio.view];
                                   [self presentViewController:moviePlayerControllerforAudio animated:YES completion:nil];
                                   //[moviePlayerControllerforAudio setFullscreen:YES animated:YES];
                                   [[NSNotificationCenter defaultCenter] addObserver:self
                                                                            selector:@selector(moviePlayerDidExitFullscreen1:)
                                                                                name:AVPlayerItemDidPlayToEndTimeNotification
                                                                              object:nil];
                                   //              [[NSNotificationCenter defaultCenter] addObserver:self
                                   //                                                       selector:@selector(doneButtonClick:)
                                   //                                                           name:MPMoviePlayerWillExitFullscreenNotification
                                   //                                                         object:nil];
                                   //[moviePlayerControllerforAudio prepareToPlay];
                                   [moviePlayerControllerforAudio.player play];
                               }];
    
    /*UIAlertAction* emailButton = [UIAlertAction
                               actionWithTitle:@"e-Mail"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   [self startMail];
                               }];*/
    UIAlertAction* deleteButton = [UIAlertAction
                               actionWithTitle:@"Delete"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   NSFileManager *fileManager = [NSFileManager defaultManager];
                                   NSString *filePath2 = [DOCUMENTS_FOLDER
                                                          stringByAppendingPathComponent:cellValue];
                                   if ([fileManager removeItemAtPath:filePath2 error:nil] != YES)
                                       fileManager = [NSFileManager defaultManager];
                                   fileNames = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:DOCUMENTS_FOLDER  error:nil];
                                   [self populateAudioFileNamesArray];
                                   [tableView_ reloadData];
                               }];
    UIAlertAction* cancelButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   [tableView_ reloadData];
                               }];
    
    
    [alert addAction:playButton];
    //[alert addAction:emailButton];
    [alert addAction:deleteButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    //UIActionSheet *ActionSheet = [[UIActionSheet alloc]
    //                              initWithTitle:@"Please select an action" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Play", @"e-Mail", @"Delete", nil];
    
    
    //[ActionSheet addButtonWithTitle:@"Cancel"];
    //ActionSheet.actionSheetStyle=UIBarStyleBlackTranslucent;
    //[ActionSheet showInView:self.view];
    
}


- (void)moviePlayerDidExitFullscreen1:(NSNotification *)theNotification {
    NSLog(@"moviePlayerDidExitFullscreen1");
    //[moviePlayerControllerforAudio stop];
    //[moviePlayerControllerforAudio.view removeFromSuperview];
    //[closeButton removeFromSuperview];
}

- (void)doneButtonClick:(NSNotification *)theNotification {
    NSLog(@"doneButtonClick");
    [moviePlayerControllerforAudio.player pause];
    [moviePlayerControllerforAudio.view removeFromSuperview];
    //[closeButton removeFromSuperview];
}

-(void) startMail{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    
    if(mailClass !=nil){
        if ([mailClass canSendMail]){
            [self displaycomposersheet];
        }
    }
}

-(void) displaycomposersheet{
    MFMailComposeViewController *picker =[[MFMailComposeViewController alloc]init];
    
    picker.mailComposeDelegate =self;
    
    [picker setSubject:@"X-Sarcoidosis Recorded Audio file"];
    
    NSString *filePath2 = [DOCUMENTS_FOLDER
                           stringByAppendingPathComponent:cellValue];
    NSMutableData *data=[NSMutableData dataWithContentsOfFile:filePath2];
    [picker addAttachmentData:data mimeType:@"wav" fileName:cellValue];
    NSString *emailbody =@"Hi..";
    [picker setMessageBody:emailbody isHTML:NO];
    [self presentViewController:picker animated:YES completion:nil];
}

//Function used to check wheather mail has been sent or not

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [self dismissViewControllerAnimated:YES completion:Nil];
}

- (BOOL)shouldAutorotate{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
//    return YES;
//}
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
//        return UIInterfaceOrientationLandscapeLeft;
//}

@end
