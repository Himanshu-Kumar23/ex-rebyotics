#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "MobileCoreServices/MobileCoreServices.h"
#import <AssetsLibrary/ALAssetsLibrary.h>
#import <AudioUnit/AudioUnit.h>
#import <CoreAudio/CoreAudioTypes.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AudioUnit/AudioUnit.h>
#import "ReaderDocument.h"

#import <AVKit/AVKit.h>


@class DetailerReaderViewController;

@protocol ReaderViewControllerDelegate <NSObject>

@optional // Delegate protocols

- (void)dismissReaderViewController:(DetailerReaderViewController *)viewController;

@end

@interface DetailerReaderViewController : UIViewController<MPMediaPickerControllerDelegate,MPMediaPlayback,UIGestureRecognizerDelegate, AVAudioRecorderDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AVCaptureFileOutputRecordingDelegate, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, MFMailComposeViewControllerDelegate, UITextFieldDelegate, UITextViewDelegate>{
    
    AVAudioPlayer *audioPlayer;
    int recFlag;
    CGFloat pointX;
    CGFloat pointY;
    AVPlayerViewController *moviePlayer1;
    UIButton *closeButton;
    NSString *readerMode;
    CGRect mainViewRect;
    UINavigationItem *navigItem;
    BOOL playVideo;
    IBOutlet UIButton *audioRecorderMain;
    IBOutlet UIButton *videoRecorderMain;
    IBOutlet UIButton *audioFileManager;
    IBOutlet UIButton *videoFileManager;
    BOOL buttonShouldBeHiddenForVideoRecording;
    BOOL buttonShouldBeHiddenForAudioRecording;
    IBOutlet UIButton *pauseAudioRecoring;
    IBOutlet UIButton *stopAudioRecoring;
    IBOutlet UIButton *startAudioRecoring;
    IBOutlet UIButton *resumeAudioRecoring;
    IBOutlet UIButton *stopVideoRecoring;
    IBOutlet UIButton *startVideoRecoring;
    IBOutlet UIView *videoPreview;
    //IBOutlet UIPopoverPresentationController *popoverController;
    UIViewController *popoverContent;
    AVAudioPlayer *audioPlayer1;
    NSMutableDictionary *recordSetting;
    NSString *ct;
    NSString *mediaPath;
    NSError *err;
    AVAudioRecorder *recorder;
    AVAudioSession *audioSession;
    NSTimer *stopWatchTimer;
    IBOutlet UILabel *stopWatchLabel;
    NSDate *startDate;
    BOOL WeAreRecording;
    AVCaptureDeviceInput *VideoInputDevice;
    AVCaptureVideoPreviewLayer *PreviewLayer;
    AVCaptureSession *CaptureSession;
	AVCaptureMovieFileOutput *MovieFileOutput;
    NSFileManager *fileManager;
    UITableViewCell *cell;
    NSArray *fileNames;
    NSString *documentsDirectory;
    NSString *cellValue;
    UIView* popoverView;
    UITableView *tableView_;
    
    BOOL alreadyRotatedTheScreen;
    
    BOOL isViewLoaded;
    IBOutlet UIToolbar *bottomToolbar;
    IBOutlet UISlider *lineSlider;
    float lineValue;
    IBOutlet UILabel *lineSliderLbl;
    IBOutlet UISwitch *audioSwitch;
    BOOL audioOnOffDecider;
    UISegmentedControl *segmentedControl;
    
    BOOL mouseSwiped;
    CGPoint lastPoint;
    float colorRval;
    float colorGval;
    float colorBval;
    int mouseMoved;
    NSMutableArray *colors;
    NSMutableArray *colorR;
    NSMutableArray *colorG;
    NSMutableArray *colorB;
    
    IBOutlet UIView *bottonsView_iPad;
    IBOutlet UIView *bottonsView_iPhone;
    
    NSMutableArray *selectionStatusArray;
}
@property (nonatomic) BOOL alreadyRotatedTheScreen;
@property (nonatomic, unsafe_unretained, readwrite) id <ReaderViewControllerDelegate> delegate;
@property (nonatomic, strong) AVPlayerViewController *moviePlayerController;
@property (nonatomic, strong) AVPlayerViewController *moviePlayerControllerforAudio;
@property (nonatomic, strong) NSString *readerMode;
@property (nonatomic, retain) IBOutlet UIButton *audioRecorderMain;
@property (nonatomic, retain) IBOutlet UIButton *videoRecorderMain;
@property (nonatomic, retain) IBOutlet UIButton *audioFileManager;
@property (nonatomic, retain) IBOutlet UIButton *videoFileManager;
@property (nonatomic, retain) IBOutlet UIButton *pauseAudioRecoring;
@property (nonatomic, retain) IBOutlet UIButton *stopAudioRecoring;
@property (nonatomic, retain) IBOutlet UIButton *startAudioRecoring;
@property (nonatomic, retain) IBOutlet UIButton *resumeAudioRecoring;
@property (nonatomic, retain) IBOutlet UIButton *stopVideoRecoring;
@property (nonatomic, retain) IBOutlet UIButton *startVideoRecoring;
@property (nonatomic, retain) IBOutlet UILabel *stopWatchLabel;
@property (nonatomic, retain) IBOutlet UIView *videoPreview;

//BUTTONS FOR IPHONE
@property (nonatomic, retain) IBOutlet UIButton *audioRecorderMain_iPhone;
@property (nonatomic, retain) IBOutlet UIButton *videoRecorderMain_iPhone;
@property (nonatomic, retain) IBOutlet UIButton *audioFileManager_iPhone;
@property (nonatomic, retain) IBOutlet UIButton *videoFileManager_iPhone;
@property (nonatomic, retain) IBOutlet UIButton *pauseAudioRecoring_iPhone;
@property (nonatomic, retain) IBOutlet UIButton *stopAudioRecoring_iPhone;
@property (nonatomic, retain) IBOutlet UIButton *startAudioRecoring_iPhone;
@property (nonatomic, retain) IBOutlet UIButton *resumeAudioRecoring_iPhone;
@property (nonatomic, retain) IBOutlet UIButton *stopVideoRecoring_iPhone;
@property (nonatomic, retain) IBOutlet UIButton *startVideoRecoring_iPhone;
@property (nonatomic, retain) IBOutlet UILabel *stopWatchLabel_iPhone;
@property (nonatomic, retain) IBOutlet UIView *videoPreview_iPhone;


//For present Mode

@property (nonatomic,retain) IBOutlet UIToolbar *bottomToolbar;
@property (nonatomic,retain) IBOutlet UIImageView *drawImage;
@property (nonatomic,retain) IBOutlet UIView *drawView;
@property (nonatomic,retain) IBOutlet UIView *stopScroll;
@property (nonatomic,retain) IBOutlet UISlider *lineSlider;
@property (nonatomic,retain) IBOutlet UILabel *lineSliderLbl;
@property (nonatomic,retain) IBOutlet UILabel *markerlabel;
@property (nonatomic,retain) IBOutlet UIBarButtonItem *colorbutton;
@property (nonatomic,retain) IBOutlet UISwitch *audioSwitch;
@property (nonatomic,retain) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic,retain) NSString *presentationName;

- (id)initWithReaderDocument:(ReaderDocument *)object;

-(IBAction)audioRecorderMainPressed:(id)sender;
-(IBAction)videoRecorderMainPressed:(id)sender;
-(IBAction)audioFileManagerPressed:(id)sender;
-(IBAction)videoFileManagerPressed:(id)sender;
-(IBAction) segmentedControlIndexChanged;
-(IBAction)lineSliderChanged:(id)sender;
-(IBAction)colorChanged:(id)sender;
//- (void)handleOrientation;
@end
