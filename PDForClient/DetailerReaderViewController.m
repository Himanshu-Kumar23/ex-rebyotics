

#import "ReaderConstants.h"
#import "DetailerReaderViewController.h"
#import "ThumbsViewController.h"
#import "ReaderMainToolbar.h"
#import "ReaderMainPagebar.h"
#import "ReaderContentView.h"
#import "ReaderThumbCache.h"
#import "ReaderThumbQueue.h"
#import "MainViewController.h"
#import "ESAudioFileNameVC.h"
#import "UIImage+PDF.h"
#import <MessageUI/MessageUI.h>
#import <Photos/Photos.h>
#import <PhotosUI/PhotosUI.h>

//#import "CSPausibleTimer.h"

#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
#define DOCUMENTS_FOLDER3 [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/"]

@interface DetailerReaderViewController () <UIScrollViewDelegate, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate,
ReaderMainToolbarDelegate, ReaderMainPagebarDelegate, ReaderContentViewDelegate, ThumbsViewControllerDelegate, UIPopoverPresentationControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@end

@implementation DetailerReaderViewController
{
    ReaderDocument *document;
    
    UIScrollView *theScrollView;
    
    ReaderMainToolbar *mainToolbar;
    
    ReaderMainPagebar *mainPagebar;
    
    NSMutableDictionary *contentViews;
    
    UIPrintInteractionController *printInteraction;
    
    NSInteger currentPage;
    
    CGSize lastAppearSize;
    
    NSDate *lastHideTime;
    
    BOOL isVisible;
    
    UITableView *tblPages;
    
    UICollectionView *colPages;
     BOOL isScrollViewSet;
    BOOL isResumeAudioRecording;
    BOOL isAudioRecordPermitted;
    
    long selectedIndexRow;
    
    BOOL isHide;
    UIButton *zoomBtn;
    BOOL isOriented;
    
    NSString *OrientationName;
}

#pragma mark Constants

#define PAGING_VIEWS 3

#define TOOLBAR_HEIGHT 44.0f
#define PAGEBAR_HEIGHT 76.0f

#define TAP_AREA_SIZE 48.0f

#pragma mark Properties

@synthesize delegate, moviePlayerController, readerMode, audioFileManager, audioRecorderMain, videoFileManager, videoRecorderMain, pauseAudioRecoring, startAudioRecoring, stopAudioRecoring, stopVideoRecoring, resumeAudioRecoring, startVideoRecoring, stopWatchLabel, videoPreview, moviePlayerControllerforAudio, bottomToolbar, drawImage, drawView, lineSlider, lineSliderLbl, colorbutton, audioSwitch, segmentedControl, stopScroll, markerlabel, alreadyRotatedTheScreen,presentationName;

@synthesize audioFileManager_iPhone,audioRecorderMain_iPhone,videoRecorderMain_iPhone,videoPreview_iPhone,videoFileManager_iPhone,pauseAudioRecoring_iPhone,stopWatchLabel_iPhone,stopVideoRecoring_iPhone,stopAudioRecoring_iPhone,startAudioRecoring_iPhone,resumeAudioRecoring_iPhone,startVideoRecoring_iPhone;
/*These line of code is for warning*/
@synthesize isPreparedToPlay;
@synthesize currentPlaybackTime,currentPlaybackRate;
-(void)prepareToPlay{}
-(void)play{}
-(void)stop{}
-(void)endSeeking{}
-(void)pause{}
-(void)beginSeekingBackward{}
-(void)beginSeekingForward{}
/**/
#pragma mark - Button Actions
-(IBAction)videoFileManagerPressed:(id)sender{}

-(IBAction)audioRecorderMainPressed:(id)sender{
    
    NSString *device = [UIDevice currentDevice].model;
    
    if (buttonShouldBeHiddenForAudioRecording == NO) {
        buttonShouldBeHiddenForAudioRecording = YES;
        
        if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
            audioFileManager.hidden = YES;
            videoRecorderMain.hidden = YES;
            videoFileManager.hidden = YES;
            startAudioRecoring.hidden = NO;
            [audioRecorderMain setImage:[UIImage imageNamed:@"BackBtn.png"] forState:UIControlStateNormal];

        }else{
            audioFileManager_iPhone.hidden = YES;
            videoRecorderMain_iPhone.hidden = YES;
            videoFileManager_iPhone.hidden = YES;
            startAudioRecoring_iPhone.hidden = NO;
            [audioRecorderMain_iPhone setImage:[UIImage imageNamed:@"BackBtn.png"] forState:UIControlStateNormal];
        }
    }else if (buttonShouldBeHiddenForAudioRecording == YES){
        
        buttonShouldBeHiddenForAudioRecording = NO;
        
        if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
            
            audioFileManager.hidden = NO;
            videoRecorderMain.hidden = NO;
            videoFileManager.hidden = NO;
            startAudioRecoring.hidden = YES;
            [audioRecorderMain setImage:[UIImage imageNamed:@"AudioRecorder.png"] forState:UIControlStateNormal];

        }else{
            audioFileManager_iPhone.hidden = NO;
            videoRecorderMain_iPhone.hidden = NO;
            videoFileManager_iPhone.hidden = NO;
            startAudioRecoring_iPhone.hidden = YES;
            [audioRecorderMain_iPhone setImage:[UIImage imageNamed:@"AudioRecorder.png"] forState:UIControlStateNormal];
        }
    }
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        isAudioRecordPermitted = granted;
    }];
}

int isAudioRecord = 0;
//Audio Start Recording

-(IBAction)startRecording:(id)sender {
    
    if ([[AVAudioSession sharedInstance] recordPermission] != AVAudioSessionRecordPermissionGranted) {
        [self showAlertWithMessage:@"Audio Permissions are needed for recording, please grant microphone access in iOS Settings App" andTitle:@"Error"];
        return;
    }
    
    
    isResumeAudioRecording = YES;
    isAudioRecord = 1;
    NSString *device = [UIDevice currentDevice].model;
    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
        audioRecorderMain.hidden = YES;
        startAudioRecoring.hidden = YES;
        pauseAudioRecoring.hidden = NO;
        stopAudioRecoring.hidden = NO;
    }else{
        audioRecorderMain_iPhone.hidden = YES;
        startAudioRecoring_iPhone.hidden = YES;
        pauseAudioRecoring_iPhone.hidden = NO;
        stopAudioRecoring_iPhone.hidden = NO;
    }
    startDate = [NSDate date];

    
    
    [audioPlayer stop];
    [audioPlayer1 stop];
    
    NSDate *myDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    ct = [formatter stringFromDate:myDate];
    
    
    recordSetting = [[NSMutableDictionary alloc] init];
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:24000.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey];
    [recordSetting setValue:[NSNumber numberWithInt:AVAudioQualityMax] forKey:AVEncoderAudioQualityKey];
    
    
    
    NSError *error;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:mediaPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:mediaPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    
    mediaPath = [NSString stringWithFormat:@"%@/%@_audio_%@%@", DOCUMENTS_FOLDER,presentationName, ct,@".wav"];
    NSURL *url = [NSURL fileURLWithPath:mediaPath];
    err = nil;
    NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:nil];
    
    if(audioData) {
        
        NSFileManager *fm = [NSFileManager defaultManager];
        [fm removeItemAtPath:[url path] error:nil];
        
    }
    
    err = nil;
    NSError *err1 = nil;
    recorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&err1];
    
    if(!recorder || err1){
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Warning"
                                     message:[err1 localizedDescription]
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    //prepare to record
    [recorder setDelegate:self];
   BOOL prepareTORecord = [recorder prepareToRecord];
    recorder.meteringEnabled = YES;
    
    BOOL audioHwavailable = audioSession.inputAvailable;
    
    err = nil;
    if (!audioHwavailable || !prepareTORecord) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Warning"
                                     message:@"Audio input hardware not available"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
        
    }else{
        
        
        [recorder record];
        recFlag = 1;
        ispaused = NO;
        stopWatchTimer = [NSTimer scheduledTimerWithTimeInterval:1.0/10.0
                                                          target:self
                                                        selector:@selector(updateTimer)
                                                        userInfo:nil
                                                         repeats:YES];
    }
    
}

int hourvalue;
int mmvalue;
- (void)updateTimer
{
    if (isResumeAudioRecording) {
        NSDate *currentDate = [NSDate date];
        NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:startDate];
        NSDate *timerDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
        NSString *timeString=[dateFormatter stringFromDate:timerDate];
        
        NSString *device = [UIDevice currentDevice].model;
        if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
            stopWatchLabel.text = timeString;
        }else{
            stopWatchLabel_iPhone.text = timeString;
        }
    }
   
}

// To pause the audio recording.
-(IBAction)pauseRecording:(id)sender{
    isResumeAudioRecording = NO;
    ispaused = YES;
    [recorder pause];
    
    NSString *device = [UIDevice currentDevice].model;
    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
        pauseAudioRecoring.hidden = YES;
        stopAudioRecoring.hidden = YES;
        resumeAudioRecoring.hidden = NO;
        
    }else{
        pauseAudioRecoring_iPhone.hidden = YES;
        stopAudioRecoring_iPhone.hidden = YES;
        resumeAudioRecoring_iPhone.hidden = NO;
        
    }
//    pauseAudioRecoring.hidden = YES;
    stopAudioRecoring.hidden = YES;
    resumeAudioRecoring.hidden = NO;
    stopWatchLabel_iPhone.hidden=YES;
    stopWatchLabel.hidden=YES;

    pauseStart = [NSDate dateWithTimeIntervalSinceNow:0];
    elapseTime = [[NSDate date] timeIntervalSinceDate:startDate];
    //elapseTime = [NSDate dateWithTimeIntervalSince1970:diff];
    //previousFireDate = [stopWatchTimer fireDate];
    //[stopWatchTimer setFireDate:[NSDate distantFuture]];
    
    
}

// To resume the audio from pause

NSDate *pauseStart, *previousFireDate, *captureSessionPauseDate;
double timerInterval = 10.0,elapseTime = 0.0;
double captureSessionTimeInterval = 0.0;
double timerElapsed = 0.0;
BOOL ispaused;

-(IBAction)resumeRecording:(id)sender{
     isResumeAudioRecording = YES;
    ispaused = NO;
    [recorder record];
    
    NSString *device = [UIDevice currentDevice].model;
    
    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
        pauseAudioRecoring.hidden = NO;
        stopAudioRecoring.hidden = NO;
        resumeAudioRecoring.hidden = YES;
    }
    else {
        pauseAudioRecoring_iPhone.hidden = NO;
        stopAudioRecoring_iPhone.hidden = NO;
        resumeAudioRecoring_iPhone.hidden = YES;
        
    }
    
    startDate = [NSDate dateWithTimeIntervalSinceNow:-elapseTime];
    stopWatchLabel_iPhone.hidden=NO;
    stopWatchLabel.hidden=NO;

    [stopWatchTimer setFireDate:startDate];
    
}

-(IBAction)stopRecording:(id)sender{
     isResumeAudioRecording = NO;
    [recorder stop];
    recFlag = 0;
    
//    UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Audio successfully recorded ! Visit Audio File Manager to Play/Email/Delete the audio file." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
//    [alt show];
    NSString *device = [UIDevice currentDevice].model;
    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
        audioRecorderMain.hidden = NO;
        [audioRecorderMain setImage:[UIImage imageNamed:@"AudioRecorder.png"] forState:UIControlStateNormal];
        audioFileManager.hidden = NO;
        videoRecorderMain.hidden = NO;
        videoFileManager.hidden = NO;
        pauseAudioRecoring.hidden = YES;
        startAudioRecoring.hidden = YES;
        stopAudioRecoring.hidden = YES;
        stopVideoRecoring.hidden = YES;
        resumeAudioRecoring.hidden = YES;
        startVideoRecoring.hidden = YES;
        stopWatchLabel.text = @"";

    }else{
        
        audioRecorderMain_iPhone.hidden = NO;
        [audioRecorderMain_iPhone setImage:[UIImage imageNamed:@"AudioRecorder.png"] forState:UIControlStateNormal];
        audioFileManager_iPhone.hidden = NO;
        videoRecorderMain_iPhone.hidden = NO;
        videoFileManager_iPhone.hidden = NO;
        pauseAudioRecoring_iPhone.hidden = YES;
        startAudioRecoring_iPhone.hidden = YES;
        stopAudioRecoring_iPhone.hidden = YES;
        stopVideoRecoring_iPhone.hidden = YES;
        resumeAudioRecoring_iPhone.hidden = YES;
        startVideoRecoring_iPhone.hidden = YES;
        stopWatchLabel_iPhone.text = @"";

    }
    NSLog(@"path: %@", DOCUMENTS_FOLDER);
    buttonShouldBeHiddenForAudioRecording = NO;
    [stopWatchTimer invalidate];
}
int videoFlag = 0;

-(IBAction)videoRecorderMainPressed:(id)sender{
    
    NSString *device = [UIDevice currentDevice].model;
    if (buttonShouldBeHiddenForVideoRecording == NO) {
        buttonShouldBeHiddenForVideoRecording = YES;
        
        if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
            
            audioRecorderMain.hidden = YES;
            audioFileManager.hidden = YES;
            videoRecorderMain.hidden = NO;
            [videoRecorderMain setImage:[UIImage imageNamed:@"BackBtn.png"] forState:UIControlStateNormal];
            [videoRecorderMain setImage:[UIImage imageNamed:@"BackBtn.png"] forState:UIControlStateHighlighted];
            videoFileManager.hidden = YES;
            pauseAudioRecoring.hidden = YES;
            startAudioRecoring.hidden = YES;
            stopAudioRecoring.hidden = YES;
            stopVideoRecoring.hidden = YES;
            resumeAudioRecoring.hidden = YES;
            startVideoRecoring.hidden = NO;
            videoPreview.hidden = NO;
        }else{
            audioRecorderMain_iPhone.hidden = YES;
            audioFileManager_iPhone.hidden = YES;
            videoRecorderMain_iPhone.hidden = NO;
            [videoRecorderMain_iPhone setImage:[UIImage imageNamed:@"BackBtn.png"] forState:UIControlStateNormal];
            [videoRecorderMain_iPhone setImage:[UIImage imageNamed:@"BackBtn.png"] forState:UIControlStateHighlighted];
            videoFileManager_iPhone.hidden = YES;
            pauseAudioRecoring_iPhone.hidden = YES;
            startAudioRecoring_iPhone.hidden = YES;
            stopAudioRecoring_iPhone.hidden = YES;
            stopVideoRecoring_iPhone.hidden = YES;
            resumeAudioRecoring_iPhone.hidden = YES;
            startVideoRecoring_iPhone.hidden = NO;
            videoPreview_iPhone.hidden = NO;
            
        }
        if (WeAreRecording == NO && videoFlag == 0) {
            
            videoPreview.hidden = NO;
            //startVideoRecordBtn.hidden = NO;
            videoFlag = 1;
            CaptureSession = [[AVCaptureSession alloc] init];
            
            //----- ADD INPUTS -----
            
            
            //ADD VIDEO INPUT
            AVCaptureDevice *VideoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
            AVCaptureDevice *captureDevice = [self frontCamera];
            if (VideoDevice){
                NSError *error;
                VideoInputDevice = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
                if (!error){
                    if ([CaptureSession canAddInput:VideoInputDevice])
                        [CaptureSession addInput:VideoInputDevice];
                }
                else{
                }
            }
            else{}
            
            //ADD AUDIO INPUT
            
            
            //NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
            
            NSError *error = nil;
            
            AVCaptureDevice *audioCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
            
            AVCaptureDeviceInput *audioInput = [AVCaptureDeviceInput deviceInputWithDevice:audioCaptureDevice error:&error];
            if (audioInput){
                
                [CaptureSession addInput:audioInput];
            }
            //----- ADD OUTPUTS -----
            
            //ADD VIDEO PREVIEW LAYER
            
            PreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:CaptureSession];
            if ([device isEqualToString:@"iPad"]|| [device isEqualToString:@"iPad Simulator"]) {
                PreviewLayer.frame = self.videoPreview.bounds;
            }else{
                PreviewLayer.frame = self.videoPreview_iPhone.bounds;
            }
            

            //[PreviewLayer setOrientation:[UIDevice currentDevice].orientation];
            //            [PreviewLayer setOrientation:AVCaptureVideoOrientationLandscapeLeft];
            //ADD MOVIE FILE OUTPUT
            //[AVCaptureConnection setVideoOrientation:[UIDevice currentDevice].orientation];
            if (self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
                [[PreviewLayer connection] setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
            }
            else if (self.interfaceOrientation == UIInterfaceOrientationLandscapeRight)
            {
                [[PreviewLayer connection] setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
            }
            else
            {
                [[PreviewLayer connection] setVideoOrientation:AVCaptureVideoOrientationPortrait];
            }
            MovieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
            
            MovieFileOutput.minFreeDiskSpaceLimit = 1024 * 1024;						//<<SET MIN FREE SPACE IN BYTES FOR RECORDING TO CONTINUE ON A VOLUME
            
            if ([CaptureSession canAddOutput:MovieFileOutput])
                [CaptureSession addOutput:MovieFileOutput];
            
            //SET THE CONNECTION PROPERTIES (output properties)
            [self CameraSetOutputProperties];			//(We call a method as it also has to be done after changing camera)
            
            
            
            [CaptureSession setSessionPreset:AVCaptureSessionPresetMedium];
            if ([CaptureSession canSetSessionPreset:AVCaptureSessionPreset640x480])		//Check size based configs are supported before setting them
                [CaptureSession setSessionPreset:AVCaptureSessionPreset640x480];
            
            
            if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
                
                videoPreview.layer.cornerRadius= 10.0f;
                [self.videoPreview.layer addSublayer:PreviewLayer];
            }else{
                videoPreview_iPhone.layer.cornerRadius= 10.0f;
                [self.videoPreview_iPhone.layer addSublayer:PreviewLayer];
                
            }
            [CaptureSession startRunning];
            
        }else if (videoFlag ==1){
            [CaptureSession stopRunning];
            //startVideoRecordBtn.hidden = YES;
            [PreviewLayer removeFromSuperlayer];
            if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
                
                videoPreview.hidden = YES;
            }else{
                videoPreview_iPhone.hidden = YES;
            }
            videoFlag = 0;
        }
    } else if (buttonShouldBeHiddenForVideoRecording == YES && videoFlag ==1){
        NSString *device = [UIDevice currentDevice].model;
        if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
            
            audioRecorderMain.hidden = NO;
            audioFileManager.hidden = NO;
            videoRecorderMain.hidden = NO;
            videoFileManager.hidden = NO;
            [videoRecorderMain setImage:[UIImage imageNamed:@"VideoRecorder.png"] forState:UIControlStateNormal];
            [videoRecorderMain setImage:[UIImage imageNamed:@"VideoRecorder.png"] forState:UIControlStateHighlighted];
            pauseAudioRecoring.hidden = YES;
            startAudioRecoring.hidden = YES;
            stopAudioRecoring.hidden = YES;
            stopVideoRecoring.hidden = YES;
            resumeAudioRecoring.hidden = YES;
            startVideoRecoring.hidden = YES;
            videoPreview.hidden= YES;
        }else{
            audioRecorderMain_iPhone.hidden = NO;
            audioFileManager_iPhone.hidden = NO;
            videoRecorderMain_iPhone.hidden = NO;
            videoFileManager_iPhone.hidden = NO;
            [videoRecorderMain_iPhone setImage:[UIImage imageNamed:@"VideoRecorder.png"] forState:UIControlStateNormal];
            [videoRecorderMain_iPhone setImage:[UIImage imageNamed:@"VideoRecorder.png"] forState:UIControlStateHighlighted];
            pauseAudioRecoring_iPhone.hidden = YES;
            startAudioRecoring_iPhone.hidden = YES;
            stopAudioRecoring_iPhone.hidden = YES;
            stopVideoRecoring_iPhone.hidden = YES;
            resumeAudioRecoring_iPhone.hidden = YES;
            startVideoRecoring_iPhone.hidden = YES;
            videoPreview_iPhone.hidden= YES;
            
        }
        buttonShouldBeHiddenForVideoRecording = NO;
        [PreviewLayer removeFromSuperlayer];
        
        [CaptureSession stopRunning];
        //[PreviewLayer removeFromSuperlayer];
        videoFlag = 0;
    }
}


int keyFMFlag=0;
int MailFMFlag = 0;

-(IBAction)stopVideoRec:(id)sender{
    
    [MovieFileOutput stopRecording];
    [CaptureSession stopRunning];
    [PreviewLayer removeFromSuperlayer];
    buttonShouldBeHiddenForVideoRecording = NO;
    NSString *device = [UIDevice currentDevice].model;
    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
        
        videoPreview.hidden = YES;
    }else{
        videoPreview_iPhone.hidden = YES;
    }
    AVCaptureConnection *c = [MovieFileOutput connectionWithMediaType:AVMediaTypeVideo];
    //Start recording
    if (c.active) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Video Info"
                                     message:@"Video successfully recorded ! Visit File Manager to Play/Email/Delete the video file."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
//        UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Video Info" message:@"The recorded video is stored in photo library. User can e-mail the file or access via iTunes" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//        [alt show];
    }
   
    
    WeAreRecording = NO;
    videoFlag = 0;
    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
        videoRecorderMain.hidden = NO;
        [videoRecorderMain setImage:[UIImage imageNamed:@"VideoRecorder.png"] forState:UIControlStateNormal];
        startVideoRecoring.hidden = YES;
        stopVideoRecoring.hidden = YES;
        audioRecorderMain.hidden = NO;
        audioFileManager.hidden = NO;
        videoFileManager.hidden = NO;
    }else {
        videoRecorderMain_iPhone.hidden = NO;
        [videoRecorderMain_iPhone setImage:[UIImage imageNamed:@"VideoRecorder.png"] forState:UIControlStateNormal];
        startVideoRecoring_iPhone.hidden = YES;
        stopVideoRecoring_iPhone.hidden = YES;
        audioRecorderMain_iPhone.hidden = NO;
        audioFileManager_iPhone.hidden = NO;
        videoFileManager_iPhone.hidden = NO;
    }
    
}

int fileManagerFlag = 0;

-(IBAction)audioFileManagerPressed:(id)sender{
    
    fileManagerFlag = 1;
    
    [self populateAudioFileNmaesArray];
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone Simulator"]||[deviceType isEqualToString:@"iPhone"]|| [deviceType isEqualToString:@"iPod touch"]){
        ESAudioFileNameVC *audioFileNameVC = [[ESAudioFileNameVC alloc] init];
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:audioFileNameVC];
        //        navController.modalPresentationStyle = UIModalPresentationPageSheet;
        
        [self presentViewController:navController animated:YES completion:nil];
    }else{
         ESAudioFileNameVC *audioFileNameVC = [[ESAudioFileNameVC alloc] init];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:audioFileNameVC];
        //        navController.modalPresentationStyle = UIModalPresentationPageSheet;
        
        [self presentViewController:navController animated:YES completion:nil];
        
//        if([self->popoverController isPopoverVisible])
//        {
//            [self->popoverController dismissPopoverAnimated:YES];
//            return;
//        }
//        
//        UIViewController* popoverContent = [[UIViewController alloc]
//                                            init];
//        popoverView = [[UIView alloc]
//                       initWithFrame:CGRectMake(0, 0, 350, 450)];
//        //popoverView.backgroundColor = [UIColor blackColor];
//        
//        tableView_ = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 350, 450) style:UITableViewStylePlain];
//        //Remember to set the table view delegate and data provider
//        
//        tableView_.delegate=self;
//        tableView_.dataSource=self;
//        
//        [popoverView addSubview:tableView_];
//        //UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:popoverController];
//        popoverContent.view = popoverView;
//        
//        //resize the popover view shown
//        //in the current view to the view's size
//        popoverContent.contentSizeForViewInPopover =
//        CGSizeMake(350, 450);
//        
//        
//        //create a popover controller
//        self->popoverController = [[UIPopoverController alloc]
//                                   initWithContentViewController:popoverContent];
//        UINavigationController *navController =
//        [[UINavigationController alloc]
//         initWithRootViewController:popoverContent];
//        
//        
//        [popoverContent.navigationItem setPrompt:@"Also access the files using File Sharing in iTunes"];
//        
//        popoverController =
//        [[UIPopoverController alloc]
//         initWithContentViewController:navController];
//        //present the popover view non-modal with a
//        //refrence to the toolbar button which was pressed
//        
//        [self->popoverController presentPopoverFromRect:[audioFileManager frame] inView:[audioFileManager superview] permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
}

-(IBAction)startVideoRec:(id)sender{
    
    WeAreRecording = YES;
    NSString *device = [UIDevice currentDevice].model;
    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
        videoRecorderMain.hidden = YES;
        startVideoRecoring.hidden = YES;
        stopVideoRecoring.hidden = NO;
        
    }else {
        
        videoRecorderMain_iPhone.hidden = YES;
        startVideoRecoring_iPhone.hidden = YES;
        stopVideoRecoring_iPhone.hidden = NO;
        
    }
    
    //Create temporary URL to record to
    
    NSError *error;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:mediaPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:mediaPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    NSDate *myDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    ct = [formatter stringFromDate:myDate];

    
    mediaPath = [NSString stringWithFormat:@"%@/%@_video_%@%@", DOCUMENTS_FOLDER,presentationName, ct,@".mov"];
    NSURL *url = [NSURL fileURLWithPath:mediaPath];
    err = nil;
    NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:nil];
    
    if(audioData) {
        
        NSFileManager *fm = [NSFileManager defaultManager];
        [fm removeItemAtPath:[url path] error:nil];
        
    }
    
    videoFlag = 1;
    NSString *deviceType = [UIDevice currentDevice].model;
    if ([deviceType isEqualToString:@"iPhone Simulator"]|| [deviceType isEqualToString:@"iPad Simulator"]) {
        NSLog(@"Can't record video on simulator.");
        return;
    }
    
    
    AVCaptureConnection *c = [MovieFileOutput connectionWithMediaType:AVMediaTypeVideo];
    //Start recording
    if (c.active) {
        //connection is active
        [MovieFileOutput startRecordingToOutputFileURL:url recordingDelegate:self];
    }else{
        NSLog(@"Can't record video without connection enable.");
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Video Info"
                                     message:@"Can't record video because no AVCaptureMovieFileOutput active/enabled connections"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       videoFlag=0;
                                       self->buttonShouldBeHiddenForVideoRecording=NO;
                                       if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
                                           [self->videoRecorderMain setImage:[UIImage imageNamed:@"VideoRecorder.png"] forState:UIControlStateNormal];
                                           [self->videoRecorderMain setImage:[UIImage imageNamed:@"VideoRecorder.png"] forState:UIControlStateHighlighted];
                                       }
                                       else
                                       {
                                           [self->videoRecorderMain_iPhone setImage:[UIImage imageNamed:@"VideoRecorder.png"] forState:UIControlStateNormal];
                                           [self->videoRecorderMain_iPhone setImage:[UIImage imageNamed:@"VideoRecorder.png"] forState:UIControlStateHighlighted];
                                       }
                                       //Handle your yes please button action here
                                   }];
        
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
//        UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Video Info" message:@"Can't record video because no AVCaptureMovieFileOutput active/enabled connections" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//        [alt show];
        WeAreRecording = NO;
        videoFlag = 0;
        if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
            videoRecorderMain.hidden = NO;
            startVideoRecoring.hidden = YES;
            stopVideoRecoring.hidden = YES;
            audioRecorderMain.hidden = NO;
            audioFileManager.hidden = NO;
            videoFileManager.hidden = NO;
        }else {
            videoRecorderMain_iPhone.hidden = NO;
            startVideoRecoring_iPhone.hidden = YES;
            stopVideoRecoring_iPhone.hidden = YES;
            audioRecorderMain_iPhone.hidden = NO;
            audioFileManager_iPhone.hidden = NO;
            videoFileManager_iPhone.hidden = NO;
        }
        
        return;
    }
    
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(void) backButtonPressed{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)yourSwitch:(id)sender {
    if([audioSwitch isOn]){
        audioOnOffDecider = YES;
        if ((([readerMode isEqualToString:@"Prepare"] || [readerMode isEqualToString:@"Practice"]) && currentPage == 3 && videoFlag == 0) || ([readerMode isEqualToString:@"Present"] && audioOnOffDecider == YES && currentPage == 3))
        {
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/1.mp3", [[NSBundle mainBundle] resourcePath]]];
            
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            audioPlayer.numberOfLoops = 1;
            
            if (audioPlayer == nil){
                NSLog(@"Erroe1");
            }
            else if (recFlag == 0){
                [audioPlayer play];
            }
        }else if((([readerMode isEqualToString:@"Prepare"] || [readerMode isEqualToString:@"Practice"]) && currentPage == 3 && videoFlag == 0)|| ([readerMode isEqualToString:@"Present"] && audioOnOffDecider == YES && currentPage == 9))
        {
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/2.mp3", [[NSBundle mainBundle] resourcePath]]];
            
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            audioPlayer.numberOfLoops = 1;
            
            if (audioPlayer == nil){
                NSLog(@"Erroe2");
            }
            else if (recFlag == 0){
                [audioPlayer play];
            }
        }        
    }else{
        audioOnOffDecider = NO;
        [audioPlayer stop];
        if (moviePlaying == YES) {
            [moviePlayerController.player pause];
            [moviePlayerController.view removeFromSuperview];
            //[closeButton removeFromSuperview];
            
            moviePlaying = NO;
        }
    }
}
int laserYes = 0;

int labelflag = 0;
-(IBAction) segmentedControlIndexChanged{
    NSLog(@"Here1");
    switch (segmentedControl.selectedSegmentIndex) {
            NSLog(@"Here2");
        case 0:
            //lineSlider.hidden = YES;
            //lineSliderLbl.hidden = YES;
            
            [stopScroll removeFromSuperview];
            [drawImage removeFromSuperview];
            colorbutton.width = 0.01;
            drawView.hidden=YES;
            drawImage.hidden = YES;
            labelflag = 1;
            break;
        case 1:
            //lineSlider.hidden = NO;
            //lineSliderLbl.hidden = NO;
            NSLog(@"Here");
            drawImage.image = nil;
            drawImage.frame = self.drawView.frame;
            drawView.hidden=NO;
            drawImage.hidden = NO;
            [self.view addSubview:stopScroll];
            [self.view addSubview:drawView];
            [self.view addSubview:drawImage];
            colorbutton.width = 0;
            labelflag = 0;
            break;
        case 2:
            //lineSlider.hidden = NO;
            //lineSliderLbl.hidden = NO;
            laserYes = 1;
            NSLog(@"Here");
            drawImage.image = nil;
            drawImage.frame = self.drawView.frame;
            drawView.hidden=NO;
            drawImage.hidden = NO;
            [self.view addSubview:stopScroll];
            [self.view addSubview:drawView];
            [self.view addSubview:drawImage];
            colorbutton.width = 0;
            labelflag = 0;
            break;
            
        default:
            break;
    }
}

-(IBAction)lineSliderChanged:(id)sender{
    lineValue = lineSlider.value;
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error
{
    /*BOOL RecordedSuccessfully = YES;
     if ([error code] != noErr)
     {
     // A problem occurred: Find out if the recording was successful.
     id value = [[error userInfo] objectForKey:AVErrorRecordingSuccessfullyFinishedKey];
     if (value)
     {
     RecordedSuccessfully = [value boolValue];
     }
     }
     if (RecordedSuccessfully)*/
    {
        //----- RECORDED SUCESSFULLY -----
        
//        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
//        if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputFileURL])
//        {
//            [library writeVideoAtPathToSavedPhotosAlbum:outputFileURL
//                                        completionBlock:^(NSURL *assetURL, NSError *error)
//             {
//                 if (error)
//                 {
//                     
//                 }
//             }];
//        }
        
//        __block PHObjectPlaceholder *placeholder;
//
//        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
//            PHAssetChangeRequest* createAssetRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:outputFileURL];
//            placeholder = [createAssetRequest placeholderForCreatedAsset];
//
//        } completionHandler:^(BOOL success, NSError *error) {
//            if (success)
//            {
//                NSLog(@"didFinishRecordingToOutputFileAtURL - success for ios9");
//            }
//            else
//            {
//                NSLog(@"%@", error);
//            }
//        }];
    }
}
- (AVCaptureDevice *)frontCamera {
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == AVCaptureDevicePositionFront) {
            return device;
        }
    }
    return nil;
}
- (void) CameraSetOutputProperties{
    
    //SET THE CONNECTION PROPERTIES (output properties)
    AVCaptureConnection *CaptureConnection = [MovieFileOutput connectionWithMediaType:AVMediaTypeVideo];
    
    //Set landscape (if required)
    if ([CaptureConnection isVideoOrientationSupported]){
        
        //        AVCaptureVideoOrientation orientation = [UIDevice currentDevice].orientation;		//<<<<<SET VIDEO ORIENTATION IF LANDSCAPE
        
        if (self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
            [CaptureConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
        }
        else if (self.interfaceOrientation == UIInterfaceOrientationLandscapeRight)
        {
            [CaptureConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
        }
        else
        {
            [CaptureConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
        }

//        [CaptureConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
    }
    
    
}
#pragma mark- ImagePicker Delegates

-(void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    NSString *moviePath = [[info objectForKey:UIImagePickerControllerMediaURL]path];
    if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
        UISaveVideoAtPathToSavedPhotosAlbum (moviePath, nil, nil, nil);
    }
    
    
    //[self dismissModalViewControllerAnimated: YES];
    //[self.vImagePreview.layer removeFromSuperlayer];
    
    
    
    
}

-(void) populateAudioFileNmaesArray {
    
    fileManager = [NSFileManager defaultManager];
    
    fileNames = [[NSArray alloc]init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsDirectory = [paths objectAtIndex:0];
    fileNames = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:DOCUMENTS_FOLDER  error:nil];
    
    NSError *error;
    fileNames = [[NSArray alloc]init];
    fileNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:DOCUMENTS_FOLDER error:nil];
    if(error){
        //Handle error
    }
    else{
        NSMutableArray *tempArray=[NSMutableArray new];
        
        for (NSString *str in fileNames) {
            if ([str containsString:@"_audio"] || [str containsString:@"_video"]) {
                [tempArray addObject:str];
            }
        }
        
        fileNames = tempArray;
    }
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return document.pageCount.integerValue;
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.contentView.layer.borderColor = [[UIColor grayColor] CGColor];
    cell.contentView.layer.borderWidth = 0.5;
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
//    CGRect rect=cell.frame;
//    rect.origin.y=0;
//    cell.frame=rect;
    UIImageView *imagethumb=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, 70, 70)];
    imagethumb.backgroundColor=[UIColor whiteColor];
    [cell.contentView addSubview:imagethumb];
    
    
    if (indexPath.row==selectedIndexRow) {
        
        cell.layer.borderColor=self.navigationController.navigationBar.tintColor.CGColor;
        cell.layer.borderWidth=2.0;
//        UIView *viewthumb=[[UIView alloc] initWithFrame:CGRectMake(0,0, 32, 42)];
//        viewthumb.backgroundColor=[UIColor grayColor];
//        viewthumb.alpha=0.6;
//        [cell.contentView addSubview:viewthumb];
    }
    else
    {
        cell.layer.borderColor=[UIColor clearColor].CGColor;
    }

    NSURL *fileUrl = document.fileURL;
    long getImageAtIndex = indexPath.row + 1;
    int intIndexValue = (int)getImageAtIndex;
    UIImage *image = [ UIImage imageWithPDFURL:fileUrl atSize:CGSizeMake(140, 140) atPage:intIndexValue];

//    UIImage *image = [mainPagebar getImageFromPage:indexPath.row+1];
    
    if (image) {
        imagethumb.image = image;
    }else{
        if (indexPath.row < document.pageCount.integerValue) {
            NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
            [colPages reloadItemsAtIndexPaths:indexPaths];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                self->selectedIndexRow=indexPath.row;
            });
        }
    }
    
    if (indexPath.row < document.pageCount.integerValue-1) {
    }

    return cell;

}

//Size of item is defined here
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(70,70);
}
// Margins around the image
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
// Inter item spacing
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}
//Inter section spacing
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    selectedIndexRow=indexPath.row;
    if ([mainPagebar getPageNumber]!=indexPath.row+1) {
        [self showDocumentPage:indexPath.row+1];
    }

}
#pragma mark - UITABLEVIEW DATA SOURCE METHODS

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([tableView isEqual:tblPages]) {
        return document.pageCount.integerValue;
    }else{
        if (colorFlag == 1){
            return [colors count];
            
        }else if (fileManagerFlag == 1){
            return [fileNames count];
        }

    }
    
       return 0.f;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     if ([tableView isEqual:tblPages]) {
         static NSString *CellIdentifier = @"Cell";
         cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
         if (cell == nil) {
             cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
         }
         cell.contentView.layer.borderColor = [[UIColor grayColor] CGColor];
         cell.contentView.layer.borderWidth = 0.5;
         UIImage *image = [mainPagebar getImageFromPage:indexPath.row+1];

         
         if (image) {
              cell.imageView.image = image;
         }else{
             if (indexPath.row < document.pageCount.integerValue) {
                 NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                 [tblPages reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[tblPages cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] setSelected:YES];
                    
                 });

                 
             }
            
         }
         
          if (indexPath.row < document.pageCount.integerValue-1) {
          }
         
//         __block NSInteger page = 0;
//         
//         CGFloat contentOffsetX = theScrollView.contentOffset.x;
//         
//         [contentViews enumerateKeysAndObjectsUsingBlock: // Enumerate content views
//          ^(id key, id object, BOOL *stop)
//          {
//              ReaderContentView *contentView = object;
//              
//              if (contentView.frame.origin.x == contentOffsetX)
//              {
//                  page = contentView.tag; *stop = YES;
//              }
//          }
//          ];
//         if (page-1 == indexPath.row) {
//             [cell setSelected:YES];
//         }
         
         
        // cell.textLabel.text = [pageNos objectAtIndex:indexPath.row];
        // NSString *key =  [NSString stringWithFormat:@"%ld-L",(long)indexPath.row];// # key
//         ReaderContentView *targetView = [contentViews objectForKey:key];
       //[cell.contentView addSubview:[mainToolbar ]];
         return cell;

     }else{
         static NSString *CellIdentifier = @"Cell";
         
         cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
         if (cell == nil) {
             cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
         }
         if (colorFlag ==1) {
             NSString *imageName = [colors objectAtIndex:indexPath.row];
             cell.imageView.image = [UIImage imageNamed:imageName];
             return cell;
         }else if (fileManagerFlag == 1){
             cellValue=[fileNames objectAtIndex:indexPath.row];
             cell.textLabel.text = cellValue;
             return cell;
         }
         return cell;
     }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([tableView isEqual:tblPages]) {
        if ([mainPagebar getPageNumber]!=indexPath.row+1) {
             [self showDocumentPage:indexPath.row+1];
        }
       
       // [mainPagebar updatePageNumberText:indexPath.row+1];
        
    }else{
        if (colorFlag==1){
            colorRval = [(NSNumber *)[colorR objectAtIndex:indexPath.row] floatValue];
            colorGval = [(NSNumber *)[colorG objectAtIndex:indexPath.row] floatValue];
            colorBval = [(NSNumber *)[colorB objectAtIndex:indexPath.row] floatValue];
            if(popoverContent != nil)
            {
                [[popoverContent presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
                popoverContent = nil;
                return;
            }
        }else if (fileManagerFlag == 1){
            cellValue=[fileNames objectAtIndex:indexPath.row];
            
            
            [self alertViewOpen];
        }

    }
    
}

-(void)reloadData {
    [NSTimer scheduledTimerWithTimeInterval:0.5
                                     target:self
                                   selector:@selector(reloadTableviewData:)
                                   userInfo:nil
                                    repeats:NO];
}

-(IBAction)reloadTableviewData:(id)sender {
    [tblPages reloadData];
}

-(void) alertViewOpen{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Warning"
                                 message:[err localizedDescription]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* playButton = [UIAlertAction
                               actionWithTitle:@"Play"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   [tableView_ reloadData];
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   //[actionSheet dismissWithClickedButtonIndex:3 animated:NO];
                                   [self performSelector:@selector(dismissPopOver) withObject:nil afterDelay:0.1];
                                   [self performSelector:@selector(playRecordedAudio) withObject:nil afterDelay:0.1];
                               }];
    
    UIAlertAction* emailButton = [UIAlertAction
                               actionWithTitle:@"e-Mail"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   [tableView_ reloadData];
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   //[actionSheet dismissWithClickedButtonIndex:3 animated:NO];
                                   [self performSelector:@selector(dismissPopOver) withObject:nil afterDelay:0.1];
                                   [self performSelector:@selector(startMail) withObject:nil afterDelay:0.1];
                               }];
    
    UIAlertAction* deleteButton = [UIAlertAction
                               actionWithTitle:@"Delete"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   NSString *filePath2 = [DOCUMENTS_FOLDER
                                                          stringByAppendingPathComponent:cellValue];
                                   if ([fileManager removeItemAtPath:filePath2 error:nil] != YES)
                                       
                                       fileManager = [NSFileManager defaultManager];
                                   NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                   documentsDirectory = [paths objectAtIndex:0];
                                   fileNames = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:DOCUMENTS_FOLDER  error:nil];
                                   
                                   [self populateAudioFileNmaesArray];
                                   [tableView_ reloadData];
                               }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                                   [tableView_ reloadData];
                               }];
    
    
    [alert addAction:playButton];
    [alert addAction:emailButton];
    [alert addAction:deleteButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
//    UIActionSheet *ActionSheet = [[UIActionSheet alloc]
//                                  initWithTitle:@"Please select an action" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Play", @"e-Mail", @"Delete", nil];
//    
//    
//    [ActionSheet addButtonWithTitle:@"Cancel"];
//    ActionSheet.actionSheetStyle=UIBarStyleBlackTranslucent;
//    [ActionSheet showInView:popoverView];
//    [ActionSheet showFromRect:audioFileManager inView:self.view animated:YES];
    
}
-(void)playRecordedAudio{
    NSString *path = [DOCUMENTS_FOLDER stringByAppendingPathComponent:cellValue];
    NSURL *movieURL = [NSURL fileURLWithPath:path];
    //            BOOL hasAudioFile= [[NSFileManager defaultManager] fileExistsAtPath:path];
    
    AVPlayer *player = [[AVPlayer alloc] initWithURL:movieURL];
    
    moviePlayerControllerforAudio = [[AVPlayerViewController alloc] init];
    
    moviePlayerControllerforAudio.player = player;
    
    //moviePlayerControllerforAudio = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    
    
//    moviePlayerControllerforAudio.fullscreen = YES;
//    moviePlayerControllerforAudio.controlStyle = MPMovieControlStyleFullscreen;
//    moviePlayerControllerforAudio.fullscreen = YES;
//    moviePlayerControllerforAudio.controlStyle = MPMovieControlStyleFullscreen;
//    moviePlayerControllerforAudio.scalingMode = MPMovieScalingModeAspectFill;
//    [moviePlayerControllerforAudio setFullscreen:YES animated:YES];
    [[moviePlayerControllerforAudio view] setAutoresizesSubviews:YES];
    [self.view addSubview:moviePlayerControllerforAudio.view];
    //[moviePlayerControllerforAudio setFullscreen:YES animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayerDidExitFullscreen1:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(doneButtonClick:)
//                                                 name:MPMoviePlayerWillExitFullscreenNotification
//                                               object:nil];
    //[moviePlayerControllerforAudio prepareToPlay];
    [moviePlayerControllerforAudio.player play];
    

}
-(void)dismissPopOver{
//    
//    if(self->popoverController.isPopoverVisible){
//        [popoverController dismissPopoverAnimated:YES];
//    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
//    
//
//    switch (buttonIndex) {
//        case 0:{
//            [actionSheet dismissWithClickedButtonIndex:3 animated:NO];
//            [self performSelector:@selector(dismissPopOver) withObject:nil afterDelay:0.1];
//            [self performSelector:@selector(playRecordedAudio) withObject:nil afterDelay:0.1];
//            break;
//        }
//        case 1:{
//            [actionSheet dismissWithClickedButtonIndex:3 animated:NO];
//            [self performSelector:@selector(dismissPopOver) withObject:nil afterDelay:0.1];
//            [self performSelector:@selector(startMail) withObject:nil afterDelay:0.1];
//            break;
//        }
//        case 2:{
//            NSString *filePath2 = [DOCUMENTS_FOLDER
//                                   stringByAppendingPathComponent:cellValue];
//            if ([fileManager removeItemAtPath:filePath2 error:nil] != YES)
//                
//                fileManager = [NSFileManager defaultManager];
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//            documentsDirectory = [paths objectAtIndex:0];
//            fileNames = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:DOCUMENTS_FOLDER  error:nil];
//            
//            [self populateAudioFileNmaesArray];
//            [tableView_ reloadData];
//            break;
//        }
//        case 3:{
//            [tableView_ reloadData];
//            break;
//        }
//    }
//}

- (void)moviePlayerDidExitFullscreen1:(NSNotification *)theNotification {
    NSLog(@"moviePlayerDidExitFullscreen");
    //[moviePlayerControllerforAudio stop];
    [moviePlayerControllerforAudio.view removeFromSuperview];
    //[closeButton removeFromSuperview];
}

- (void)doneButtonClick:(NSNotification *)theNotification {
    NSLog(@"doneButtonClick");
    [moviePlayerControllerforAudio.player pause];
    [moviePlayerControllerforAudio.view removeFromSuperview];
    //[closeButton removeFromSuperview];
}

-(void) startMail{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    
    if(mailClass !=nil){
        if ([mailClass canSendMail]){
            [self displaycomposersheet];
        }
        else{
            [self launchmailappdevice];
        }
    }
    else{
        [self launchmailappdevice];
    }
}

-(void) displaycomposersheet{
    MFMailComposeViewController *picker =[[MFMailComposeViewController alloc]init];
    
    picker.mailComposeDelegate =self;
    
    [picker setSubject:@"X-Detailer Recorded Audio file"];
    
    
    NSString *filePath2 = [DOCUMENTS_FOLDER
                           stringByAppendingPathComponent:cellValue];
    NSMutableData *data=[NSMutableData dataWithContentsOfFile:filePath2];
    [picker addAttachmentData:data mimeType:@"wav" fileName:cellValue];
    NSString *emailbody =@"Hi";
    [picker setMessageBody:emailbody isHTML:NO];
    [self presentViewController:picker animated:YES completion:nil];
}

//Function used to check wheather mail has been sent or not

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [self dismissViewControllerAnimated:YES completion:Nil];
}

-(void) launchmailappdevice{
}
#pragma mark Support methods

- (void)updateScrollViewContentSize
{
    NSInteger count = [document.pageCount integerValue];    
    if (count > PAGING_VIEWS) count = PAGING_VIEWS; // Limit
    
    if (theScrollView.frame.size.height == 0 || theScrollView.frame.size.width == 0)
    {
        theScrollView.frame=mainViewRect;
    }
    CGFloat contentHeight = theScrollView.bounds.size.height;
    NSLog(@"height %f",contentHeight);
    CGFloat contentWidth = (theScrollView.bounds.size.width * count);
    NSLog(@"wid %f",contentWidth);

    theScrollView.contentSize = CGSizeMake(contentWidth, contentHeight);
}

- (void)updateScrollViewContentViews
{
    [self updateScrollViewContentSize]; // Update the content size
    
    NSMutableIndexSet *pageSet = [NSMutableIndexSet indexSet]; // Page set
    
    [contentViews enumerateKeysAndObjectsUsingBlock: // Enumerate content views
     ^(id key, id object, BOOL *stop)
     {
         ReaderContentView *contentView = object; [pageSet addIndex:contentView.tag];
     }
     ];
    
    __block CGRect viewRect = CGRectZero; viewRect.size = theScrollView.bounds.size;
    
    __block CGPoint contentOffset = CGPointZero; NSInteger page = [document.pageNumber integerValue];
    
    [pageSet enumerateIndexesUsingBlock: // Enumerate page number set
     ^(NSUInteger number, BOOL *stop)
     {
         NSNumber *key = [NSNumber numberWithInteger:number]; // # key
         
         ReaderContentView *contentView = [contentViews objectForKey:key];
         
         contentView.frame = viewRect; if (page == number) contentOffset = viewRect.origin;
         
         viewRect.origin.x += viewRect.size.width; // Next view frame position
     }
     ];
    
    if (CGPointEqualToPoint(theScrollView.contentOffset, contentOffset) == false)
    {
        theScrollView.contentOffset = contentOffset; // Update content offset
    }
}

- (void)updateToolbarBookmarkIcon{
    NSInteger page = [document.pageNumber integerValue];
    
    BOOL bookmarked = [document.bookmarks containsIndex:page];
    
    [mainToolbar setBookmarkState:bookmarked]; // Update
}
-(void)playAudioHavingName:(NSString *)audio{
    NSString *filePath = [NSString stringWithFormat:@"%@/%@",[[NSBundle mainBundle] resourcePath],audio];
    NSLog(@"filePath: %@, audio: %@", filePath, audio);
   
    NSURL *url = [NSURL fileURLWithPath:filePath];
    
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    audioPlayer.numberOfLoops = 0;
    
    if (audioPlayer == nil){
        NSLog(@"Erroe3");
    }
    else if (recFlag == 0){
        [audioPlayer play];
    }
}
- (void)showDocumentPage:(NSInteger)page{
    
    for (int i=0; i<[document.pageCount integerValue]; i++) {
        [selectionStatusArray addObject:[NSNumber numberWithBool:YES]];
    }
    
    if ([readerMode isEqualToString:@"Prepare"] ) {
        [colPages reloadData];

        NSInteger minValue; NSInteger maxValue;
        NSInteger maxPage = [document.pageCount integerValue];
        NSInteger minPage = 1;
        
        if ((page < minPage) || (page > maxPage)) return;
        
        if (maxPage <= PAGING_VIEWS) // Few pages
        {
            minValue = minPage;
            maxValue = maxPage;
        }
        else // Handle more pages
        {
            minValue = (page - 1);
            maxValue = (page + 1);
            
            if (minValue < minPage)
            {minValue++; maxValue++;}
            else
                if (maxValue > maxPage)
                {minValue--; maxValue--;}
        }
        
        NSMutableIndexSet *newPageSet = [NSMutableIndexSet new];
        
        NSMutableDictionary *unusedViews = [contentViews mutableCopy];
        
        CGRect viewRect = CGRectZero; viewRect.size = theScrollView.bounds.size;
        
        for (NSInteger number = minValue; number <= maxValue; number++)
        {
            NSNumber *key = [NSNumber numberWithInteger:number]; // # key
            
            ReaderContentView *contentView = [contentViews objectForKey:key];
            
            if (contentView == nil) // Create a brand new document content view
            {
                NSURL *fileURL = document.fileURL; NSString *phrase = document.password; // Document properties
                
                contentView = [[ReaderContentView alloc] initWithFrame:viewRect fileURL:fileURL page:number password:phrase];
                
                [theScrollView addSubview:contentView]; [contentViews setObject:contentView forKey:key];
                
                contentView.message = self; [newPageSet addIndex:number];
            }
            else // Reposition the existing content view
            {
                contentView.frame = viewRect; [contentView zoomReset];
                
                [unusedViews removeObjectForKey:key];
            }
            
            viewRect.origin.x += viewRect.size.width;
        }
        
        [unusedViews enumerateKeysAndObjectsUsingBlock: // Remove unused views
         ^(id key, id object, BOOL *stop)
         {
             [contentViews removeObjectForKey:key];
             
             ReaderContentView *contentView = object;
             
             [contentView removeFromSuperview];
         }
         ];
        
        unusedViews = nil; // Release unused views
        
        CGFloat viewWidthX1 = viewRect.size.width;
        CGFloat viewWidthX2 = (viewWidthX1 * 2.0f);
        
        CGPoint contentOffset = CGPointZero;
        
        if (maxPage >= PAGING_VIEWS)
        {
            if (page == maxPage)
                contentOffset.x = viewWidthX2;
            else
                if (page != minPage)
                    contentOffset.x = viewWidthX1;
        }
        else
            if (page == (PAGING_VIEWS - 1))
                contentOffset.x = viewWidthX1;
        
        if (CGPointEqualToPoint(theScrollView.contentOffset, contentOffset) == false)
        {
            theScrollView.contentOffset = contentOffset; // Update content offset
        }
        
        if ([document.pageNumber integerValue] != page) // Only if different
        {
            document.pageNumber = [NSNumber numberWithInteger:page]; // Update page number
        }
        
        NSURL *fileURL = document.fileURL; NSString *phrase = document.password; NSString *guid = document.guid;
        
        if ([newPageSet containsIndex:page] == YES) // Preview visible page first
        {
            NSNumber *key = [NSNumber numberWithInteger:page]; // # key
            
            ReaderContentView *targetView = [contentViews objectForKey:key];
            
            [targetView showPageThumb:fileURL page:page password:phrase guid:guid];
            
            [newPageSet removeIndex:page]; // Remove visible page from set
        }
        
        [newPageSet enumerateIndexesWithOptions:NSEnumerationReverse usingBlock: // Show previews
         ^(NSUInteger number, BOOL *stop)
         {
             NSNumber *key = [NSNumber numberWithInteger:number]; // # key
             
             ReaderContentView *targetView = [contentViews objectForKey:key];
             
             [targetView showPageThumb:fileURL page:number password:phrase guid:guid];
         }
         ];
        
        newPageSet = nil; // Release new page set
        
        [mainPagebar updatePagebar]; // Update the pagebar display
        
        [self updateToolbarBookmarkIcon]; // Update bookmark
        
        UIImage *image = [mainPagebar getImageFromPage:1];
        UIDeviceOrientation orientation= [[UIDevice currentDevice] orientation];
        if(!isOriented && !UIDeviceOrientationIsLandscape(orientation) && (image.size.width>image.size.height))
        {
            currentPage=1;
            isOriented=YES;
            OrientationName=@"Landscape";
            [[UIDevice currentDevice] setValue:@(UIDeviceOrientationLandscapeLeft) forKey:@"orientation"];
            [UINavigationController attemptRotationToDeviceOrientation];
        }
        else if (!isOriented && !UIDeviceOrientationIsPortrait(orientation) &&  image.size.width<image.size.height) {
            currentPage=1;
            isOriented=YES;
            OrientationName=@"Portrait";
            [[UIDevice currentDevice] setValue:@(UIDeviceOrientationPortrait) forKey:@"orientation"];
            [UINavigationController attemptRotationToDeviceOrientation];
        }
        else
        {
            
        }


        currentPage = page; // Track current page number
        
        NSLog(@"current page is %ld",(long)currentPage);
        
        /*if (currentPage != 6 || currentPage != 7) {
         [moviePlayerController stop];
         [moviePlayerController.view removeFromSuperview];
         [closeButton removeFromSuperview];
         }*/
        if ((([readerMode isEqualToString:@"Prepare"] ) && currentPage == 3 && videoFlag == 0) || ([readerMode isEqualToString:@"Present"] && audioOnOffDecider == YES && currentPage == 2) || ([readerMode isEqualToString:@"Practice"] && audioOnOffDecider == YES && currentPage == 2)){
            [self playAudioHavingName:@"1"];
        }else if((([readerMode isEqualToString:@"Prepare"] ) && currentPage == 5 && videoFlag == 0)){
            [self playAudioHavingName:@"2"];
        }else{
            [audioPlayer stop];
        }

    }
    
    else if ([readerMode isEqualToString:@"Practice"] || [readerMode isEqualToString:@"Present"]){
        
        [colPages reloadData];
        NSInteger minValue; NSInteger maxValue;
        NSInteger maxPage = [document.pageCount integerValue];
        NSInteger minPage = 1;
        
        if ((page < minPage) || (page > maxPage)) return;
        
        if (maxPage <= PAGING_VIEWS) // Few pages
        {
            minValue = minPage;
            maxValue = maxPage;
        }
        else // Handle more pages
        {
            minValue = (page - 1);
            maxValue = (page + 1);
            
            if (minValue < minPage)
            {minValue++; maxValue++;}
            else
                if (maxValue > maxPage)
                {minValue--; maxValue--;}
        }
        
        NSMutableIndexSet *newPageSet = [NSMutableIndexSet new];
        
        NSMutableDictionary *unusedViews = [contentViews mutableCopy];
        
        CGRect viewRect = CGRectZero; viewRect.size = theScrollView.bounds.size;
        
        for (NSInteger number = minValue; number <= maxValue; number++)
        {
            NSNumber *key = [NSNumber numberWithInteger:number]; // # key
            
            ReaderContentView *contentView = [contentViews objectForKey:key];
            
            if (contentView == nil) // Create a brand new document content view
            {
                NSURL *fileURL = document.fileURL; NSString *phrase = document.password; // Document properties
                
                contentView = [[ReaderContentView alloc] initWithFrame:viewRect fileURL:fileURL page:number password:phrase];
                
                [theScrollView addSubview:contentView]; [contentViews setObject:contentView forKey:key];
                
                contentView.message = self; [newPageSet addIndex:number];
            }
            else // Reposition the existing content view
            {
                contentView.frame = viewRect; [contentView zoomReset];
                
                [unusedViews removeObjectForKey:key];
            }
            
            viewRect.origin.x += viewRect.size.width;
        }
        
        [unusedViews enumerateKeysAndObjectsUsingBlock: // Remove unused views
         ^(id key, id object, BOOL *stop)
         {
             [contentViews removeObjectForKey:key];
             
             ReaderContentView *contentView = object;
             
             [contentView removeFromSuperview];
         }
         ];
        
        unusedViews = nil; // Release unused views
        
        CGFloat viewWidthX1 = viewRect.size.width;
        CGFloat viewWidthX2 = (viewWidthX1 * 2.0f);
        
        CGPoint contentOffset = CGPointZero;
        
        if (maxPage >= PAGING_VIEWS)
        {
            if (page == maxPage)
                contentOffset.x = viewWidthX2;
            else
                if (page != minPage)
                    contentOffset.x = viewWidthX1;
        }
        else
            if (page == (PAGING_VIEWS - 1))
                contentOffset.x = viewWidthX1;
        
        if (CGPointEqualToPoint(theScrollView.contentOffset, contentOffset) == false)
        {
            theScrollView.contentOffset = contentOffset; // Update content offset
        }
        
        if ([document.pageNumber integerValue] != page) // Only if different
        {
            document.pageNumber = [NSNumber numberWithInteger:page]; // Update page number
        }
        
        NSURL *fileURL = document.fileURL; NSString *phrase = document.password; NSString *guid = document.guid;
        
        if ([newPageSet containsIndex:page] == YES) // Preview visible page first
        {
            NSNumber *key = [NSNumber numberWithInteger:page]; // # key
            
            ReaderContentView *targetView = [contentViews objectForKey:key];
            
            [targetView showPageThumb:fileURL page:page password:phrase guid:guid];
            
            [newPageSet removeIndex:page]; // Remove visible page from set
        }
        
        [newPageSet enumerateIndexesWithOptions:NSEnumerationReverse usingBlock: // Show previews
         ^(NSUInteger number, BOOL *stop)
         {
             NSNumber *key = [NSNumber numberWithInteger:number]; // # key
             
             ReaderContentView *targetView = [contentViews objectForKey:key];
             
             [targetView showPageThumb:fileURL page:number password:phrase guid:guid];
         }
         ];
        
        newPageSet = nil; // Release new page set
        
        [mainPagebar updatePagebar]; // Update the pagebar display
        
        [self updateToolbarBookmarkIcon]; // Update bookmark
        
        UIImage *image = [mainPagebar getImageFromPage:1];
        UIDeviceOrientation orientation= [[UIDevice currentDevice] orientation];
        if(!isOriented && !UIDeviceOrientationIsLandscape(orientation) && (image.size.width>image.size.height))
        {
            currentPage=1;
            isOriented=YES;
            OrientationName=@"Landscape";
            [[UIDevice currentDevice] setValue:@(UIDeviceOrientationLandscapeLeft) forKey:@"orientation"];
            [UINavigationController attemptRotationToDeviceOrientation];
        }
        else if (!isOriented && !UIDeviceOrientationIsPortrait(orientation) &&  image.size.width<image.size.height) {
            currentPage=1;
            isOriented=YES;
            OrientationName=@"Portrait";
            [[UIDevice currentDevice] setValue:@(UIDeviceOrientationPortrait) forKey:@"orientation"];
            [UINavigationController attemptRotationToDeviceOrientation];
        }
        else
        {
            
        }

        currentPage = page; // Track current page number
        
        NSLog(@"current page is %ld",(long)currentPage);
        if ((([readerMode isEqualToString:@"Prepare"] || [readerMode isEqualToString:@"Practice"]) && currentPage == 2 && videoFlag == 0) || ([readerMode isEqualToString:@"Present"] && audioOnOffDecider == YES && currentPage == 2)){
            
            [self playAudioHavingName:@"1"];

        }else if ([readerMode isEqualToString:@"Prepare"] ){
            [self playAudioHavingName:@"2"];
        }else{
            [audioPlayer stop];
        }
    }
    
    NSArray *ArrayPdfs=[self findFiles:@"pdf"];
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory ,NSUserDomainMask, YES);
    NSString *documentsDirectory = [sysPaths objectAtIndex:0];
    NSString *fileName = [ArrayPdfs objectAtIndex:0];
    NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:@"AudioVideoFiles.plist"];
    
    if ([readerMode isEqualToString:@"Prepare"]){
        fileName = @"Rebyota.pdf";
    }
    NSMutableDictionary *plistDict; // needs to be mutable
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    } else {
        // Doesn't exist, start with an empty dictionary
        plistDict = [[NSMutableDictionary alloc] init];
    }
    
    NSDictionary *objectionHandlingQuestions = [plistDict objectForKey:fileName];
    NSLog(@"Obj: %@",objectionHandlingQuestions);
    
    //        NSArray *allKeysFiles=[objectionHandlingQuestions allKeys];
    NSString *fileNametobePlayed=[objectionHandlingQuestions objectForKey:[NSString stringWithFormat:@"%ld",(long)currentPage]];
    
    if ([readerMode isEqualToString:@"Prepare"]){
        if ([fileNametobePlayed containsString:@".mp3"]) {
            [self stopVideoPlayer];
            [self playAudioHavingName:fileNametobePlayed];
            moviePlaying=NO;
            //[self didRotate:nil];
        }
        
        else
        {
            [self stopVideoPlayer];
            [audioPlayer stop];
            moviePlaying=NO;
            //[self didRotate:nil];
        }
    }
}

-(NSMutableArray *)findFiles:(NSString *)extension {
    
    NSMutableArray *matches = [[NSMutableArray alloc]init];
    NSFileManager *fManager = [NSFileManager defaultManager];
    NSString *item;
    
    NSString *documentsDir = [self getDocumentDirectory];
    [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:documentsDir]];
    
    NSArray *contents = [fManager contentsOfDirectoryAtPath:documentsDir error:nil];
    
    // >>> this section here adds all files with the chosen extension to an array
    for (item in contents){
        if ([[item pathExtension] isEqualToString:extension]) {
            [matches addObject:item];
        }
    }
    
    return matches;
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL{
    
    if ([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]) {
        NSError *error = nil;
        
        BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                        
                                      forKey: NSURLIsExcludedFromBackupKey error: &error];
        if(!success){
            
            NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
            
        }
        return success;
    }else{
        return NO;
    }
    
}

-(NSString *) getDocumentDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}


- (void)showDocument:(id)object
{
    [self updateScrollViewContentSize]; // Set content size
    
    [self showDocumentPage:[document.pageNumber integerValue]];
    
    document.lastOpen = [NSDate date]; // Update last opened date
    
    isVisible = YES; // iOS present modal bodge
}

#pragma mark UIViewController methods

- (id)initWithReaderDocument:(ReaderDocument *)object
{
    id reader = nil; // ReaderViewController object
    
    if ((object != nil) && ([object isKindOfClass:[ReaderDocument class]]))
    {
        if ((self = [super initWithNibName:@"DetailerReaderViewController" bundle:nil])) // Designated initializer
            
            //        if ((self = [super initWithNibName:nil bundle:nil])) // Designated initializer
        {
            NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
            
            [notificationCenter addObserver:self selector:@selector(applicationWill:) name:UIApplicationWillTerminateNotification object:nil];
            
            [notificationCenter addObserver:self selector:@selector(applicationWill:) name:UIApplicationWillResignActiveNotification object:nil];
            
            [object updateProperties]; document = object; // Retain the supplied ReaderDocument object for our use
            
            [ReaderThumbCache touchThumbCacheWithGUID:object.guid]; // Touch the document thumb cache directory
            
            reader = self; // Return an initialized ReaderViewController object
        }
    }
    
    return reader;
}

-(void)hideAllButtonsforPrepareMode{
    NSString *device = [UIDevice currentDevice].model;
    bottomToolbar.hidden = YES;
    lineSliderLbl.hidden = YES;
    markerlabel.hidden = YES;
    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
        
        audioRecorderMain.hidden = YES;
        audioFileManager.hidden = YES;
        videoRecorderMain.hidden = YES;
        videoFileManager.hidden = YES;
        pauseAudioRecoring.hidden = YES;
        startAudioRecoring.hidden = YES;
        stopAudioRecoring.hidden = YES;
        stopVideoRecoring.hidden = YES;
        resumeAudioRecoring.hidden = YES;
        startVideoRecoring.hidden = YES;
        videoPreview.hidden = YES;
    }else{
        audioRecorderMain_iPhone.hidden = YES;
        audioFileManager_iPhone.hidden = YES;
        videoRecorderMain_iPhone.hidden = YES;
        videoFileManager_iPhone.hidden = YES;
        pauseAudioRecoring_iPhone.hidden = YES;
        startAudioRecoring_iPhone.hidden = YES;
        stopAudioRecoring_iPhone.hidden = YES;
        stopVideoRecoring_iPhone.hidden = YES;
        resumeAudioRecoring_iPhone.hidden = YES;
        startVideoRecoring_iPhone.hidden = YES;
        videoPreview_iPhone.hidden = YES;
    }
}
-(void)setUpForPrepareMode{
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    CGFloat constantVal = isHide?0:PAGEBAR_HEIGHT;
    

    UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];

    if (UIDeviceOrientationIsPortrait(orientation)) {
        
        if([deviceType isEqualToString:@"iPhone Simulator"]|| [deviceType isEqualToString:@"iPhone"] || [deviceType isEqualToString:@"iPod touch"]){
            
            mainViewRect = CGRectMake(0,0, self.view.bounds.size.width,self.view.bounds.size.height-constantVal);
        }else {
            mainViewRect =  CGRectMake(0,0, self.view.bounds.size.width,self.view.bounds.size.height-constantVal);
        }
    }
    else
    {
        
        mainViewRect = CGRectMake(0,0, self.view.bounds.size.width,self.view.bounds.size.height);
        
    }

    [self hideAllButtonsforPrepareMode];
}
-(void)setUpForPresentMode{
    NSString *device = [UIDevice currentDevice].model;
    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
    }
    mainViewRect = CGRectMake(27, 45, 1020, 660);
    
    [self hideAllButtonsforPrepareMode];
    
    audioOnOffDecider = NO;
    audioSwitch.on = NO;
    mouseMoved = 0;
    stopScroll = [[UIView alloc]initWithFrame:mainViewRect];
    drawView = [[UIView alloc]initWithFrame:CGRectMake(281, 63, 514, 626)];
    drawView.hidden = YES;
    drawImage = [[UIImageView alloc]initWithFrame:CGRectMake(281, 63, 514, 626)];
    drawImage.hidden = YES;
    lineValue = 10.0;
    colorRval = 1.0;
    colorGval = 0;
    colorBval = 0;
}
-(void)setUpForPracticeMode{
    
//    NSString *device = [UIDevice currentDevice].model;
//    
//    if([device isEqualToString:@"iPhone Simulator"]|| [device isEqualToString:@"iPhone"] || [device isEqualToString:@"iPod touch"]){
//        if ([UIScreen mainScreen].bounds.size.height == 320) {
//            mainViewRect = CGRectMake(0,12, self.view.bounds.size.width,self.view.bounds.size.height-50);
//            
//        }else{
//            mainViewRect = CGRectMake(0,2, self.view.bounds.size.width,self.view.bounds.size.height);
//        }
//        
//    }else {
//        mainViewRect = self.view.bounds;
//    }
    NSString *device = [UIDevice currentDevice].model;
    CGFloat width,height,y_origin;
    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                            (self.navigationController.navigationBar.frame.size.height ?: 0.0));

    UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
    CGFloat constantVal=0;
    if (UIDeviceOrientationIsPortrait(orientation)) {
        constantVal = isHide?0:PAGEBAR_HEIGHT;
    }
    NSString *deviceType = [UIDevice currentDevice].model;

    
    if (UIDeviceOrientationIsPortrait(orientation)) {
        
        if([deviceType isEqualToString:@"iPhone Simulator"]|| [deviceType isEqualToString:@"iPhone"] || [deviceType isEqualToString:@"iPod touch"]){
            
            mainViewRect = CGRectMake(0,0, self.view.bounds.size.width,self.view.bounds.size.height-constantVal);
        }else {
            mainViewRect =  CGRectMake(0,0, self.view.bounds.size.width,self.view.bounds.size.height-constantVal);
        }
    }
    else
    {
        
        mainViewRect = CGRectMake(0,0, self.view.bounds.size.width,self.view.bounds.size.height);
        
    }

//    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
//        width =self.view.bounds.size.width;
//        height = self.view.bounds.size.height-bottonsView_iPad.frame.size.height-constantVal;
//        y_origin =TOOLBAR_HEIGHT+1;
//    }else{
//        if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(@"7.1")) {
//            width =self.view.bounds.size.height;
//            height = self.view.bounds.size.width-bottonsView_iPhone.frame.size.height-constantVal;
//        }else{
//            width =self.view.bounds.size.width;
//            height =self.view.bounds.size.height-bottonsView_iPhone.frame.size.height-constantVal;
//        }
//        //        width =[UIScreen mainScreen].bounds.size.height;
//        //        height = [UIScreen mainScreen].bounds.size.width-100;
//
//        y_origin = TOOLBAR_HEIGHT-12;
//    }
//    CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
//
//    if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
//        mainViewRect =CGRectMake(0,0, width,height-34); //[UIScreen mainScreen].bounds; //CGRectMake(27, 45, 977, 610);
//    }
//    else
//    {
//
//        UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
//
//        if (UIDeviceOrientationIsPortrait(orientation)) {
//
//            mainViewRect =CGRectMake(0,0, width,height); //[UIScreen mainScreen].bounds; //CGRectMake(27, 45, 977, 610);
//        }
//        else
//        {
//
//            mainViewRect =CGRectMake(0,0, width,height); //[UIScreen mainScreen].bounds; //CGRectMake(27, 45, 977, 610);
//
//        }
//
//    }
    buttonShouldBeHiddenForAudioRecording = NO;
    buttonShouldBeHiddenForVideoRecording = NO;
    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
        audioRecorderMain.hidden = NO;
        audioFileManager.hidden = NO;
        videoRecorderMain.hidden = NO;
        videoFileManager.hidden = NO;
        pauseAudioRecoring.hidden = YES;
        startAudioRecoring.hidden = YES;
        stopAudioRecoring.hidden = YES;
        stopVideoRecoring.hidden = YES;
        resumeAudioRecoring.hidden = YES;
        startVideoRecoring.hidden = YES;
        videoPreview.hidden = YES;
    }else {
        audioRecorderMain_iPhone.hidden = NO;
        audioFileManager_iPhone.hidden = NO;
        videoRecorderMain_iPhone.hidden = NO;
        videoFileManager_iPhone.hidden = NO;
        pauseAudioRecoring_iPhone.hidden = YES;
        startAudioRecoring_iPhone.hidden = YES;
        stopAudioRecoring_iPhone.hidden = YES;
        stopVideoRecoring_iPhone.hidden = YES;
        resumeAudioRecoring_iPhone.hidden = YES;
        startVideoRecoring_iPhone.hidden = YES;
        videoPreview_iPhone.hidden = YES;
    }
    
    bottomToolbar.hidden = YES;
    lineSliderLbl.hidden = YES;
    markerlabel.hidden = YES;
    audioSession = [AVAudioSession sharedInstance];
    NSError *error = nil;
    [audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&error];
    if(error) {
        UIAlertController *AlertController = [UIAlertController alertControllerWithTitle:@"Unable to setup AudioSession" message:@"Sorry Audio Recorder/Player setup failed" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [AlertController addAction:cancel];
        [self presentViewController:AlertController animated:YES completion:nil];
        return;
    }
    error = nil;
    [audioSession setActive:YES error:&error];
    if (error) {
        NSLog(@"****** ERROR unable to set audioSession Active");
    }

}
-(void)addScrollViewToMainView{
    
    CGRect theFrame = mainViewRect;
    theFrame.size.width = mainViewRect.size.width;
    
    UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
    
    if (UIDeviceOrientationIsPortrait(orientation)) {
        theFrame.origin.x = 0;
    }
    else
    {
        theFrame.origin.x = colPages.frame.origin.x+colPages.frame.size.width;
    }
//    NSString *device = [UIDevice currentDevice].model;
//    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
//        theFrame.origin.y = mainViewRect.origin.y+30;
//        theFrame.size.height = mainViewRect.size.height-30;
//    }
    
    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                            (self.navigationController.navigationBar.frame.size.height ?: 0.0));

    theFrame.origin.y = mainViewRect.origin.y+topbarHeight;
    
    if ([readerMode isEqualToString:@"Prepare"]) {
        theFrame.size.height = mainViewRect.size.height-topbarHeight;
    }
    else
    {
    theFrame.size.height = mainViewRect.size.height-topbarHeight;
    }
    if (UIDeviceOrientationIsPortrait(orientation)) {
        theFrame.size.width = mainViewRect.size.width;
    }
    else
    {
        theFrame.size.width = mainViewRect.size.width-(2*(colPages.frame.origin.x+colPages.frame.size.width));
    }
    mainViewRect = theFrame;
    theScrollView = [[UIScrollView alloc] initWithFrame:mainViewRect]; // All
    theScrollView.scrollsToTop = NO;
    theScrollView.pagingEnabled = YES;
    theScrollView.delaysContentTouches = NO;
    theScrollView.showsVerticalScrollIndicator = NO;
    theScrollView.showsHorizontalScrollIndicator = NO;
    theScrollView.contentMode = UIViewContentModeRedraw;
    theScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    theScrollView.backgroundColor = [UIColor whiteColor];
    theScrollView.userInteractionEnabled = YES;
    theScrollView.autoresizesSubviews = NO;
    theScrollView.delegate = self;
    [self.view addSubview:theScrollView];
}
-(void)addGesturesToView{
    UITapGestureRecognizer *singleTapOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    singleTapOne.numberOfTouchesRequired = 1; singleTapOne.numberOfTapsRequired = 1; singleTapOne.delegate = self;
    [self.view addGestureRecognizer:singleTapOne];
    
    UITapGestureRecognizer *doubleTapOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    doubleTapOne.numberOfTouchesRequired = 1; doubleTapOne.numberOfTapsRequired = 2; doubleTapOne.delegate = self;
    [self.view addGestureRecognizer:doubleTapOne];
    
    UITapGestureRecognizer *doubleTapTwo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    doubleTapTwo.numberOfTouchesRequired = 2; doubleTapTwo.numberOfTapsRequired = 2; doubleTapTwo.delegate = self;
    [self.view addGestureRecognizer:doubleTapTwo];
    
    [singleTapOne requireGestureRecognizerToFail:doubleTapOne]; // Single tap requires double tap to fail
    
}

- (void) orientationChanged:(NSNotification *)note
{
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    isResumeAudioRecording = NO;
    fileManagerFlag = 0;
    colorFlag = 0;
    WeAreRecording = NO;
    isOriented=NO;
    selectionStatusArray=[NSMutableArray new];
    //MainViewController *mvc = [[MainViewController alloc]init];
    
    NSLog(@"Mode:%@", readerMode);
    playVideo = NO;
    
    assert(document != nil); // Must have a valid ReaderDocument
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    pauseAudioRecoring_iPhone.titleLabel.adjustsFontSizeToFitWidth = YES;
    stopAudioRecoring_iPhone.titleLabel.adjustsFontSizeToFitWidth = YES;
    resumeAudioRecoring_iPhone.titleLabel.adjustsFontSizeToFitWidth = YES;
    audioRecorderMain_iPhone.titleLabel.adjustsFontSizeToFitWidth = YES;
    audioFileManager_iPhone.titleLabel.adjustsFontSizeToFitWidth = YES;
    startAudioRecoring_iPhone.titleLabel.adjustsFontSizeToFitWidth = YES;
    startVideoRecoring_iPhone.titleLabel.adjustsFontSizeToFitWidth = YES;
    videoRecorderMain_iPhone.titleLabel.adjustsFontSizeToFitWidth = YES;
    stopVideoRecoring_iPhone.titleLabel.adjustsFontSizeToFitWidth = YES;

    //CGRect viewRect = self.view.bounds; // View controller's view bounds
    //CGRect viewRect = CGRectMake(20, 20, 985, 600);
    
    if ([readerMode isEqualToString:@"Prepare"]) {
        [self setUpForPrepareMode];
    }else if ([readerMode isEqualToString:@"Practice"]){
        [self setUpForPracticeMode];
    }else if ([readerMode isEqualToString:@"Present"]){
        [self setUpForPresentMode];
    }
    [self addUIElementsToMainView];
    [self addPagesTableView];

    [self addScrollViewToMainView];
    
    CGRect toolbarRect = mainViewRect;
    toolbarRect.size.height = TOOLBAR_HEIGHT;
//    toolbarRect.origin.x = mainViewRect.origin.y;
//    toolbarRect.origin.y = mainViewRect.origin.x;
//    toolbarRect.size.width = TOOLBAR_HEIGHT;
    
    mainToolbar = [[ReaderMainToolbar alloc] initWithFrame:toolbarRect document:document]; // At top
    
    mainToolbar.transform = CGAffineTransformMakeRotation(M_PI_2);
    
    mainToolbar.delegate = self;
    if ([readerMode isEqualToString:@"Prepare"]) {
        //[self.view addSubview:mainToolbar];
        //UIBarButtonItem *backBtn = [[UIBarButtonItem alloc]init];
        //[mainToolbar addSubview:backBtn];
        
    }
    //[self.view addSubview:mainToolbar];
    //    [bottomToolbar setHidden:NO];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    
    self.navigationItem.leftBarButtonItem = backItem;
    
    if ([readerMode isEqualToString:@"Prepare"]) {
        self.title =@"Detailer - Prepare Mode";
    }else if ([readerMode isEqualToString:@"Practice"]){
        self.title =@"Detailer - Practice Mode";
    }else if ([readerMode isEqualToString:@"Present"]){
        self.title =@"Detailer - Present Mode";
    }
    CGRect pagebarRect = mainViewRect;
    pagebarRect.size.height = PAGEBAR_HEIGHT;
    
    if ([readerMode isEqualToString:@"Prepare"]) {
        pagebarRect.origin.y = (mainViewRect.size.height - PAGEBAR_HEIGHT);
    }else if ([readerMode isEqualToString:@"Practice"]){
        pagebarRect.origin.y = (mainViewRect.size.height - PAGEBAR_HEIGHT );
    }
    
    mainPagebar = [[ReaderMainPagebar alloc] initWithFrame:pagebarRect document:document]; // At bottom
    
    mainPagebar.delegate = self;
    if ([readerMode isEqualToString:@"Prepare"] || [readerMode isEqualToString:@"Practice"]) {
        [self.view addSubview:mainPagebar];
    }
    [self addGesturesToView];
    contentViews = [NSMutableDictionary new]; lastHideTime = [NSDate date];
    [mainToolbar hideToolbar];
    [mainPagebar hidePagebar];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appHasGoneInBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    isScrollViewSet = NO;
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:UIDeviceOrientationDidChangeNotification object:nil];
    
    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                            (self.navigationController.navigationBar.frame.size.height ?: 0.0));

    zoomBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [zoomBtn setFrame:CGRectMake(10, topbarHeight+10, 24, 24)];
    [zoomBtn setBackgroundImage:[UIImage imageNamed:@"zoom"] forState:UIControlStateNormal];
    [zoomBtn addTarget:self action:@selector(zoomOutScreen) forControlEvents:UIControlEventTouchUpInside];
    
    isViewLoaded = NO;
    [self performSelector:@selector(ISViewLoaded) withObject:self afterDelay:3.0];
   // [self.view addSubview:zoomBtn];
    
    
    //iphone buttons
    audioRecorderMain_iPhone.layer.shadowColor = [UIColor grayColor].CGColor;
    audioRecorderMain_iPhone.layer.shadowOpacity = 1.0;
    audioRecorderMain_iPhone.layer.shadowRadius = 5;
    audioRecorderMain_iPhone.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    
    pauseAudioRecoring_iPhone.layer.shadowColor = [UIColor grayColor].CGColor;
    pauseAudioRecoring_iPhone.layer.shadowOpacity = 1.0;
    pauseAudioRecoring_iPhone.layer.shadowRadius = 5;
    pauseAudioRecoring_iPhone.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);

    stopAudioRecoring_iPhone.layer.shadowColor = [UIColor grayColor].CGColor;
    stopAudioRecoring_iPhone.layer.shadowOpacity = 1.0;
    stopAudioRecoring_iPhone.layer.shadowRadius = 5;
    stopAudioRecoring_iPhone.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);

    resumeAudioRecoring_iPhone.layer.shadowColor = [UIColor grayColor].CGColor;
    resumeAudioRecoring_iPhone.layer.shadowOpacity = 1.0;
    resumeAudioRecoring_iPhone.layer.shadowRadius = 5;
    resumeAudioRecoring_iPhone.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);

    audioFileManager_iPhone.layer.shadowColor = [UIColor grayColor].CGColor;
    audioFileManager_iPhone.layer.shadowOpacity = 1.0;
    audioFileManager_iPhone.layer.shadowRadius = 5;
    audioFileManager_iPhone.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);

    startAudioRecoring_iPhone.layer.shadowColor = [UIColor grayColor].CGColor;
    startAudioRecoring_iPhone.layer.shadowOpacity = 1.0;
    startAudioRecoring_iPhone.layer.shadowRadius = 5;
    startAudioRecoring_iPhone.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    
    startVideoRecoring_iPhone.layer.shadowColor = [UIColor grayColor].CGColor;
    startVideoRecoring_iPhone.layer.shadowOpacity = 1.0;
    startVideoRecoring_iPhone.layer.shadowRadius = 5;
    startVideoRecoring_iPhone.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);

    stopVideoRecoring_iPhone.layer.shadowColor = [UIColor grayColor].CGColor;
    stopVideoRecoring_iPhone.layer.shadowOpacity = 1.0;
    stopVideoRecoring_iPhone.layer.shadowRadius = 5;
    stopVideoRecoring_iPhone.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    
    videoRecorderMain_iPhone.layer.shadowColor = [UIColor grayColor].CGColor;
    videoRecorderMain_iPhone.layer.shadowOpacity = 1.0;
    videoRecorderMain_iPhone.layer.shadowRadius = 5;
    videoRecorderMain_iPhone.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);

    //ipad buttons
    
    audioRecorderMain.layer.shadowColor = [UIColor grayColor].CGColor;
    audioRecorderMain.layer.shadowOpacity = 1.0;
    audioRecorderMain.layer.shadowRadius = 5;
    audioRecorderMain.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    
    pauseAudioRecoring.layer.shadowColor = [UIColor grayColor].CGColor;
    pauseAudioRecoring.layer.shadowOpacity = 1.0;
    pauseAudioRecoring.layer.shadowRadius = 5;
    pauseAudioRecoring.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    
    stopAudioRecoring.layer.shadowColor = [UIColor grayColor].CGColor;
    stopAudioRecoring.layer.shadowOpacity = 1.0;
    stopAudioRecoring.layer.shadowRadius = 5;
    stopAudioRecoring.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    
    resumeAudioRecoring.layer.shadowColor = [UIColor grayColor].CGColor;
    resumeAudioRecoring.layer.shadowOpacity = 1.0;
    resumeAudioRecoring.layer.shadowRadius = 5;
    resumeAudioRecoring.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    
    audioFileManager.layer.shadowColor = [UIColor grayColor].CGColor;
    audioFileManager.layer.shadowOpacity = 1.0;
    audioFileManager.layer.shadowRadius = 5;
    audioFileManager.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    
    startAudioRecoring.layer.shadowColor = [UIColor grayColor].CGColor;
    startAudioRecoring.layer.shadowOpacity = 1.0;
    startAudioRecoring.layer.shadowRadius = 5;
    startAudioRecoring.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    
    startVideoRecoring.layer.shadowColor = [UIColor grayColor].CGColor;
    startVideoRecoring.layer.shadowOpacity = 1.0;
    startVideoRecoring.layer.shadowRadius = 5;
    startVideoRecoring.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    
    stopVideoRecoring.layer.shadowColor = [UIColor grayColor].CGColor;
    stopVideoRecoring.layer.shadowOpacity = 1.0;
    stopVideoRecoring.layer.shadowRadius = 5;
    stopVideoRecoring.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
    
    videoRecorderMain.layer.shadowColor = [UIColor grayColor].CGColor;
    videoRecorderMain.layer.shadowOpacity = 1.0;
    videoRecorderMain.layer.shadowRadius = 5;
    videoRecorderMain.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);



}
-(void)ISViewLoaded
{
    isViewLoaded = YES;

}
-(void)zoomOutScreen
{
    colPages.hidden=!colPages.hidden;
    if (colPages.hidden) {
        isHide=YES;
    }
    else
    {
        isHide=NO;
    }
    [self didRotate:nil];
}

- (void) didRotate:(NSNotification *)notification{
//    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    // do stuff
    NSLog(@"LOaded");
    if (isViewLoaded == YES)
    {
        return;
    }
    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                            (self.navigationController.navigationBar.frame.size.height ?: 0.0));

    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)[colPages collectionViewLayout];
    UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];

    if (UIDeviceOrientationIsPortrait(orientation)) {
        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
            colPages.frame=CGRectMake(0, self.view.bounds.size.height-PAGEBAR_HEIGHT-34, self.view.bounds.size.width,PAGEBAR_HEIGHT);
        }
        else
        {
            colPages.frame=CGRectMake(0, self.view.bounds.size.height-PAGEBAR_HEIGHT, self.view.bounds.size.width,PAGEBAR_HEIGHT);
        }
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    else if (UIDeviceOrientationIsLandscape(orientation))
    {
        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
            colPages.frame=CGRectMake(30, topbarHeight,PAGEBAR_HEIGHT,self.view.bounds.size.height-topbarHeight);
            NSLog(@"%f,%f,%f,%f",colPages.frame.size.height,colPages.frame.size.width,colPages.frame.origin.x,colPages.frame.origin.x);
        }
        else
        {
            colPages.frame=CGRectMake(0, topbarHeight,PAGEBAR_HEIGHT,self.view.bounds.size.height-topbarHeight);
            NSLog(@"%f,%f,%f,%f",colPages.frame.size.height,colPages.frame.size.width,colPages.frame.origin.x,colPages.frame.origin.x);
        }
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
    

    NSString *deviceType = [UIDevice currentDevice].model;
    CGFloat constantVal=0;
    CGFloat sideMargin=0;
    if (UIDeviceOrientationIsPortrait(orientation)) {
        constantVal = isHide?0:PAGEBAR_HEIGHT;
    }
    else{
        sideMargin=colPages.frame.origin.x+colPages.frame.size.width;
    }
    if([deviceType isEqualToString:@"iPhone Simulator"]|| [deviceType isEqualToString:@"iPhone"] || [deviceType isEqualToString:@"iPod touch"]){
        bottonsView_iPad = nil;
        CGFloat y_Origin,width ;
        if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(@"7.1")) {
            y_Origin =[UIScreen mainScreen].bounds.size.width-constantVal;
            width =[UIScreen mainScreen].bounds.size.height;
        }else{
            y_Origin =[UIScreen mainScreen].bounds.size.height-constantVal;
            width =[UIScreen mainScreen].bounds.size.width;
        }
        //        y_Origin =[UIScreen mainScreen].bounds.size.width-100;
        //        width =[UIScreen mainScreen].bounds.size.height;
        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;

        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
            
            [bottonsView_iPhone setFrame:CGRectMake(sideMargin, y_Origin-bottonsView_iPhone.frame.size.height-34,width-(2*sideMargin), bottonsView_iPhone.frame.size.height)];
        }
        else
        {
            [bottonsView_iPhone setFrame:CGRectMake(sideMargin, y_Origin-bottonsView_iPhone.frame.size.height,width-(2*sideMargin), bottonsView_iPhone.frame.size.height)];
            
        }
//        [self.view addSubview:bottonsView_iPhone];
    }else{
        bottonsView_iPhone = nil;
        [bottonsView_iPad setFrame:CGRectMake(sideMargin, self.view.frame.size.height-constantVal-bottonsView_iPad.frame.size.height, self.view.bounds.size.width-(2*sideMargin), bottonsView_iPad.frame.size.height)];
//        [self.view addSubview:bottonsView_iPad];
    }

    if ([readerMode isEqualToString:@"Prepare"]) {
    
        if (UIDeviceOrientationIsPortrait(orientation)) {
            
            if([deviceType isEqualToString:@"iPhone Simulator"]|| [deviceType isEqualToString:@"iPhone"] || [deviceType isEqualToString:@"iPod touch"]){
                
                mainViewRect = CGRectMake(0,0, self.view.bounds.size.width,self.view.bounds.size.height-constantVal);
            }else {
                mainViewRect =  CGRectMake(0,0, self.view.bounds.size.width,self.view.bounds.size.height-constantVal);
            }
        }
        else
        {
            
            mainViewRect = CGRectMake(0,0, self.view.bounds.size.width,self.view.bounds.size.height);
            
        }

    }else if ([readerMode isEqualToString:@"Practice"]){
        NSString *device = [UIDevice currentDevice].model;
        CGFloat width,height,y_origin;
        
        UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
        CGFloat constantVal=0;

        if (UIDeviceOrientationIsPortrait(orientation)) {
            constantVal = isHide?0:PAGEBAR_HEIGHT;
        }
        
        if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
            width =self.view.bounds.size.width;
            height = self.view.bounds.size.height-bottonsView_iPad.frame.size.height-constantVal;
            y_origin =TOOLBAR_HEIGHT+1;
        }else{
            if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(@"7.1")) {
                width =self.view.bounds.size.height;
                height = self.view.bounds.size.width-bottonsView_iPhone.frame.size.height-constantVal;
            }else{
                width =self.view.bounds.size.width;
                height =self.view.bounds.size.height-bottonsView_iPhone.frame.size.height-constantVal;
            }
            //        width =[UIScreen mainScreen].bounds.size.height;
            //        height = [UIScreen mainScreen].bounds.size.width-100;
            
            y_origin = TOOLBAR_HEIGHT-12;
        }
        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
        
        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
            mainViewRect =CGRectMake(0,0, width,height-34); //[UIScreen mainScreen].bounds; //CGRectMake(27, 45, 977, 610);
        }
        else
        {
            
            UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
            
            if (UIDeviceOrientationIsPortrait(orientation)) {
                
                mainViewRect =CGRectMake(0,0, width,height); //[UIScreen mainScreen].bounds; //CGRectMake(27, 45, 977, 610);
            }
            else
            {
                
                mainViewRect =CGRectMake(0,0, width,height); //[UIScreen mainScreen].bounds; //CGRectMake(27, 45, 977, 610);
                
            }
            
        }
    }
    NSLog(@"Main %f,%f,%f,%f",mainViewRect.origin.x,mainViewRect.origin.y,mainViewRect.size.width,mainViewRect.size.height);

    CGRect theFrame = mainViewRect;
    
    if (UIDeviceOrientationIsPortrait(orientation)) {
        theFrame.origin.x = 0;
    }
    else
    {
        theFrame.origin.x = colPages.frame.origin.x+colPages.frame.size.width;
    }
    //    NSString *device = [UIDevice currentDevice].model;
    //    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
    //        theFrame.origin.y = mainViewRect.origin.y+30;
    //        theFrame.size.height = mainViewRect.size.height-30;
    //    }
    
    
    theFrame.origin.y = mainViewRect.origin.y+topbarHeight;
    
    if ([readerMode isEqualToString:@"Prepare"]) {
        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
        
        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
            theFrame.size.height = mainViewRect.size.height-topbarHeight-30;
        }
        else
        {
            theFrame.size.height = mainViewRect.size.height-topbarHeight;
        }
    }
    else
    {
        theFrame.size.height = mainViewRect.size.height-topbarHeight;
    }
    if (UIDeviceOrientationIsPortrait(orientation)) {
        theFrame.size.width = mainViewRect.size.width;
    }
    else
    {
        theFrame.size.width = mainViewRect.size.width-(2*(colPages.frame.origin.x+colPages.frame.size.width));
    }


    mainViewRect = theFrame;
    NSLog(@"check %f,%f,%f,%f",theFrame.origin.x,theFrame.origin.y,theFrame.size.height,theFrame.size.width);

    theScrollView.frame=mainViewRect;
    NSLog(@"%f,%f,%f,%f",theScrollView.frame.origin.x,theScrollView.frame.origin.y,theScrollView.frame.size.width,theScrollView.frame.size.height);
    
    CGRect toolbarRect = mainViewRect;
    toolbarRect.size.height = TOOLBAR_HEIGHT;
    //    toolbarRect.origin.x = mainViewRect.origin.y;
    //    toolbarRect.origin.y = mainViewRect.origin.x;
    //    toolbarRect.size.width = TOOLBAR_HEIGHT;
    mainToolbar.frame=toolbarRect;
    
    mainToolbar.transform = CGAffineTransformMakeRotation(M_PI_2);

    CGRect pagebarRect = mainViewRect;
    pagebarRect.size.height = PAGEBAR_HEIGHT;
    mainPagebar.frame=pagebarRect;

//    [self viewDidLoad];
    isResumeAudioRecording = NO;
    //for iPhone the height of scrollview is getting zero .so need to set frame here
    if (!isScrollViewSet) {
        if ([readerMode isEqualToString:@"Practice"] ) {
            theScrollView.frame = mainViewRect;
        }
        isScrollViewSet = YES;
    }
    
    if (CGSizeEqualToSize(lastAppearSize, CGSizeZero) == false)
    {
        if (CGSizeEqualToSize(lastAppearSize, self.view.bounds.size) == false)
        {
            [self updateScrollViewContentViews]; // Update content views
        }
        
        lastAppearSize = CGSizeZero; // Reset view size tracking
    }
    fileManagerFlag = 0;
    colorFlag = 0;
    [mainToolbar hideToolbar];
    [mainPagebar hidePagebar];
    
    [zoomBtn setFrame:CGRectMake(10, topbarHeight+10, 24, 24)];
    [self updateScrollViewContentSize]; // Set content size

    [self showDocumentPage:currentPage];
//    [self viewDidAppear:YES];

}

-(void)addLoader {
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    isResumeAudioRecording = NO;
    //for iPhone the height of scrollview is getting zero .so need to set frame here
    if (!isScrollViewSet) {
        if ([readerMode isEqualToString:@"Practice"] ) {
            theScrollView.frame = mainViewRect;
        }
         isScrollViewSet = YES;
    }
   
    if (CGSizeEqualToSize(lastAppearSize, CGSizeZero) == false)
    {
        if (CGSizeEqualToSize(lastAppearSize, self.view.bounds.size) == false)
        {
            [self updateScrollViewContentViews]; // Update content views
        }
        
        lastAppearSize = CGSizeZero; // Reset view size tracking
    }
    fileManagerFlag = 0;
    colorFlag = 0;
    [mainToolbar hideToolbar];
     [mainPagebar hidePagebar];
   
}

-(IBAction)appHasGoneInBackground:(NSNotificationCenter *)notification {
    [stopWatchTimer invalidate];
    [audioPlayer stop];
    [audioPlayer1 stop];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

//    if (CGSizeEqualToSize(theScrollView.contentSize, CGSizeZero)) // First time
//    {
        [self performSelector:@selector(showDocument:) withObject:nil afterDelay:1.5];
//    }
    [self didRotate:nil];

#if (READER_DISABLE_IDLE == TRUE) // Option
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    
#endif // end of READER_DISABLE_IDLE Option
}

-(void)addPagesTableView {
//    CGRect rect=[UIScreen mainScreen].bounds;
//    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
//                            (self.navigationController.navigationBar.frame.size.height ?: 0.0));
    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(32, 42);
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    colPages  = [[UICollectionView alloc] initWithFrame:CGRectMake(4, [UIScreen mainScreen].bounds.size.height-((IS_IPHONE)?84:124), [UIScreen mainScreen].bounds.size.width-8,PAGEBAR_HEIGHT) collectionViewLayout:flowLayout];
    colPages.backgroundColor=[UIColor colorWithRed:250/255.f green:250/255.f blue:250/255.f alpha:1.0];
//    UICollectionView *colPages = [[UICollectionView alloc] initWithFrame:CGRectMake(4, topbarHeight, 100,[UIScreen mainScreen].bounds.size.height-topbarHeight)collectionViewLayout:];
    [colPages registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    [colPages setShowsHorizontalScrollIndicator:NO];
    [colPages setShowsVerticalScrollIndicator:NO];
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        if ([[UIScreen mainScreen] nativeBounds].size.height == 2436 || [[UIScreen mainScreen] nativeBounds].size.height ==2688 || [[UIScreen mainScreen] nativeBounds].size.height == 1792) {
            colPages.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-PAGEBAR_HEIGHT, [UIScreen mainScreen].bounds.size.width,PAGEBAR_HEIGHT);
        }
        else
        {
            colPages.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-PAGEBAR_HEIGHT, [UIScreen mainScreen].bounds.size.width,PAGEBAR_HEIGHT);
        }
    }
    
    NSString *device = [UIDevice currentDevice].model;
    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
        colPages.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-PAGEBAR_HEIGHT, [UIScreen mainScreen].bounds.size.width,PAGEBAR_HEIGHT);
    }
    
    UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                            (self.navigationController.navigationBar.frame.size.height ?: 0.0));
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)[colPages collectionViewLayout];
    
    if (UIDeviceOrientationIsPortrait(orientation)) {
        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
            colPages.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-PAGEBAR_HEIGHT-34, [UIScreen mainScreen].bounds.size.width,PAGEBAR_HEIGHT);
        }
        else
        {
            colPages.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-PAGEBAR_HEIGHT, [UIScreen mainScreen].bounds.size.width,PAGEBAR_HEIGHT);
        }
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    else if (UIDeviceOrientationIsLandscape(orientation))
    {
        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {
            colPages.frame=CGRectMake(30, topbarHeight,PAGEBAR_HEIGHT,[UIScreen mainScreen].bounds.size.height-topbarHeight);
            NSLog(@"%f,%f,%f,%f",colPages.frame.size.height,colPages.frame.size.width,colPages.frame.origin.x,colPages.frame.origin.x);
        }
        else
        {
            colPages.frame=CGRectMake(0, topbarHeight,PAGEBAR_HEIGHT,[UIScreen mainScreen].bounds.size.height-topbarHeight);
            NSLog(@"%f,%f,%f,%f",colPages.frame.size.height,colPages.frame.size.width,colPages.frame.origin.x,colPages.frame.origin.x);
        }
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }

    
    
    [colPages setDataSource:self];
    [colPages setDelegate:self];
    [colPages setAllowsMultipleSelection:NO];
    [self.view addSubview:colPages];
    [colPages reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    selectedIndexRow=indexPath.row;
//    UITableViewCell *tcell = [colPages cellForItemAtIndexPath:indexPath];
//    [tcell setSelected:YES];
//    tblPages = [[UITableView alloc] initWithFrame:CGRectMake(4, topbarHeight, 100,[UIScreen mainScreen].bounds.size.height-topbarHeight) style:UITableViewStylePlain];
//
//    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
//        if ([[UIScreen mainScreen] nativeBounds].size.height == 2436 || [[UIScreen mainScreen] nativeBounds].size.height ==2688 || [[UIScreen mainScreen] nativeBounds].size.height == 1792) {
//            tblPages = [[UITableView alloc] initWithFrame:CGRectMake(34, topbarHeight, 100,[UIScreen mainScreen].bounds.size.height-topbarHeight) style:UITableViewStylePlain];
//        }
//        else
//        {
//            tblPages = [[UITableView alloc] initWithFrame:CGRectMake(4, topbarHeight, 100,[UIScreen mainScreen].bounds.size.height-topbarHeight) style:UITableViewStylePlain];
//        }
//    }
//
//    NSString *device = [UIDevice currentDevice].model;
//    if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
//        [tblPages setFrame:CGRectMake(tblPages.frame.origin.x, tblPages.frame.origin.y+20, tblPages.frame.size.width, tblPages.frame.size.height-40)];
//    }
//
//
//
//    tblPages.layer.cornerRadius = 10.0f;
//    tblPages.layer.borderWidth = 5.0f;
//    tblPages.layer.borderColor = [[UIColor grayColor] CGColor];
//    tblPages.layer.cornerRadius = 10.0f;
//    tblPages.layer.borderWidth = 5.0f;
//    tblPages.layer.borderColor = [[UIColor grayColor] CGColor];
//    [tblPages setDataSource:self];
//    [tblPages setDelegate:self];
//    [tblPages setBackgroundColor:[UIColor clearColor]];
//    [tblPages setAllowsMultipleSelection:NO];
//    [self.view addSubview:tblPages];
//    [tblPages reloadData];
//
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//    UITableViewCell *tcell = [tblPages cellForRowAtIndexPath:indexPath];
//    [tcell setSelected:YES];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    lastAppearSize = self.view.bounds.size; // Track view size
    
#if (READER_DISABLE_IDLE == TRUE) // Option
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    
#endif // end of READER_DISABLE_IDLE Option
}

- (void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
}

- (void)dealloc{
    
#ifdef DEBUG
    NSLog(@"%s", __FUNCTION__);
#endif
    
    mainToolbar = nil; mainPagebar = nil;
    
    theScrollView = nil; contentViews = nil; lastHideTime = nil;
    
    lastAppearSize = CGSizeZero; currentPage = 0;
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}


/*
 - (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
 {
 //if (isVisible == NO) return; // iOS present modal bodge
 
 //if (fromInterfaceOrientation == self.interfaceOrientation) return;
 }
 */

- (void)didReceiveMemoryWarning
{
#ifdef DEBUG
    NSLog(@"%s", __FUNCTION__);
#endif
    
    [super didReceiveMemoryWarning];
}


#pragma mark UIScrollViewDelegate methods


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    if (![scrollView isKindOfClass:[UICollectionView class]]) {
        __block NSInteger page = 0;
        
        CGFloat contentOffsetX = scrollView.contentOffset.x;
        
        [contentViews enumerateKeysAndObjectsUsingBlock: // Enumerate content views
         ^(id key, id object, BOOL *stop)
         {
             ReaderContentView *contentView = object;
             
             if (contentView.frame.origin.x == contentOffsetX)
             {
                 page = contentView.tag; *stop = YES;
             }
         }
         ];
        
        if (page != 0) [self showDocumentPage:page]; // Show the page
        
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:page-1 inSection:0];
//        //UITableViewCell *tcell = [tableView_ cellForRowAtIndexPath:indexPath];
//        selectedIndexRow=indexPath.row;
//        UICollectionViewCell *tcell = [colPages cellForItemAtIndexPath:indexPath];
//        [tcell setSelected:NO];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (![scrollView isKindOfClass:[UICollectionView class]]) {
        
        __block NSInteger page = 0;
        
        CGFloat contentOffsetX = scrollView.contentOffset.x;
        
        [contentViews enumerateKeysAndObjectsUsingBlock: // Enumerate content views
         ^(id key, id object, BOOL *stop)
         {
             ReaderContentView *contentView = object;
             
             if (contentView.frame.origin.x == contentOffsetX)
             {
                 page = contentView.tag; *stop = YES;
             }
         }
         ];
        
        if (page != 0) [self showDocumentPage:page]; // Show the page
        
//        UICollectionViewCell *tcell = [colPages cellForItemAtIndexPath:indexPath];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:page-1 inSection:0];
        if (indexPath.row < 0)
        {
            return;
        }
        [colPages scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
        
        //UITableViewCell *tcell = [tableView_ cellForRowAtIndexPath:indexPath];
        selectedIndexRow=indexPath.row;
        [colPages reloadData];
//        [tcell setSelected:YES];
    }
    
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (![scrollView isKindOfClass:[UICollectionView class]]) {
        [self showDocumentPage:theScrollView.tag]; // Show page
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:theScrollView.tag-1 inSection:0];
//        selectedIndexRow=indexPath.row;
//        UICollectionViewCell *tcell = [colPages cellForItemAtIndexPath:indexPath];
//        [tcell setSelected:YES];
        
        theScrollView.tag = 0; // Clear page number tag
    }
    }
  

#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)recognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isKindOfClass:[UIScrollView class]]) return YES;
    
    return NO;
}

#pragma mark UIGestureRecognizer action methods

- (void)decrementPageNumber
{
    if (theScrollView.tag == 0) // Scroll view did end
    {
        NSInteger page = [document.pageNumber integerValue];
        NSInteger maxPage = [document.pageCount integerValue];
        NSInteger minPage = 1; // Minimum
        
        if ((maxPage > minPage) && (page != minPage))
        {
            CGPoint contentOffset = theScrollView.contentOffset;
            
            contentOffset.x -= theScrollView.bounds.size.width; // -= 1
            
            [theScrollView setContentOffset:contentOffset animated:YES];
            
            theScrollView.tag = (page - 1); // Decrement page number
        }
    }
}

- (void)incrementPageNumber
{
    if (theScrollView.tag == 0) // Scroll view did end
    {
        NSInteger page = [document.pageNumber integerValue];
        NSInteger maxPage = [document.pageCount integerValue];
        NSInteger minPage = 1; // Minimum
        
        if ((maxPage > minPage) && (page != maxPage))
        {
            CGPoint contentOffset = theScrollView.contentOffset;
            
            contentOffset.x += theScrollView.bounds.size.width; // += 1
            
            [theScrollView setContentOffset:contentOffset animated:YES];
            
            theScrollView.tag = (page + 1); // Increment page number
        }
    }
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateRecognized)
    {
        CGRect viewRect = recognizer.view.bounds; // View bounds
        
        CGPoint point = [recognizer locationInView:recognizer.view];
        
        CGRect areaRect = CGRectInset(viewRect, TAP_AREA_SIZE, 0.0f); // Area
        
        if (CGRectContainsPoint(areaRect, point)) // Single tap is inside the area
        {
            NSInteger page = [document.pageNumber integerValue]; // Current page #
            
            NSNumber *key = [NSNumber numberWithInteger:page]; // Page number key
            
            ReaderContentView *targetView = [contentViews objectForKey:key];
            
            id target = [targetView processSingleTap:recognizer]; // Target
            
            if (target != nil) // Handle the returned target object
            {
                if ([target isKindOfClass:[NSURL class]]) // Open a URL
                {
                    NSURL *url = (NSURL *)target; // Cast to a NSURL object
                    
                    if (url.scheme == nil) // Handle a missing URL scheme
                    {
                        NSString *www = url.absoluteString; // Get URL string
                        
                        if ([www hasPrefix:@"www"] == YES) // Check for 'www' prefix
                        {
                            NSString *http = [NSString stringWithFormat:@"http://%@", www];
                            
                            url = [NSURL URLWithString:http]; // Proper http-based URL
                        }
                    }
                    
                    if ([[UIApplication sharedApplication] openURL:url] == NO)
                    {
#ifdef DEBUG
                        NSLog(@"%s '%@'", __FUNCTION__, url); // Bad or unknown URL
#endif
                    }
                }
                else // Not a URL, so check for other possible object type
                {
                    if ([target isKindOfClass:[NSNumber class]]) // Goto page
                    {
                        NSInteger value = [target integerValue]; // Number
                        
                        [self showDocumentPage:value]; // Show the page
                    }
                }
            }
            /*else if (currentPage == 1 && (pointX >=75 && pointX <= 450) && (pointY >=200 && pointY <= 400)){
             NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"FINAL_V2-1-quicktime" ofType:@"mov"]];
             NSLog(@"url: %@", url);
             moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:url];
             
             moviePlayerController.controlStyle = MPMovieControlStyleFullscreen ;
             moviePlayerController.scalingMode = MPMovieScalingModeAspectFit;
             moviePlayerController.shouldAutoplay = NO;
             //moviePlayerController.fullscreen = YES;
             [moviePlayerController.view setFrame:CGRectMake(70, 170, 300, 200)];
             [theScrollView addSubview:moviePlayerController.view];
             [moviePlayerController setFullscreen:YES animated:YES];
             [[NSNotificationCenter defaultCenter] addObserver:self
             selector:@selector(moviePlayerDidExitFullscreen:)
             name:MPMoviePlayerDidExitFullscreenNotification
             object:nil];
             //NSNumber *reason = []
             [moviePlayerController prepareToPlay];
             [moviePlayerController play] ;
             
             moviePlayer1 = [[MPMoviePlayerViewController  alloc] initWithContentURL:url];
             //[self presentMoviePlayerViewControllerAnimated:moviePlayer1];
             [self presentModalViewController:moviePlayer1 animated:YES];
             moviePlayer1.moviePlayer.movieSourceType = MPMovieSourceTypeFile;
             [theScrollView addSubview:moviePlayer1.view];
             //moviePlayer.view.frame = CGRectMake(0, 0, 300, 400);
             [moviePlayer1.moviePlayer play];
             moviePlayer1 = nil;
             }*/
            else // Nothing active tapped in the target content view
            {
                if ([lastHideTime timeIntervalSinceNow] < -0.75) // Delay since hide
                {

//                    BOOL animateIn = self.navigationController.navigationBarHidden;
                    if ((mainToolbar.hidden == YES) || (mainPagebar.hidden == YES))
                    {
                        BOOL animateIn = mainToolbar.hidden;
                        //[[UIApplication sharedApplication] setStatusBarHidden:!animateIn withAnimation:animateIn?UIStatusBarAnimationNone:UIStatusBarAnimationSlide];
                        


                        [mainToolbar hideToolbar]; [mainPagebar hidePagebar]; // Show
                        
                        colPages.hidden=!colPages.hidden;
                        if (colPages.hidden) {
                            isHide=YES;
                        }
                        else
                        {
                            isHide=NO;
                        }
                        [self.navigationController setNavigationBarHidden:isHide animated:YES];

                        [self didRotate:nil];

                    }
                }
            }
            
            return;
        }
        
        CGRect nextPageRect = viewRect;
        nextPageRect.size.width = TAP_AREA_SIZE;
        nextPageRect.origin.x = (viewRect.size.width - TAP_AREA_SIZE);
        
        if (CGRectContainsPoint(nextPageRect, point)) // page++ area
        {
            if ([readerMode isEqualToString:@"Prepare"]) {
                
                [self showDocumentPage:currentPage+1];
            }else if ([readerMode isEqualToString:@"Practice"]) {
                
                [self showDocumentPage:currentPage+1];
            }

//            [self incrementPageNumber];
            return;
        }
        
        CGRect prevPageRect = viewRect;
        prevPageRect.size.width = TAP_AREA_SIZE;
        
        if (CGRectContainsPoint(prevPageRect, point)) // page-- area
        {
            if ([readerMode isEqualToString:@"Prepare"]) {
                
                [self showDocumentPage:currentPage-1];
            }else if ([readerMode isEqualToString:@"Practice"]) {
                
                [self showDocumentPage:currentPage-1];
            }

//            [self decrementPageNumber];
            return;
        }
    }
}

//- (void)moviePlayerDidExitFullscreen:(NSNotification *)theNotification {
//    NSLog(@"hi");
//    //[moviePlayerController.view removeFromSuperview];
//    //[closeButton removeFromSuperview];
//}

- (void)handleDoubleTap:(UITapGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateRecognized)
    {
        CGRect viewRect = recognizer.view.bounds; // View bounds
        
        CGPoint point = [recognizer locationInView:recognizer.view];
        
        CGRect zoomArea = CGRectInset(viewRect, TAP_AREA_SIZE, TAP_AREA_SIZE);
        
        if (CGRectContainsPoint(zoomArea, point)) // Double tap is in the zoom area
        {
            NSInteger page = [document.pageNumber integerValue]; // Current page #
            ReaderContentView *targetView;
            UIInterfaceOrientation orientation= [[UIApplication sharedApplication] statusBarOrientation];
            if(UIInterfaceOrientationIsLandscape(orientation))
            {
                NSString *key = [NSString stringWithFormat:@"%ld-L",(long)page];
                targetView = [contentViews objectForKey:key];
                
            }
            else{
                NSNumber *key = [NSNumber numberWithInteger:page];
                targetView = [contentViews objectForKey:key];// Page number key
            }
            
            switch (recognizer.numberOfTouchesRequired) // Touches count
            {
                case 1: // One finger double tap: zoom ++
                {
                    [targetView zoomIncrement]; break;
                }
                    
                case 2: // Two finger double tap: zoom --
                {
                    [targetView zoomDecrement]; break;
                }
            }
            
            return;
        }
        
        CGRect nextPageRect = viewRect;
        nextPageRect.size.width = TAP_AREA_SIZE;
        nextPageRect.origin.x = (viewRect.size.width - TAP_AREA_SIZE);
        
        if (CGRectContainsPoint(nextPageRect, point)) // page++ area
        {
            [self incrementPageNumber]; return;
        }
        
        CGRect prevPageRect = viewRect;
        prevPageRect.size.width = TAP_AREA_SIZE;
        
        if (CGRectContainsPoint(prevPageRect, point)) // page-- area
        {
            [self decrementPageNumber]; return;
        }
    }
}
BOOL moviePlaying = NO;

#pragma mark ReaderContentViewDelegate methods

- (void)contentView:(ReaderContentView *)contentView touchesBegan:(NSSet *)touches
{
    if ((mainToolbar.hidden == NO) || (mainPagebar.hidden == NO))
    {
        if (touches.count == 1) // Single touches only
        {
            UITouch *touch = [touches anyObject]; // Touch info
            
            CGPoint point = [touch locationInView:self.view]; // Touch location
            
            CGRect areaRect = CGRectInset(self.view.bounds, TAP_AREA_SIZE, TAP_AREA_SIZE);
            
            if (CGRectContainsPoint(areaRect, point) == false) return;
        }
        BOOL animateIn = mainToolbar.hidden;
        //[[UIApplication sharedApplication] setStatusBarHidden:!animateIn withAnimation:animateIn?UIStatusBarAnimationNone:UIStatusBarAnimationSlide];
        

        [mainToolbar hideToolbar];
        [mainPagebar hidePagebar]; // Hide
        
        
        colPages.hidden=!colPages.hidden;
        if (colPages.hidden) {
            isHide=YES;
        }
        else
        {
            isHide=NO;
        }
        
        [self.navigationController setNavigationBarHidden:isHide animated:YES];

        [self didRotate:nil];

        lastHideTime = [NSDate date];
    }
    
    UITouch *tap = [touches anyObject];
    CGPoint touchPoint = [tap locationInView:self.view];
    pointX = touchPoint.x;
    pointY = touchPoint.y;
    NSLog(@" Coordinates are: %f, %f ", pointX, pointY);
    UILongPressGestureRecognizer *longpress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPress:)];
    longpress.minimumPressDuration = 1.0;
    [theScrollView addGestureRecognizer:longpress];
    
    if ([readerMode isEqualToString:@"Prepare"]) {
        mainViewRect = self.view.bounds;
    }else if ([readerMode isEqualToString:@"Practice"]){
        mainViewRect = CGRectMake(20, 20, 985, 600);
    }
    if (currentPage != 6 || currentPage != 7) {
        [moviePlayerController.player pause];
        [moviePlayerController.view removeFromSuperview];
        moviePlaying = NO;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (segmentedControl.selectedSegmentIndex == 1){
        
        mouseSwiped = NO;
        UITouch *touch = [touches anyObject];
        
        if ([touch tapCount] == 2) {
            drawImage.image = nil;
            return;
        }
        
        lastPoint = [touch locationInView:self.drawView];
        lastPoint.y -= 2;
    }
    if (segmentedControl.selectedSegmentIndex == 2){
        
        mouseSwiped = NO;
        UITouch *touch = [touches anyObject];
        
        if ([touch tapCount] == 2) {
            drawImage.image = nil;
            return;
        }
        
        lastPoint = [touch locationInView:self.drawView];
        lastPoint.y -= 2;
    }
    
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (segmentedControl.selectedSegmentIndex == 2){
        mouseSwiped = NO;
    }else if (segmentedControl.selectedSegmentIndex == 1){
        mouseSwiped = YES;
        UITouch *touch = [touches anyObject];
        CGPoint currentPoint = [touch locationInView:self.drawView];
        currentPoint.y -= 2;
        UIGraphicsBeginImageContext(self.drawView.frame.size);
        [drawImage.image drawInRect:CGRectMake(0, 0, self.drawView.frame.size.width, self.drawView.frame.size.height)];
        //[drawImage.image drawInRect:CGRectMake(281, 63, 514, 626)];
        CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), lineValue);
        //CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.6, 1.0, 0.0, 0.4);
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), colorRval, colorGval, colorBval, 1);
        CGContextBeginPath(UIGraphicsGetCurrentContext());
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        lastPoint = currentPoint;
        
        mouseMoved++;
        
        if (mouseMoved == 10) {
            mouseMoved = 0;
        }
    }
}

int colorFlag = 0;

-(IBAction)colorChanged:(id)sender{
    colorFlag = 1;
    
    colors = [[NSMutableArray alloc]init];
    [colors addObject:@"1.png"];
    [colors addObject:@"2.png"];
    [colors addObject:@"3.png"];
    [colors addObject:@"4.png"];
    [colors addObject:@"5.png"];
    [colors addObject:@"6.png"];
    [colors addObject:@"7.png"];
    [colors addObject:@"8.png"];
    colorR = [[NSMutableArray alloc]init];
    colorG = [[NSMutableArray alloc]init];
    colorB = [[NSMutableArray alloc]init];
    [colorR addObject:[NSNumber numberWithFloat:0.5]];
    [colorR addObject:[NSNumber numberWithFloat:0]];
    [colorR addObject:[NSNumber numberWithFloat:0]];
    [colorR addObject:[NSNumber numberWithFloat:1]];
    [colorR addObject:[NSNumber numberWithFloat:0.5]];
    [colorR addObject:[NSNumber numberWithFloat:1]];
    [colorR addObject:[NSNumber numberWithFloat:1]];
    [colorR addObject:[NSNumber numberWithFloat:0.5]];
    
    [colorG addObject:[NSNumber numberWithFloat:1]];
    [colorG addObject:[NSNumber numberWithFloat:1]];
    [colorG addObject:[NSNumber numberWithFloat:0]];
    [colorG addObject:[NSNumber numberWithFloat:0.07]];
    [colorG addObject:[NSNumber numberWithFloat:0.5]];
    [colorG addObject:[NSNumber numberWithFloat:0.85]];
    [colorG addObject:[NSNumber numberWithFloat:0]];
    [colorG addObject:[NSNumber numberWithFloat:0]];
    
    [colorB addObject:[NSNumber numberWithFloat:0]];
    [colorB addObject:[NSNumber numberWithFloat:1]];
    [colorB addObject:[NSNumber numberWithFloat:0.54]];
    [colorB addObject:[NSNumber numberWithFloat:0.58]];
    [colorB addObject:[NSNumber numberWithFloat:0.5]];
    [colorB addObject:[NSNumber numberWithFloat:0]];
    [colorB addObject:[NSNumber numberWithFloat:0]];
    [colorB addObject:[NSNumber numberWithFloat:0.5]];
    
    if(popoverContent != nil)
    {
        //[self->popoverController dismissPopoverAnimated:YES];
        [[popoverContent presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
        popoverContent = nil;
        return;
    }
    
    //    UINavigationController *container = [[UINavigationController alloc] init];
    popoverContent = [[UIViewController alloc]
                                        init];
    
    popoverView = [[UIView alloc]
                   initWithFrame:CGRectMake(0, 0, 60, 350)];
    //popoverView.backgroundColor = [UIColor blackColor];
    
    tableView_ = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 60, 350) style:UITableViewStylePlain];
    //Remember to set the table view delegate and data provider
    
    tableView_.delegate=self;
    tableView_.dataSource=self;
    
    [popoverView addSubview:tableView_];
    //UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:popoverController];
    popoverContent.view = popoverView;
    
    //resize the popover view shown
    //in the current view to the view's size
    popoverContent.preferredContentSize = CGSizeMake(60, 350);
    
    
    
    //create a popover controller
//    self->popoverController = [[UIPopoverController alloc]
//                               initWithContentViewController:popoverContent];

    UIPopoverPresentationController *popoverController = [popoverContent popoverPresentationController];
    popoverController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    popoverController.barButtonItem = sender;
    popoverController.delegate = self;
    
    
    //present the popover view non-modal with a
    //refrence to the toolbar button which was pressed
//    [self->popoverController presentPopoverFromBarButtonItem:sender
//                                    permittedArrowDirections:UIPopoverArrowDirectionUp
//                                                    animated:YES];
}
-(void)playVideoWithURL:(NSURL *)url andForframe:(CGRect )frame{
    
    AVPlayer *player = [[AVPlayer alloc] initWithURL:url];
    
    moviePlayerController = [[AVPlayerViewController alloc] init];
    
    moviePlayerController.player = player;
    
    
    //moviePlayerController = [[AVPlayerViewController alloc] initWithContentURL:url];
    //NSLog(@"current time: %g", moviePlayerController.currentPlaybackTime);
    
    //moviePlayerController.controlStyle = MPMovieControlStyleDefault;
    //moviePlayerController.scalingMode = MPMovieScalingModeAspectFit;
    //moviePlayerController.shouldAutoplay = NO;
    //moviePlayerController.fullscreen = YES;
    
    [moviePlayerController.view setFrame:frame];
    [self.view addSubview:moviePlayerController.view];
    //[moviePlayerController setFullscreen:YES animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayerDidExitFullscreen:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:nil];
    //[moviePlayerController prepareToPlay];
    [moviePlayerController.player play] ;
    
}
-(void)stopVideoPlayer{
    [moviePlayerController.player pause];
    [moviePlayerController.view removeFromSuperview];
    moviePlaying = NO;
}
-(void)videoPlayerForPrepareReaderMode{
    
    NSString *device = [UIDevice currentDevice].model;

    if (currentPage == 11){
        
        CGRect videoFrame ;
        if (([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) &&  (pointX >=620 && pointX <= 940) && (pointY >=200 && pointY <= 457)) {
            videoFrame =CGRectMake(610, 190, 395, 285);
        }else{
            if ((pointX >=320 && pointX <= 450) && (pointY >=100 && pointY <= 200)) {
                videoFrame =CGRectMake(285, 75, 180, 130);
            }
        }
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"FINAL_V2-1-quicktime" ofType:@"mov"]];
        
        if (moviePlaying == NO) {
            moviePlaying = YES;
            [self playVideoWithURL:url andForframe:videoFrame];
        }
        
        
    }else if (currentPage == 13  ){
        
        CGRect videoFrame ;NSURL *url;
        if ([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) {
            if ((pointX >=100 && pointX <= 440) && (pointY >=200 && pointY <= 460)) {
                videoFrame =CGRectMake(21, 196, 433, 273);
                url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Intro slides video" ofType:@"mp4"]];
            }else if ((pointX >=620 && pointX <= 940) && (pointY >=200 && pointY <= 457)){
                videoFrame =CGRectMake(610, 190, 395, 285);
                url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"C_diff_Draft_05_10_2013" ofType:@"mov"]];
            }
        }else{
            //iphone
            if ((pointX >=20 && pointX <= 190) && (pointY >=80 && pointY <= 200)) {
                videoFrame =CGRectMake(12, 75,200, 130);
                url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Intro slides video" ofType:@"mp4"]];
            }else if ((pointX >=280 && pointX <= 470) && (pointY >=80 && pointY <= 257)){
                videoFrame =CGRectMake(295,80, 180, 130);
                url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"C_diff_Draft_05_10_2013" ofType:@"mov"]];
            }
        }
        if (moviePlaying == NO) {
            moviePlaying = YES;
            [self playVideoWithURL:url andForframe:videoFrame];
        }
    }
    else{
        [self stopVideoPlayer];
    }
}
-(void)videoPlayerForPresentReaderMode{
    NSString *device = [UIDevice currentDevice].model;
    
    CGRect videoFrame ;

    if (currentPage == 8 ) {
        
        if (([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) &&  (pointX >=370 && pointX <= 730) && (pointY >=170 && pointY <= 432) && audioOnOffDecider == YES) {
            videoFrame =CGRectMake(385, 190, 390, 283);
        }else{
        //iphone
            videoFrame =CGRectMake(385, 190, 390, 283);
        }
        if (moviePlaying == NO) {
            
            moviePlaying = YES;
            playVideo = NO;
            NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"FINAL_V2-1-quicktime" ofType:@"mov"]];
            [self playVideoWithURL:url andForframe:videoFrame];
        }
        
    }else if (currentPage == 9){
        
        if (([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) &&  (pointX >=290 && pointX <= 690) && (pointY >=176 && pointY <= 434)  && audioOnOffDecider == YES){
        
        }else{
        
        }

        if (moviePlaying == NO) {
            
            moviePlaying = YES;
            NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Intro slides video" ofType:@"mp4"]];
            
            [self playVideoWithURL:url andForframe:CGRectMake(303, 194, 430, 285)];
            
        }
    } else if (currentPage == 10){
        if (([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) &&  (pointX >=290 && pointX <= 690) && (pointY >=176 && pointY <= 434)  && audioOnOffDecider == YES){
        }else{
        
        }

        if (moviePlaying == NO) {
            
            moviePlaying = YES;
            NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"C_diff_Draft_05_10_2013" ofType:@"mov"]];
            [self playVideoWithURL:url andForframe:CGRectMake(303, 194, 430, 285)];
        }
    }
    else{
        [self stopVideoPlayer];
    }
}
-(void)videoPlayerForPracticeReaderMode{
    
    NSString *device = [UIDevice currentDevice].model;
    CGRect videoFrame ;
    if (currentPage == 8 ){
        
        if (([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) && (pointX >=370 && pointX <= 730) && (pointY >=170 && pointY <= 432)){
            videoFrame = CGRectMake(370, 176, 370, 267);
        }else{
            if((pointX >=275 && pointX <= 380) && (pointY >=95 && pointY <= 160)){
                videoFrame = CGRectMake(265, 80, (IS_IPHONE)?80:120, 90);
            }
        }
        if (moviePlaying == NO) {
            
            moviePlaying = YES;
            playVideo = NO;
            NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"FINAL_V2-1-quicktime" ofType:@"mov"]];
            [self playVideoWithURL:url andForframe:videoFrame];
        }
        
        
    }else if (currentPage == 9 ){
        if (([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) && (pointX >=290 && pointX <= 690) && (pointY >=176 && pointY <= 434)){
            videoFrame = CGRectMake(290, 176, 410, 265);
        }else{
            if((pointX >=240 && pointX <= 350) && (pointY >=96 && pointY <= 174)){
            videoFrame = CGRectMake(230, 85, 140, 95);
            }
        }
        if (moviePlaying == NO) {
            
            moviePlaying = YES;
            NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Intro slides video" ofType:@"mp4"]];
            [self playVideoWithURL:url andForframe:videoFrame];
            
        }
    }else if (currentPage == 10 ){
        
        if (([device isEqualToString:@"iPad Simulator"] || [device isEqualToString:@"iPad"]) && (pointX >=290 && pointX <= 690) && (pointY >=176 && pointY <= 434)){
            videoFrame = CGRectMake(280, 166, 470, 295);
        }else{
            if((pointX >=260 && pointX <= 400) && (pointY >=96 && pointY <= 174)){
            videoFrame = CGRectMake(250,80, 140, 95);
            }
        }

        if (moviePlaying == NO) {
            
            moviePlaying = YES;
            NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"C_diff_Draft_05_10_2013" ofType:@"mov"]];
            [self playVideoWithURL:url andForframe:videoFrame];
        }
    }
    else{
        [self stopVideoPlayer];
        
    }

}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    
    if ([touch tapCount] == 2) {
        drawImage.image = nil;
        return;
    }
    if(!mouseSwiped && segmentedControl.selectedSegmentIndex == 1) {
        UIGraphicsBeginImageContext(self.drawView.frame.size);
        [drawImage.image drawInRect:CGRectMake(0, 0, self.drawView.frame.size.width, self.drawView.frame.size.height)];
        CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), lineValue +0.0);
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), colorRval, colorGval, colorBval, 1.0);
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        CGContextFlush(UIGraphicsGetCurrentContext());
        drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }else if (!mouseSwiped && segmentedControl.selectedSegmentIndex == 2) {
        drawImage.image = nil;
        UIGraphicsBeginImageContext(self.drawView.frame.size);
        [drawImage.image drawInRect:CGRectMake(0, 0, self.drawView.frame.size.width, self.drawView.frame.size.height)];
        CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), lineValue +0.0);
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), colorRval, colorGval, colorBval, 1.0);
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        CGContextFlush(UIGraphicsGetCurrentContext());
        drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
}

-(void) handleLongPress: (UILongPressGestureRecognizer *) recognizer{
    NSLog(@"Long Press for playing Video");
    
    if ([readerMode isEqualToString:@"Prepare"] && recFlag == 0 && videoFlag == 0) {
        [self videoPlayerForPrepareReaderMode];
    }
    if ([readerMode isEqualToString:@"Practice"] && recFlag == 0 && videoFlag == 0) {
        [self videoPlayerForPracticeReaderMode];
    }
    if ([readerMode isEqualToString:@"Present"] && recFlag == 0 && videoFlag == 0) {
        [self videoPlayerForPresentReaderMode];
    }
}

-(void)CloseVideo: (id)sender{
    [self stopVideoPlayer];
}


#pragma mark ReaderMainToolbarDelegate methods

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar doneButton:(UIButton *)button
{
#if (READER_STANDALONE == FALSE) // Option
    
    [document saveReaderDocument]; // Save any ReaderDocument object changes
    
    [[ReaderThumbQueue sharedInstance] cancelOperationsWithGUID:document.guid];
    
    [[ReaderThumbCache sharedInstance] removeAllObjects]; // Empty the thumb cache
    
    if (printInteraction != nil) [printInteraction dismissAnimated:NO]; // Dismiss
    
    if ([delegate respondsToSelector:@selector(dismissReaderViewController:)] == YES)
    {
        [delegate dismissReaderViewController:self]; // Dismiss the ReaderViewController
    }
    else // We have a "Delegate must respond to -dismissReaderViewController: error"
    {
        NSAssert(NO, @"Delegate must respond to -dismissReaderViewController:");
    }
   
#endif // end of READER_STANDALONE Option
}

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar thumbsButton:(UIButton *)button
{
    if (printInteraction != nil) [printInteraction dismissAnimated:NO]; // Dismiss
    
    ThumbsViewController *thumbsViewController = [[ThumbsViewController alloc] initWithReaderDocument:document];
    
    thumbsViewController.delegate = self; thumbsViewController.title = self.title;
    
    thumbsViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    thumbsViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:thumbsViewController animated:NO completion:nil];
    //    [self presentModalViewController:thumbsViewController animated:NO];
}

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar printButton:(UIButton *)button
{
#if (READER_ENABLE_PRINT == TRUE) // Option
    
    Class printInteractionController = NSClassFromString(@"UIPrintInteractionController");
    
    if ((printInteractionController != nil) && [printInteractionController isPrintingAvailable])
    {
        NSURL *fileURL = document.fileURL; // Document file URL
        
        printInteraction = [printInteractionController sharedPrintController];
        
        if ([printInteractionController canPrintURL:fileURL] == YES) // Check first
        {
            UIPrintInfo *printInfo = [NSClassFromString(@"UIPrintInfo") printInfo];
            
            printInfo.duplex = UIPrintInfoDuplexLongEdge;
            printInfo.outputType = UIPrintInfoOutputGeneral;
            printInfo.jobName = document.fileName;
            
            printInteraction.printInfo = printInfo;
            printInteraction.printingItem = fileURL;
            printInteraction.showsPageRange = YES;
            
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
                [printInteraction presentFromRect:button.bounds inView:button animated:YES completionHandler:
                 ^(UIPrintInteractionController *pic, BOOL completed, NSError *error)
                 {
#ifdef DEBUG
                     if ((completed == NO) && (error != nil)) NSLog(@"%s %@", __FUNCTION__, error);
#endif
                 }
                 ];
            }else // Presume UIUserInterfaceIdiomPhone
            {
                [printInteraction presentAnimated:YES completionHandler:
                 ^(UIPrintInteractionController *pic, BOOL completed, NSError *error)
                 {
#ifdef DEBUG
                     if ((completed == NO) && (error != nil)) NSLog(@"%s %@", __FUNCTION__, error);
#endif
                 }
                 ];
            }
        }
    }
#endif // end of READER_ENABLE_PRINT Option
}

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar emailButton:(UIButton *)button
{
#if (READER_ENABLE_MAIL == TRUE) // Option
    
    if ([MFMailComposeViewController canSendMail] == NO) return;
    
    if (printInteraction != nil) [printInteraction dismissAnimated:YES];
    
    unsigned long long fileSize = [document.fileSize unsignedLongLongValue];
    
    if (fileSize < (unsigned long long)15728640) // Check attachment size limit (15MB)
    {
        NSURL *fileURL = document.fileURL; NSString *fileName = document.fileName; // Document
        
        NSData *attachment = [NSData dataWithContentsOfURL:fileURL options:(NSDataReadingMapped|NSDataReadingUncached) error:nil];
        
        if (attachment != nil) // Ensure that we have valid document file attachment data
        {
            MFMailComposeViewController *mailComposer = [MFMailComposeViewController new];
            
            [mailComposer addAttachmentData:attachment mimeType:@"application/pdf" fileName:fileName];
            
            [mailComposer setSubject:fileName]; // Use the document file name for the subject
            
            mailComposer.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            mailComposer.modalPresentationStyle = UIModalPresentationFormSheet;
            
            mailComposer.mailComposeDelegate = self; // Set the delegate
            [self presentViewController:mailComposer animated:YES completion:nil];
            
        }
    }
    
#endif // end of READER_ENABLE_MAIL Option
}

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar markButton:(UIButton *)button
{
    if (printInteraction != nil) [printInteraction dismissAnimated:YES];
    
    NSInteger page = [document.pageNumber integerValue];
    
    if ([document.bookmarks containsIndex:page]) // Remove bookmark
    {
        [mainToolbar setBookmarkState:NO]; [document.bookmarks removeIndex:page];
    }
    else // Add the bookmarked page index to the bookmarks set
    {
        [mainToolbar setBookmarkState:YES]; [document.bookmarks addIndex:page];
    }
}

#pragma mark MFMailComposeViewControllerDelegate methods

/*- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
 {
 #ifdef DEBUG
 if ((result == MFMailComposeResultFailed) && (error != NULL)) NSLog(@"%@", error);
 #endif
 
	[self dismissModalViewControllerAnimated:YES]; // Dismiss
 }*/

#pragma mark ThumbsViewControllerDelegate methods

- (void)dismissThumbsViewController:(ThumbsViewController *)viewController
{
    [self updateToolbarBookmarkIcon]; // Update bookmark icon
    [self dismissViewControllerAnimated:NO completion:nil];
    //    [self dismissModalViewControllerAnimated:NO]; // Dismiss
}

- (void)thumbsViewController:(ThumbsViewController *)viewController gotoPage:(NSInteger)page
{
    [self showDocumentPage:page]; // Show the page
}

#pragma mark ReaderMainPagebarDelegate methods

- (void)pagebar:(ReaderMainPagebar *)pagebar gotoPage:(NSInteger)page
{
    [self showDocumentPage:page]; // Show the page
}
#pragma mark UIApplication notification methods

- (void)applicationWill:(NSNotification *)notification
{
    [document saveReaderDocument]; // Save any ReaderDocument object changes
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        if (printInteraction != nil) [printInteraction dismissAnimated:NO];
    }
}
-(void)addUIElementsToMainView{
    
    //Add bottom buttons
    NSString *deviceType = [UIDevice currentDevice].model;
    UIDeviceOrientation orientation=[[UIDevice currentDevice] orientation];
    CGFloat constantVal=0;
    CGFloat sideMargin =0;
    if (UIDeviceOrientationIsPortrait(orientation)) {
        constantVal = isHide?0:PAGEBAR_HEIGHT;
    }
    else
    {
        sideMargin=colPages.frame.origin.x+colPages.frame.size.width;
    }

    if([deviceType isEqualToString:@"iPhone Simulator"]|| [deviceType isEqualToString:@"iPhone"] || [deviceType isEqualToString:@"iPod touch"]){
        bottonsView_iPad = nil;
        CGFloat y_Origin,width ;
        if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(@"7.1")) {
            y_Origin =[UIScreen mainScreen].bounds.size.width-constantVal;
            width =[UIScreen mainScreen].bounds.size.height;
        }else{
            y_Origin =[UIScreen mainScreen].bounds.size.height-constantVal;
            width =[UIScreen mainScreen].bounds.size.width;
        }
//        y_Origin =[UIScreen mainScreen].bounds.size.width-100;
//        width =[UIScreen mainScreen].bounds.size.height;

        CGFloat sizeRect=[[UIScreen mainScreen] nativeBounds].size.height;
        
        if (sizeRect==2436 || sizeRect==2688 || sizeRect==1792) {

            [bottonsView_iPhone setFrame:CGRectMake(sideMargin, y_Origin-bottonsView_iPhone.frame.size.height-34,width-(2*sideMargin), bottonsView_iPhone.frame.size.height)];
        }
        else
        {
            [bottonsView_iPhone setFrame:CGRectMake(sideMargin, y_Origin-bottonsView_iPhone.frame.size.height,width-(2*sideMargin), bottonsView_iPhone.frame.size.height)];

        }
        [self.view addSubview:bottonsView_iPhone];
    }else{
        bottonsView_iPhone = nil;
        [bottonsView_iPad setFrame:CGRectMake(sideMargin,self.view.bounds.size.height-constantVal-bottonsView_iPad.frame.size.height, self.view.bounds.size.width-(2*sideMargin), bottonsView_iPad.frame.size.height)];
        [self.view addSubview:bottonsView_iPad];
    }
    
}

#pragma mark - DEVICE ORIENTATION 

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{

    if (isVisible == NO) return; // iOS present modal bodge

    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        if (printInteraction != nil) [printInteraction dismissAnimated:NO];
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration{

    if (isVisible == NO) return; // iOS present modal bodge

    [self updateScrollViewContentViews]; // Update content views
    [self showDocumentPage:currentPage];
    lastAppearSize = CGSizeZero; // Reset view size tracking
}

//- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
//{
//    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
//
//    // Something
//}
//
//- (BOOL)shouldAutorotate{
//    return YES;
//}

//- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
//    return UIInterfaceOrientationMaskLandscape;
//
//}
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
//
//    return YES;
//
//}
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
//
//    return UIInterfaceOrientationLandscapeLeft;
//
//}

- (BOOL)shouldAutorotate{
    return YES;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    UIImage *image = [mainPagebar getImageFromPage:1];
    if (image.size.width>image.size.height) {
        OrientationName=@"Landscape";
        return UIInterfaceOrientationMaskLandscape;
    }
    if (image.size.width<image.size.height) {
        OrientationName=@"Portrait";
        return UIInterfaceOrientationMaskPortrait;
    }

    return UIInterfaceOrientationMaskAll;
}

-(BOOL) prefersStatusBarHidden {
    return !mainToolbar.hidden;
}

-(UIStatusBarAnimation) preferredStatusBarUpdateAnimation {
    
    if(mainToolbar.hidden) {
        return UIStatusBarAnimationNone;
    } else {
        return UIStatusBarAnimationSlide;
    }
}

/* audioRecorderDidFinishRecording:successfully: is called when a recording has been finished or stopped. This method is NOT called if the recorder is stopped due to an interruption. */
- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag{
//    NSLog(@"Audio Recorder Didfinished With Success: %@",flag?@"YES":@"NO");
    NSString *message = @"";
    NSString *title = @"";
    if (flag) {
        title = @"Success";
        message = @"Audio successfully recorded ! Visit File Manager to Play/Email/Delete the audio file.";

    }else{
        title = @"Error";
        message = @"Audio recording failed, please try again after sometime";
    }
    [self showAlertWithMessage:message andTitle:title];
}

/* if an error occurs while encoding it will be reported to the delegate. */
- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError * __nullable)error{
    if (error) {
        NSLog(@"Recorder Failed with Error:%@", [error description]);
    }
}
-(void) showAlertWithMessage:(NSString *) msg andTitle:(NSString *)title{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:msg
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action) {
                                   //Handle your yes please button action here
                               }];
    
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}
@end
