//
//  MainViewController.h
//  PDForClient
//
//  Created by Nagavardhan IV on 24/05/13.
//  Copyright (c) 2013 Nagavardhan IV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailerReaderViewController.h"

@interface MainViewController : UIViewController <ReaderViewControllerDelegate>{
    
    NSString *mode;
    
}

@property (nonatomic, strong) NSString *mode;

@end
