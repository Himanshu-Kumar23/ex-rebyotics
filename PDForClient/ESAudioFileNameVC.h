//
//  ESAudioFileNameVC.h
//  X Detailer
//
//  Created by Jayaprada Behera on 26/02/15.
//  Copyright (c) 2015 EIQ Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailerReaderViewController.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ESAudioFileNameVC : UIViewController

@end
